import sys

print "ScrumPy installing on debian 8"


#
##
### installs correctly folowing the modifications at rev 1005
##
#

PyVer   =  sys.version[:3]     # python version
PyPfx   =  sys.exec_prefix     # path to binary
PyCom = "python"+PyVer   # binary

GccVer = ""                          # gcc version, leave empty for the distribution default

PyDevPkg = "python" + PyVer +"-dev"                # python development package .
PyInc = PyPfx+"/include/python"+PyVer+"/"    # python  include path


Packages=[
    "gcc"+GccVer,
    "g++"+GccVer,
    "make",
    PyDevPkg,
    "libgmp3-dev",   # gnu multiple precision library - development package
    "swig",          # this can vary  between distros, maybe need (e.g.) swig-python as well
    "gnuplot-x11",
	"libsundials-serial-dev",
	"libcminpack-dev",
    
    "idle", 

    "python-sympy",
    
    "python-ply",    # python lex/yacc
    "python-numpy",
    "python-scipy", 
    "python-gmpy",
    "python-tk",     
    "python-qt4",    # needed by ete2 tree explorer
    "libsbml5-python"
]

InstallCommand="apt-get --yes install "

LinkFlags = "-shared -Wl,--copy-dt-needed-entries,--no-as-needed"
# for gcc <= 4.4 LinkFlags = "-shared"
