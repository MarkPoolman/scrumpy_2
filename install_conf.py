#
##
### install_conf - Identify platform speicific featured for install.py
##
#


import os, sys,  platform

def GetDistID():    
    Dist,  Ver,  Name = platform.dist()
    Ver = Ver.split(".")[0]
    return Dist+Ver
    

DistName = GetDistID()

print "Distribution ", DistName

try:
    DistConfModule = {
        "Ubuntu12" : "Ubuntu14", 
        "Ubuntu14" : "Ubuntu14", 
        "Ubuntu15" : "Ubuntu14",
        "Ubuntu18" : "Ubuntu18",
        "Ubuntu20" : "Ubuntu18",
        "debian6"   : "debian6", 
        "debian7"   : "debian7",
        "debianjessie/sid": "debian8",
        "debian8"  : "debian8", 
        "debian9": "debian8",  # conf is the same for 8 and 9
		"Ubuntu16": "debian8",
        "centos7" : "centos7"
    }[DistName]
except:
    "Cant't find a configuration for %s, giving up!" %  DistName
    sys.exit()
    

print "importing", DistConfModule

DistConf = None # dummy to prevent Eric reporting Undefined name warnings for references to DistConf - see following line
exec "import %s as DistConf" % DistConfModule
        

# install managed packages
def InstallPackages():

    fails=[]
    for p in DistConf.Packages:
       if os.system(DistConf.InstallCommand+p) !=0:
           fails.append(p)

    return fails
        
        
# select specific ScrumPy exension modules - leave as true except for development/debugging purposes
Kinetics=True
ElModes=True
LP = True


# where to put the executable and ScrumPy package files
# at present you will still need write permission in  /usr/lib/pythonPyVer/dist-packages
ExecPath = "/usr/local/bin"
InstallPath = "/usr/lib/python" + DistConf.PyVer + "/dist-packages/ScrumPy"

# how to install a bundled python library with distutils
PyInst = DistConf.PyCom+" setup.py install "


# how to configure/make/install the third party clibs with autoconf tools
pfx  = "--prefix="+InstallPath+"/cextns"
epfx = "--exec-prefix="+InstallPath+"/cextns"
Configure = " ".join(("./configure", pfx, epfx))
ConfMakeInst = "\n".join((Configure,"make","make install"))


#
## Sort out params for c/c++ compilation and linking of the extension llibraries
#

# Paths for the extension modules
ExtnPath = InstallPath+"/cextns"
ExtnLib  = ExtnPath+"/lib"
ExtnInc  = ExtnPath+"/include"

OtherInc = ["/usr/include/numpy"] 

IncludePaths = [DistConf.PyInc,  ExtnInc] + OtherInc

LibPaths = [ExtnLib] 
Libs = ["gmp", "stdc++", "m", "dl", "sundials_cvode", "sundials_nvecserial", "minpack"]


# make any platform specific changes to paths and libs
if hasattr(DistConf,  "IncludePaths"):
    IncludePaths += DistConf.IncludePaths

if hasattr(DistConf,  "LibPaths"):
    LibPaths += DistConf.LibPaths
    
if hasattr(DistConf,  "SubstLibs"):
    SubstLibs = DistConf.SubstLibs
    for oldlib in SubstLibs:
        Libs[Libs.index(oldlib)] = SubstLibs[oldlib]


# commands to compile and link
CC  = "gcc"+DistConf.GccVer   # c compiler
CPP = "g++"+DistConf.GccVer   # c++ compiler
LNK = "gcc"+DistConf.GccVer   # linker

#
## flags passed to compiler/linker
#

CInclFlags = ""
for i in IncludePaths:
    CInclFlags += " -I"+i+" "

LinkFlags = DistConf.LinkFlags
for L in LibPaths:
    LinkFlags += " -L"+L + " "
for L in Libs:
    LinkFlags += "-l"+L +" "

CCompileFlags="-c  -O2 -fPIC -DHAVE_CONFIG_H" + CInclFlags
CPPCompileFlags="-c -O2 -fPIC -DHAVE_CONFIG_H" + CInclFlags


#compile and link commands as passed to "make"
CCompile   = "'" + CC  + " "+ CCompileFlags +"'"
CPPCompile = "'" +CPP + " " +CPPCompileFlags+"'"
Link = "'" +LNK + " " + LinkFlags+"'"

# leave these
ThirdPartyPy = ["gmpy"]           # packages w/ seperate setup.py scripts
ThirdPartyC  = ["glpk"] #["gmp","glpk"]           # C libs with configure/make/install

