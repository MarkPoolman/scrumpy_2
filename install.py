#!/usr/bin/python



import os, sys,stat, shutil, compileall

import install_conf
from install_conf import InstallPath, InstallPackages,  CCompile,  CPPCompile,  Link


def ThirdParty():

    print "tp!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

    for tpc in install_conf.ThirdPartyC:
        srcdir = InstallPath + "/ThirdParty/src/" + tpc
        print "\n\nThirdParty C", srcdir,  install_conf.ConfMakeInst
        os.chdir(srcdir)
        os.system(install_conf.ConfMakeInst)
        print "\n\nThirdParty C done \n\n\n"

    for tpp in install_conf.ThirdPartyPy:
        srcdir = InstallPath + "/ThirdParty/src/" + tpp

        os.chdir(srcdir)
        os.system(install_conf.PyInst)
    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!tp"




MakeDirs = [
    "Structural/clib",
    "Kinetic/clib", 
    "LP"
]

def  Make():

    MakeArgs = " CCompile="+CCompile
    MakeArgs += " CPlusCompile="+CPPCompile
    MakeArgs += " Link="+Link

    fails = []

    for d in MakeDirs:
        path = InstallPath + "/" + d

        command = "cd " + path + " ; make "+MakeArgs

        print "\n\n",  command,  "\n\n"

        if os.system(command) !=0:
            fails.append(command)

    return fails


def NeedCopy(src,dst):
    """ pre: src exixts"""
    try:
        return stat.ST_MTIME(os.stat(src)) > stat.ST_MTIME(os.stat(dst))
    except:
        return 1


def Mirror(src, dst):

    if "/.svn" in src:
       return

    try:
        os.mkdir(dst)
    except:
        pass

    names = os.listdir(src)
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        if os.path.isdir(srcname):
            Mirror(srcname, dstname)
        elif NeedCopy(srcname,dstname):
            try:
                shutil.copy2(srcname, dstname)
                shutil.copystat(srcname, dstname)
            except (IOError, os.error), why:
                print "Can't copy %s to %s: %s" % (`srcname`, `dstname`, str(why))


def InstallPrereqs():

    fails = InstallPackages()
    nf = len(fails)
    if nf != 0:
        print "The following packages failed to install\n" + "\n".join(fails)
    return nf==0



def MakeExec():

    SetLD = "LD_LIBRARY_PATH=" + install_conf.ExtnLib +":$LD_LIBRARY_PATH "
    pycmd = SetLD+sys.executable + " "
    Perms = stat.S_IRWXU|stat.S_IXGRP|stat.S_IXOTH|stat.S_IRGRP|stat.S_IROTH

    StartScrumPy_t = InstallPath + "/tkGUI/StartScrumPy.py"
    ScrumPy_t = pycmd + StartScrumPy_t
    Exec_t = install_conf.ExecPath + "/ScrumPy"

    open(Exec_t, "w").write(ScrumPy_t)
    os.chmod(Exec_t, Perms)
    
    
    StartScrumPy_i = InstallPath + "/IdleGUI/StartScrumPy.py"
    ScrumPy_i = pycmd + StartScrumPy_i
    Exec_i = install_conf.ExecPath + "/ScrumPy2"

    open(Exec_i, "w").write(ScrumPy_i)
    os.chmod(Exec_i, Perms)
    
    
    
    





def Main():

    if not os.path.exists(InstallPath):
        os.system("mkdir -p " + InstallPath)

    if not "NOPKG" in sys.argv:

        if not InstallPrereqs():
            print "\nFailed to install prerequisite packages\nProceed recklessly anyway ?\n"
            if raw_input("Proceed ? ").upper() != "Y":
                print "\nGiving up\n"
                return

    if "LIBONLY" in sys.argv:

        for md in MakeDirs:
            Mirror("./ScrumPy/"+md, InstallPath + "/" + md)
    else:

        Mirror("./ScrumPy",InstallPath)
        ThirdParty()
        compileall.compile_dir(InstallPath)
        MakeExec()


    #SaveConfig(InstallPath)
    makefails = Make()
    print "\n\n makefails -",  makefails, "\n\n"
    if len(makefails) !=0:
        makefails.insert(0, "Failed to build extensions:")
        print "\n! ".join(makefails)
        return




if __name__=="__main__":

    Main()

