import sys


PyVer   =  sys.version[:3]     # python version
PyPfx   =  sys.exec_prefix     # path to binary
PyCom = "python"+PyVer   # binary

GccVer = ""                          # gcc version, leave empty for the distribution default

PyDevPkg = "python" + PyVer +"-dev"                # python development package .
PyInc = PyPfx+"/include/python"+PyVer+"/"    # python  include path


Packages=[
    "gcc"+GccVer,
    "g++"+GccVer,
    "make",
    "cmake",         # for scope currently in dev.
    PyDevPkg,
    "libgmp3-dev",   # gnu multiple precision library - development package
    "swig",          # this can vary  between distros, maybe need (e.g.) swig-python as well
    "gnuplot-qt",
    "python-sympy",
    "libsundials-cvode2", 
    "libsundials-dev",
    "libcminpack-dev",
    
    "idle-python2.7", 
    "python-numpy",
    "python-scipy", 
    "python-gmpy",
    "blt",           # GUI extensions
    "python-pmw",    # should chain off of blt, but maybe not ?
    "python-tk",     # ditto
    "python-qt4",    # needed by ete2 tree explorer
    "libsbml5-python" # read/write sbml
 
]

InstallCommand="apt-get --yes install "

LinkFlags = "-shared -Wl,--copy-dt-needed-entries,--no-as-needed"
# for gcc <= 4.4 LinkFlags = "-shared"
