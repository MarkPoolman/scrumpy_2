
Banner = """ScrumPy - Metabolic Modelling in Python

Doc is out of date in places at time of writing,
(but a jolly good read for all that)

Python 2.7 version. 

"""

Version  = "1.0-BETA"
LastMod  = "25/4/2020"
Revision = "1442"
Comment="scipy dev branch"
Title = "ScrumPy - Metabolic Modelling in Python"
 # LastMod and Revision updated automatically by BuildRel



Banner = "\n".join((Banner, "Version : "+Version, "Last update : "+ LastMod, "Revision : "+Revision,  Comment))

