

#done:
#-mip subc of ScrumPyLP.lp




import sys, types, exceptions

SysStderr = sys.stderr
NullF =  open("/dev/null", "w")

import ScrumPyLP,  glpk

lp=glpk.lp

lin=glpk.lin
bina=glpk.bina

from ScrumPy.Util import Set, DynMatrix
from ScrumPy.Structural import StoMat
from ScrumPy.Data import DataSets

Sym2Num = glpk.Sym2Num
Str2Num = glpk.Str2Num
Num2Str = glpk.Num2Str

#import  Mtx

Num2Str.update({
    Sym2Num['LPX_LP'] : 'LP',
    Sym2Num['LPX_MIP'] : 'MIP',
    Sym2Num['LPX_CV'] : lin,
    Sym2Num['LPX_IV'] : bina,
    Sym2Num['LPX_I_UNDEF'] : 'MIP Undefined',
    Sym2Num['LPX_I_FEAS'] : 'MIP Feasible',
    Sym2Num['LPX_I_OPT'] : 'MIP Optimal',
    Sym2Num['LPX_I_NOFEAS'] : 'MIP No Feasible',
    Sym2Num['LPX_UNBND'] : 'Unbound',    
    })
for k in Num2Str.keys():
    Str2Num[Num2Str[k]] = k

ZEROROUND = 8
ZEROVAL = pow(10, -ZEROROUND)
EPSILON = pow(ZEROVAL, 0.5)


MAX_FLUX = pow(10,3)
MIN_FLUX = EPSILON
TOL = pow(10, -8)


INT_TAG = 'int_'
REV_TAG = '_back'



class mip(ScrumPyLP.lp):

    def __init__(self, model, External=False):
        
        ScrumPyLP.lp.__init__(self,model, External,  klass='MIP')
        self.IntTag = INT_TAG #MIP specific
        
#        self.ObjVal = 0.0
#        self.UpdateStatus()
#        self.IntTag = INT_TAG #MIP specific
#        self.DelCols(self.sm.cnames[:])
#        self.DelRows(self.sm.rnames[:])
#        self.AddCols(self.sm.cnames[:])
#        self.AddRows(self.sm.rnames[:])
#        for r in self.sm.rnames:
#            row = map(float, self.sm[r])
#            row += [0.0] * len(self.sm.cnames) #MIP specific
#            self.SetRowVals(r, row)
#            self.SetRowBounds(r, 0, 0)
# 
   ##utility   

    def _arg2num(self, arg, valids = []):
        num = arg
        if arg in Str2Num:
            num = Str2Num[arg]
        elif arg in Sym2Num:
            num = Sym2Num[arg]
        if not num in Num2Str or (valids and not Num2Str[num] in valids):
            raise exceptions.ValueError, str(arg)
        return num
        

    def _num2str(self, num):
        return Num2Str[num]

##access


    
    def GetObjValue(self):
        return {'LP' : self._glp_lpx_get_obj_val, 'MIP' : self._glp_lpx_mip_obj_val}[self.GetClass()]()


    def GetStatus(self, Stype = "Generic"):
##        return self.Status
        return {'LP' : self._glp_lpx_get_status, 'MIP' : self._glp_lpx_mip_status}[self.GetClass()]()


    def IsStatusOptimal(self):
        return self.GetStatusMsg() in ['Optimal', 'MIP Optimal']


    def GetObjDirec(self):
        return self._num2str(self._glp_lpx_get_obj_dir())
    

    def GetColVal(self, cname):
        cid = self.GetColIdxs([cname])[0]
        fun = {'LP' : lambda c: self._glp_lpx_get_col_prim(c), 'MIP' : lambda c: self._glp_lpx_mip_col_val(c)}[self.GetClass()]
        return fun(cid)


    def GetObjCoef(self, cname):
        return self._glp_lpx_get_obj_coef(self.GetColIdxs([cname])[0])
    


##editing

#    def AddCols(self, cnames):
#
#        lp.AddCols(self, cnames)
#        if self.GetClass() == 'MIP':
#            intnames = [self.IntTag + cname for cname in cnames]
#            lp.AddCols(self, intnames)
#            for intname in intnames:
#                self.SetColKind(intname, 'Integer')
#                self.SetColBounds(intname, 0, 1)



    def SetColKind(self, cname, kind):
        if self.GetClass() == 'MIP':
            cid = self.GetColIdxs([cname])[0]
            kind = self._arg2num(kind, [bina, lin])
            self._glp_lpx_set_col_kind(cid, kind)


    def SetColVals(self, col, vals):

        col = self.GetColIdxs([col])[0]
        lenv = len(vals)
        if len(vals) != self.GetNumRows():
            raise exceptions.IndexError, "incorrect col length" 
        zs = []
        nzs = []
        for i in range(lenv):
            if vals[i] == 0:
                zs.append(i)
            else:
                nzs.append(i)
        zs.reverse()
        if len(zs) !=0:
            vals = vals[:] # copy, because we are going to change the contents
        for i in zs:
            del vals[i]
        nzs = glpk.IntArray(glpk.AddOne(nzs))
        vals = glpk.DoubleArray(vals)
        self._glp_lpx_set_mat_col(col, len(vals), nzs._array, vals._array)
    
##solving

    def Solve(self, PrintStatus = True):
        lp.Solve(self)
        if self.GetClass() == 'MIP':
            self._glp_lpx_integer()
        if PrintStatus:
            print self.GetStatusMsg()
        self.ObjVal = self.GetObjVal()            
        self.UpdateStatus()

    def UpdateStatus(self):
        self.Status = {'LP' : self._glp_lpx_get_status, 'MIP' : self._glp_lpx_mip_status}[self.GetClass()]()        

##safe overloads

    def DelCols(self, cols):
        if len(cols) > 0:
            if len(cols) == len(set(cols)):
               lp.DelCols(self, cols)
            else:
                raise 'duplicates in the list!'


    def DelRows(self, rows):
        if len(rows) > 0:
            if len(rows) == len(set(rows)):
                lp.DelRows(self, rows)
            else:
                raise 'duplicates in the list!'


            
    def ShortestMode(self, rn, lowBound = 1, *args, **argd): 
        """pre: MIP"""
        self.SetObjDirec('Min')
        rnback = self.GetOpposite(rn)

        if self.cidxs.has_key(rnback):
            self.Block(rnback)
        rv = self.OptLenSol(pos = [rn], lowBound = lowBound, *args, **argd)
        forval = self.ObjVal

        self.Block(rn)
        if self.cidxs.has_key(rnback):
            backward = self.OptLenSol(pos = [rnback], lowBound = lowBound, *args, **argd)

        if forval == 0 or 0 < self.ObjVal < forval:
            rv = backward        
        return  rv


    def MakePositive(self, pos = [], lowBound = EPSILON):
        for targ in pos:
            self.SetFluxBounds({targ:(lowBound, None)})


    def OptLenSol(self, pos = [], lowBound = 1, *args, **argd):
        self.MakePositive(pos, lowBound = lowBound)
        cont = self.GetColNames(lin)
        self.SetLenObjective(varlist = cont)
        self.Solve()
        return self.GetPrimSol()



    def Block(self, reac, on = True):
        if on:
            self.SetFixedFlux({reac:0.0})
        else:
            self.ClearFluxConstraint(reac)
        

    def GetOpposite(self, rn):
        rv = rn
        if REV_TAG in rv:
            rv = rv.replace(REV_TAG, '')
        else:
            rv += REV_TAG
        return rv  




#    def add_row_constraint(self, sto = None, lowBound = 0, upBound = 0, name = None):
#        ''' PRIVATE '''
#        name = name or 'Row ' + str(self.GetNumRows() + 1)
#        self.AddRows([name])
#        self.SetRowBounds(name, lowBound, upBound)
#        if type(sto) == types.DictType:
#            self.SetRowFromDic(name, sto)
#        elif type(sto) == types.ListType:
#            self.SetRowVals(name, sto)





    def SetLenObjective(self, varlist = []):  
        
         obj = {}
         conts = varlist or self.GetColNames(lin)
         self.SetInt2ContConstr(vars = conts)
         for contname in conts:
             intname = self.IntTag + contname
             obj[intname] = 1.0
         self.SetObjective(obj)
        
        
    def SetInt2ContConstr(self,  use_fluxcoeff = True,  vars = []):
        ''' 
                pre     :   True
                            use_fluxcoeff - if True rxn in vars (v_j):  MIN_FLUX <= v_j <= MAX_FLUX, if active.
                            If False, 0 <= v_j <= 1.
                            vars - constrained rxn, if None all variables in self are used.
                            
                post    :   for v_j in vars: v_j - min_coeff*int_v_j >=0 (if self.GetObjDirec()==Max),
                            -v_j + max_coeff*int_v_j >=0 (if self.GetObjDirec()==Min). min_coeff = MIN_FLUX
                            if use_fluxcoeff, else 0, max_coeff = MAX_FLUX if use_coeff, else 1.
                            Forces int_v_j to 1 if v_j is active.
        '''
        conts = vars or self.GetColNames(lin)
        contcoef = {'Max' : 1, 'Min' : -1}[self.GetObjDirec()]  ##max: int <= cont; min : int >= cont
        intcoef = - contcoef
        if use_fluxcoeff:
            factor = {'Max' : MIN_FLUX, 'Min' : MAX_FLUX}[self.GetObjDirec()]
            intcoef *= factor
        for contname in conts:                                                             
            intname = self.IntTag + contname
##            self.AddCol(intname, lowBound = 0, upBound = 1, kind = 'Integer', tmp = tmp)
            constraint = {contname : contcoef, intname : intcoef}
            self._add_row_constraint(constraint, 0.0, None)  ##max: cont - int >= 0; min : - cont + int >= 0



 ##excluding solutions   
    def GetNonNegNullSpace(self, dim, cnames):
        """pre: 
                    self.GetClass() == MIP, 
                    dim is the nullspace dimension, 
                    cnames is the list of linear variables in correct order.
        post: rv = non-negative nullspace matrix of the LP constraint matrix"""

        rv = DataSets.DataSet(ItemNames=self.model.sm.cnames)
        self.SetObjDirec('Min')
        self.SetLenObjective()
        self.AddSumConstraint(cnames = cnames)
        
        while len(rv.rnames) < dim:
            self.Solve()
            if self.GetStatusMsg() != 'MIP Optimal':
                raise exceptions.StandardError, "Failed to calculate a non-negative nullspace"
            newsol = self.GetPrimSol(kind = lin)
            rv.UpdateFromDic(newsol)
            intsol = self.GetPrimSol(kind =bina)
            self.SetIntegerCut(intsol)
            
        return rv
    

    def SetIntegerCut(self, intnames, coef = 1):
        """pre: self.GetObjDirec() == 'Min'; for each intname in intnames: self.GetColKind(intname) == 'binary'.
        post: any solution involving all variables in intnames is excluded."""
        cut = len(intnames) - 1
        cut *= coef
        self.AddSumConstraint(None, cut, intnames)


    def ExcludeSolution(self, vars):
        intnames = map(lambda var: self.IntTag + var, vars)
        self.SetIntegerCut(intnames)
        
        
    def FindAltSols(self, same_objval = True, tol=TOL):
        
        rv = DataSets.DataSet(ItemNames=self.GetColNames(lin))
        if same_objval:
            ref_obj = self.GetObjVal()
            go_func = lambda self: (self.GetObjVal() - ref_obj) <= tol
        else:
            go_func = lambda self: True
        
        while go_func(self) and self.IsStatusOptimal():
            this_sol = self.GetPrimSol()
            excl_sol = self.GetPrimSol(FixBack=False)
            rv.UpdateFromDic(this_sol)
            self.ExcludeSolution(excl_sol)
            self.Solve()
        
        return rv

 

    def GetPrimSol(self, IncZeroes=False,FixBack=True,AsMtx=False,kind = lin):
    
            sol = {}    
            if self.IsStatusOptimal():
                for r in self.GetColNames(kind = kind):
                    val = self.GetColVal(r)
                    if IncZeroes or not glpk.IsZero(val):
                        sol[r] = val
    
            for rev in Set.Intersect(self.sm.Reversed, sol.keys()):
                sol[rev] *= -1
    
            if FixBack:     #### all this lacks elegance - needs a re-think.svn
                for reac in sol.keys():
                    if reac.endswith("_back"):
                        val = sol[reac]
                        if not glpk.IsZero(val):
                            r2 = reac[:-5]
                            if not sol.has_key(r2):
                                sol[r2] = 0.0
                            sol[r2] -= val
                        del sol[reac]
    
            if  AsMtx:
                rv = StoMat.StoMat(cnames=["lp_sol"],rnames=sol.keys(),Conv=float)
                for r in sol:
                    rv[r][0] = sol[r]
            else:
                rv = sol
    
            return rv

 


