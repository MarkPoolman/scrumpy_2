



"""
MIP (cplex) implementation by Hassan Hartman based on ScrumPyLP by Mark Poolman and ScrumPyQP by Thomas Pfau

"""

#todo: - split into lp,mip and qp
#         - make internally compatible and compatible with MetaLP

import sys, types, exceptions


SysStderr = sys.stderr
NullF =  open("/dev/null", "w")


#Some definitions of static variables, all inequaleties are inclusive (>=, <=) 
GREATER_THAN = "G"
LESS_THAN = "L"
EQUAL_TO = "E"
RANGED = "R"

import cplex
import ScrumPyCMIP

#internal definitions for problem types
lin =ScrumPyCMIP.lin 
quad ='quadratic'
indi = ScrumPyCMIP.indi
bina = ScrumPyCMIP.bina  #not currenly in use

indivar_tag =  ScrumPyCMIP.indivar_tag
indiconstr_tag =  ScrumPyCMIP.indiconstr_tag
constr_tag = ScrumPyCMIP.constr_tag 





########################### Mappings for human readability
Status2Str = ScrumPyCMIP.Status2Str
ObjectiveMap = ScrumPyCMIP.ObjectiveMap
###############################

VerySmall = ScrumPyCMIP.VerySmall
IsZero = ScrumPyCMIP.IsZero
SplitRev=ScrumPyCMIP.SplitRev
_fix_back=ScrumPyCMIP._fix_back
UndoSplit=ScrumPyCMIP.UndoSplit
RemoveIsoRevs = ScrumPyCMIP.RemoveIsoRevs

###############################


class cqp(ScrumPyCMIP.cmip):

    def __init__(self, model,infinity = cplex.infinity, Direc='Min',  sm=None):
        
        ScrumPyCMIP.cmip.__init__(self, model,infinity = cplex.infinity, Direc='Min',  sm=None)

        self.set_problem_name("Mixed Integer Quadratic Problem")
        self.set_problem_type(self.problem_type.MIQP) 
        self.Klass='CQP'     
        self.quadratic_constraints_names = []
        self.indicator_constraints_names = []
        
    ##
    ##
    #   Objective Modifications:
    ##
    ##

    ##
    #   General
    ##

    def AddToObjective(self,reacs,var_type=lin):
        if var_type==lin:
            self.SetLinObjective(reacs)
        elif var_type==quad:
            self.SetQuadObjective(reacs)
    
    def SetObjective(self,reacs,var_type=lin):
        self.ClearObjective(var_type)
        if var_type==lin:
            self.SetLinObjective(reacs)
        elif var_type==quad:
            self.SetQuadObjective(reacs)

    def ClearObjective(self,  var_type=lin):
        self.objective.set_linear(zip(self.variables_names[:],[0]*len(self.variables_names)))
        
        if var_type==quad:
            self.objective.set_quadratic_coefficients(zip(self.variables_names[:],self.variables_names[:],[0]*len(self.variables_names)))
#            if len(self.indicator_var_names)>0:
#                self.objective.set_quadratic_coefficients(zip(self.indicator_var_names[:],self.variables_names[:],[0]*len(self.indicator_var_names)))

    def SetObjCoefsFromDic(self, d, var_type=lin):
        if var_type==lin:
            self.SetLinObjCoefsFromDic(d)  
        elif var_type==quad:
            self.SetQuadObjCoefsFromDic(d)     

    def SetObjCoefsFromLists(self, cs, coefs,var_type=lin):
        if var_type==lin:
            self.SetLinObjCoefsFromLists(cs, coefs)
        elif var_type==quad:
            self.SetQuadObjCoefsFromLists(cs, coefs)

    def SetObjCoef(self,c,coef,var_type=lin):
        if var_type==lin:
            self.SetLinObjCoef(c,coef)
        elif var_type==quad:
            self.SetQuadObjCoef(c,coef)
           
    def GetObjective(self,var_type=lin):
        res={}
        if var_type==lin:
            res.update(dict(zip(self.variables_names,self.objective.get_linear())))
        elif var_type==quad: 
            res.update(dict(zip(self.variables_names,self.objective.get_quadratic_coefficients(zip(self.variables_names,self.variables_names)))))
        for r in res.keys():
            if res[r] == 0:
                del res[r]
        return res
    
    ##
    #   Quadratic
    ##
    
    def SetQuadObjective(self,reacs):
        
        if type(reacs)==types.DictType:
            target = {}
            for reac in reacs.keys():
                target[reac] = reacs[reac]
                back = self._BackReac(reac)
                if back != None:
                    target[back] = reacs[reac]
            self.objective.set_quadratic_coefficients(zip(target.keys(),target.keys(),target.values()))
            
        else:
            target = reacs[:]
            for reac in target:
                back = self._BackReac(reac)
                if back != None and back not in target:
                    target.append(back)
                
            self.objective.set_quadratic_coefficients(zip(target,target,[1]*len(target)))

    def SetQuadObjCoefsFromLists(self, cs, coefs):

        lenc = len(cs)
        if lenc != len(coefs):
            raise exceptions.IndexError, "cs and coefs not of same length !"

        self.objective.set_quadratic_coefficients(zip(cs,cs,coefs))

    def SetQuadObjCoefsFromDic(self, d):

        self.SetQuadObjCoefsFromLists(d.keys(), d.values())

    def SetQuadObjCoef(self, c, coef):
        """set a Quadratic coefficient for a specific variable"""
        if c in self.variables_names:# or c in self.indicator_var_names:
            self.objective.set_quadratic_coefficients(c,c,coef)
            
  
 
    ##
    ##
    #   Quadratic constraint modifications
    ##
    ##

    ##
    #   Add quadratic constraints
    ##

    def AddQuadRow(self,quadvars,quadcoefs,name=None,lin_row=cplex.SparsePair(ind = [0], val = [0.0]),rhs=0,sense="L"):
        """Add a quadratic constraint,
            quadvars is a list of quadratic variables,
            quadcoefs is a list of the coefficients of the quadratic variables
            lin_row is a complete linear row with coefs for all variables
            this function  does not allow setting 'off diagonal' constraints"""
        if len(quadvars) != len(quadcoefs):
            raise exceptions.IndexError, "Inconsistant length of variables and coefficients"
        if name == None:
            name = "quad_"+str(self.quadratic_constraints.get_num())
        if coefs != None:
            coefs = lin_row[:]
            lin_row=[[],[]]
            i = 0
            for coef in coefs:
                if coef != 0:
                    lin_row[0].append(i)
                    line_row[1].append(coef)
                i+=1       
        quad_expr=[[],[],[]]
        i = 0
        self.quadratic_constraints.add(name=name,lin_expr=lin_row,quad_expr=[quadvars,quadvars,quadcoefs],rhs=rhs,sense=sense)
        self.quadratic_constraints_names.append(name)
        
    ##
    #   Delete Quadratic Constraints
    ##

    def DelQuadRow(self,r):
        if r in self.quadratic_constraints_names:
            self.quadratic_constraints.delete(r)
            del self.quadratic_constraints_names[self.quadratic_constraints_names.index(r)]
        
    
   
