
"""
ScrumPyLP - (c) Mark Poolman 2008 onwards.
A module to allow LP optimisation and analysis of of metabolic models with ScrumPy.


 This file is part of ScrumPy - Metabolic Modelling in Python

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

#TODO remove the "_back" string literals and replace with a global constant





import sys, types, exceptions

SysStderr = sys.stderr
NullF =  open("/dev/null", "w")




import glpk

lin=glpk.lin
bina=glpk.bina


from ScrumPy.Util import Set, DynMatrix
from ScrumPy.Structural import StoMat


def SplitRev(sm,Conv=float): # copy of sm with reversible reacs split into forward and back irreversible pairs

    rv = sm.Copy(Conv)
    rv.Reversed = reversed = []
    rv.Backs = backs = []

    for r in sm.cnames:
        if sm.RevProps[r] == StoMat.t_Rever:
            sto = sm.InvolvedWith(r)
            rback = r+"_back"
            rv.NewReaction(rback,sto)
            rv.MulCol(rback,k=-1)
            backs.append(rback)

        elif sm.RevProps[r] == StoMat.t_BackIrrev:
           rv.MulCol(r,  k=-1)
           reversed.append(r)

    return rv


def SplitAll(sm,Conv=float):

    sm = sm.Copy(Conv)
    irbacks = []
    for r in sm.cnames[:]:
        sto = sm.InvolvedWith(r)
        for met in sto:
            sto[met] *= -1
        newreac = r+"_back"
        sm.NewReaction(newreac,sto)
        if r in sm.Irrevs:
            irbacks.append(newreac)

    sm.Irrevs = sm.cnames[:]
    sm.irbacks = irbacks
    return sm

class lp(glpk.lp):

    def __init__(self, model, External=False,  klass='LP'):
        
    
        self.model = model
        name = model.md.GetRootFile().split(".")[0]
        if not External:
            sm = self.sm = SplitRev(model.sm)
        else:
            sm = self.sm = SplitRev(model.smx)
        glpk.lp.__init__(self,name)
        self.Klass = klass
        self.ObjVal = 0.0
        self.AddRows(sm.rnames[:]) #from glpk.lp
        self.AddCols(sm.cnames[:]) #from self
            
        
        for reac in sm.cnames:
            self.SetColBounds(reac,0)
        for r in sm.rnames:
            row = map(float, sm[r])
            if klass == 'MIP':
                row += [0.0] * len(sm.cnames)
            self.SetRowVals(r, row)
            self.SetRowBounds(r, 0, 0)
        

    def _irrev_check(self,reac,rate):
        print "_irev_check disabled"
        return
        #
        #        if  rate <0 and reac in self.sm.Irrevs:
        #            sys.stderr.write("""
        #            ! Warning  -ve rate is inconsistent with irreverisibily
        #            ! assumptions. (doing it anyway) """)


    def _BackReac(self, reac):

        rback = reac + "_back"
        if rback in self.sm.Backs:
            return rback
        return None
        
    def _add_row_constraint(self, sto = None, lowBound = 0, upBound = 0, name = None):
        ''' PRIVATE '''
        name = name or 'Row ' + str(self.GetNumRows() + 1)
        self.AddRows([name])
        self.SetRowBounds(name, lowBound, upBound)
        if type(sto) == types.DictType:
            self._set_row_fr_dic(name, sto)
        elif type(sto) == types.ListType:
            self.SetRowVals(name, sto)
            
            
    def _set_row_fr_dic(self, name, dic):
        '''PRIVATE: help function to _add_row_constraint '''
        
        row = [0] * self.GetNumCols()
        for var in dic:
            nvar = self.GetColIdxs([var])[0]
            row[nvar - 1] = dic[var]
        self.SetRowVals(name, row)  

    def GetClass(self):
        return self.Klass
        
    def AddCols(self, cnames,  IntTag='int_'):
        
        glpk.lp.AddCols(self, cnames)
        if self.GetClass() == 'MIP':
            intnames = [IntTag + cname for cname in cnames]
            glpk.lp.AddCols(self, intnames)
            for intname in intnames:
                self.SetColKind(intname, bina)
                self.SetColBounds(intname, 0, 1)
        
    def SetObjective(self,reacs):
        
        if type(reacs) == types.StringType:
            return self.SetObjective([reacs])

        if type(reacs)==types.DictType:
            reacs = dict(reacs)
            for reac in reacs.keys():
                back = self._BackReac(reac)
                if back != None:
                    reacs[back] = reacs[reac]
            self.SetObjCoefsFromDic(reacs)

        else:
            reacs = reacs[:]
            for reac in reacs:
                back = self._BackReac(reac)
                if back != None and back not in reacs:
                    reacs.append(back)
            self.SetObjCoefsFromLists(reacs,[1]*len(reacs))

    def SetObjCoef(self, c, coef):

        glpk.lp.SetObjCoef(self, c, coef)

        if not self.sm.IsIrrev(c):
            glpk.lp.SetObjCoef(self, c+"_back", coef)
            
            
    def CheckReac(self, reac):
        
        if not reac in self.model.sm.cnames:
            print >>sys.stderr,  "! %s doesn't is not a reaction ! " %reac 
            return False
            
        return True

    def CleanReacDic(self,  dic):
        " remove keys in dic that are not reactions in the model, print warning if any removed"
    
        NotFound = Set.Complement(dic.keys(),  self.model.sm.cnames)
        if len(NotFound) != 0:
            print >>sys.stderr,  "! ignoring non-existent reaction(s)",  ", ".join(NotFound)
            for r in NotFound:
                del dic[r]
                
                
    def CleanReacList(self, l):
        " remove items in l that are not reactions in the model, print warning if any removed"
        NotFound = Set.Complement(l,  self.model.sm.cnames)
        if len(NotFound) != 0:
            print >>sys.stderr,  "! ignoring non-existant reacation(s)",  ", ".join(NotFound)
            for r in NotFound:
                l.remove(r)
                
        



    def SetFixedFlux(self, reacs):
        
        self.CleanReacDic(reacs)
    
        for reac in reacs:

            rate = reacs[reac]
            if reac in self.sm.Reversed:
                rate *= -1

            rback = reac + "_back"
            hasback = rback in self.sm.Backs

            if rate >= 0:
                self.SetColBounds(reac, rate, rate)
                if hasback:
                    self.SetColBounds(rback, 0,0)
            else:
                if not hasback:
                    sys.stderr.write("! Setting inconsitant flux in irreversible reaction %s\n" % reac)
                    self.SetColBounds(reac, rate, rate)
                else:
                    self.SetColBounds(reac, 0, 0)
                    self.SetColBounds(rback, -rate, -rate)



    def FiniteBoundFlux(self, reac, lo, hi):

        
        if reac in self.sm.Reversed: # makes rate positive for reversed reacs 
           lo, hi = -hi,-lo # dont need to consider None since SetFluxBounds does 

        if hi < lo:
            lo,hi = hi,lo

        rback = self._BackReac(reac)
        if rback == None:
            if lo < 0:
                raise exceptions.ValueError, "attempt to set -ve bound on irreversible "+reac
            self.SetColBounds(reac, lo,hi)
        else:
            if hi < 0:
                self.SetColBounds(reac,0,0)
                self.SetColBounds(rback, -hi, -lo)
            elif lo < 0:
                self.SetColBounds(reac,0,hi)
                self.SetColBounds(rback, 0, -lo)
            else:
                self.SetColBounds(reac,lo,hi)
                self.SetColBounds(rback, 0, 0)




    def SetFluxBounds(self, reacs):
        """ Pre: reacs = {name:(lo,hi)...}, (lo<=hi OR hi==None) """
        
        self.CleanReacDic(reacs)

        for reac in reacs:

            lo, hi = reacs[reac]

            if lo == hi == None:
                self.UnboundFlux(reac)

            elif lo == hi:
                self.SetFixedFlux({reac:lo})

            elif not None in (lo,hi):
                self.FiniteBoundFlux(reac, lo, hi)

            else:                                  # lo == None xor hi == None
                rback = self._BackReac(reac)

                if rback == None:                  # irreversible
                    self.SetColBounds(reac, lo, hi)
                else:                              # reversible reaction
                    if lo == None:                 # lo == None
                        if hi < 0.0:
                            self.SetColBounds(reac, 0,0)
                            self.SetColBounds(rback, -hi, None)
                        else:
                            self.SetColBounds(reac,  0, hi)
                            self.SetColBounds(rback, 0, None)
                    else:
                        if lo < 0.0:                # hi == None
                            self.SetColBounds(reac, 0,None)
                            self.SetColBounds(rback, 0, -lo)
                        else:
                            self.SetColBounds(reac, lo,None)
                            self.SetColBounds(rback, 0, 0)


    def SetSumFluxConstraint(self,  reacs, total,  name):

        self.AddRows([name])
        nc  =self.GetDims()[1]
        vals = [0.0] * nc  # TODO - need a GetRow/ColVec in glpk
        reacs = reacs[:]
        for r in reacs[:]:
            rback = self._BackReac(r)
            if rback != None:
                reacs.append(rback)
        idxs = glpk.SubOne(self.GetColIdxs(reacs))
        
        for idx in idxs:
            vals[idx] = 1.0
        self.SetRowVals(name,vals)
        self.SetRowBounds(name,  total, total)
        
    def AddSumConstraint(self, lowBound = 1, upBound = 1.0, cnames = []):
        cnames = cnames or self.GetColNames()
        sto = dict.fromkeys(cnames, 1)
        self._add_row_constraint(sto, lowBound, upBound) 

     

    def UnboundFlux(self, reac):   
        raise exceptions.NameError("UnboundFlux() is deprecated, use ClearFluxConstraint(reac) instead") 


    def ClearFluxConstraint(self, reac):
        """ pre: reac in self.GetReacNames
           post: flux constraint(s) removed from reac """

        self.SetColBounds(reac, 0, None)

        rback = self._BackReac(reac)
        if rback != None:
            self.SetColBounds(rback, 0, None)


    def ClearFluxConstraints(self, reacs=[]):
        """ pre: reacs ==[] OR reacs is sibset of self.GetReacNames()
           post: reacs ==[] => all reactions constraints cleared
                 ELSE constraints of reacs cleared """

        if reacs == []:
            reacs = self.GetReacNames()

        for reac in reacs:
            self.ClearFluxConstraint(reac)



    def Solve(self,PrintStatus=True):
        glpk.lp.Solve(self)
        if PrintStatus:
            print self.GetStatusMsg()


    def GetPrimSol(self, IncZeroes=False,FixBack=True,AsMtx=False):

        sol = {}

        if self.IsStatusOptimal():
            for r in range(len(self.sm.cnames)):
                name = self.sm.cnames[r]
                val = self._glp_lpx_get_col_prim(r+1)
                if IncZeroes or not glpk.IsZero(val):
                    sol[name] = self._glp_lpx_get_col_prim(r+1)

        for rev in Set.Intersect(self.sm.Reversed,  sol.keys()):
            sol[rev] *= -1

        if FixBack:     #### all this lacks elegance - needs a re-think.svn
            for reac in sol.keys():
                if reac.endswith("_back"):
                    val = sol[reac]
                    if not glpk.IsZero(val):
                        r2 = reac[:-5]
                        if not sol.has_key(r2):
                            sol[r2] = 0.0
                        sol[r2] -= val
                    del sol[reac]

        if  AsMtx:
            rv = StoMat.StoMat(cnames=["lp_sol"],rnames=sol.keys(),Conv=float)
            for r in sol:
                rv[r][0] = sol[r]
        else:
            rv = sol

        return rv
        
        
    def SolRateVector(self):
        """pre: self.IsStatusOptimal()
          post:  column rate vector corresponding to current sol, conformant with m.sm"""
        
        return self.model.sm.RateVector(self.GetPrimSol())
        
        
    def SolStoDic(self,  tol=1e-12):
        """pre: self.IsStatusOptimal()
          post:  Net soichiometry of current sol as a dictionary"""
     
        StoVec = self.model.smx.Mul(self.SolRateVector())
        StoVec.ZapZeroes(tol)
        return StoVec.InvolvedWith(StoVec.cnames[0])
          
          
          
          
          
          
          
          


    def GetSimplexAsMtx(self):

        rnames = self.GetRowNames()
        cnames = self.GetColNames()
        rows   = self.GetRowsAsLists()

        rv = DynMatrix.matrix(rnames=rnames, cnames=cnames, Conv=float)
        rv.rows =rows

        return rv


    def GetReacNames(self):
        """pre: TRUE
          post: list of reaction names used to generate this LP"""

        return filter(lambda s: not s.endswith("_back"),  self.sm.cnames)








    def MatchFlux(self,fluxes, IncZeroes=False, AsMtx=False, SupressWarnings=True):

        if SupressWarnings:
            sys.stderr = NullF

        self.SetFixedFlux(fluxes)
        self.SetObjective(self.sm.cnames)
        self.Solve()

        if SupressWarnings:
            sys.stderr = SysStderr

        return self.GetPrimSol(IncZeroes=IncZeroes, AsMtx=AsMtx)







