
/***

glpk_ll.i - (c) Mark Poolman 2008 onwards.
swig interface file for glpk_ll, low-level interface for python and the glpk library of
Andrew Makhorin (mao AT gnu DOT org) which can be found at www.gnu.org/software/glpk


This module is still experimental and is

  *************************   NOT TO BE REDISTRIBUTED  ***************************

without the prior consent of the author.


***/


%module glpk_ll
%{
#include "glpk.h"
%}
%include "carrays.i"
%array_functions(int, intArray)
%array_functions(double, doubleArray)
%include "../cextns/include/glpk.h"
/*%include "/usr/local/include/glpk.h"*/
/*%include "/usr/include/glpk.h"*/

/* modify as appropriate */
