
"""
glpk __init__ - (c) Mark Poolman 2008 onwards.

Just a package holder at present.
See other modules in this directory

This module is still experimental and is

  *************************   NOT TO BE REDISTRIBUTED  ***************************

without the prior consent of the author.

"""


import sys

from ScrumPy.Util import  Kali
import ScrumPyLP,  ScrumPyMIP


lp = ScrumPyLP.lp 
mip = ScrumPyMIP.mip

try:
    import cplex
    HaveCPlex = True
except:
    HaveCPlex = False

class NoCPlex(Kali.Kali):
    def __getattr__(self,  a):
        print >>sys.stderr,  "\n! CPlex not found, no valid results from this function !"

if HaveCPlex:
    import  ScrumPyCLP, ScrumPyCMIP,  ScrumPyCQP
    clp = ScrumPyCLP.clp
    cmip = ScrumPyCMIP.cmip
    cqp = ScrumPyCQP.cqp
else:
    clp = cpq = cmip = NoCPlex("cplex")

