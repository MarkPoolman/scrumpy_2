
"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

import sys,types,  math


from ScrumPy.Util  import DynMatrix, Sci,  Set

import Stats, Histo,  Plotter





class  DataSet(DynMatrix.matrix):

    def __init__(self, FromFile=None, ItemNames=[], SrcDic=None,FromMtx=None, Conv=float,  Delim="\t"):
        """ pre: No more than one non-default argument
            (FileName != None) -> (File conforms to FileSpec)
           post: Usable """

        self.Delim = Delim
        self.FileName=None


        if FromMtx != None:
            DynMatrix.matrix.__init__(self,Conv=Conv, FromMtx=FromMtx)
        elif FromFile != None:
            DynMatrix.matrix.__init__(self,Conv=Conv)
            self.ReadFile(FromFile)
        elif SrcDic != None:
            DynMatrix.matrix.__init__(self,cnames=ItemNames,Conv=Conv)
            self.SrcDic = SrcDic
        elif ItemNames != []:
            DynMatrix.matrix.__init__(self,Conv=Conv,cnames=ItemNames)
        else:
            DynMatrix.matrix.__init__(self,Conv=Conv)



        self.Plotter = Plotter.Plotter()
        self.Plotter.SetMissing()
        self.PlotX = 0
        self.PlotY = 1


    def Copy(self):

        return   DataSet(FromMtx=self)

    def ClearData(self):
        """ remove all rows, leave cols and other info intact"""

        self.rows = []
        self.rnames =[]


    def Subset(self, names):
        rv = DataSet()
        for name in names:
            rv.NewCol(self.GetCol(name),name)
        rv.rnames  = self.rnames[:]
        return rv


    def ReadFile(self, File):

        if type(File) == types.StringType:
            self.FileName = File
            File = open(File)

        self.Clear()

        self.cnames = File.readline().strip().split(self.Delim)

        for line in File.readlines():
            self.NewRow(line.strip().split(self.Delim))



    def WriteFile(self,File=None):

        if File == None and self.FileName == None:
            print "No file to write to !"

        else:
            if type(File) == types.StringType:
                self.FileName = File
                File = open(File, "w")

            else:
                File = open(self.FileName, "w")

            print >>File,  self.Delim.join(self.cnames)

            for r in self:
                print >>File, self.Delim.join(map(str, r))



    def WriteFileAsColPairs(self, File,ref):

        if type(File)==types.StringType:
            File = open(File, "w")

        refc = self.GetCol(ref)

        for cname in self.cnames:
            if cname != ref:
                File.write("#" + ref+" "+cname+"\n")
                col = self.GetCol(cname)
                for i in range(len(refc)):
                    File.write(str(refc[i])+"\t"+str(col[i]) + "\n")
                File.write("\n")



    def Update(self, label=None):
        #
        # TODO: self.UpdateFromDic(self.SrcDic) ??
        #
        if label==None:
            label = "Row_" + str(len(self))
        row = [0]*len(self.cnames)
        self.NewRow(row,label)
        try:
            for k in self.cnames:
                self[label,k]= self.SrcDic[k]
        except:
            print """"
            !!! DataSet.Update failed - either not created with source dctionary
            !!! or source dictionary keys has new keys
            """


    def UpdateFromDic(self, dic, name=None):

        self.NewRow()
        if name != None:
            self.rnames[-1] = name
        for k in dic.keys():  
		# iterate explicitly on keys because some "dictionary-like" classes might not support direct iteration
            if not k in self.cnames:
                self.NewCol(name=k)
            self[-1,k] = dic[k]


    def _get(self, Name):
        try:
            return self[Name]
        except:
            return self.GetCol(Name)

    def Sum(self, Name):
        """ pre: len(self[Name]) > 0
            post: self.Sum(Name) == Arithmetic sum of self[Name] """

        return sum(self._get(Name))

    def Mean(self, Name):
        """ pre: len(self[Name]) > 0
            post: self.Mean(Name)    == arithmetic mean of self[Name] """

        return Stats.Mean(self._get(Name))


    def ColMeans(self):
        """ pre: len(self) >0
          post: list of mean values of items in self """

        return map(self.Mean,  self.cnames)


    def Variance(self, Name):
        """ pre: len(self[Name]) > 1
            post: self.Variance(Name)== variance self[Name] """

        return Stats.Var(self._get(Name))


    def StdDev(self, Name):
        """ pre: len(self[Name]) > 1
            post: self.StdeDev(Name) == standard deviation of self[Name] """

        return Stats.StdDev(self._get(Name))

    def StdErr(self, Name):
        """ pre: len(self[Name]) > 1
            post: self.StdErr(Name)  ==  standard error of the mean of self[Name] """

        return self.StdDev(Name)/math.sqrt(len(self._get(Name)))


    def Lower_n_ile(self, Name, n):
        """ pre: len(self[Name]) > 0, 0 < n <= 1
            post: self.Lower_n_ile(self, Name, n) == value of self[Name] which is the limit of the
            lower nth portion of self[Name] """

        return Stats.Lower_n_ile(self._get(Name),n)


    def Upper_n_ile(self, Name, n):
        """ pre: len(self[Name]) > 0, 0 < n <= 1
            post: self.Lower_n_ile(self, Name, n) == value of self[Name] which is the limit of the
            upper nth portion of self[Name] """

        return Stats.Upper_n_ile(self._get(Name),n)


    def Median(self, Name):
        """ pre: len(self[Name]) > 0
            post: self.Median(Name) == Median value of self[Name] """

        return Stats.Median(self._get(Name))


    def TTest(self, Name_a, Name_b):
            """ Student's (two tailed) t and p(t) """
            return Stats.TTest(self._get(Name_a),self._get(Name_b))


    def FTest(self, Name_a,  Name_b):
            """ F and p(F) """
            return Stats.FTest(self._get(Name_a),self._get(Name_b))


    def Pearsons(self,  ref,  targs=[]):
        """ pre: ref and targs in self.cnames
           post: dictionary of {targ: Pearsonsr(ref, targ)}"""

        rv = {}
        ref = self.GetCol(ref)
        for t in targs:
           rv[t] = Stats.Pearson_r(ref, self.GetCol(t))

        return rv


    def Spearmans(self,  ref,  targs=[]):
        """ pre: ref and targs in self.cnames
           post: dictionary of {targ: Pearsonsr(ref, targ)}"""

        rv = {}
        ref = self.GetCol(ref)
        for t in targs:
           rv[t] = Stats.Spearman_r(ref, self.GetCol(t))

        return rv


    def Deriv(self, wrt):

        ## much easier to do with rows, so
        if wrt in self.cnames:
            self.Transpose()
            rv = self.Deriv(wrt)
            self.Transpose()
            rv.Transpose()
            return rv

        rv = DataSet()

        x = self[wrt]
        lenx_mi_1 = len(x)-1
        for rname in self.rnames:
            if rname != wrt:
                retrow = []
                y = self[rname]
                for n in range(1,lenx_mi_1):
                    retrow.append(Sci.Deriv(x[n-1:n+2],y[n-1:n+2]))
            else:
                retrow = x[1:-1]
            rv.NewRow(retrow, rname)
        rv.cnames = self.cnames[1:-1]
        print self.cnames,rv.cnames

        return rv


    def MeanDuplicates(self,  RefCol, SetPlotXRef=True, OrderByRef=True ):
        """ pre: self.GetCol(RefCol)
           post: Data set of average values for which RefCol value is duplicated in self
        """

        rv = DataSet(ItemNames=self.cnames)
        if SetPlotXRef:
            rv.SetPlotX(RefCol)

        Uniques = Set.MakeSet(self.GetCol(RefCol))
        if OrderByRef:
            Uniques.sort()


        for u in Uniques:
            uds = self.Filter(lambda x: x==u,  RefCol)
            rv.NewRow(uds.ColMeans())

        return rv






    #
    ##
    ###  Plotting functions #########################
    ##
    #

    def SetPlotX(self, x):
        """"pre: True
           post:    if self has a column of name or index x, this will used as x axis for plotting
                  else warning message on terminal"""

        if x in self.cnames:
            self.PlotX = x
            self.Plot()
        else:
            print "!! ",  x,  "not found - ignoring !!"


    def SetPlotY(self,  y):
        """"pre: True
           post:    if: self has a column of name or index y, this will used as the y axis for 3d surface plots
                  else: warning message on terminal"""

        if y in self.cnames:
            self.PlotY = y



    def SetLogAxis(self, Axis="x", On=True):
        """ pre: Axis="x"|"y", On boolean """

        self.Plotter.SetLog(Axis, On)
        self.Plot()


    def SetPlotStyle(self, Data=[], Style="linesp"):

        if Data == []:
            Data = self.GetPlotItems()
        elif type(Data) == types.StringType:
            Data = [Data]

        for d in Data:
            self.Plotter.SetStyle(d, Style)

        self.Plot()




    def AddToPlot(self, ys, style="linespoints",  AutoPlot=True,  XclX=True):
        """ pre:  ys is a string or list of strings
                  style is a valid gnuplot style
            post:   if: present items in ys are added to the current plot
                  else: warning(s) on terminal
                  XclX => independent variable ignored if in ys
                  AutoPlot => plot is updated immediatly (applies to all other methods with AutoPlot parameter
        """

        if not type(ys) == types.ListType:
            ys = [ys]
            
        if XclX and self.PlotX in ys:
            ys.remove(self.PlotX)

        for y in ys:
            if y in self.cnames:
                self.Plotter.AddData(y, [self.GetCol(self.PlotX), self.GetCol(y)],style)
            else:
                print >>sys.stderr, "! ignoring attempt to plot non-existent value ",y, " !"

        if AutoPlot:
            self.Plot()


    def AddToSurfPlot(self, zs, style="lines"):

        if not type(zs) == types.ListType:
            zs = [zs]

        for z in zs:
            if z in self.cnames:
                self.Plotter.AddData(z, [self.GetCol(self.PlotX), self.GetCol(self.PlotY),  self.GetCol(z)],style)
            else:
                print >>sys.stderr, "! ignoring attempt to plot non-existent value ",z, " !"

        #if AutoPlot:
        #    self.SurfPlot()









    def AddMatchesToPlot(self, substr,  style="linespoints",   AutoPlot=True,  XclX = True):
        """ pre:  substr is a string
                  style is a valid gnuplot style
            post: item names  containing substr are added to the current plot """

        self.AddToPlot(filter(lambda col: substr in col, self.cnames), style,  AutoPlot,  XclX)



    def RemoveFromPlot(self, ys, AutoPlot=False):
        """ pre: ys is a string or list of strings
           post: ys is/are not in the current plot"""

        if not type(ys) == types.ListType:
            ys = [ys]

        for y in ys:
            self.Plotter.RemoveData(y)

        if AutoPlot:
            self.Plot()
            
 


    def RemoveMatchesFromPlot(self, substr,  AutoPlot=True):
        """ pre: substr is a string
           post: items whose name matches gsubstr are removed from the plot """

        self.RemoveFromPlot(filter(lambda key: substr in key, self.Plotter.keys()), AutoPlot)



    def RemoveAllFromPlot(self):
        """ pre: True
           post: self.GetPlotItems==[]"""

        self.RemoveFromPlot(self.Plotter.keys())




    def ReplacePlotItems(self, Items,AutoPlot=True, XclX=True):
        """ pre: Util.Set.IsSubset(Items, self.cnames)
           post: Util.Set.IsEqual(Items, self.GetPlot)"""

        self.RemoveAllFromPlot()
        self.AddToPlot(Items, AutoPlot,  XclX)





    def GetPlotItems(self):
        """ return a list of names of data currently being plotted"""

        return self.Plotter.keys()


    def GetPlotData(self, x, y):
        """ return a Plotter.Plotdata object, corresponding to col x and y,
            useful for combining data from more than one data set in a single plot.
            see the Data.Plotter module """

        return Plotter.Plotdata([self.GetCol(self.PlotX), self.GetCol(y)])


    def SavePlot(self, filename):
        """ save the current plot in gnuplot readable format to filename """

        self.Plotter.SavePlot(filename)


    def SavePlotPDF(self, filename):

        self.Plotter.SaveA4PDF(filename)


    def Plot(self, MaxPoints=0):
        """ update the current plot window or plot in new one if not present"""

        if MaxPoints ==0:
            start=0
        else:
            lenself = len(self)
            if lenself > MaxPoints:
                start= lenself - MaxPoints

        x=self.GetCol(self.PlotX)[start:]
        for k in self.Plotter.keys():
            y = self.GetCol(k)[start:]
            self.Plotter[k].UpdateData([x, y])

        self.Plotter.Plot()


    def SurfPlot(self):

        self.Plotter.SurfPlot()




    def Histogram(self, name, nbins,**kwargs):
        if name in self.cnames:
            return Histo.Histo(self.GetCol(name), nbins, **kwargs)
        else:
            return Histo.Histo(self[name], nbins, **kwargs)


    def HistogramAll(self, nbins, **kwargs):
        data = []
        for r in self.rows:
            data.extend(r)
        return Histo.Histo(data, nbins, **kwargs)


    def HistogramUD(self, nbins,Diag=True, **kwargs):
        """ pre: self is square """
        data = []
        if Diag:
            for i in range(len(self)):
                data.extend(self[i][i:])
        else:
            for i in range(len(self)-1):
                data.extend(self[i][i+1:])

        return Histo.Histo(data, nbins, **kwargs)




def TopAbsNFromDic(dic,  n,  cname):
     
    sortedvals = sorted(dic.values(), lambda x,y: cmp(abs(x), abs(y)), reverse=True)
    aminval = abs(sortedvals[n-1])

    rv = DataSet(ItemNames=[cname])
    for k in dic:
        v = dic[k]
        if abs(v)>= aminval:
            rv.NewRow([v], k)
            
    rv.SortBy(cname,Descend=True)
    return rv
    



