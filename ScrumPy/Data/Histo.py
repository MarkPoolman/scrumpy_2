import types

from ScrumPy.Util import Sci
import  Plotter

class Histo(dict):

    def __init__(self,vals, bins=10,name="Histogram"):
        """pre: nbins>1, NOT(len(vals)==0 AND (minv==None OR maxv==None))"""

        self.vals = []
        self.bins = bins

        self.name  = name
        self.style = "boxes"
        self.Plotter = Plotter.GPPlotter()
       
        self.AddVals(vals)
        
    
        
    def AddVals(self, vals, bins=None, Replot=False):

        self.vals+=vals
        if bins==None:
            bins = self.bins
        else:
            self.bins = bins
            
        if type(bins) == types.StringType:
            if bins.lower() == "auto":
                self.bins = bins = 1+int(Sci.Log2(len(self.vals))) # Sturges' formula
                
        self.clear()
        
        bounds, counts = self.Boundaries,  self.Counts = Sci.HistoLists(self.vals,  bins=bins)
        
        for n in range(len(counts)):
            mid = (bounds[n] + bounds[n+1])/2.0
            self[mid] = counts[n]
        
                
    
        if Replot:
            self.Plot()


    def __str__(self):
        rv = ""
        ks = self.keys()
        ks.sort()
        for k in ks:
            rv += " ".join([str(k), str(self[k])])+"\n"
        return rv



    def AsTuples(self):

        ks = self.keys()
        ks.sort()
        rv = []
        for k in ks:
            rv.append( (k,self[k]))
        return rv

    def AsLists(self):
        keys = self.keys()
        keys.sort()
        vals = map(lambda k: self[k], keys)
        return keys, vals

    def AsList(self):
        return self.AsLists()[1]


    def SetupPlot(self):

        p = self.Plotter
        if p.has_key(self.name):
            p[self.name].UpdateData(self.AsLists())
        else:
            p.AddData(self.name, self.AsLists())

        minb = self.Boundaries[0]
        maxb =self.Boundaries[-1]
        brange = (maxb-minb) * 1.1
        print minb, maxb, brange
        minb= maxb - 0.95*brange
        maxb = minb + brange
        print minb, maxb, brange
        
        maxy = str(max(self.values()) * 1.1)
        
        self.Plotter.Options['xrange'] = 'set xra [' + str(minb) +':'+str(maxb)+']'
        self.Plotter.Options['yrange'] = 'set yra [0:' + maxy + ']'

    def Plot(self):
        self.SetupPlot()
        self.Plotter[self.name].Options["Style"] = self.style
        self.Plotter.Plot()

    def GetPlotData(self):
        self.SetupPlot()
        return self.AsLists()

    




    def Save(self, file):

        if type(file)==types.StringType:
            file = open(file,"w")

        file.write(str(self))





def test():
    from random import gauss

    g = lambda x: gauss(0,1)
    return Histo(map(g, range(5000)))

