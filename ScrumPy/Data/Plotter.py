import sys

from ScrumPy.Util import Set,  Gnuplot


def EnQuote(s):
    return s.join(("'", "'"))

def EnDQuote(s):
    return s.join(('"', '"'))



class Data:
    """ base class to hold data sent to a plotter """

    def __init__(self, data,  options={}):
        """ pre: data is list of equal length lists of ints or floats """

        self.Options = dict(options)
        self.ValidOptions = {}
        self.UpdateData(data)

    def UpdateData(self, data):
        self.data = data


    def CheckOption(self,  key, val=None):

        rv = len(self.ValidOptions) ==0 # ie we have set some valid
        if self.ValidOptions.has_key(key):
            if val in self.ValidOptions[key] or val == None:  # val == None => only check key
                rv =True
            else:
                print "Invalid value", str(val),  "for option",  key
        else:
            print "no such option -",  key

        return rv


    def SetOption(self, key, val):

        #if self.CheckOption(key, val):
        self.Options[key] = val


    def GetOptions(self):
        pass

    def GetData(self):
        pass


class PlotterBase(dict):

    pass



class wxData: #(Data):

    DefaultOpts = {
          "Style":"line",      # or "marker"
          "Substyle":"SOLID",
          "colour" : "BLACK",
          "width"  :2,
          "marker"  :"circle",    # only for style marker
          "Title"    : ""
       }


    def __init__(self,  data):

        if len(data)==1:
            self.y = data[0]
            self.x = range(len(self.y))
        else:
            self.x = data[0]
            self.y = data[1]

        self.Options = wxData.DefaultOpts



    def GetData(self):
        return self.x, self.y


    def GetOptions(self):
        return self.Options














def ValidNum(x):
    try:
        return str(float(x)) != "nan"
    except:
        return False





class GPData(Data):
    """ data for gnuplot """

    DefaultOpts ={
        "Style": 'linesp',
        "Using":"1:2",
        "PType": '',
        "Colour": '',
        "Title": ''
    }

    def Sanitise(self, data):

        xdat,  ydat = data[0], data[1]

        checkx = map(ValidNum, xdat)
        while False in checkx:
            idx = checkx.index(False)
            for l in checkx,  xdat,  ydat:
                del l[idx]

        checky = map(ValidNum, ydat)
        while False in checky:
            idx = checky.index(False)
            for l in checky,  xdat,  ydat:
                del l[idx]


    def UpdateData(self,data):
        self.Sanitise(data)
        self.data = data


    def __init__(self, data):
        self.Sanitise(data)
        Data.__init__(self, data)
        self.ValidOptions = dict(self.DefaultOpts) #  ?? why ? - surely CheckOptions ?
        self.Options.update(self.DefaultOpts)

        if len(data) == 3:
            self.Options["Using"] = "1:2:3"
            self.IsSurf = True
        else:
            self.IsSurf = False


    def GetData(self):

        lend = len(self.data[0])
        lines = []
        for n in range(lend):
            lines.append(" ".join(map(str,map(lambda l:l[n], self.data))))

        return "\n".join(lines)+"\ne\n"


    def GetSurfData(self):

        if self.data[0][0] == self.data[0][1]:
            indep = 0
        else:
            indep = 1

        lend = len(self.data[0])
        lines = [" ".join(map(str,map(lambda l:l[0], self.data)))]

        for n in range(1, lend):
            if self.data[indep][n-1] != self.data[indep][n]:
                lines.append("")

            lines.append(" ".join(map(str,map(lambda l:l[n], self.data))))

        return "\n".join(lines)+"\ne\n"




    def GetOptions(self):
        return " ".join((
            "'-'",
            "using",self.Options["Using"],
            "t"+EnDQuote(self.Options["Title"]),
            "with ", self.Options["Style"],
            str(self.Options["Colour"]),
            str(self.Options["PType"])
        ))


    def SetStyle(self, Style):
        self.Options["Style"] = Style







class GPPlotter(PlotterBase):

    default_term = '""' # let gnuplot decide - can be changed to any terminal gnuplot knows about
    pref_term = "qt" # prefered  terminal to use as default, if avaiable
    plot_cmd = "plot "
    splot_cmd = "splot "
    

    DefaultOptions = {
        "Logx" : "unset log x",
        "Logy" : "unset log y",
        "Logz" : "unset log z",
        "xrange": "set xra [:]",
        "yrange" : "set yra [:]",
        "zrange" : "set zra [:]",
        "out" : "set out "
    }

    def __init__(self,  GP=None):
        """ GP==None: use  the global gnuplot process, 
             else pre: GP=Gnuplot.Gnuplot() and use that instead"""
        
        if GP == None:
            self._gp = Gnuplot.GlobalGP
        else:
            self._gp = GP
            
        if self.pref_term in self._gp.Terminals():
            self.default_term = self.pref_term
        
        self.DefaultOptions["term"] =  "set term " + self.default_term
            
    
        self.Options = {}
        self.Options.update(self.DefaultOptions)
        self.PlotNames = []
        self.SurfPlotNames = []
        
        self.WinNo = self._gp.NewWinNo()
        self.Options["term"] += " "+str(self.WinNo)
        
    def __del__(self):
        self._gp.FreeWinNo(self.WinNo)
        
        


    def ToGP(self, cmd):
        return self._gp(cmd)
        
    def Reset(self):
        self.ToGP("reset ; set out")


    def GetOptions(self):

       return "\n".join(["set auto"]+self.Options.values())+"\n"


    def SetMissing(self, MissStr = '"nan"'):

        self.ToGP('set datafile missing '+ MissStr)


    def AddData(self, name, data, style="linespoints"):

        if not self.has_key(name):
            self[name] = GPData(data)
            if self[name].IsSurf:
                self.SurfPlotNames.append(name)
            else:
                self.PlotNames.append(name)

        self[name].Options["Title"] = name
        self[name].Options["Style"] = style


    def RemoveData(self, name):

        if self.has_key(name):
            del self[name]
            if name in  self.PlotNames:
                self.PlotNames.remove(name)
            if name in self.SurfPlotNames:
                self.SurfPlotNames.remove(name)




    def AutoName(self):

        for k in self.keys():
            self[k].Options["Title"] =k




    def PlotStr(self, Names=[]):

        preamble = data = "#" # send gnuplot a comment if no data available


        if len(Names)==0:
            Names = self.PlotNames

        if len(Names) == 0:
            Names = self.SurfPlotNames

        XY  = Set.DoesIntersect(Names, self.PlotNames)
        XYZ = Set.DoesIntersect(Names, self.SurfPlotNames)

        if XY and XYZ:
            print "!! Can't plot 2d and 3d data at the same time - using 2d only !!"
            XYZ = False
            Names = Set.Intersect(Names, self.PlotNames)

        if XY:
            preamble = self.GetOptions()+self.plot_cmd+",".join(map(lambda n: self[n].GetOptions(), Names))
            data = "\n".join(map(lambda n:self[n].GetData(), Names))

        if XYZ:
             preamble = self.GetOptions()+self.splot_cmd+",".join(map(lambda n: self[n].GetOptions(), Names))
             data = "\n".join(map(lambda n:self[n].GetSurfData(), Names))

        
        return preamble+"\n"+data


    def Plot(self,Names=""):
        
        self.Reset()

        self.ToGP(self.PlotStr(Names))


    def SurfPlot(self, Hidden=True):
        
        self.Reset()

        if Hidden:
            self.ToGP("set hidden3d")
        else:
            self.ToGP("unset hidden3d")


        self.ToGP(self.PlotStr(self.SurfPlotNames))



    def Replot(self):
        self.Plot(self.PlotNames)

    def SavePlot(self,filename):
        """ save the current plot in gp format to filename """

        print >>open(filename,"w"), self.PlotStr()



    def SetRange(self, Axis="x",  lo="", hi=""):
        """default args are auto scale (lo,hi)"""

        optk = Axis.lower()+"range"
        lo= str(lo)
        hi = str(hi)
        self.Options[optk] = "".join(("set ",  optk,  "[", lo, ":", hi, "]"))


    def SetLog(self, Axis="x", On=True):

        Axis = Axis.lower()
        optk = "Log" + Axis
        if On:
            cmd = "set"
        else:
            cmd = "unset"

        self.Options[optk] = " ".join((cmd, "log", Axis ))


    def SetStyle(self, Data, Style):

        if self.has_key(Data):
            self[Data].SetStyle(Style)
        else:
            print >>sys.stderr,  "!! Can't set style,",  Data,  "not in Plotter !!"
            #
            # TODO: check style options available from gnuplot on the fly
            #



    def SaveData(self, FName, Names=""):

        open(FName,"w").write(self.PlotStr(Names))


    def SaveGraphic(self, FName, term, opts):

        oldterm = self.Options["term"]
        out = self.Options["out"]
        self.Options["term"] = " ".join(("set term", term, opts))
        print self.Options["term"]
        self.Options["out"] = "set out "+EnQuote(FName)
        self.Replot()
        self.ToGP("set out") # make sure gnuplot flushes and closes the output file
        self.Options["term"] = oldterm
        self.Options["out"] = out



    def SavePS(self,  file,  opts = "enhanced eps colour solid linewidth 2"):

       self.SaveGraphic(file, "post", opts)


    def SaveSVG(self, file, opts = "enhanced linewidth 2"):

        self.SaveGraphic(file, "svg", opts)


    def SaveA4PDF(self, file):
        """ pdf suitable to print direct to A4 - use 90% of A4 area. """

        self.SaveGraphic(file,  "pdf", " lw 4 size 10.53,7.47")



Plotter = GPPlotter
Plotdata = GPData
