
"""
NOTE:
 apt-get install libxml2-dev libz-dev libbz2-dev python-pip
 pip install python-libsbml to get libsbml 
 
 If not otherwise in repositories.

see http://sbml.org/Software/libSBML/docs/python-api/libsbml-downloading.html#dl-python
"""


import os, sys,  atexit

# svn test




from ModelDescription.ModelDescription import ModelDescription as ModelDesc
from ModelDescription import  SBML
#TODO: shouldn't import SBML here - it's the ModelDesc's job to take care of this
from Kinetic.Model import Model as  KineticModel


from Util import Procs
atexit.register(Procs.KillChildren)
#NB interactive UIs will need to call Procs.KillChildren when the main loop terminates
# the above is for batch mode running


Location =  __file__.rsplit("/",1)[0]
ExtraPaths=[
            Location,
            os.sep.join((Location, "ThirdParty")),
            "/usr/local/ScrumPy",               # global local (iyswim) extra modules
]



NoFile = "No File Supplied"
DefaultUI = None
IsInit=False


def Init(TheGui=None):

    global IsInit,  DefaultUI

    if IsInit:
        print "ignore re-init"
        
    else:
        
        DefaultUI = TheGui
    
        home =os.getenv("HOME")
        if home != None:
            ExtraPaths.append(home +"/.ScrumPy")
        
        for path in ExtraPaths:
            if not path in sys.path:
                sys.path.append(path)
        
        IsInit=True


def uiModel(FileName,  ui):
    
    m = None
    
    if FileName == None:
        FileName = ui.GetFileName()
        if FileName == "":
            FileName = None
            ui.ErrorMsg(NoFile)
            
    if FileName != None:
        m =  NouiModel(FileName)  # ie a gui model is a nogui model with an added ui
        ui.BindModel(m)
        
    return m
            
    
            
            
            

def NouiModel(FileName):
    
    rv  = None
    
    if FileName == None:
        print NoFile
    
    elif SBML.LooksLikeSBML(FileName):
        SpyFile = SBML.SpyFNameToSBML(FileName)
        SBML.SBML2Spy(FileName, SpyFile=SpyFile)
        rv = NouiModel(SpyFile)
        
    else:
        md = ModelDesc(FileName)
        rv =  KineticModel(md) # (will only initialise the structural components if not kinetic)
        
    return rv



def Model(FileName=None,  ui = None):
    
    if ui == None:
        ui = DefaultUI
    
    
    print "ui",  ui
    
    if ui != None:
        return uiModel(FileName,  ui)
        
    else:
        return NouiModel(FileName)
       
            
        
    
