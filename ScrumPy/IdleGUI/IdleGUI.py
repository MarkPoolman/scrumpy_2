

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2005

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

import os,  types,  exceptions


from ScrumPy.ModelDescription import  SBML

from EdWin import EdWin

root = None # Tkinter.Toplevel()
# root is declared here so that it can be seen in the
# implementations of the following functions.
# the software calling these functions should ensure
# that it has been assigned first




############################## messages,
# all we do is make sure that messages.*
# get the right root

import Messages
def ErrorMsg(Message="", **kwargs):
    Messages.ErrorMsg(Parent=root,Message=Message, **kwargs)

def WarnMsg(Message = "",Parent=None, **kwargs):
    Messages.WarnMsg(Parent=root,Message=Message,**kwargs)

def InfoMsg(Message = "",**kwargs):
    Messages.InfoMsg(Parent=root,Message=Message,**kwargs)
    
    
def AskYesNo(Message="",  **kwargs):
    return Messages.AskYesNo(Parent=root,Message=Message,**kwargs)



################################ file dialogs, as messages

import FileDialogs

def LoadFile():
    return FileDialogs.LoadFile(Parent=root)

def LoadFileName():
    return FileDialogs.LoadFileName(Parent=root)

def SaveFile():
    return FileDialogs.SaveFile(Parent=root)

def SaveFileName():
    return FileDialogs.SaveFileName(Parent=root)
    
def GetFileName():
    return FileDialogs.GetFileName(Parent=root)

class  BindModel:
    
    def __init__(self, m):
        
        if root == None:
            raise exceptions.ValueError,  "root not initialised!"
        
        self.root = root
        self.Model = m
        m.ui = self
        self.RootFile = m.md.GetRootFilePath()
        self.Path =  m.md.Path
        self.Init()
        
    def __del__(self): 
        self.Close()     # ensure editor windows are closed when deleted
        
    def Close(self): 
        for e in self.Editors.values(): # can't gaurentee that __del__ will be called so provide a method to close everything
            e.close()
        
        
    def ReportErrors(self, MaxRep=10):
        
        LastMsg =""
        errors = self.Model.md.Errors
        n_err = len(errors)
        
        if n_err !=0:
        
            if n_err > MaxRep:
                LastMsg = "\nA further " + str(n_err - MaxRep) + "errors not reported"
                errors = errors[:MaxRep]
      
            for err in errors:
                fname = os.sep.join((self.Path, err.FName))
                line = err.LineNo
                if self.Editors.has_key(fname) and  type(line) == types.IntType:
                    ed = self.Editors[fname]
                    ed.gotoline(line)
                # TODO: how to make EdWins with errors stay on top ??
            
            msg = "\n".join(map(str, errors))+LastMsg
            ErrorMsg(msg)
            
            
            
        
    def ReportWarnings(self, MaxRep=10):
        
        n_err = len(self.Model.md.Errors) 
        warnings = self.Model.md.Warnings
        n_warn = len(warnings)
        LastMsg =""
        
        if  n_err == 0 and n_warn !=0:     
        
            if n_warn > MaxRep:
                LastMsg = "\nA further " + str(n_err - MaxRep) + "warnings not reported"
                warnings = warnings[:MaxRep]
      
            for warn in warnings:
                fname = os.sep.join((self.Path, warn.FName))
                line = int(warn.LineNo)
                ed = self.Editors[fname]
                ed.gotoline(line)
                
            
            msg = "\n".join(map(str, warnings))+LastMsg
            WarnMsg(msg)
     
        
    def Init(self):
        
        m = self.Model
        self.Editors = {}
        for f in m.md.FileNames:
            f = os.sep.join((m.md.Path, f))
            self.Editors[f] = EdWin(f, self)

        self.ReportWarnings() # warnings first, so that subsequent errors            
        self.ReportErrors()   # will have their windows placed on top
        
        
    def ReInit(self):
        
        self.Close()
        m = self.Model
        self.Editors = {}
        for f in m.md.FileNames:
            f = os.sep.join((m.md.Path, f))
            self.Editors[f] = EdWin(f, self)

        
    def Reload(self):
        
        for e in self.Editors:
            self.Editors[e].SaveFile(e)
            self.Editors[e].close(Ask=False)
     
        self.Model.Reload(FromUI=True)
        self.Init()
        
        
    def SaveSBML(self,  FileName):
        SBML.Spy2SBML(self.Model,  FileName)
        
        
        
    def HideChildren(self):
        
        for f in self.Editors:
            if f != self.RootFile:
                try:
                    self.Editors[f].top.iconify()
                except:
                    pass
                
                
    def ShowChildren(self):
        
        for f in self.Editors:
            if f != self.RootFile:
                try:
                    self.Editors[f].top.deiconify()
                except:
                    self.Editors[f] = EdWin(f, self)

        
            
        


##############################################

def SimpleMtxView(mtx): # API compatibility with wx gui

    print mtx




