#!/usr/bin/python

#TODO make sure the installer sets the execute bit on this !
import sys,  os

from platform import system
try:
    import Tkinter 
except ImportError:
    print >>sys.stderr,  "! No Tkinter !\n! Giving up !"
    sys.exit(1)

import idlelib
from idlelib  import  PyShell

import ScrumPy
import IdleGUI



def main():
    
    root= Tkinter.Tk(className="ScrumPydle")
    
    PyShell.use_subprocess = False
    PyShell.capture_warnings(True)
    
    for i in range(len(sys.path)):
        sys.path[i] = os.path.abspath(sys.path[i])

    else:
        Dir = os.getcwd()
        if not Dir in sys.path:
            sys.path.insert(0, Dir)


    # set application icon
    icondir = os.sep.join((idlelib.__path__[0],  "Icons"))
    if system() == 'Windows':
        iconfile = os.path.join(icondir, 'idle.ico')
        root.wm_iconbitmap(default=iconfile)
    elif Tkinter.TkVersion >= 8.5:
        ext = '.png' if Tkinter.TkVersion >= 8.6 else '.gif'
        iconfiles = [os.path.join(icondir, 'idle_%d%s' % (size, ext))
                     for size in (16, 32, 48)]
        icons = [Tkinter.PhotoImage(file=iconfile) for iconfile in iconfiles]
        root.tk.call('wm', 'iconphoto', str(root), "-default", *icons)

    PyShell.fixwordbreaks(root)
    root.withdraw()
    flist = PyShell.flist = PyShell.PyShellFileList(root)
   
    PyShell.macosxSupport.setupApp(root, flist)

    #if PyShell.macosxSupport.isAquaTk():
        # There are some screwed up <2> class bindings for text
        # widgets defined in Tk which we need to do away with.
        # See issue #24801.
     #   root.unbind_class('Text', '<B2>')
     #   root.unbind_class('Text', '<B2-Motion>')
     #   root.unbind_class('Text', '<<PasteSelection>>')

    shell = flist.open_shell()
   
    #if PyShell.macosxSupport.isAquaTk() and flist.dict:
        # On OSX: when the user has double-clicked on a file that causes
        # IDLE to be launched the shell window will open just in front of
        # the file she wants to see. Lower the interpreter window when
        # there are open files.
     #   shell.top.lower()
        
#
##
### Here we do the ScrumPy specific stuff
##
#
    
    IdleGUI.root = root
    ScrumPy.Init(IdleGUI)

    shell.shell_title = ScrumPy.BasicInfo.Title
    #shell.interp.locals["root"] = root
    shell.interp.locals["ScrumPy"] = ScrumPy
    shell.interp.execsource("print (ScrumPy.BasicInfo.Banner)")
    
  

    while flist.inversedict:  # keep IDLE running while files are open.
        root.mainloop()
    root.destroy()
    PyShell.capture_warnings(False)

if __name__ == "__main__":
    main()
    PyShell.capture_warnings(False)  # Make sure turned off; see issue 18081
    ScrumPy.Procs.KillChildren()
