"""


ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
import tkFileDialog

def LoadFile(Parent=None):
    return tkFileDialog.askopenfile(parent=Parent) # OK, maybe we can do something a bit more fancy later
    
def LoadFileName(Parent=None):
    return tkFileDialog.askopenfilename(parent=Parent)
    
def SaveFile(Parent=None):
    return tkFileDialog.asksaveasfile(parent=Parent) # see previous comment

def SaveFileName(Parent=None):
    return tkFileDialog.SaveAs(parent=Parent).show()


def GetFileName(Parent=None,  title="Select a file"):   # get a file name, regardless of whether or not it exists
    try:
        return tkFileDialog.asksaveasfilename(parent=Parent,  title=title,  confirmoverwrite=False)
    except:
        return tkFileDialog.askopenfilename(parent=Parent,  title=title)
        # older tk versions don't recognise "confirmoverwrite"
