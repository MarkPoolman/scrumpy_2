import string

import Util.Set
import types

from IOContainer import IODict
from Set import Set
import Mtx
import Round

dflt_dsep = '\t'
dflt_start = '# '

class Relation(IODict):
    """     Relation is a dictionary of sets."""


    def __init__(self, dsep = dflt_dsep, lsep = Set.dflt_lsep, lstart = '', filename = None, sorting = False, comment_symbol = '//'):
        """pre: dsep is a string, separating a key from a set
        in the string representation of the object.
        lsep is the separator for the sets.
        post: an empty Relationis created.
        """
        self.sorting = sorting
        self.dsep = dsep
        self.lsep = lsep
        self.lstart = lstart
        self.comment_symbol = comment_symbol
        self[None] = []
        if filename:
            try:
                self.Open(filename, 'load')
            except:
                self.Open(filename, 'txt')
        

    def __eq__(self, other):
        rv = False
        if not other:
            if len(self.TrueKeys()) == len(self.TrueItems()) == 0:
                rv = True
        else:
            rv = IODict.__eq__(self, other)
        return rv            


    #Editing

    def __setitem__(self, Key, List):
        """pre: Key is any object , List is a list.
        post: an entry key : Set(list) is made."""
        dict.__setitem__(self, Key, Set(List, self.lsep, sorting = self.sorting))


    
    def update(self, Other):
        """pre: other is a Relation.
        post: self is updated with the contents of other"""
        for key in Other.keys():
            self.AddList(key, Set(Other[key], sorting = self.sorting))


    def copy(self):
        rv = Relation(sorting = self.sorting, lsep = self.lsep, dsep = self.dsep, lstart = self.lstart)
        for k in self:
            rv.AddList(k, Set(self[k], sorting = self.sorting))
        return rv            


    def AddKey(self, Key):
        """post: self.keys() = self.keys() U key"""
        if not self.has_key(Key):
            self[Key] = []


    def AddKeys(self, Keys):
        for Key in Keys:
            self.AddKey(Key)


    def Add(self, Key, Item):
        """post: self[key] = self[key] U [item]"""        

        if not self.has_key(Key):
            self[Key] = Set(lsep = self.lsep, sorting = self.sorting)
        self[Key].append(Item)


    def AddList(self, Key, List):
        """pre: List is a list.
        post: self[Key] = self[Key] U List"""      

        if Key in self:
            self[Key] += List
        else:
            self[Key] = List


    def SymmAdd(self, a, b):
        """post: self[a] = self[a] U [b] and self[b] = self[b] U [a]"""
        self.Add(a, b)
        self.Add(b, a)            


    def SymmAddList(self, List):
        """pre: List is a list.
        post:   for i in List self[i] = self[i] U List"""
        for i in List:
            self.AddList(i, List)


    def DelItem(self, item):
        for k in self:
            self[k].remove(item)


    #IO

    def ReadLine(self, Line):
        """pre: Line is a string containig dsep.
        post: contents of Line red"""

        Line = Line[len(self.lstart) : ]
        [key, rest] = Line.split(self.dsep, 1)
        if key == 'None':
            key = None                 
        set = Set(lsep = self.lsep, sorting = self.sorting)
        set.ReadStr(rest)
        self.AddList(key, set)            

    def ReadStr(self, Str):
        """pre: Str is a string.
        post: contents of lines red"""
        lines = Str.split('\n')
        for line in lines:
            if len(line) > 0 and not line.isspace() and not line.startswith(self.comment_symbol) and self.lstart in line:
                self.ReadLine(line)

            
    def ShowLine(self, key, start = dflt_start, dsep = None):
        """pre: start is a string
        if self.has_key(key): show_line(key, start) = "start" + str(key) + dsep + str(self[key]) + "\\n"
        else: show_line(key, start) = "start" + str(key) + dsep + []\\n"""

        dsep = dsep or self.dsep
        res = start + str(key) + dsep

        if self.has_key(key):        
            res += str(self[key])
        else:
            res += "[]"
        return res + '\n'


    def ShowLines(self, List, start = dflt_start, dsep = None):

        res = ''
        for key in Set(List, sorting = self.sorting):
            res += self.ShowLine(key, start, dsep)
        return res

    def __str__(self):
        return self.ShowLines(self.TrueKeys(), start = self.lstart) + self.ShowLine(None, self.lstart)


    #Data access
   
    def TrueLength(self):
        return len(self) - 1

    
    def TrueKeys(self):
        """post:    R.TrueKeys() -> R.keys() \ None"""
        
        keys = self.keys()
        keys.remove(None)
        return keys


    def TrueItems(self):
        """post:    R.TrueItems() -> U(R[key], key in R.TrueKeys())"""
        rv = Set(lsep = self.lsep, sorting = self.sorting)
        for key in self.TrueKeys():
            rv += self[key]
        return rv


    def AllItems(self):
        """post:    R.AllItems() -> R[None] U R.TrueItems()"""
        return self[None] + self.TrueItems()


    def GetKeys(self, item):
        rv = []
        for key in self.TrueKeys():
            if item in self[key]:
                rv.append(key)
        return rv                
    

    def GetReverse(self, items = []):
        """pre: R fulfills the disjunction condition.
        post:   R.getReverse() -> R':
            R[b] is []: b in R'[None]
            else if a in R[b] ->
                if b is None: [] in R'[a]
                else b in R'[a]"""
        
        rv = Relation(self.dsep, self.lsep, sorting = self.sorting, lstart = self.lstart)
        items = items or self.AllItems()

        for item in items:
            rv.AddList(item, self.GetKeys(item))

        return rv


    def SelectList(self, List):
        rv = Relation(self.dsep, self.lsep, sorting = self.sorting, lstart = self.lstart)
        for k in self:
            if k in List:
                rv.AddList(k, self[k])
        return rv



    def SelectByItems(self, items):
        rv = self.copy()
        for k in self.keys():
            for item in self[k]:
                if not item in items:
                    rv[k].remove(item)
        return rv                    
                    
##        return self.GetReverse().SelectList(items).GetReverse()


    
    def GetSymmetric(self):
        rv = Relation(self.dsep, self.lsep, sorting = self.sorting, lstart = self.lstart)
        for k in self.TrueKeys():
            rv.SymmAddList(self[k] + [k])
        return rv            


    def ListGroups(self):
        rv = []
        for val in self.values():
            if not val in rv and len(val) > 0:
                rv.append(val)
        return rv  


    def ListSymmGroups(self):
        return  self.GetSymmetric().ListGroups()


    def SelectByLen(self, length):
        rv = Relation(self.dsep, self.lsep, sorting = self.sorting, lstart = self.lstart)
        for k in self:
            if len(self[k]) == length:
                rv.AddList(k, self[k])
        return rv


    def SelectByMinLen(self, length):
        rv = Relation(self.dsep, self.lsep, sorting = self.sorting, lstart = self.lstart)
        for k in self:
            if len(self[k]) >= length:
                rv.AddList(k, self[k])
        return rv
    
                       
    def SelectByMaxLen(self, length):
        rv = Relation(self.dsep, self.lsep, sorting = self.sorting, lstart = self.lstart)
        for k in self:
            if len(self[k]) <= length:
                rv.AddList(k, self[k])
        return rv


    def ExclusiveItems(self, key):
        rv = []
        if len(self[key]) > 0:
            rev = self.GetReverse(self[key])
            rv = rev.SelectByMaxLen(1).TrueKeys()
        return rv


    def ToMtx(self):
        rv = Utils.GetStoMat(cnames = self.TrueKeys())
        for k in self:
            Utils.Mtx.AddList(rv, k, self[k])
        return rv            

    def FromMtx(self, M):
        for cname in M.cnames:
            self.AddList(cname, M.InvolvedWith(cname))
        for rname in M.rnames:
            if Round.IsAllZero(M[rname]):
                self.Add(None, rname)



def BuildRelation(vlist, hlist, func, dsep=dflt_dsep, lsep=Set.dflt_lsep):
    """pre: vlist and hlist are lists, func is a function returning a boolean
    post:   BuildRelation(vlist, hlist, func) -> R: for v, h in vlist, hlist: h in R[v] if func(v, h), h in R[None] else"""

    rv = Relation(dsep, lsep)
    vset = Set(vlist)
    hset = Set(hlist)

    rv[None] = hset
    
    for v in vset:
        rv.AddKey(v)
        for h in hset:
            if func(v, h):
                rv[None].remove(h)
                rv.Add(v, h)
    
    return rv        
    

def Dict2Relation(dct):
    rv = Relation()
    for k in dct:
        rv.AddList(k, dct[k].keys())
    return rv


##    ##mtx methods
##
##    def InvolvedWith(self, name):
##        rv = []
##        if name in self:
##            rv = self[name]
##        else: 
##            rv = self.GetKeys(name)
##        return {}.fromkeys(rv, 1)
##
##    def NewCol(self, vals = None, name = None):
##        pass
##        
##    def NewRow(self, vals = None, name = None):
##        pass
##
##    def DelCol(self, name):
##        pass
##
##    def DelRow(self, name):
##        pass
