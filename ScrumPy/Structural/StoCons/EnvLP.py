import Utils.Mtx as Mtx
import Utils.Sto

import MtxLP
import ConvLP

parent = ConvLP.ConvLp
class EnvLp(parent):

    def __init__(self, N, target, scale, Dir = -1):
        self.Dir = Dir
        parent.__init__(self, N, scale)
        self.SetLenObjective(tmp = False)
        self.SetTarget(target, Dir = Dir)

    def SetTarget(self, target, Dir = -1):
        self.Target = target
        List = {1 : self.N.Externs, -1: self.Target}[Dir]
##        List = self.Target
        for x in List:#self.N.Externs:
            tx = self.GetOppDir(x, 0)
            coef = float(self.Target.get(x, 0)) / self.Scale
            self.AddTxConstraint(tx, coef, tmp = False)#, Dir = Dir)

    ##access        

    def GetProperDir(self, x, coef = 0):
        return parent.GetProperDir(self, x, self.Dir)

    def AllTransporters(self, externs = []):
        externs = externs or self.N.Externs
        return map(self.GetProperDir, externs)

    def GetComposition(self):
        rv = self.GetConversion()
        for name in rv.rnames:
            rv[name, 0] += self.Dir * self.Target.get(name, 0)   
            if rv[name, 0]: rv[name, 0] *= self.Dir
        Mtx.DelZeros(rv)            
        return rv

    ##edit
    def AddTxConstraint(self, tx, val, tmp = True):
        self.AddConstantConstraints({tx : val}, tmp = tmp)
        
    def ExcludeSolution(self, sol, tmp = True, kind = 'Continuous'):
        excl = []
        for rn in self.AllTransporters():
            if rn in sol:
                excl.append(rn)
        parent.ExcludeSolution(self, excl, tmp = tmp, kind = kind)

    def MakePositive(self, externs, blockopposite = False, tmp = True):
        txs = self.AllTransporters(externs)
        parent.MakePositive(self, txs, lowBound = 1.0 / self.Scale, tmp = tmp)
        if blockopposite:
            opps = map(self.GetOpposite, txs)
            self.BlockList(opps, tmp = tmp, withopposites = False)

##el compositions

    def ElVectorInvolving(self, ext):
        self.MakePositive([ext], tmp = True)
        self.Solve()
        self.CleanTemps(1)
        return self.GetComposition()


    def ElVectorsInvolving(self, ext):
        rv = Mtx.StoMat(rnames = self.N.Externs, Conv = float)
        go = True
        while go:
            c = self.ElVectorInvolving(ext)
            if not self.IsStatusOptimal():
                go = False                
            else:
                rv.AugCol(c)
                self.ExcludeLastSolution()
        Mtx.NumerateCols(rv)                    
        self.CleanTemps(len(rv.cnames))            
        return rv

    def ElVectors(self, involve = True):
        if involve:
            rv = Mtx.StoMat(rnames = self.N.Externs, Conv = float)
            for x in self.N.Externs:
                ss = self.ElVectorsInvolving(x)
                rv.AugCol(ss)
            Mtx.NumerateCols(rv)            
            Mtx.DelIsoCols(rv)
        else:
            rv = Mtx.StoMat(rnames = self.N.Externs, Conv = float)
            go = True
            while go:
                self.Solve()
                if not self.IsStatusOptimal():
                    go = False                
                else:
                    c = self.GetComposition()
                    rv.AugCol(c)
                    self.ExcludeLastSolution()
            self.CleanTemps(len(rv.cnames))                      
        Mtx.NumerateCols(rv)
        return rv       

