import string

import Util.Set

from IOContainer import IOList


class Set(IOList):
    """Set is a distinct (containing unique elements) list."""

    dflt_lsep = ', '

    def __init__(self, List = [], lsep = dflt_lsep, sorting = True):
        """pre: List is a list, lsep is a string.
        post: a set, containing unique elements from the List, is created.
        lsep separates items in string representation.
        """
        self.lsep = lsep
        self.sorting = sorting
        self.extend(List)
   
    #######################################################################################
    ##list functions  

    def extend(self, other):
        """pre: other is a list,
        post: self = self U other"""

        for item in other:
            if not item in self:
                list.append(self, item)
        if self.sorting:            
            self.sort()


    def append(self, item):
        """pre: none,
        post: self = self U [item]"""

        if not item in self:
            list.append(self, item)
        if self.sorting:            
            self.sort()


    def __add__(self, other):
        """pre other is a list,
        post: return self U other"""

        return Set(self[:] + other, self.lsep)


    def __iadd__(self, other):
        """pre: other is a list,
        post: self = self U other"""

        self.extend(other)
        return self


    def remove(self, item):
        """post:    self = self \ item"""
        if item in self:
            list.remove(self, item)

    #######################################################################################
    ##IO

    def item2str(self, item):
        rv = item
        if type(rv) == str:
            try:
                s = float(item)
                rv = "'" + rv + "'"
            except:
                pass
        else:
            rv = str(rv)
        return rv            

    def __str__(self):
        return string.join(map(lambda x : self.item2str(x), self), self.lsep)


    def str2item(self, s):
        rv = s.strip()
        if not rv[0] in ["'", '"'] and not rv[-1] in ["'", '"']:
            try:
                rv = int(rv)
            except:
                try:
                    rv = float(rv)
                except:
                    pass
        return rv                

        
    def ReadStr(self, Line):
        """pre: line is a string.
        post: self = self U line.split(lsep), self is returned"""
        if len(Line) > 0 and not Line.isspace():
            List = Line.split(self.lsep)
            List = map(lambda x: self.str2item(x), List)
            self.extend(List)


    #######################################################################################
    ##Analysis

    def Encloses(self, other):
        """pre: other is a list,
        post: True if e in other => e in self, False else"""

        return Util.Set.IsSubset(other, self) == 1


def Jaccard(a, b):
    """pre: a, b are lists.
    post:   Jaccard(a, b) -> Jaccard's similarity index between sets based on a and b."""

    return float(len(Util.Set.Intersect(a, b))) / len(Util.Set.Union(a, b))


def SetEditDistance(A, B):
    """pre: A, B are lists.
    post:   SetEditDistance -> editing distance between sets based on A and B"""
    
    return max(len(Util.Set.Complement(A, B)), len(Util.Set.Complement(B, A)))


def SetEditDistNorm(A, B):
    """pre: A, B are lists.
    post:   SetEditDistance -> editing distance between sets based on A and B"""
    
    return float(SetEditDistance(A, B)) / max(len(A), len(B))


def Distance(A, B):
    rv = 0
    if len(A) != 0 or len(B) != 0:
        rv = Util.Set.Distance(A, B)
    return rv        


def MultiSect(sets):
    rv = Set(sets[0])
    for set in sets[1:]:
        rv = Util.Set.Intersect(rv, set)
    return list(rv)


def Parse(s):
    rv = Set()
    rv.ReadStr(s)
    return rv



def UpdateList(List, Update, sign = True):
    fun = {False : Util.Set.Complement, True : Util.Set.Union}[sign]
    Update = fun(List, Update)       
    del List[:]
    List.extend(Update)





def Union(a, b):
    rv = set(a)
    rv.update(b)
    return list(rv)
    

def Intersect(a, b):
    """ Intersection of a pair of sets """
    rv = set(a).intersection(b)
    return list(rv)
    
def Intersects(setl):
    """ pre: setl is a list of sets, len(setl) > 1
       post: the intersection of all sets in setl """

    rv = setl[0]
    for s in setl[1:]:
        rv = Intersect(rv,s)
    return rv

def Difference(a,b):
    rv = set(a).symmetric_difference(b)
    return list(rv)

def Distance(a,b):
    union = Union(a,b)
    intersect = Intersect(a,b)
    diff =  Complement(union, intersect)
    return float(len(diff))/len(union)


    # a\b
def Complement(a, b):
    rv = set(a).difference(b)
    return list(rv)



def DoesIntersect(a,b):
    for el in a:
        if el in b:
            return True
    return False

   # a is subset of b 
def IsSubset(a, b):
    for el in a:
        if not el in b:
            return False
    return True
    
    
def IsEqual(a, b):    
    if IsSubset(a, b) and len(a) == len(b):
        return True
    else:
        return False



def Merge(SetList, __first=True):
    """ Pre: SetList is a list of sets, __first is private
       Post: Merge(SetList) == list of of sets, such that intersection in SetList are mereged,
               e.g. Merge([[1,2],[2,3],[4,5]]) => [[1,2,3],[4,5]]
    """

    if __first:
        SetList = SetList[:]
        __first = False
        
    Seen = 0   # not a set, so won't be in SetList from User
    lensets = len(SetList)
    rv = []
    for idx1 in range(lensets):
        if SetList[idx1] != Seen:
            newset = SetList[idx1]
            SetList[idx1] = Seen
            for idx2 in range(idx1+1, lensets):
                s2 = SetList[idx2]
                if s2 != Seen and DoesIntersect(newset, s2):
                    newset = Union(newset,s2)
                    SetList[idx2] = Seen
            rv.append(newset)
    if len(rv) < lensets:
            return Merge(rv, __first)
    else:
            return rv

def ElimSubs(SetList ):
    """ Pre: SetList is a list of sets
       Post: ElimSubs(sl) == a subset of the sets in SetList such that no set is a subset of any other
    """


    rv = []
    ssidxs = {}
    LenSetList = len(SetList)
    for i1 in range(LenSetList):
        for i2 in range(i1+1, LenSetList):
            s1 = SetList[i1]
            s2 = SetList[i2]
            if IsSubset(s1,s2):
                ssidxs[i1] = 1
            elif IsSubset(s2, s1):
                ssidxs[i2] = 1
                
    ssidxs = ssidxs.keys()
    for i1 in range(LenSetList):
        if not i1 in ssidxs:
            rv.append(SetList[i1])
    return rv

