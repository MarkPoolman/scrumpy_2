
/*
Automatically generated c code from ScrumPy
Not recomended for human consumption !
*/
#include<math.h>
#include<stdlib.h>
#include <ScampiModelBase.h>

static double calvin1Time ; 
double MetVals[18] ;
double DiffVals[18] ;
double RateVals[21] ;
double ParamVals[65] ;

enum { PGA_ch, ATP_ch, BPGA_ch, GAP_ch, Pi_ch, DHAP_ch, FBP_ch, F6P_ch, G6P_ch, G1P_ch, E4P_ch, X5P_ch, SBP_ch, S7P_ch, R5P_ch, Ru5P_ch, RuBP_ch, ADP_ch } ;
enum { Ru5Pk, PGM, TPT_PGA, X5Piso, Rubisco, StSynth, G3Pdh, TKL2, PGK, Ald1, PGI, TPT_DHAP, FBPase, TKL1, TPI, R5Piso, LReac, Ald2, SBPase, TPT_GAP, StPase } ;
enum { x_CO2, Rbco_vm, Rbco_km, Rbco_KiPGA, Rbco_KiFBP, Rbco_KiSBP, Rbco_KiPi, x_NADPH_ch, Rbco_KiNADPH, EQMult, q2, x_Proton_ch, x_NADP_ch, q3, q4, q5, FBPase_ch_vm, FBPase_ch_km, FBPase_ch_KiF6P, FBPase_ch_KiPi, q14, q15, x_Starch_ch, Vstsyn_ch, stsyn_ch_km1, stsyn_ch_Ki, stsyn_ch_km2, stsyn_ch_ka1, stsyn_ch_ka2, stsyn_ch_ka3, StPase_Vm, StPase_km, StPase_kiG1P, q7, q8, SBPase_ch_vm, SBPase_ch_km, SBPase_ch_KiPi, q10, q11, q12, Ru5Pk_ch_vm, Ru5Pk_ch_km1, Ru5Pk_ch_KiPGA, Ru5Pk_ch_KiRuBP, Ru5Pk_ch_KiPi, Ru5Pk_ch_KiADP1, Ru5Pk_ch_km2, Ru5Pk_ch_KiADP2, x_Pi_cyt, x_PGA_cyt, PGA_xpMult, TP_Piap_vm, TP_Piap_kPGA_ch, TP_Piap_kPi_cyt, TP_Piap_kPi_ch, TP_Piap_kDHAP_ch, TP_Piap_kGAP_ch, x_GAP_cyt, x_DHAP_cyt, LR_vm, LR_kmADP, LR_kmPi, CSUM_RuBP_ch, CSUM_ADP_ch } ;
const char *MetNames[]={"PGA_ch","ATP_ch","BPGA_ch","GAP_ch","Pi_ch","DHAP_ch","FBP_ch","F6P_ch","G6P_ch","G1P_ch","E4P_ch","X5P_ch","SBP_ch","S7P_ch","R5P_ch","Ru5P_ch","RuBP_ch","ADP_ch"};

const char *RateNames[]={"Ru5Pk","PGM","TPT_PGA","X5Piso","Rubisco","StSynth","G3Pdh","TKL2","PGK","Ald1","PGI","TPT_DHAP","FBPase","TKL1","TPI","R5Piso","LReac","Ald2","SBPase","TPT_GAP","StPase"};

const char *ParamNames[]={"x_CO2","Rbco_vm","Rbco_km","Rbco_KiPGA","Rbco_KiFBP","Rbco_KiSBP","Rbco_KiPi","x_NADPH_ch","Rbco_KiNADPH","EQMult","q2","x_Proton_ch","x_NADP_ch","q3","q4","q5","FBPase_ch_vm","FBPase_ch_km","FBPase_ch_KiF6P","FBPase_ch_KiPi","q14","q15","x_Starch_ch","Vstsyn_ch","stsyn_ch_km1","stsyn_ch_Ki","stsyn_ch_km2","stsyn_ch_ka1","stsyn_ch_ka2","stsyn_ch_ka3","StPase_Vm","StPase_km","StPase_kiG1P","q7","q8","SBPase_ch_vm","SBPase_ch_km","SBPase_ch_KiPi","q10","q11","q12","Ru5Pk_ch_vm","Ru5Pk_ch_km1","Ru5Pk_ch_KiPGA","Ru5Pk_ch_KiRuBP","Ru5Pk_ch_KiPi","Ru5Pk_ch_KiADP1","Ru5Pk_ch_km2","Ru5Pk_ch_KiADP2","x_Pi_cyt","x_PGA_cyt","PGA_xpMult","TP_Piap_vm","TP_Piap_kPGA_ch","TP_Piap_kPi_cyt","TP_Piap_kPi_ch","TP_Piap_kDHAP_ch","TP_Piap_kGAP_ch","x_GAP_cyt","x_DHAP_cyt","LR_vm","LR_kmADP","LR_kmPi","CSUM_RuBP_ch","CSUM_ADP_ch"};

double Func_PGA_ch(void){ return +-1.0*RateVals[TPT_PGA]+2.0*RateVals[Rubisco]+-1.0*RateVals[PGK];}
double Func_ATP_ch(void){ return +-1.0*RateVals[Ru5Pk]+-1.0*RateVals[StSynth]+-1.0*RateVals[PGK]+1.0*RateVals[LReac];}
double Func_BPGA_ch(void){ return +-1.0*RateVals[G3Pdh]+1.0*RateVals[PGK];}
double Func_GAP_ch(void){ return +1.0*RateVals[G3Pdh]+-1.0*RateVals[TKL2]+-1.0*RateVals[Ald1]+-1.0*RateVals[TKL1]+-1.0*RateVals[TPI]+-1.0*RateVals[TPT_GAP];}
double Func_Pi_ch(void){ return +1.0*RateVals[TPT_PGA]+2.0*RateVals[StSynth]+1.0*RateVals[G3Pdh]+1.0*RateVals[TPT_DHAP]+1.0*RateVals[FBPase]+-1.0*RateVals[LReac]+1.0*RateVals[SBPase]+1.0*RateVals[TPT_GAP]+-1.0*RateVals[StPase];}
double Func_DHAP_ch(void){ return +-1.0*RateVals[Ald1]+-1.0*RateVals[TPT_DHAP]+1.0*RateVals[TPI]+-1.0*RateVals[Ald2];}
double Func_FBP_ch(void){ return +1.0*RateVals[Ald1]+-1.0*RateVals[FBPase];}
double Func_F6P_ch(void){ return +-1.0*RateVals[PGI]+1.0*RateVals[FBPase]+-1.0*RateVals[TKL1];}
double Func_G6P_ch(void){ return +-1.0*RateVals[PGM]+1.0*RateVals[PGI];}
double Func_G1P_ch(void){ return +1.0*RateVals[PGM]+-1.0*RateVals[StSynth]+1.0*RateVals[StPase];}
double Func_E4P_ch(void){ return +1.0*RateVals[TKL1]+-1.0*RateVals[Ald2];}
double Func_X5P_ch(void){ return +-1.0*RateVals[X5Piso]+1.0*RateVals[TKL2]+1.0*RateVals[TKL1];}
double Func_SBP_ch(void){ return +1.0*RateVals[Ald2]+-1.0*RateVals[SBPase];}
double Func_S7P_ch(void){ return +-1.0*RateVals[TKL2]+1.0*RateVals[SBPase];}
double Func_R5P_ch(void){ return +1.0*RateVals[TKL2]+-1.0*RateVals[R5Piso];}
double Func_Ru5P_ch(void){ return +-1.0*RateVals[Ru5Pk]+1.0*RateVals[X5Piso]+1.0*RateVals[R5Piso];}
double Func_RuBP_ch(void){ return +1.0*RateVals[Ru5Pk]+-1.0*RateVals[Rubisco];}
double Func_ADP_ch(void){ return +1.0*RateVals[Ru5Pk]+1.0*RateVals[StSynth]+1.0*RateVals[PGK]+-1.0*RateVals[LReac];}

static double  (*  DiffFunArray[])() ={Func_PGA_ch,Func_ATP_ch,Func_BPGA_ch,Func_GAP_ch,Func_Pi_ch,Func_DHAP_ch,Func_FBP_ch,Func_F6P_ch,Func_G6P_ch,Func_G1P_ch,Func_E4P_ch,Func_X5P_ch,Func_SBP_ch,Func_S7P_ch,Func_R5P_ch,Func_Ru5P_ch,Func_RuBP_ch,Func_ADP_ch } ; 
double Func_Ru5Pk(void){ return ParamVals[Ru5Pk_ch_vm]*MetVals[Ru5P_ch]*MetVals[ATP_ch]/((MetVals[Ru5P_ch]+ParamVals[Ru5Pk_ch_km1]*(1+MetVals[PGA_ch]/ParamVals[Ru5Pk_ch_KiPGA]+MetVals[RuBP_ch]/ParamVals[Ru5Pk_ch_KiRuBP]+MetVals[Pi_ch]/ParamVals[Ru5Pk_ch_KiPi]))*(MetVals[ATP_ch]*(1+MetVals[ADP_ch]/ParamVals[Ru5Pk_ch_KiADP1])+ParamVals[Ru5Pk_ch_km2]*(1+MetVals[ADP_ch]/ParamVals[Ru5Pk_ch_KiADP2]))) ; } ;
double Func_PGM(void){ return ParamVals[EQMult]*(MetVals[G6P_ch]-MetVals[G1P_ch]/ParamVals[q15]) ; } ;
double Func_TPT_PGA(void){ return ParamVals[PGA_xpMult]*ParamVals[TP_Piap_vm]*MetVals[PGA_ch]/(ParamVals[TP_Piap_kPGA_ch]*(1+(1+ParamVals[TP_Piap_kPi_cyt]/ParamVals[x_Pi_cyt])*(MetVals[Pi_ch]/ParamVals[TP_Piap_kPi_ch]+MetVals[PGA_ch]/ParamVals[TP_Piap_kPGA_ch]+MetVals[DHAP_ch]/ParamVals[TP_Piap_kDHAP_ch]+MetVals[GAP_ch]/ParamVals[TP_Piap_kGAP_ch]))) ; } ;
double Func_X5Piso(void){ return ParamVals[EQMult]*(MetVals[X5P_ch]-MetVals[Ru5P_ch]/ParamVals[q12]) ; } ;
double Func_Rubisco(void){ return ParamVals[Rbco_vm]*MetVals[RuBP_ch]/(MetVals[RuBP_ch]+ParamVals[Rbco_km]*(1+MetVals[PGA_ch]/ParamVals[Rbco_KiPGA]+MetVals[FBP_ch]/ParamVals[Rbco_KiFBP]+MetVals[SBP_ch]/ParamVals[Rbco_KiSBP]+MetVals[Pi_ch]/ParamVals[Rbco_KiPi]+ParamVals[x_NADPH_ch]/ParamVals[Rbco_KiNADPH])) ; } ;
double Func_StSynth(void){ return ParamVals[Vstsyn_ch]*MetVals[G1P_ch]*MetVals[ATP_ch]/((MetVals[G1P_ch]+ParamVals[stsyn_ch_km1])*(1+MetVals[ADP_ch]/ParamVals[stsyn_ch_Ki])*(MetVals[ATP_ch]+ParamVals[stsyn_ch_km2])+ParamVals[stsyn_ch_km2]*MetVals[Pi_ch]/(ParamVals[stsyn_ch_ka1]*MetVals[PGA_ch])+(ParamVals[stsyn_ch_ka2]*MetVals[F6P_ch])+(ParamVals[stsyn_ch_ka3]*MetVals[FBP_ch])) ; } ;
double Func_G3Pdh(void){ return ParamVals[EQMult]*(MetVals[BPGA_ch]*ParamVals[x_NADPH_ch]*ParamVals[x_Proton_ch]-ParamVals[x_NADP_ch]*MetVals[GAP_ch]*MetVals[Pi_ch]/ParamVals[q3]) ; } ;
double Func_TKL2(void){ return ParamVals[EQMult]*(MetVals[GAP_ch]*MetVals[S7P_ch]-MetVals[X5P_ch]*MetVals[R5P_ch]/ParamVals[q10]) ; } ;
double Func_PGK(void){ return ParamVals[EQMult]*(MetVals[PGA_ch]*MetVals[ATP_ch]-(MetVals[BPGA_ch]*MetVals[ADP_ch]/ParamVals[q2])) ; } ;
double Func_Ald1(void){ return ParamVals[EQMult]*(MetVals[DHAP_ch]*MetVals[GAP_ch]-MetVals[FBP_ch]/ParamVals[q5]) ; } ;
double Func_PGI(void){ return ParamVals[EQMult]*(MetVals[F6P_ch]-MetVals[G6P_ch]/ParamVals[q14]) ; } ;
double Func_TPT_DHAP(void){ return ParamVals[TP_Piap_vm]*MetVals[DHAP_ch]/(ParamVals[TP_Piap_kDHAP_ch]*(1+(1+ParamVals[TP_Piap_kPi_cyt]/ParamVals[x_Pi_cyt])*(MetVals[Pi_ch]/ParamVals[TP_Piap_kPi_ch]+MetVals[PGA_ch]/ParamVals[TP_Piap_kPGA_ch]+MetVals[DHAP_ch]/ParamVals[TP_Piap_kDHAP_ch]+MetVals[GAP_ch]/ParamVals[TP_Piap_kGAP_ch]))) ; } ;
double Func_FBPase(void){ return ParamVals[FBPase_ch_vm]*MetVals[FBP_ch]/(MetVals[FBP_ch]+ParamVals[FBPase_ch_km]*(1+MetVals[F6P_ch]/ParamVals[FBPase_ch_KiF6P]+MetVals[Pi_ch]/ParamVals[FBPase_ch_KiPi])) ; } ;
double Func_TKL1(void){ return ParamVals[EQMult]*(MetVals[F6P_ch]*MetVals[GAP_ch]-MetVals[E4P_ch]*MetVals[X5P_ch]/ParamVals[q7]) ; } ;
double Func_TPI(void){ return ParamVals[EQMult]*(MetVals[GAP_ch]-MetVals[DHAP_ch]/ParamVals[q4]) ; } ;
double Func_R5Piso(void){ return ParamVals[EQMult]*(MetVals[R5P_ch]-MetVals[Ru5P_ch]/ParamVals[q11]) ; } ;
double Func_LReac(void){ return ParamVals[LR_vm]*MetVals[ADP_ch]*MetVals[Pi_ch]/((MetVals[ADP_ch]+ParamVals[LR_kmADP])*(MetVals[Pi_ch]+ParamVals[LR_kmPi])) ; } ;
double Func_Ald2(void){ return ParamVals[EQMult]*(MetVals[E4P_ch]*MetVals[DHAP_ch]-MetVals[SBP_ch]/ParamVals[q8]) ; } ;
double Func_SBPase(void){ return ParamVals[SBPase_ch_vm]*MetVals[SBP_ch]/(MetVals[SBP_ch]+ParamVals[SBPase_ch_km]*(1+MetVals[Pi_ch]/ParamVals[SBPase_ch_KiPi])) ; } ;
double Func_TPT_GAP(void){ return ParamVals[TP_Piap_vm]*MetVals[GAP_ch]/(ParamVals[TP_Piap_kGAP_ch]*(1+(1+ParamVals[TP_Piap_kPi_cyt]/ParamVals[x_Pi_cyt])*(MetVals[Pi_ch]/ParamVals[TP_Piap_kPi_ch]+MetVals[PGA_ch]/ParamVals[TP_Piap_kPGA_ch]+MetVals[DHAP_ch]/ParamVals[TP_Piap_kDHAP_ch]+MetVals[GAP_ch]/ParamVals[TP_Piap_kGAP_ch]))) ; } ;
double Func_StPase(void){ return ParamVals[StPase_Vm]*MetVals[Pi_ch]/(MetVals[Pi_ch]+ParamVals[StPase_km]*(1+MetVals[G1P_ch]/ParamVals[StPase_kiG1P])) ; } ;

static double  (*  RateFunArray[])() ={Func_Ru5Pk,Func_PGM,Func_TPT_PGA,Func_X5Piso,Func_Rubisco,Func_StSynth,Func_G3Pdh,Func_TKL2,Func_PGK,Func_Ald1,Func_PGI,Func_TPT_DHAP,Func_FBPase,Func_TKL1,Func_TPI,Func_R5Piso,Func_LReac,Func_Ald2,Func_SBPase,Func_TPT_GAP,Func_StPase } ; 
double Func_CONS_RuBP_ch(void){ return (ParamVals[CSUM_RuBP_ch]-(+(0.5 * MetVals[PGA_ch])+(0.5 * MetVals[ATP_ch])+MetVals[BPGA_ch]+(0.5 * MetVals[GAP_ch])+(0.5 * MetVals[Pi_ch])+(0.5 * MetVals[DHAP_ch])+MetVals[FBP_ch]+(0.5 * MetVals[F6P_ch])+(0.5 * MetVals[G6P_ch])+(0.5 * MetVals[G1P_ch])+(0.5 * MetVals[E4P_ch])+(0.5 * MetVals[X5P_ch])+MetVals[SBP_ch]+(0.5 * MetVals[S7P_ch])+(0.5 * MetVals[R5P_ch])+(0.5 * MetVals[Ru5P_ch]) ) ); } 

double Func_CONS_ADP_ch(void){ return (ParamVals[CSUM_ADP_ch]-(+MetVals[ATP_ch] ) ); } 


static double  (*ConsFunArray[])() ={Func_CONS_RuBP_ch,Func_CONS_ADP_ch } ; 
ScampiModel Actual_calvin1={
"calvin1",
21,
RateNames,
RateFunArray,
RateVals,
18,
2,
16,
MetNames,
DiffFunArray,
ConsFunArray,
DiffVals,
MetVals,
65,
ParamNames,
ParamVals,
&calvin1Time,
0,
0
 } ;
ScampiModel_ptr calvin1 =&Actual_calvin1 ;
