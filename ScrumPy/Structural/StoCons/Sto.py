from Util import Seq
import Util.Set

from Set import Set
import Round
import Mtx

IgnoredAtoms = ['H']
IgnoredCpd = ['electron']


def InAndOut(stodct):
    sub, pro = Set(), Set()
    for key in stodct:
        if stodct[key] < 0:
            sub.append(key)
        elif stodct[key] > 0:
            pro.append(key)
        else:
            sub.append(key)
            pro.append(key)
    return sub, pro


def BothSides(stodct):
    rv = False
    sub, pro = InAndOut(stodct)
    for met in sub:
        if met in pro:
            rv = True
    return rv


def IsValid(stodct):
    rv = True
    sub, pro = InAndOut(stodct)
##    if sub == pro or len(sub) == 0 or len(pro) == 0:
    if (len(sub) and not len(pro)) or (len(pro) and not len(sub)):
        rv = False
    return rv        




def AbsCoefs(stodct):
    rv = stodct.copy()
    for key in rv:
        rv[key] = abs(rv[key])
    return rv            


def IsMany2Many(stodct):
    sub, pro = InAndOut(stodct)
    return len(sub) > 1 and len(pro) > 1


def IsOne2One(stodct):
    sub, pro = InAndOut(stodct)
    return len(sub) == 1 and len(pro) == 1


def IsOne2Many(stodct):
    sub, pro = InAndOut(stodct)
    return len(sub) > 1 and len(pro) == 1 or len(sub) == 1 and len(pro) > 1

def Invert(sto):
    for k in sto:
        sto[k] *= -1

def Inverted(sto):
    sto = sto.copy()
    Invert(sto)
    return sto

def AreScalMul(sto1, sto2):
    rv = set(sto1) == set(sto2)
    if rv and len(sto1):
        keys = sto1.keys()
        k = keys[0]
        scal = sto1[k] / sto2[k]
        i = 1
        while rv and i < len(keys):
            k = keys[i]
            scal2 = sto1[k] / sto2[k]
            dif = abs(scal2 - scal)
            if not Round.IsZero(dif):
                rv = False
            i += 1                
    return rv

def AreEqual(sto1, sto2):
    rv = True
    keys = list(set(sto1.keys()).union(sto2.keys()))
    i = 1
    while rv and i < len(keys):
        k = keys[i]
        rv = Round.AreEqual(sto1.get(k, 0), sto2.get(k, 0))
        i += 1                
    return rv    

##################################################################
#normalise

def Normalise(dct, fun = min):
    if len(dct) > 1:
        keys = dct.keys()
        vals = map(lambda k: dct[k], keys)
        Seq.Normalise(vals, nidx = vals.index(fun(vals)))
        for i in range(len(keys)):
            val = float(vals[i])
            val = float(str(val))
            dct[keys[i]] = val

def DivideAndDelete(sto, key):
        if key in sto:
            val = sto[key]
            del sto[key]
            if val != 0:
                for k in sto:
                    sto[k] /= val
                            
                        
def DelZeroes(sto):
    for k in sto.keys():
            if sto[k] == 0:
                    del sto[k]    


################################################
##atomic balance
                    
def GetBalance(stodct, fun, exclude = []):
    rv = {}
    for met in stodct:
        if not met in exclude:
            fd = fun(met)
            if len(fd) == 0:
                raise NameError(met + ': empty formula! ')
            for atom in fd:
                if not atom in rv:
                    rv[atom] = 0.0
                rv[atom] += float(fd[atom] * stodct[met])
    for k in rv:
        if Mtx.IsZero(rv[k]):
            rv[k] = 0.0
    return rv


def IsBalanced(sto, fun, ignore = [], exclude = []):
    """post: 1 for 'yes', '0' for 'no', -1 for 'don't know'"""
    rv = 1
    try:
        bal = GetBalance(sto, fun = fun, exclude = exclude)
        for met in bal.keys():
            if met in ignore:
                del bal[met]
        rv = Seq.AllEq(bal.values(), 0)
    except:
        rv = -1
    return rv     
