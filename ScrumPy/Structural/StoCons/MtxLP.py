import exceptions

import MIP
lp = MIP.Mip
import types
import Util.Set

import Sto
import Round
import Mtx
EPSILON = pow(Round.ZEROVAL, 0.5) 

class MtxLp(lp):
    
    def __init__(self, mtx, direc = 'Min', klass = 'LP', lowBound = 0, upBound = None, name = 'Unnamed mtx problem'):

        lp.__init__(self, name = name, klass = klass)
        self.AddRows(mtx.rnames[:] or ['empty row'])
        self.AddCols(mtx.cnames[:] or ['empty col'])
        self.Cnames = mtx.cnames
        
        for rname in mtx.rnames:
            row = map(float, mtx[rname])
            if klass == 'MIP':
                row += [0.0] * len(mtx.cnames)
            self.SetRowVals(rname, row)
            self.SetRowBounds(rname, 0, 0)

        for cname in mtx.cnames:
            self.SetColBounds(cname, lowBound, upBound)

        self.SetObjDirec(direc)

        self.tmp = []


##data access

    def GetColNames(self, kind = 'both'):
        rv = []
        if kind == 'both':
            rv = self.cnames.values()
        elif kind in ['Continuous', 'Integer']:
            for cname in self.cidxs:
                if self.GetColKind(cname) == kind:
                    rv.append(cname)
        return rv
            

    def GetRowNames(self):
        return self.rnames.values()


    def GetObjective(self, withzeroes = False, kind = 'both'):
        rv = {}
        for cname in self.GetColNames(kind):
            val = self.GetObjCoef(cname)
            if withzeroes or val != 0:
                rv[cname] = val
        return rv            


    def GetRowSto(self, name, withzeroes = False):
        rv = {}
        row = self.GetRow(name)
        for cname in self.GetColNames():
            n = self.GetColIdxs([cname])[0]
            if withzeroes or n > 0:
                rv[cname] = row[n -1]
        return rv


    def GetSolution(self, kind = 'Continuous', withzeroes = False, asmatrix = False, normalise = True):
        rv = {}
        cnames = self.GetColNames(kind = kind)
        if self.IsStatusOptimal():
            for cname in cnames:
                val = self.GetColVal(cname, kind = kind)
                if val and not Round.IsZero(val):
                    rv[cname] = val
                elif withzeroes:
                    rv[cname] = 0.0
        if normalise:
            Sto.Normalise(rv)
        if asmatrix:
            rv = self.SolutionAsMtx(rv, cnames = self.Cnames)
        return rv


    def SolutionAsMtx(self, solution = {}, cnames = []):
        return Mtx.Dict2Mtx(dct = solution, names = cnames or self.GetColNames(), tx = True, Conv = float)



##temporary variables and constraints

    def MakeTemp(self, name, doit = True):
        if doit:
            self.tmp.append(name)


    def CleanTemps(self, nlast = 0):
        tmp = self.tmp[-nlast :]
        tmprnames = Util.Set.Intersect(tmp, self.GetRowNames())
        tmpcnames = Util.Set.Intersect(tmp, self.GetColNames('both'))
        self.DelRows(tmprnames)
        self.DelCols(tmpcnames)
        if None in tmp:
            self.ResetObjective()
        self.tmp = self.tmp[:-nlast]            
       

##deleting

    def DelCols(self, cols):
        lp.DelCols(self, cols)
        self.Cnames = Util.Set.Complement(self.Cnames, cols)
        

    def DelReacs(self, rnlist):
        pass


    def DelMets(self, metlist):
        pass


##variables
        
    def AddCol(self, cname, vals = [], lowBound = 0,  upBound = None, kind = 'Continuous', tmp = False):
        self.AddCols([cname])
        self.SetColBounds(cname, lowBound, upBound)
        self.SetColKind(cname, kind)
        if len(vals) > 0:
            self.SetColVals(cname, vals)
        self.MakeTemp(cname, tmp)

    def AddStoich(self, sto,  cname, lowBound = 0,  upBound = None, kind = 'Continuous', tmp = False):
        vals = [0] * self.GetNumRows()
        for met in sto:
            idx = self.GetRowIdxs([met])[0]
            vals[idx - 1] = sto[met]
        self.AddCol(cname, vals, lowBound = lowBound, upBound = upBound, kind = kind, tmp = tmp)
        

##objective
    def SetObjDirec(self, direc):
        lp.SetObjDirec(self, direc)
            

    def SetObjective(self, obj = None, tmp = False):
        self.ResetObjective()
        objtype = type(obj)
        if objtype == types.DictType:
            self.SetObjCoefsFromDic(obj)
        elif objtype == types.ListType:
            self.SetObjCoefsFromLists(obj, [1] * len(obj))
        self.MakeTemp(None, tmp)
                    
 
    def ResetObjective(self):
        obj = self.GetObjective().keys()
        self.SetObjCoefsFromLists(obj, [0] * len(obj))


    def SetSumObjective(self, coef = 1, cnames = [], tmp = False):
        cnames = cnames or self.GetColNames()
        self.SetObjCoefsFromLists(cnames, [coef]*len(cnames))
        self.MakeTemp(None, tmp)


    def SetLenObjective(self, varlist = [], tmp = False):
        """pre: self.GetClass() == MIP.
        post: the length (i. e. number of nonzero components) of a solution is optimised"""
        
        obj = {}
        conts = varlist or self.GetColNames('Continuous')
        contcoef = {'Max' : 1, 'Min' : -1}[self.GetObjDirec()]  ##max: int <= cont; min : int >= cont
        intcoef = - contcoef
        for contname in conts:                                                             
            intname = self.IntTag + contname
##            self.AddCol(intname, lowBound = 0, upBound = 1, kind = 'Integer', tmp = tmp)
            constraint = {contname : contcoef, intname : intcoef}
            self.AddConstraint(constraint, 0, None, tmp = tmp)                 ##max: cont - int >= 0; min : - cont + int >= 0
            obj[intname] = 1.0
        self.SetObjective(obj, tmp = tmp)

##constraints
   
    def AddConstraint(self, sto = None, lowBound = 0, upBound = 0, name = None, tmp = False):
        name = name or 'Row ' + str(self.GetNumRows() + 1)
        self.AddRows([name])
        self.SetRowBounds(name, lowBound, upBound)
        if type(sto) == types.DictType:
            self.SetRowFromDic(name, sto)
        elif type(sto) == types.ListType:
            self.SetRowVals(name, sto)
        self.MakeTemp(name, tmp)            
            


    def SetRowFromDic(self, name, dic):
        row = [0] * self.GetNumCols()
        for var in dic:
            nvar = self.GetColIdxs([var])[0]
            row[nvar - 1] = dic[var]
        self.SetRowVals(name, row)            


    def MakePositive(self, pos = [], tmp = False, lowBound = EPSILON):
        for targ in pos:
            self.AddConstraint({targ : 1}, lowBound, None, tmp = tmp)

    def MakeNegative(self, pos = [], tmp = False, lowBound = EPSILON):
        for targ in pos:
            self.AddConstraint({targ : -1}, lowBound, None, tmp = tmp)
            
    def AddConstantConstraints(self, constraints = {}, tmp = False):
        for targ in constraints:
            constant = constraints[targ]
            self.AddConstraint({targ : 1}, constant, constant, tmp = tmp)


    def AddSumConstraint(self, lowBound = 1, upBound = 1.0, cnames = [], tmp = False):
        cnames = cnames or self.GetColNames()
        sto = dict.fromkeys(cnames, 1)
        self.AddConstraint(sto, lowBound, upBound, tmp = tmp)


##    def Block(self, var, tmp = False):
##        self.AddConstraint({var : 1}, 0, 0, tmp = tmp)

    def Block(self, var, tmp = False, on = True):
        if on:
            self.AddConstraint({var : 1}, 0, 0, tmp = tmp, name = var + '_block')
        else:
            self.DelRows([var + '_block'])
        


    def BlockList(self, vars, tmp = False):
        self.AddConstantConstraints(dict.fromkeys(vars, 0), tmp = tmp)



    def ExtractRows(self, rows):
        rv = {}
        for row in rows:
            rv[row] = self.GetRowSto(row)
        self.DelRows(rows)
        return rv


    def RestoreRows(self, stodct):
        for row in stodct:
            self.AddConstraint(sto = stodct[row], lowBound = 0, upBound = 0, name = row)

##testing a solution

    def TestSolution(self, sol):
        self.AddConstantConstraints(sol, tmp = True)
        self.Solve()
        rv = self.IsStatusOptimal()
        self.CleanTemps(nlast = len(sol))
        return rv
    
##optimisation by affine sum and by number of positive components

    def SolveAndClean(self, nlast = 0, *args, **argd):
        self.Solve()
        rv =  self.GetSolution(*args, **argd)
        self.CleanTemps(nlast = nlast)
        return rv        
            

    def OptSumSol(self, pos = [], lowBound = EPSILON, *args, **argd):
        self.MakePositive(pos, tmp = True, lowBound = lowBound)
        self.SetSumObjective(tmp = True)
        return self.SolveAndClean(nlast = len(pos) + 1, *args, **argd)    


    def OptLenSol(self, pos = [], lowBound = 1, *args, **argd):
        self.MakePositive(pos, tmp = True, lowBound = lowBound)
        cont = self.GetColNames('Continuous')
        self.SetLenObjective(varlist = cont, tmp = True)
        return self.SolveAndClean(nlast = len(pos) + 1 + len(cont), *args, **argd)    


    def OptSingleVarSol(self, var, *args, **argd):
        normalise = False
        if 'normalise' in argd:
            normalise = argd.pop('normalise')        
        self.SetObjective([var], tmp = True)
        return self.SolveAndClean(nlast = 1, normalise = normalise, *args, **argd) 


    def PositiveSol(self, *args, **argd):
        cont = self.GetColNames('Continuous')
        self.MakePositive(cont, tmp = True)
        return self.SolveAndClean(nlast = len(cont), *args, **argd)


    def HasPositiveSol(self):
        cont = self.GetColNames('Continuous')
        self.MakePositive(cont, tmp = True)
        self.Solve()
        rv = self.IsStatusOptimal()
        self.CleanTemps(nlast = len(cont))
        return rv


##excluding solutions

    def SetIntegerCut(self, intnames, tmp = False, coef = 1):
        """pre: self.GetObjDirec() == 'Min'; for each intname in intnames: self.GetColKind(intname) == 'Integer'.
        post: any solution involving all variables in intnames is excluded."""
        cut = len(intnames) - 1
        cut *= coef
        self.AddSumConstraint(None, cut, intnames, tmp = tmp)


    def ExcludeSolution(self, vars, tmp = False):
        intnames = map(lambda var: self.IntTag + var, vars)
        self.SetIntegerCut(intnames, tmp = tmp)


##    def  SolutionsInvolving(self, var, nmax = -1, fun = None):
##        rv = []
##        fun = fun or (lambda rn: self.OptSumSol([var]))
##        self.SetObjDirec('Min')
##        self.SetLenObjective(tmp = True)
##        mode = fun(var)
##        
##        while len(mode) > 0 and (nmax < 0 or len(rv) < nmax):
##            rv.append(mode)
##            self.ExcludeSolution(mode, tmp = True)
##            mode = fun(var)
##            
##        self.CleanTemps(nlast = 1 + 2 * len(self.GetColNames('Continuous')) + len(rv))
##        return rv


    def MinSolutions(self, nmax = -1, disjoint = False, *args, **argd):

        rv = []
        tmpvars = 0
        do, i = True, 0
        while do and (nmax < 0 or i < nmax): 
            self.Solve()
            sol = self.GetSolution()
            sol2 = self.GetSolution(*args, **argd)
            if len(sol) > 0:
                rv.append(sol2)
                if disjoint:
                    self.BlockList(sol, tmp = True)
                    tmpvars += len(sol)
                else:
                    self.ExcludeSolution(sol, tmp = True)
                    tmpvars += 1
            else:
                do = False
            i += 1                    
        self.CleanTemps(nlast = tmpvars)        
        return rv


    def MinSumSolutions(self,  nmax = -1, *args, **argd):
        self.SetSumObjective(tmp = True)
        rv = self.MinSolutions(nmax = nmax, disjoint = True, *args, **argd)
        self.CleanTemps(nlast = 1)
        return rv


    def MinLenSolutions(self, nmax = -1, disjoint = False, *args, **argd):
        cont = self.GetColNames('Continuous')
        self.SetLenObjective(varlist = cont, tmp = True)
        rv = self.MinSolutions(nmax = nmax, disjoint = disjoint, *args, **argd)
        self.CleanTemps(nlast = 1 + len(cont))
        return rv


##positive nullspace

    def GetNonNegNullSpace(self, dim, cnames):
        """pre: self.GetClass() == MIP, dim is the nullspace dimension, cnames is the list of continuous variables in correct order.
        post: rv = non-negative nullspace matrix of the LP constraint matrix"""
                
        rv = Mtx.StoMat(rnames = cnames, Conv = float)

        self.SetObjDirec('Min')
        self.SetLenObjective(tmp = True)
        self.AddSumConstraint(cnames = cnames, tmp = True)
                
        while len(rv.cnames) < dim:
            self.Solve()
            if self.GetStatusMsg() != 'MIP Optimal':
                raise exceptions.StandardError, "Failed to calculate a non-negative nullspace"
            colmtx = self.GetSolution(kind = 'Continuous', asmatrix = True)
            rv.NewCol(colmtx.GetCol(0))
            intsol = self.GetSolution(kind = 'Integer')
            self.SetIntegerCut(intsol, tmp = True)

        self.CleanTemps()
        return rv            



##coupling analysis

    def GetOptRatio(self, var, const, direc = 'Min'):
        rv = - 1
        self.SetObjDirec(direc)
        self.AddConstraint({const : 1}, 1, 1, tmp = True)
        self.SetObjective([var], tmp = True)
        self.Solve()
        sol = self.GetSolution()
        self.CleanTemps(nlast = 2)
        if var in sol and const in sol:
            rv = sol[var] / sol[const]
        return rv            


    def GetCouplingType(self, var1, var2):
        rv = '?'
        rat1 = self.GetOptRatio(var1, var2, 'Min')
        rat2 = self.GetOptRatio(var1, var2, 'Max')
        if rat1 <= 0 and rat2 <= 0:
            rv = '||'
        elif rat1 == rat2:
            rv = '<=>'
        elif rat1 > 0 and rat2 > 0:
            rv = '<->'
        elif rat1 > 0:
            rv = '<-'
        elif rat2 > 0:
            rv = '->'
        return rv            


##positivity analysis        

    def IsIdenticallyZero(self, var, lowBound = EPSILON):
        self.SetObjDirec('Min')
        self.MakePositive([var], tmp = True, lowBound = lowBound)
        self.Solve()
        rv = not self.IsStatusOptimal()
        self.CleanTemps(nlast = 1)
        return rv



##essentiality analysis

    def IsEssentialFor(self, var, targ, lowBound = EPSILON):
        self.Block(var, tmp = True)
        rv = self.IsIdenticallyZero(targ, lowBound = lowBound)
        self.CleanTemps(nlast = 1)        
        return rv


    def Essentials(self, targ, vars, lowBound = EPSILON):
        rv = []
        if targ in vars:
            rv.append(targ)
        for var in vars:
            if var != targ and self.IsEssentialFor(var, targ, lowBound = lowBound):
                rv.append(var)
        return rv          


    def Damage(self, var, targs, lowBound = EPSILON):
        rv = [var]
        for targ in targs:
            if var != targ and self.IsEssentialFor(var, targ, lowBound = lowBound):
                rv.append(targ)
        return rv


    def IsCutSetFor(self, vars, targ, lowBound = EPSILON):
        self.BlockList(vars, tmp = True)
        rv = self.IsIdenticallyZero(targ, lowBound = lowBound)
        self.CleanTemps(nlast = len(vars))        
        return rv


    def EssentialityCoef(self, quer, targ):
        self.SetObjDirec('Max')
        self.SetObjective([targ], tmp = True)
        self.Solve()
        max = self.GetObjValue()
        self.Block(quer, tmp = True)
        self.Solve()
        rv = (max - self.GetObjValue()) / max
        self.CleanTemps(2)
        return rv
        


    def ControlCoef(self, quer, targ, delta = 0.1, refpoint = 0.5):
        self.SetObjDirec('Max')
        self.SetObjective([targ], tmp = True)

        self.SetColBounds(quer, lo = 0, hi = refpoint - delta)
        self.Solve()
        t1 = self.GetObjValue()

        self.SetColBounds(quer, lo = 0, hi = refpoint + delta)
        self.Solve()
        t2 = self.GetObjValue()

        rv = (t2 - t1)  / (2 * delta)        
        self.SetColBounds(quer, 0, 1)
        self.CleanTemps(1)
        return rv        

##net stoichiometries

    def TestStoich(self, sto):
        self.SetObjDirec('Max')
        self.AddStoich(sto, '__sto__', lowBound = 0, upBound = 1, tmp = True)
        self.SetObjective(['__sto__'], tmp = True)
        self.Solve()
        rv = not Round.IsZero(self.GetColVal('__sto__'))
        self.CleanTemps(nlast = 2)
        return rv


#####################################################################
##localisation

    def ExtractedLocus(self, fun, rnlist = []):
        rv = rnlist[:] or self.GetReacs()
        for rn in rv[:]:
            if fun(rn, rv):
                rv.remove(rn)
        return rv


    def DisjointLoci(self, testfun, extractfun, rnlist = []):
        rv = []
        rnlist = rnlist or self.GetReacs()
        while testfun(rnlist):
            locus = self.ExtractedLocus(fun = extractfun, rnlist = rnlist)
            rv.append(locus)
            rnlist = Util.Set.Complement(rnlist, locus)
        return rv
