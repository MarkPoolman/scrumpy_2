import exceptions
from LP import glpk
lp = glpk.lp
Sym2Num = glpk.Sym2Num
Str2Num = glpk.Str2Num
Num2Str = glpk.Num2Str

import  Mtx

Num2Str.update({
    Sym2Num['LPX_LP'] : 'LP',
    Sym2Num['LPX_MIP'] : 'MIP',
    Sym2Num['LPX_CV'] : 'Continuous',
    Sym2Num['LPX_IV'] : 'Integer',
    Sym2Num['LPX_I_UNDEF'] : 'MIP Undefined',
    Sym2Num['LPX_I_FEAS'] : 'MIP Feasible',
    Sym2Num['LPX_I_OPT'] : 'MIP Optimal',
    Sym2Num['LPX_I_NOFEAS'] : 'MIP No Feasible',
    Sym2Num['LPX_UNBND'] : 'Unbound',    
    })
for k in Num2Str.keys():
    Str2Num[Num2Str[k]] = k


INT_TAG = 'int_'    

class Mip(lp):

    def __init__(self, name = None, klass = 'LP'):
        lp.__init__(self, name)
##        klass = self._arg2num(klass, ['MIP', 'LP'])
##        self._glp_lpx_set_class(klass)
        self.Klass = klass
        self.ObjVal = 0.0
        self.UpdateStatus()
        self.IntTag = INT_TAG

 ##utility   

    def _arg2num(self, arg, valids = []):
        num = arg
        if arg in Str2Num:
            num = Str2Num[arg]
        elif arg in Sym2Num:
            num = Sym2Num[arg]
        if not num in Num2Str or (valids and not Num2Str[num] in valids):
            raise exceptions.ValueError, str(arg)
        return num
        

    def _num2str(self, num):
        return Num2Str[num]

##access

    def GetClass(self, ):
        return self.Klass
##        return self._num2str(self._glp_lpx_get_class())
    

    def GetObjValue(self):
        return {'LP' : self._glp_lpx_get_obj_val, 'MIP' : self._glp_lpx_mip_obj_val}[self.GetClass()]()


    def GetStatus(self, Stype = "Generic"):
        return self.Status
##        return {'LP' : self._glp_lpx_get_status, 'MIP' : self._glp_lpx_mip_status}[self.GetClass()]()


    def IsStatusOptimal(self):
        return self.GetStatusMsg() in ['Optimal', 'MIP Optimal']


    def GetObjDirec(self):
        return self._num2str(self._glp_lpx_get_obj_dir())
    

    def GetColKind(self, cname):
        rv = 'Continuous'
        if self.GetClass() == 'MIP':
            cid = self.GetColIdxs([cname])[0]
            rv = self._num2str( self._glp_lpx_get_col_kind(cid))
        return rv            


    def GetColVal(self, cname, kind = None):
        cid = self.GetColIdxs([cname])[0]
        fun = {'LP' : lambda c: self._glp_lpx_get_col_prim(c), 'MIP' : lambda c: self._glp_lpx_mip_col_val(c)}[self.GetClass()]
        return fun(cid)


    def GetObjCoef(self, cname):
        return self._glp_lpx_get_obj_coef(self.GetColIdxs([cname])[0])
    


##editing

    def AddCols(self, cnames):
        lp.AddCols(self, cnames)
        if self.GetClass() == 'MIP':
            intnames = [self.IntTag + cname for cname in cnames]
            lp.AddCols(self, intnames)
            for intname in intnames:
                self.SetColKind(intname, 'Integer')
                self.SetColBounds(intname, 0, 1)



    def SetColKind(self, cname, kind):
        if self.GetClass() == 'MIP':
            cid = self.GetColIdxs([cname])[0]
            kind = self._arg2num(kind, ['Integer', 'Continuous'])
            self._glp_lpx_set_col_kind(cid, kind)


    def SetColVals(self, col, vals):

        col = self.GetColIdxs([col])[0]
        lenv = len(vals)
        if len(vals) != self.GetNumRows():
            raise exceptions.IndexError, "incorrect col length" 
        zs = []
        nzs = []
        for i in range(lenv):
            if vals[i] == 0:
                zs.append(i)
            else:
                nzs.append(i)
        zs.reverse()
        if len(zs) !=0:
            vals = vals[:] # copy, because we are going to change the contents
        for i in zs:
            del vals[i]
        nzs = glpk.IntArray(glpk.AddOne(nzs))
        vals = glpk.DoubleArray(vals)
        self._glp_lpx_set_mat_col(col, len(vals), nzs._array, vals._array)
    
##solving

    def Solve(self, PrintStatus = False):
        lp.Solve(self)
        if self.GetClass() == 'MIP':
            self._glp_lpx_integer()
        if PrintStatus:
            print self.GetStatusMsg()
        self.ObjVal = self.GetObjVal()            
        self.UpdateStatus()

    def UpdateStatus(self):
        self.Status = {'LP' : self._glp_lpx_get_status, 'MIP' : self._glp_lpx_mip_status}[self.GetClass()]()        

##safe overloads

    def DelCols(self, cols):
        if len(cols) > 0:
            if len(cols) == len(set(cols)):
                lp.DelCols(self, cols)
            else:
                raise 'duplicates in the list!'


    def DelRows(self, rows):
        if len(rows) > 0:
            if len(rows) == len(set(rows)):
                lp.DelRows(self, rows)
            else:
                raise 'duplicates in the list!'                            
