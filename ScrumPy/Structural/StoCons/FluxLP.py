import Util.Set

import MtxLP
parent = MtxLP.MtxLp
import Mtx, Sto, Relation
Relation = Relation.Relation

import LP

REV_TAG = '_back'

class FluxLp(parent):

    def __init__(self, mtx, lowBound = 0, upBound = None, *args, **argd):

        parent.__init__(self, mtx, lowBound = lowBound, upBound = upBound, *args, **argd)
        self.Reacs = set(mtx.cnames)
        self.Mets = set(mtx.rnames)

        for rn in self.GetReacs():
            col = mtx.GetCol(rn)
            col = map(lambda x: -x, col)            
            self.AddCol(rn + REV_TAG, col, lowBound, upBound, 'Continuous')

        self.Irrevs = set()
        self.MakeIrrev(mtx.Irrevs[:])
       

##access

    def GetReacs(self):
        return list(self.Reacs)


    def GetColNames(self, kind = 'Continuous'):
        rv = []
        if kind == 'Reaction':
            rv = self.GetReacs()
        else:
            rv = parent.GetColNames(self, kind)
        return rv            


    def GetIrrevs(self):
        return list(self.Irrevs)


    def GetMets(self):
        return list(self.Mets)

            
    def GetOpposite(self, rn):
        rv = rn
        if REV_TAG in rv:
            rv = rv.replace(REV_TAG, '')
        else:
            rv += REV_TAG
        return rv            


    def GetColVal(self, rn, kind = None):
        rv = parent.GetColVal(self, rn)
        if kind == 'Reaction':
            rv -= parent.GetColVal(self, self.GetOpposite(rn))
        return rv
    

    def GetReacVal(self, rn):
        return self.GetColVal(rn, 'Reaction')



    def GetSolution(self, kind = 'Reaction', *args, **argd):
        return parent.GetSolution(self, kind = kind, *args, **argd)

##editing

    def MakeIrrev(self, rnlist, on = True):
        rnlist = set(rnlist).intersection(self.GetReacs())
        backlist = map(self.GetOpposite, rnlist)
        constdict = dict.fromkeys(backlist, 0)
        self.AddConstantConstraints(constdict)                  
        self.Irrevs.update(rnlist)


    def SetIrrev(self, rn, on = True):
        back = self.GetOpposite(rn)
        if on and rn in self.GetReacs():
            self.AddConstraint({back : 1}, 0, 0, name = rn + '_irr')
            self.Irrevs.add(rn)
        elif not on and rn in self.Irrevs:
            self.DelRows([rn + '_irr'])
            self.Irrevs.remove(rn)


    def AddTransporterCol(self, met, tmp = False, coef = 1):
        txname = '_TX_' + met
        vals = [0] * self.GetNumRows()
        idx = self.GetRowIdxs([met])[0]
        vals[idx - 1] = coef
        self.AddCol(txname, vals, tmp = tmp)
        return txname


    def AddReac(self, name, sto = {}, irrev = False):
##        vals = [0] * self.GetNumRows()
##        for met in sto:
##            idx = self.GetRowIdxs([met])[0]
##            vals[idx - 1] = sto[met]
##        self.AddCol(name, vals)
##        vals = map(lambda x: -x, vals)
##        self.AddCol(name + '_back', vals)
        sto = sto.copy()
        self.AddStoich(sto, name)
        Utils.Sto.Invert(sto)
        self.AddStoich(sto, name + '_back')  
        self.Reacs.update([name])
        self.Mets.update(sto.keys())
        if irrev:
            self.MakeIrrev([name])


    def DelReacs(self, rnlist):
        rnbacks = map(self.GetOpposite, rnlist)
        parent.DelCols(self, list(rnlist) + rnbacks)
        if self.GetClass() == 'MIP':
            intcols = [self.IntTag + rn for rn in list(rnlist) + rnbacks]
            parent.DelCols(self, intcols)
        self.Reacs.difference_update(rnlist)
        self.Irrevs.difference_update(rnlist)


    def DelMets(self, metlist):
        parent.DelRows(self, metlist)
        self.Mets = Util.Set.Complement(self.Mets, metlist)


    def AddConstantConstraints(self, constraints = {}, tmp = False):
        for k in constraints.keys():
            val = constraints[k]
            if val < 0:
                constraints[self.GetOpposite(k)] = -val
                del constraints[k]
        parent.AddConstantConstraints(self, constraints = constraints, tmp = tmp)
        
##analysis
    def BlockOpposite(self, rn, tmp = False, on = True):
        self.Block(self.GetOpposite(rn), tmp = tmp, on = on)

    def BlockReac(self, rn, tmp = False, on = True):
        if rn in self.GetReacs():
            self.Block(rn, tmp = tmp, on = on)
            self.BlockOpposite(rn, tmp = tmp, on = on)


    def BlockReacs(self, rnlist, tmp = False):
        for rn in rnlist:
            self.BlockReac(rn, tmp = tmp)


    def BlockList(self, varlist, tmp = False, withopposites = True):
        if withopposites:
            self.BlockReacs(varlist, tmp = tmp)
        else:
            parent.BlockList(self, varlist, tmp = tmp)


##feasible directions

    def IsLivePos(self, rn):
        return not parent.IsEssentialFor(self, self.GetOpposite(rn), rn, lowBound = 1)

    def IsLiveNeg(self, rn):
        return not parent.IsEssentialFor(self, rn, self.GetOpposite(rn), lowBound = 1)        
        

    def FeasDirec(self, rn):
        rv = 0
        if self.IsLivePos(rn):
            rv = 1
        elif self.IsLiveNeg(rn):
            rv = -1
        return rv


    def IsLiveReversible(self, rn):
        return self.IsLivePos(rn) and self.IsLiveNeg(rn)
    

##liveness analysis

    def IsLive(self, rn):
        return self.IsLivePos(rn) or self.IsLiveNeg(rn)


    def LiveReacs(self, rnlist = [], MIP = True):
        rnlist = rnlist or self.GetReacs()
        rv = set()
        if MIP:
            found = [0]
            while found:
                    rnlist = set(rnlist).difference(rv)
                    self.SetLenObjective(rnlist)
                    found = set(self.SolveAndClean()).intersection(rnlist)
                    rv.update(found)
            rnlist = set(rnlist).difference(rv)
        for rn in rnlist:
            if self.IsLive(rn):  
                rv.add(rn)
        return list(rv)        


##flux mode analysis

    def MinSumMode(self, rn, lowBound = 1, *args, **argd):
        self.SetObjDirec('Min')
        rnback = self.GetOpposite(rn)

        self.Block(rnback, tmp = True)
        rv = self.OptSumSol(pos = [rn], lowBound = lowBound, *args, **argd)
        self.CleanTemps(nlast = 1)
        forval = self.ObjVal
       
        self.Block(rn, tmp = True)
        backward = self.OptSumSol(pos = [rnback], lowBound = lowBound, *args, **argd)
        self.CleanTemps(nlast = 1)

        if forval == 0 or 0 < self.ObjVal < forval:
            rv = backward
        return rv
        


    def ShortestMode(self, rn, lowBound = 1, *args, **argd):
        """pre: MIP"""
        self.SetObjDirec('Min')
        rnback = self.GetOpposite(rn)

        self.Block(rnback, tmp = True)
        rv = self.OptLenSol(pos = [rn], lowBound = lowBound, *args, **argd)
        self.CleanTemps(nlast = 1)
        forval = self.ObjVal

        self.Block(rn, tmp = True)
        backward = self.OptLenSol(pos = [rnback], lowBound = lowBound, *args, **argd)
        self.CleanTemps(nlast = 1)

        if forval == 0 or 0 < self.ObjVal < forval:
            rv = backward        
        return  rv


    def MaxRateMode(self, rn, *args, **argd):
        self.SetObjDirec('Max')
        rv = self.OptSingleVarSol(rn, *args, **argd)
        return  rv

    def MaxRate(self, rn, *args, **argd):
        return self.MaxRateMode(rn, *args, **argd).get(rn, 0.0)


##essentiality analysis

    def IsEssentialFor(self, rn, targ, lowBound = MtxLP.EPSILON):
        self.BlockReac(rn, tmp = True)
        rv = not self.IsLive(targ)
        self.CleanTemps(nlast = 2)
        return rv



    def Essentials(self, targ, rnlist = []):
        rnlist = rnlist or self.GetReacs()
        return parent.Essentials(self, targ, rnlist)



    def Damage(self, rn, targs = []):
        targs = targs or self.GetReacs()
        targs = Util.Set.Intersect(targs, self.LiveReacs())
        return parent.Damage(self, rn, targs)


    def IsCutSetFor(self, rnlist, targ):
        rnlist = Util.Set.Intersect(rnlist, self.GetReacs())
        self.BlockReacs(rnlist, tmp = True)
        rv = not self.IsLive(targ)
        self.CleanTemps(nlast = 2 * len(rnlist))
        return rv        


    def EssentialityMtx(self, rnlist = []):
        rnlist = rnlist or self.GetReacs()
        rv = Utils.Mtx.StoMat(rnames = rnlist, cnames = rnlist)
        for rn in rnlist:
            essentials = self.Essentials(rn, rnlist)
            for essential in essentials:
                rv[essential, rn] = 1
        return rv


    def EssentialityCoef(self, quer, targ):
        self.Block(self.GetOpposite(targ), tmp = True)
        self.SetObjDirec('Max')
        self.SetObjective([targ], tmp = True)
        self.Solve()
        max = self.GetObjValue()
        if max == 0:
            rv = 0
        else:            
            self.BlockReac(quer, tmp = True)
            self.Solve()
            rv = (max - self.GetObjValue()) / max
            self.CleanTemps(4)
        return rv
    

    def ControlCoef(self, quer, targ, delta = 0.1, refpoint = 0.5):
        self.Block(self.GetOpposite(targ))
        rv = parent.ControlCoef(self, quer, targ, delta, refpoint)
        self.CleanTemps(2)
        return rv                
    

##nutrient analysis    

    def CanConsume(self, met):
        txname = self.AddTransporterCol(met, tmp = True)
        rv = not self.IsIdenticallyZero(txname)
        self.CleanTemps(nlast = 1)
        return rv


    def ConsumedMets(self, metlist = []):
        rv = []
        lst = metlist or self.GetMets()
        for met in lst:
            if self.CanConsume(met):
                rv.append(met)
        return rv


    def ConvertedTo(self, seed, targs = []):
        rv = []
        targs = Util.Set.Complement(targs or self.GetMets(), [seed])
        if targs:
            txname = self.AddTransporterCol(seed, tmp = False, coef = -1)
            rv += self.ConsumedMets(targs)
            self.DelCols([txname])
        return rv


    def CanProduce(self, met):
        txname = self.AddTransporterCol(met, tmp = True, coef = -1)
        rv = not self.IsIdenticallyZero(txname)
        self.CleanTemps(nlast = 1)
        return rv

    def ProducedMets(self, metlist = []):
        rv = []
        lst = metlist or self.GetMets()
        for met in lst:
            if self.CanProduce(met):
                rv.append(met)
        return rv        

##coupling analysis

    def GetOptRatio(self, var, const, direc = 'Min'):
        self.BlockOpposite(var, tmp = True)
        self.BlockOpposite(const, tmp = True)
        rv = parent.GetOptRatio(self, var, const, direc = direc)
        self.CleanTemps(nlast = 2)
        return rv


##excluding modes

    def ExcludeSolution(self, mode, tmp = False, kind = 'Reaction'):
        rnlist = list(mode)
        if kind == 'Reaction':
            rnlist = []
            for rn in mode:
                rn2 = rn
                if mode[rn] < 0:
                    rn2 = self.GetOpposite(rn)
                rnlist.append(rn2)
            parent.ExcludeSolution(self, rnlist, tmp = tmp)
        parent.ExcludeSolution(self, rnlist, tmp = tmp)



##leakage modes

    def ShortestLeakageMode(self, uncons_vec, *args, **argd):
        self.SetObjDirec('Min')
        
        self.AddCol(cname = '__tx__', vals = uncons_vec, tmp = True)
        rv = self.OptLenSol(pos = ['__tx__'], *args, **argd)
        self.CleanTemps(nlast = 1)
        return rv

##net stoichiometries

    def ModeWithSto(self, stoich, maximise = None, shortest = False, pos = [], asmatrix = False, lowBound = 1, minimise = None):
        stoich = stoich.copy()
        Utils.Sto.Invert(stoich)
        self.AddReac('__sto__', stoich, irrev = True)
        if maximise == None and minimise == None:
            fun = {True: self.__class__.OptLenSol, False : self.__class__.OptSumSol}[shortest]
            rv = fun(self, ['__sto__'] + pos, lowBound = lowBound, normalise = False)
        elif maximise:
            self.AddConstantConstraints({'__sto__' : 1}, tmp = True)
            rv = self.MaxRateMode(maximise)
            self.CleanTemps(nlast = 1)
        elif minimise:
            self.AddConstantConstraints({'__sto__' : 1}, tmp = True)
            self.SetObjective([minimise], tmp = True)
            self.Solve()
            rv = self.GetSolution()
            self.CleanTemps(nlast = 2)                              
        Utils.Sto.DivideAndDelete(rv, '__sto__')
        if asmatrix:
            rv = Mtx.Dict2Mtx(rv, self.Cnames, True)
        self.DelReacs(['__sto__'])
        return rv



##    def Sto2FeasibleMode(self, stoich, N, asmatrix = False, normalise = True):
##        obj = []
##        for x in N.Externs:
##            tx = list(N.InvolvedWith(x))[0]
##            tx_back = self.GetOpposite(tx)
##            obj += [tx, tx_back]
##            if x in stoich:        
##                self.AddConstraint({tx : 1, tx_back : 1}, 1, None, tmp = True)
##        self.SetObjective(obj, tmp = True)
##        self.Solve()
##        rv = self.GetSolution(asmatrix = asmatrix, normalise = normalise)
##        self.CleanTemps(1 + len(stoich))            
##        return rv    



##    def ExtendNetSto(self, stoich, N, asmatrix = False, normalise = True):
##        obj = []
##        for x in N.Externs:
##            tx = list(N.InvolvedWith(x))[0]
##            tx_back = self.GetOpposite(tx)
##            obj += [tx, tx_back]
##            if x in stoich:
##                val = stoich[x] / N[x, tx]
##                if val < 0:
##                    tx = tx_back
##                val = abs(val)
##                self.MakePositive([tx], lowBound = float(val), tmp = True)
##                self.BlockOpposite(tx, tmp = True)
##        self.SetObjective(obj, tmp = True)
##        self.Solve()
##        mode = self.GetSolution(asmatrix = True)
##        rv = N.Mul(mode)
##        Utils.Mtx.DelZeros(rv)
##        if not asmatrix:
##            rv = rv.InvolvedWith('v')
##        self.CleanTemps(1 + 2 * len(stoich))            
##        return rv    
    
##    def ExtendNetSto(self, stoich, N, shortest = True, asmatrix = False):
##        obj, pos = [], []
##        for x in N.Externs:
##            iw = N.InvolvedWith(x)
##            tx = iw.keys()[0]
##            tx_back = self.GetOpposite(tx)
##            obj += [tx, tx_back]
##            if x in stoich:            
##                if (stoich[x] < 0) != (iw[tx] < 0):
##                    tx = tx_back
##                pos.append(tx)
##        if shortest:
##            self.SetLenObjective(obj, tmp = True)
##        else:            
##            self.SetObjective(obj, tmp = True)
##        self.MakePositive(pos, tmp = True)
##        self.Solve()
##        mode = self.GetSolution(asmatrix = True)
##        rv = N.Mul(mode)   
##        if not asmatrix:
##            rv = rv.InvolvedWith('v')
##        nlast = 1 + len(pos)
##        if shortest:
##            nlast += 2 * len(obj)
##        self.CleanTemps(nlast)            
##        return rv        

##        rv = self.OptLenSol(pos = [rn], lowBound = lowBound, *args, **argd)
##        self.CleanTemps(nlast = 1)
##        forval = self.ObjVal
##
##        backward = self.OptLenSol(pos = [rnback], lowBound = lowBound, *args, **argd)
##        self.CleanTemps(nlast = 1)
##
##        if forval == 0 or 0 < self.ObjVal < forval:
##            rv = backward        
##        return  rv

##    def IsLiveBound(self, rn):
##        """pre: upBound = 1"""        
##        self.SetObjective({rn : 1, self.GetOpposite(rn) : -1})
##        self.SetObjDirec('Max')
##        self.Solve()
##        return self.GetColVal(rn) != 0        
##
##
##    def LiveReacsBound(self, rnlist = []):
##        """pre: ubBound = 1"""
##        rv = []
##        rnlist = rnlist or self.GetReacs()
##        self.SetObjDirec('Max')
##        for rn in rnlist:
##            self.SetObjective({rn : 1, self.GetOpposite(rn) : -1})
##            self.Solve()
##            if self.GetColVal(rn):  
##                rv.append(rn)
##        return rv


##
##    def MaxReacRateMode(self, rn, *args, **argd):
##        self.SetObjDirec('Max')
##        rnback = self.GetOpposite(rn)
##
##        self.Block(rnback, tmp = True)
##        rv = self.OptSingleVarSol(rn, *args, **argd)
##        self.CleanTemps(nlast = 1)
##        forval = self.ObjVal
##
##        self.Block(rn, tmp = True)
##        backward = self.OptSingleVarSol(rnback, *args, **argd)
##        self.CleanTemps(nlast = 1)        
##
##        if self.ObjVal > forval:
##            rv = backward        
##        return rv


##    def SomeLiveReacs(self, rnlist = []):
##        """class = MIP"""        
##        rnlist = rnlist or self.GetReacs()
##        self.SetObjDirec('Max')
##
##        self.SetLenObjective(rnlist, tmp = True)           
##        self.Solve()
##        f = self.GetSolution()
##        self.CleanTemps(nlast = 1 + len(rnlist))
##
##        backs = map(lambda rn: self.GetOpposite(rn), rnlist)
##        self.SetLenObjective(backs, tmp = True)
##        self.Solve()
##        b = self.GetSolution()
##        self.CleanTemps(nlast = 1 + len(rnlist)) 
##
##        return Util.Set.Union(f.keys(), b.keys())
