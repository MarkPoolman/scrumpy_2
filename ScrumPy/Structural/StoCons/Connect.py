import Util.Set
import Util.Seq

def _components(mtx):
    rv = []

    uncounted = Util.Set.Union(mtx.rnames, mtx.cnames)
    size = len(uncounted)
    if size == 0:
        rv = [([], 0.0)]
    else:
        while len(uncounted) > 0:
            seed = uncounted[0]
            achieved = AchievableFrom(mtx, seed)
            uncounted = Util.Set.Complement(uncounted, achieved)
            tup = (achieved, float(len(achieved)) / size)
            rv.append(tup)         
    
    return rv



def AchievableFrom(mtx, seed):

    visited = []    

    def visit(node, visited):
        if not node in visited:
            visited.append(node) 
            for neighbour in mtx.InvolvedWith(node):
                visit(neighbour, visited)          

    visit(seed, visited)

    return visited



def Components(mtx):
    rv = _components(mtx)
    rv.sort(lambda a, b : cmp(b[-1], a[-1]))
    rv = map(lambda comp: comp[0], rv)
    return rv




def SMComponents(mtx):
    rv = []
        
    size = len(mtx.cnames)
    if size == 0:
        rv = [([], [], 0.0)]
    else:
        comps = _components(mtx)
        for comp in comps:
            nodes = comp[0]
            mets, reacs = [], []
            for node in nodes:
                if node in mtx.rnames:
                    mets.append(node)
                else:
                    reacs.append(node)
            tup = (mets, reacs, float(len(reacs)) / size)
            rv.append(tup)

    rv.sort(lambda a, b : cmp(b[-1], a[-1]))
    return rv



def ListBridges(mtx):

    rv = []

    Mtx = mtx.Copy()
    cnum = len(_components(Mtx))

    for rn in Mtx.cnames:
        col = Mtx.GetCol(rn)
        Mtx.DelCol(rn)
        if len(_components(Mtx)) > cnum:
            rv.append(rn)
        Mtx.NewCol(col, name = rn)

    return rv           


def NSBridges(sm):
    import sys
    
    rv = []
           
    k = sm.NullSpace()
    nsubs = len(_components(k))

    for rn in k.rnames:
        if Util.Seq.AllEq(k[rn], 0):
            sm.DelCol(rn)
    sm.ZapZeroes()
    k = sm.NullSpace()
    nsubs = len(_components(k))
    print nsubs

    rnlist = sm.cnames
    for rn in rnlist:
        col = sm.GetCol(rn)
        sm.DelCol(rn)
        k = sm.NullSpace()
        if len(_components(sm)) > nsubs:
            rv.append(rn)
            sys.__stdout__.write(rn + '\n')
            sys.__stdout__.flush()
        sm.NewCol(col, name = rn)
    
    return rv


def IsOrphan(M, rname):
    return not rname in M.Externs and M[rname].count(0) == len(M.cnames) - 1

def Orphans(M):
    rv = M.OrphanMets()
    rv = set(rv).difference(M.Externs)
    return list(rv)


def CoreNetwork(M):
    rv = M.Copy()
    o = Orphans(M)
    while len(o) > 0:
        for met in o:
            if met in rv.rnames:
                for rn in rv.InvolvedWith(met):
                    rv.DelCol(rn)
        rv.ZapZeroes()                        
        o = Orphans(rv)
    rv.Irrevs = Util.Set.Intersect(rv.Irrevs, rv.cnames)            
    return rv
