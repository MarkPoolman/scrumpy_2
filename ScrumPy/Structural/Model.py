
"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""


import sys


from ScrumPy.Util import Seq, DynMatrix, Set,  Sci
from ScrumPy import LP


#,Tree
import StoMat , EnzSubset, ElModes




class Model:

    def __init__(self, md):
       self.md = md
       self.Init()
       
    def Reload(self):
        self.md.Reload()
        if hasattr(self, "sm"):
            del self.sm, self.smx # in case we have changed to a pure diff eqn model
        self.Init()
        
        
    def Hide(self):
        if hasattr(self, "ui"):
            self.ui.Close()
            
    def Show(self):
         if hasattr(self, "ui"):
            self.ui.ReInit()
       
    def Init(self):
        
        md = self.md

        if (len(md.Errors) == 0) and not md.ExplicitOnly(): #  and   not md.ExplicitOnly():
            # it is the job of the ui to decide what do about errors in md
            #  and if we only have explicit diff equations there's no stoichiometries
           
           IntMets   = md.GetIntMetNames()
           if len(IntMets) >0:  # 0 => explicit diffs only so skip the rest
                   
               ExtMets   = md.GetExtMetNames()
               Reactions = md.GetReacNames()
               el_t = md.GetElType()
            
               self.sm =  sm  = StoMat.StoMat(rnames=IntMets, Conv=el_t,  cnames=Reactions)
               self.smx = smx = StoMat.StoMat(rnames=IntMets+ExtMets, Conv=el_t,  cnames=Reactions)
            
               for reac in Reactions:
            
                   sm.RevProps[reac] = smx.RevProps[reac] = md.GetReacDirec(reac)
                   sto = md.GetStoDic(reac)
                   for met in sto.keys():
                       smx[met, reac] = sto[met]
                       if not met in ExtMets:
                           sm[met, reac] = sto[met]


    def DelReactions(self,Reactions):
        """ pre: Reactions is a list of strings
          post: reactions removed from self """

        for r in Reactions:
            if not r in self.sm.cnames:
                sys.stderr.write("Warning: tried to delete non-existent reaction, " + r + "\n")
            else:
                self.sm.DelReac(r)
                self.smx.DelReac(r)


        self.sm.ZapZeroes()
        self.smx.ZapZeroes()
        self.smx.Externs = Set.Intersect(self.smx.Externs, self.smx.rnames)
        self.sm.Externs = self.smx.Externs[:]




    def IsIrrev(self, name):
        """pre: name is a reaction name
          post: self.IsIrrev(name) => name is an irreversible reaction """

        print "StructuralModel.IsIrrev due for removal"

        #return self.sm.IsIrrev(name)


    def NetSto(self,vels):
        "return a matrix of the net stoichs represented by the rate information in vels"

        rv = DynMatrix.matrix()
        sm = self.smx.ToNumPyMtx(conv=int)
        v = vels.ToNumPyMtx(conv=int)
        res = sm * v
        rv.FromNumPyMtx(res)
        rv.rnames = self.smx.rnames
        rv.cnames = vels.cnames
        return rv


    def Externals(self):
        return self.md.GetExtMetNames()

    def Transporters(self,NamesOnly=False):
        seen = {}
        rv = []
        exs = self.Externals()
        for e in exs:
            for tx in self.smx.InvolvedWith(e):
                if not seen.has_key(tx):
                    seen[tx] = 1
                    rv.append([tx, Set.Intersect(exs,self.smx.Reactants(tx))])

        if NamesOnly:
            return seen.keys()
            return rv


    def FindIsoforms(self):
        stodic = {}
        for reac in self.smx.cnames:
            strsto = str(self.smx.InvolvedWith(reac))
            if stodic.has_key(strsto):
                stodic[strsto].append(reac)
            else:
                stodic[strsto] = [reac]
        rv = []
        for reac in stodic.values():
            if len(reac)>1:
                rv.append(reac)

        return rv


    def DelIsoforms(self):
        isos = self.FindIsoforms()
        rv = {}
        for iso in isos:
            rv[iso[0]] = iso[1:]
            for reac in iso[1:]:
                self.sm.DelReac(reac)
                self.smx.DelReac(reac)
        return rv


    def OrphanMets(self):
        return self.smx.OrphanMets()


    def Core(self):
        """ pre: True
           post: A copy of the external stoichiometry matrix with all internal orphans
                 and associated reactions removed. """

        if self.smx.IsDeQuoted():
            self.smx.ReQuote()
            smx = self.Core()
            self.smx.DeQuote()
            smx.DeQuote()
        else:
            smx = self.smx.Copy()
            orphans  = Set.Complement(smx.OrphanMets(), smx.Externs)
            while len(orphans) > 0:
                print "len(o)", len(orphans)
                for met in orphans:
                    if not met in smx.Externs:
                        for rn in smx.InvolvedWith(met):
                            smx.DelReac(rn)
                        smx.DelRow(met)

                smx.ZapZeroes()
                orphans  = Set.Complement(smx.OrphanMets(), smx.Externs)

        return smx


    def DeadReactions(self):
        rv  = []
        k = self.sm.NullSpace()
        for r in k.rnames:
            if Seq.AllEq(k[r],0):
                rv.append(r)
        return rv

    def DisconnectsK(self, thresh=1e-6,exts=False):
        """ pre: True
           post: return list of lists of disconnected subsystems by null-space analysis of self.sm
                    exts => use the external stoichiometry matrix
                    first item is possible empty list of dead reactions
        """
        if exts:
            k = self.smx.OrthNullSpace()
        else:
            k = self.sm.OrthNullSpace()
        deads = []
        for reac in k.rnames[:]:
            if Seq.AllMatch(k[reac], lambda x:abs(x)<thresh):
                deads.append(reac)
                k.DelRow(reac)
        return k.NonZeroR([deads],thresh)


    def MaxCycles(self):
        return self.DisconnectsK(exts=1)[1:]


    def EnzSubsets(self):
        """ pre: True
            post: return EnzSubset.EssDic(self)
                     see EnzSubset module for details
        """

        return EnzSubset.EssDic(self)



    def ElModes(self, Alg = "C",   max_size = None):
        ''' pre     :   Alg = "B", "C" or "X" (Binary, Canonical or cpleX algorithm)
                        binary - use binary algorithm, else use canonical algorithm.
                        use_cplex - If True use cplex-based algorithm, only possible if cplex is installed.
                        max_size - maximum number of EMs to compute, only available if use_cplex.
                        
            post    :   return ModesDB() instance of self. 
        '''
        
        return ElModes.GetElModes(self,  Alg,  max_size)
        

    def ConsMoieties(self):

        smt = self.sm.Copy(tx = 1)
        ksmt = smt.NullSpace()
        deps = []
        for c in ksmt.cnames:
            for r in ksmt.rnames:
                if ksmt[r,c] != 0 and Seq.NumOfMatches(ksmt[r], lambda x: x!=0) ==1:
                    deps.append(r)
                    break

        for d in deps:
            ksmt.MakeFirstRow(d)
            c = deps.index(d)
            val = ksmt[0,c]
            ksmt.cnames[c] = d
            ksmt.DivCol(c,k=val)

        return ksmt


    def GetLP(self):
        
        return LP.lp(self)
       


##
    def ReacTree(self):

        def diff(a,b):
            return 1- abs(Sci.CosTheta(a,b))

        k = self.sm.OrthNullSpace()
        rdm = k.RowDiffMtx(diff)
        t= rdm.ToNJTree()
        t.rdm = rdm
        return t
