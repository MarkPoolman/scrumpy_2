

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

import sys,  types,  exceptions

import StoMat, Decompose
from clib.ElModes import ElModes
from clib.BitEM import BitEM
from ScrumPy.Util import   Set, Seq, Types, DynMatrix
from ScrumPy.Data import Histo
from ScrumPy import  LP


#from ScrumPy.LP import ScrumPyCMIP
#FIXME: !! This breaks if cplex not present !!


neg = lambda x: x<0
pos = lambda x: x>0

class holder:
    pass

def deb(msg):
    sys.__stderr__.write("ElModes.py" + msg + "\n")

class ModesDB:      # a data base type thing, to allow searching of elementary modes etc.

    def __init__(self, tabl,srcmodel,File=None,):
        #TODO: make sure all methods behave when binary algorithm is used!
        
        if File != None:
            self.ReadFile(File) 
            
        else:
            
            self.model = srcmodel
        
            self.mo = tabl
                
            missing = Set.Complement(self.model.sm.cnames, self.mo.rnames)
            for reac in missing:
                self.mo.NewRow(name=reac)
       
        
            self.mo.RowReorder(self.model.sm.cnames)
            self.sto =  srcmodel.smx.Mul(self.mo)
            self.sto.RevProps = tabl.RevProps
            
    def __getitem__(self,  i):
        return self.mo.cnames[i]




    def _retmosto(self):

        mo  = StoMat.StoMat(len(self.mo))
        mo.rnames = self.mo.rnames[:]
        return mo


    def __len__(self):
        if len(self.mo) >0:
            return len(self.mo[0])
        return 0

        
    def WriteFile(self,  File):
        '''
            pre     :   self.mo.cnames = self.model.sm.cnames
                        File - name of out file
            post    :   self.mo, self.model.sm(x),  written to File in current directory

        '''

        if type(File)== types.StringType:
            File = open(File, "w")
        self.mo.WriteFile(File)
        self.model.smx.WriteFile(File)
        self.model.sm.WriteFile(File)
        

    def ReadFile(self, File):
        '''
            pre     :   File created by self.WriteFile()
            post    :   self is ModesDB instance for modes in File.
        '''

        if type(File)== types.StringType:
            File = open(File)

        mo = StoMat.StoMat()
        mo.ReadFile(File)
        srcmodel = holder
        srcmodel.smx = StoMat.StoMat()
        srcmodel.smx.ReadFile(File)
        srcmodel.sm = StoMat.StoMat()
        srcmodel.sm.ReadFile(File)
        
        new_line = File.readline()
        while new_line == '\n':
            new_line = File.readline()
        
        self.__init__(mo, srcmodel)


    def NormByName(self,names):
        """ Normalise all modes and stos such that they consume 1 mol of the first
            species in  names that they utilise """
        
        for mo in self.mo.cnames:
            for name in names:
                coeff = self.sto[name,mo]
                if coeff <0:
                    self.mo.DivCol(mo, k=-coeff)
                    self.sto.DivCol(mo, k=-coeff)
                    break



    def Stos(self):
        "return a string repesentation of stoichiometries"
        return "".join(map(self.sto.ReacToStr, self.sto.cnames))

    
   
    def Modes(self,  **kwargs):
    
        retlist = []
        for e in self.mo.cnames:
            molist = [e]
            rd = self.mo.InvolvedWith(e)
            for r in rd:
                molist.append(" ".join((str(rd[r]),r)))
            retlist.append(", ".join(molist))
        return "\n\n".join(retlist)


    def LenHist(self,nbins=20):
        """ histogram of mode lengths. """

        lens  = map(lambda em: self.LenOf(em), self.mo.cnames)
        return Histo.Histo(lens,nbins=nbins)



    def _rowsearch(self, name, crit, mtx, Select=True,  **kwargs):

        if not Select:
            return self._rowsearch(name, lambda x: not crit(x), mtx)

        matches = mtx.InvolvedWith(name, crit).keys()
        mo = self.mo.SubSys(matches)
        return ModesDB(mo, self.model)


    def Consumes(self, name, Select=True):
        return self._rowsearch(name, lambda x: x<0, self.sto, Select)

    def Produces(self,name, Select=True):
        return self._rowsearch(name, lambda x: x>0, self.sto, Select)

    def Used(self, name, Select=True):
        return self._rowsearch(name, lambda x: x!=0, self.sto, Select)

    def Unused(self, name, Select=True):
        return self._rowsearch(name, lambda x: x==0, self.sto, Select)

    def PosFlux(self,name, Select=True):
        return self._rowsearch(name, lambda x: x>0, self.mo, Select)

    def SelectFlux(self, name, Select=True):
        return self._rowsearch(name, lambda x: x<0, self.mo, Select)

    def HasFlux(self, name, Select=True):
        return self._rowsearch(name, lambda x: x!=0, self.mo, Select)

    def NoFlux(self, name, Select=True):
        return self._rowsearch(name, lambda x: x==0, self.mo, Select)

    def PartSto(self, names,  **kwargs):
        rv = DynMatrix.matrix()
        for name in names:
            rv.NewCol(self.sto.GetCol(name), name)
        rv.rnames = self.sto.rnames
        return rv

    def ReacsOf(self, m):
        return self.mo.InvolvedWith(m)

    def ModesOf(self,  reac,  **kwargs):
        '''
            pre      :   reac in self.mo.rnames
            post    :   dictionary of modes involving reac (values are coefficients) has been returned
        '''
        return self.mo.InvolvedWith(reac)
    
    def ModeIntersect(self,  reacs):
        '''
            pre         :   reacs in self.mo.rnames
            post       :   list of modes involved with reacs has been returned    
        '''
        all_mds= {}
        for r in reacs:
            all_mds[r] = self.ModesOf(r).keys()
        
        return Set.Intersects(all_mds.values())
        
    

    def ConsumesAnyOf(self, names,   **kwargs):

        rvd = {}
        for name in names:
            rvd.update( self.sto.InvolvedWith(name, neg))

        ems = rvd.keys()
        mo = self.mo.SubSys(ems)

        return ModesDB(mo,  self.model)

    def ConsumesOnly(self, names,   **kwargs):

        rv = self.ConsumesAnyOf(names)
        for em in rv.sto.cnames[:]:
            if len(rv.sto.InvolvedWith(em, neg))!=1:
                rv.mo.DelCol(em)
                rv.sto.DelCol(em)

        return rv



    def ProducesAnyOf(self, names, **kwargs):
        rvd = {}
        for name in names:
            rvd.update( self.sto.InvolvedWith(name, pos))

        ems = rvd.keys()
        mo = self.mo.SubSys(ems)
     

        return ModesDB(mo,  self.model)

    def ProducesOnly(self, names,  **kwargs):
        
        rv = self.ProducesAnyOf(names)
        for em in rv.sto.cnames[:]:
            if len(rv.sto.InvolvedWith(em, pos))!=1:
                rv.mo.DelCol(em)
                rv.sto.DelCol(em)

        return rv


    def ProducesAtLeast(self, names,  **kwargs):
        '''
            pre         :       names is subset of self.sto.rnames
            
            post        :       The set of modes produces metabolites in names, and possibly other metabolites, has been returned.
        '''

        rv = self.ProducesAnyOf(names)
        for em in rv.sto.cnames[:]:
            if not Set.IsSubset(names, rv.sto.InvolvedWith(em, pos).keys()):
                rv.mo.DelCol(em)
                rv.sto.DelCol(em)
        
        return rv


    def Integise(self, RecalcSto = False):
        """ convert rational -> integer representation
            RecalcSto -  recalculate self.sto from self.mo and
            self.model.smx. If True, self.mo.cnames = self.model.smx.cnames
        """

        self.mo.IntegiseC()
        if RecalcSto:
            self.sto = self.model.smx.Mul(self.mo)
            self.sto.RevProps.update(self.mo.RevProps)
        else:
            self.sto.IntegiseC()




    def Futile(self):
        """Strictly internal cycles - ie no net stoichiometry"""

        ems = []
        for e in self.sto.cnames:
            if Seq.AllEq(self.sto.GetCol(e),0):
                ems.append(e)
        mo = self.mo.SubSys(ems)
        sto = self.sto.SubSys(ems)
        return ModesDB(mo,  self.model,  sto)

    def GetDupStos(self):
        self.sto.Transpose()
        rv = self.sto.DupRows()
        self.sto.Transpose()
        return rv


    def DelDupStos(self):
        """ remove elmodes with identical stoichs """
        for dups in self.GetDupStos():
            for d in dups[1:]:
                self.sto.DelCol(d)
                self.mo.DelCol(d)

    def MergeDupStos(self):
        """ merge elmodes with duplicate stoichs to form non-elementary modes """
        dups = self.GetDupStos()
        self.sto.Transpose()
        self.mo.Transpose()
        for dup in dups:
            for d in dup[1:]:
                self.sto.DelRow(d)
            self.mo.AddAndRemoveRows(dup)
        self.sto.Transpose()
        self.mo.Transpose()


    def Decompose(self,DataSet,**opts):
        return Decompose.Decompose(self, DataSet, **opts)


    def StoOf(self, ElMo, **kwargs):
        """ returns a string repn of the net Sto of ElMo"""
        return self.sto.ReacToStr(ElMo)



    def LenOf(self, mo):
        """ The number of reactions in mode, mo """

        return  len(filter(lambda x: x!=0,self.mo.GetCol(mo)))


    def Shortest(self):
        rv = []
        best = len(self.mo[0])
        for mode in self.mo.cnames:
            length = self.LenOf(mode)
            if length < best:
                rv = [mode]
                best = length
            elif length == best:
                rv.append(mode)
        return rv



    def Irrevs(self):
        """ pre: true:
           post: e.Irrves() == a posibly empty list of irreverible mode names
        """

        return filter(lambda em:self.IsIrrev(em),  self.mo.cnames)


    def IsIrrev(self, em):

        return self.mo.RevProps[em] == "->"
        
        
    def RenameEM(self,  Old,  New):
        
        for cnames in self.mo.cnames,  self.sto.cnames:
            cnames[cnames.index(Old)] = New
            
        self.sto.RevProps[New] = self.sto.RevProps[Old]
        del    self.sto.RevProps[Old]
        
        
    def SigDic(self):
        
        rv = {}
        for em in self.mo.cnames:
            sig = hash("".join(sorted(self.mo.InvolvedWith(em).keys())))
            rv[sig] = em
        
        return rv
        
    def ConsistentNames(self, other):
        """ rename EMs in self an other such that modes common to both have the same label nad unique lables otherwise"""
        
        SelfSigs = self.SigDic()
        for New, Old in SelfSigs.items():
            self.RenameEM(Old, New)
        
        OtherSigs = other.SigDic()
        for New, Old in OtherSigs.items():
            other.RenameEM(Old, New)
            
        CommonSigs = Set.Intersect(SelfSigs, OtherSigs)
       
        n_EM = 0
        for sig in CommonSigs:
            New = "ComEM_" + str(n_EM)
            self.RenameEM(sig, New)
            other.RenameEM(sig, New)
            n_EM+=1
            del SelfSigs[sig]
            del OtherSigs[sig]
            
        for sig in SelfSigs:
            New = "ElMo_" + str(n_EM)
            self.RenameEM(sig, New)
            n_EM+=1
            
        for sig in OtherSigs:
            New = "ElMo_" + str(n_EM)
            other.RenameEM(sig, New)
            n_EM+=1
            
        
        
        









class Tableau(StoMat.StoMat): 
    
    def __init__(self, sm):
        """
        Tableau using the canonical Schuster et al algorithn
        pre: sm = StoMat.StoMat(...)  """

         
        StoMat.StoMat.__init__(self, FromMtx=sm)
        if self.Conv != Types.ArbRat:
            self.ChangeType(Types.ArbRat)
       
        self.RevProps.update(sm.RevProps)
        self.Transpose()
        # start with a transposed copy of the stoichiometry matrix

        id = DynMatrix.GetIdent(len(sm.cnames))           # get ident matrix of same size
        id.cnames = sm.cnames[:]                          # ident col names will be reaction names
        self.AugCol(id)                                   # augment transposed stomat with ident

        self.nMets = len(sm.rnames)                       # Make data available in convenient form
        self.RowLen = len(self.rows[0])                   # for the C library
        self.IrrevIdxs = []                               # indices of irreversible reactions in self
        for name in sm.GetIrrevs():                          # from those in the original sm
            self.IrrevIdxs.append(self.rnames.index(name)+self.nMets)
            self.IrrevLen = len(self.IrrevIdxs)

        self.Results = []                                   # where the C lib will deposit the results


    def GetElModes(self):
        '''
                pre     :   self  - True. 
                post    :   self.cnames - elementary modes, self.rnames - reactions
        '''
        
        ElModes.ElModes(self)  # ElModes is the low-level C entity
           
        self.cnames = []
        self.rows=[]

        print "" , # very wierd - Idle ? - next line sometimes excepts without *some* write to stdout/stderr
        for i in self.rnames:
            self.rows.append([])
            
        for r in range(len(self.Results)):
            cname = "ElMo_" + str(r)
            self.NewCol(map(
                    self.Conv,
                    self.Results[r][self.nMets:]
                  ),
                  cname
            )
            
        self.BuildRP() # sort out the rerversibilities
        
        
    def BuildRP(self):
        
        oldrp = dict(self.RevProps)
        self.RevProps = {}
            
        for e in self.cnames:
            rps = map(lambda r: oldrp[r], self.InvolvedWith(e))
            rev = "<>"
            if "->" in rps:
                rev = "->"
            elif"<-" in rps:
                rev = "<-"
            self.RevProps[e] = rev
        
                
    
        
        
        
class BTableau(Tableau):
    
    def __init__(self, m):
        
        self.model = m
        
        ssm,  csm,  csmx = m.EnzSubsets().CondensedSMs()
        # subset mtx, condensed sm, condensed external sm
        self.ssm = ssm

        
        sm = LP.ScrumPyLP.SplitRev(csm,  Conv = Types.ArbRat)
        StoMat.StoMat.__init__(self, FromMtx=sm)
        
        k = sm.NullSpace()
        k.Transpose()   
        k.RedRowEch()  
        self.rnames = []
        self.cnames = k.cnames
        self.rows = []
        for r in range(len(k.rows)):
            self.NewRow(k.rows[r],k.rnames[r])
        self.nReac=len(self.cnames)
        self.BackIdx = []
        self.Backs = sm.Backs
        sm.ColReorder(k.cnames)
        cands = Set.Difference(sm.cnames,sm.Backs)
        for b in sm.Backs:
            for c in cands:
                if b.split('_back')[0]==c:
                    idx = [sm.cnames.index(c), sm.cnames.index(b)]
                    self.BackIdx.append(idx)
                    
        self.Results = []
                    
                    
    def GetElModes(self):
        '''
                pre     :   self  - True. 
                post    :   self.cnames - elementary modes, self.rnames - reactions.
        '''
    
        BitEM.BitEM(self) #low-level swig inteface to 
        self.rnames = self.cnames[:] #since rows in self.Results are added as columns,
                                     #results from clib are effectively transposed into self
     
           
        self.cnames = []
        self.rows=[]
        self.RevProps = {}
        print "" , # very wierd - Idle ? - next line sometimes excepts without *some* write to stdout/stderr
        for i in self.rnames:
            self.rows.append([])
            
        for r in range(len(self.Results)):
            cname = "ElMo_" + str(r)
            self.NewCol(self.Results[r],cname)

        self.UndoSplit()
        self.RemoveIsoRevs()
        
    def SimpleElModes(self):
        self.GetElModes()
        self.Expand()
        self.ToNumeric()
        self.RevProps = dict(self.model.sm.RevProps)
        self.BuildRP()
        

    def UndoSplit(self):
        ''' PRIVATE '''
        for b in self.Backs:
            back_row = self.GetRow(b)
            org_row = self.GetRow(b.split('_back')[0])
            for i in range(len(back_row)):
                bool = (back_row[i]!=org_row[i])
                self[b.split('_back')[0],i] = self.Conv(bool)   
            self.DelRow(b)
        #NB, no need to do anything with reversed reactions
        
        
    def RemoveIsoRevs(self):
        ''' PRIVATE. isostoichiometric modes, introduced by
        binary method if mode is reversible,  removed and modes reordered such that 
        self.cnames[0] = ElModes_0, ... self.cnames[n] = ElModes_n
        '''
        isos = self.FindIsoforms()
        if len (isos)>0:
            for iso in isos:
                if len(iso) == 2:
                    self.DelCol(iso[1])
                elif len(iso)>2:
                    sys.stderr.write("len"+str(iso)+"> 2! \n")
            for i in range(len(self.cnames[:])):
                cname = "ElMo_" + str(i)
                self.cnames[i] = cname
        
        
    def Expand(self):
        
        expanded = self.ssm.Mul(self)
        self.rnames,  self.cnames,  self.rows = expanded.rnames,  expanded.cnames,  expanded.rows
        
                
    
    def MakeNumerical(self, modes):
        ''' pre : calculated by binary algorithm '''
        
         
        def mode_is_wrong_way( k):
            #pre :   len(k.cnames)==1
            
            rv  = False
            for r in k.rnames:
                coeff = k[r,0]
                rev = self.model.sm.RevProps[r]
                if (coeff < 0 and rev == StoMat.t_Irrev) or (coeff > 0 and rev == StoMat.t_BackIrrev):
                    rv = True
            return rv


        mat = DynMatrix.matrix(rnames = self.model.sm.cnames)
        mat_dic = mat.ToDicDic()
        col_no = 0 
        for mode in modes:
            subsys = self.model.sm.SubSys(self.InvolvedWith(mode).keys())
            sub = subsys.NullSpace()
            sub_k = DynMatrix.matrix(FromMtx=sub)
            if len(sub_k.cnames) > 1:
                sys.stderr.write("Warning! Nullspace of "+mode+ " has > 1 column!\n")
            elif len(sub_k.cnames) == 0:
                sys.stderr.write("Warning! " + mode +" is not at steady state!\n")
            else:
                sub_k.cnames[0] = mode
                col_no+=1
                wrong_way = mode_is_wrong_way(sub_k)
                if wrong_way:
                    sub_k.MulCol(0,k=-1)
                    wrong_way = mode_is_wrong_way(sub_k)
                    if wrong_way:
                        sys.stderr.write("Warning! "+mode+" is inconsistent! \n")
                if not wrong_way:
                    sub_dict = sub_k.ToDicDic()
                    for k in sub_dict:
                        if mat_dic.has_key(k):
                            mat_dic[k].update(sub_dict[k])
                        else:
                            mat_dic[k]=sub_dict[k]
                   
        mat.FromDicDic(mat_dic)
        st_mat=StoMat.StoMat(FromMtx=mat)
        st_mat.binary=False
       
        return st_mat
        
        
    def ToNumeric(self):
        """ convert self from bool to numeric"""
        
        num = self.MakeNumerical(self.cnames)
        self.rnames,  self.cnames,  self.rows = num.rnames,  num.cnames,  num.rows

                        




class XTableau(Tableau):
    
    def __init__(self, m):
        
        self.m = m
        self.Conv = m.sm.Conv
        
    
    def GetElModes(self, MaxSize=None, Verbose=False):
        
        mip = LP.cmip(self.m)
        
        if not Verbose:
            mip.set_warning_stream(None)
            
        rawmodes  =mip.GetElMoBySize(MaxSize)
            
        modes = mip.PostProcess(rawmodes) #TODO: move this functionality here
        self.RevProps = modes.RevProps
        self.cnames = modes.cnames
        self.rnames = modes.rnames
        self.rows = modes.rows
        
        
        

def CanonEMs(m):
    
    ssm, sm, smx = m.EnzSubsets().CondensedSMs()
    
    if len(ssm.cnames) <2: # 0 or 1 subsets means 0 or 1 ems - nothing else to do
        rv = ssm
    else:
        t = Tableau(sm)
        t.GetElModes()
        ssm.ColReorder(t.rnames)
        rv = ssm.Mul(t)
        rv.RevProps = t.RevProps
        
    return rv
    
    
def CPlexEMs(m, MaxSize,  Verbose=False):
    
    t = XTableau(m)
    t.GetElModes(MaxSize, Verbose)
    return t
    
    
def BinEMs(m):
    
    t = BTableau(m)
    t.SimpleElModes()
    return (t)
    

def GetElModes(m, Alg,  MaxSize=None):
    
    Alg = Alg.upper()
    
    if Alg != "X" and MaxSize != None:
        print "! Warning: MaxSize ignored (Alg must be 'X') !"
        
    if Alg == "X":
        modes = CPlexEMs(m,  MaxSize)
    elif Alg == "B":
        modes = BinEMs(m)
    elif Alg == "C":
        modes = CanonEMs(m)
    else:
        raise exceptions.NameError("Unknown elementary modes algorithm %s (must  be 'B', 'C' or 'X')"  %Alg)
        
    return ModesDB(modes, m)


#def FancyGetElModes(sm):
#
#    def ModSort(row, mtx):
#        return Sci.Mod(mtx[row])
#    sort = Sorter.Sorter()
#    for r in sm.rnames:
#        sort.append(r)
#    sorted = sort.Sort(ModSort,mtx=sm)
#    print sorted
#    sm.RowReorder(sorted)
#    t = Tableau(sm)
#
#    t.GetElModes()
#
#    return t
#



