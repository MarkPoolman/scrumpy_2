

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

import  types

from ScrumPy.Util import DynMatrix, Seq,Set,Sci,  Types
# NB "types" is the standard module, "Types" contains some local type defs

from ScrumPy.ModelDescription.lexer import t_Irrev,  t_Rever, t_BackIrrev
# t_BackIrrev is imported by other modules - ignore warnings from eric

# Exceptions
NoSuchMet = "No such metabolite"
NoSuchreac = "No such reaction"
DupReac = "Duplicate reaction"

class StoMat(DynMatrix.matrix):

    def __init__(self, r=0, c=0, **kwargs):

        DynMatrix.matrix.__init__(self,r,c, **kwargs)
        self.RevProps = {}
        self.Externs = []
        #self.Irrevs = self.GetIrrevs()


        # TODO: StoMat needs a proper copy constructer


    def DelRow(self, r):

        if type(r) == types.IntType:
            r = self.rnames[r]

        if r in self.Externs:
            self.Externs.remove(r)

        DynMatrix.matrix.DelRow(self,r)


    def DelReac(self, c):

        if type(c) == types.IntType:
            c = self.cnames[c]

        del self.RevProps[c]
        DynMatrix.matrix.DelCol(self,c)



    def WriteFile(self,  File):

        if type(File) == types.StringType:
            File = open(File, "w")

        DynMatrix.matrix.WriteFile(self, File)

        print >>File,  "\nself.RevProps=",  self.RevProps


    def ReadFile(self, File):

        if  type(File) == types.StringType:
            File = open(File)

        DynMatrix.matrix.ReadFile(self, File)

        rp = File.readline()
        
        exec rp
        #self.RevProps=RevProps





    def MakeExtern(self, name):

        self.DelRow(name)
        self.Externs.append(name)
        
    def GetRevs(self):
        return filter(lambda reac: self.RevProps[reac] == t_Rever,  self.cnames)

    def GetIrrevs(self):
       return filter(lambda reac: self.RevProps[reac] != t_Rever,  self.cnames)

    def MakeIrrev(self,name):

        self.RevProps[name] = t_Irrev


    def MakeRevers(self,name):

        self.RevProps[name] = t_Rever


    def InvolvedWith(self, name, crit=None):
        if crit==None:
            def crit(x):
                return x!=0


        if name in  self.cnames:
            list = self.GetCol(name)
            namelist = self.rnames
        elif name in self.rnames:
            list = self.GetRow(name)
            namelist = self.cnames
        else:
            print "name ", name, "not found"
            list = []

        rv = {}
        for idx in range(len(list)):

            if crit( list[idx]):
                rv[namelist[idx]] = self.Conv(list[idx])

        return (rv)


    def Products(self, Reaction):
        return self.InvolvedWith(Reaction, lambda x: x>0).keys()

    def Substrates(self, Reaction):
        return self.InvolvedWith(Reaction, lambda x: x<0).keys()

    def Reactants(self, Reaction):
        """ List of reactant names involved with Reaction """
        return self.InvolvedWith(Reaction).keys()

    def Transporters(self):
        rv = {}
        for met in self.Externs:
            rv.update(self.InvolvedWith(met))
        return rv.keys()


    def OrphanMets(self):

        rv = []

        for met in self.rnames:
            if len(self.InvolvedWith(met))==1:
                rv.append(met)

        return rv


    def Delete(self, name):
        if name in self.cnames:
            self.DelReac(name)
            for name in self.rnames:                 # ugly, find nicer way
                if Seq.AllEq(self.GetRow(name), 0):
                    self.DelRow(name)
        elif name in self.rnames:
            self.DelRow(name)
            for name in self.cnames:
                if Seq.AllEq(self.GetCol(name), 0):
                    self.DelReac(name)
        else:
            print "can't find ", name



    def SetSto(self, reac,  Sto,  reset = False):
       

        if reset:
            self.MulCol(reac, k=0)

        for met in Sto.lhs:
            coeff = met[0]
            name = met[1]
            self[name,reac] = coeff
            self[name,reac] *= -1 # cos coeff is a string until it gets into self

        for met in Sto.rhs:
            coeff = met[0]
            name = met[1]
            self[name,reac] = coeff

        self.RevProps[reac] = Sto.Revers
        
        

    def NewReaction(self, reac, StoDic,  Rev= t_Rever):

        if reac in self.cnames:
            raise DupReac

        self.NewCol(name=reac)
        for met in StoDic.keys():
            if not met in self.rnames:
                self.NewRow(name=met)
            self[met,reac]=StoDic[met]

        self.RevProps[reac] = Rev




    def Copy(self, *args,**kwargs):

        rv =  DynMatrix.matrix.Copy(self,*args, **kwargs)
        rv.RevProps = dict(self.RevProps)

        rv.Externs = self.Externs[:]

        return rv


    def ReacToStr(self,reac):

        def d2l(d):
            rv = []
            for k in d.keys():
                rv.append(" ".join((str(d[k]),k)))
            return rv

        substs = self.InvolvedWith(reac, lambda x: x<0)
        for k in substs.keys():
            substs[k] *= -1
        substs = d2l(substs)


        prods = d2l(self.InvolvedWith(reac, lambda x: x>0))

        sep = self.RevProps[reac].join((" "," "))



        rv = reac+":\n\t" + " + ".join(substs) + sep + " + ".join(prods) + "\n\t~\n"

        if len(substs) ==0 or len(prods)==0:
            print reac, "missing substrate or product !"
            rv = "#"+ "\n#".join(rv.split("\n"))+"\n"

        return rv
        
        
        
    def AsEqn(self,c):

	rhs = []

	elems = self.InvolvedWith(c)
	for id,coeff in elems.items():
		if coeff == 1:
			coeffstr = ""
		else:
			coeffstr = str(coeff) + "*"
		
		rhs.append(coeffstr + id)
	return "+".join(rhs) + "-" + c

    def AsEqns(self):
        return map(lambda c:self.AsEqn(c), self.cnames)

    def IntegiseReacs(self):
        for reac in self.cnames:
            col = self.GetCol(reac)
            if Seq.AllEq(col, 0):
                self.DelCol(reac)
            else:
                #Seq.Normalise(col,KeepSign=False)
                self.SetCol(reac, Seq.Integise(col))


    def NiceOrder(self):

        txs = self.Transporters()
        interns = Set.Complement(self.cnames,txs)
        txs.sort()
        interns.sort()
        self.ColReorder(txs+interns)


    def ElTypeAsScrumPy(self):

        return Types.TypeToDirective[type(self.Conv(1))]




    def ToScrumPy(self, QuoteD, File, Head="Structural()\nDeQuote()\n", Externs=[]):
        #TODO: pick up QuoteD and externs from the parent model where possible,
        
        Externs = filter(lambda s: not s.upper().startswith("X_"), Externs)
        Externs = Set.Intersect(self.rnames, Externs)
        
        self.cnames = map(lambda s: QuoteD[s], self.cnames)
        self.rnames = map(lambda s: QuoteD[s], self.rnames)
        Externs =  map(lambda s: QuoteD[s], Externs)
        
        if len(Externs) >0:
            Head += "External("+ ", ".join(Externs)+")\n\n"
        
        for r in self.RevProps.keys():
            self.RevProps[QuoteD[r]] = self.RevProps[r]
        
        Body = "\n".join(map(lambda r: self.ReacToStr(r), self.cnames))

        self.cnames = map(lambda s: QuoteD[s], self.cnames)
        self.rnames = map(lambda s: QuoteD[s], self.rnames)
        
        if not hasattr(File,"write"):
            File = open(File,"w")
        File.write(Head+Body)
        
        
    def IsIrrev(self,name):
        return  self.RevProps[name] != t_Rever


    def Connectedness(self, met):
        return Seq.NumOfMatches(self[met], lambda x: x != 0)


    def Unbals(self):
        " return list of mets that do not have >0 producer and >0 consumer "

        rv = []

        for m in self.rnames:
            row = self[m]
            if not (Seq.HasMatches(row, lambda x: x>0) and Seq.HasMatches(row, lambda x: x<0)):
                rv.append(m)
        return rv


    def RateVector(self,  FluxDic={}, Conv=None,  Name="RateVec"):
        """ pre: FluxDic keys are subset of self.cnames
                    FluxDic values can be converterted to self.Conv
           post: rv = self.RateVector() =>
                      rv.Dims() == (len(self.cnames),1)
                      rv.Conv == self.Conv
                      rv[f,0] == FluxDic[f] or zero """
                      
        if Conv==None:
            Conv = type(FluxDic.values()[0])
            
        

        #rv =  DynMatrix.matrix(ncol=1,rnames = self.cnames,cnames=[Name], Conv=self.Conv)
        rv =  StoMat(rnames = self.cnames,cnames=[Name], Conv=self.Conv)
        for f in FluxDic:
            rv[f, 0] = FluxDic[f]
        
        return rv


    def SubSys(self, reacs):

        rv = StoMat(cnames=reacs)

        for reac in reacs:
            stod = self.InvolvedWith(reac)
            for met in stod.keys():
                if not met in rv.rnames:
                    rv.NewRow(name=met)
                rv[met, reac] = self[met, reac]
                
            rv.RevProps[reac] = self.RevProps[reac]

        return rv




    def FindIsoforms(self):
        stodic = {}
        for reac in self.cnames:
            strsto = str(self.InvolvedWith(reac))
            if stodic.has_key(strsto):
                stodic[strsto].append(reac)
            else:
                stodic[strsto] = [reac]
        rv = []
        for reac in stodic.values():
            if len(reac)>1:
                rv.append(reac)

        return rv




    def OrthSubSysts(self,tol=1e-6):
        """ a list of lists of reaction names of the orthogonal subsystems in self,
        The first, possibly empty, list contains the dead reactions """

        k = self.OrthNullSpace()
        dead = []
        for reac in k.rnames[:]:
            if max(map(abs, k[reac])) <tol:
                dead.append(reac)
                k.DelRow(reac)

        rv = [dead]

        while len(k.rnames) >0:
           ref = k.rnames[0]
           curss = [ref]

           for reac in k.rnames[1:]:
               if not Sci.AreOrthogonal(k[ref],k[reac],tol):
                   curss.append(reac)
                   k.DelRow(reac)

           k.DelRow(ref)
           rv.append(curss)
        return rv


    def ReactionNJTree(self):
        k = self.OrthNullSpace()
        t = k.RowDiffMtx(Sci.Theta).ToNJTree()
        t.Consolidate(thresh= 1e-8)
        return t
        
          
    def Neighbours(self, id):
        """ pre: id in self.cnames or id in self.rnames
           post: id in self.cnames => list of neighbouring reactions
                 id in self.rnames => list of neighbouring metabolites
        """
        
        rv = {}

        for n1 in self.InvolvedWith(id):
            rv.update(self.InvolvedWith(n1))

        return rv.keys()
                    
            
    def ConnectedNet(self, id):
        """ pre: Neighbours(self, id)
           post: id in self.cnames => network of reactions that are (in)direct neigbors of id
                 id in self.rnames => network of metabolites that are (in)direct neigbors of id
        """
        
        def ConNet (id):
            if not rv.has_key(id):
                rv[id]=1
                for n in self.Neighbours(id):
                    ConNet(n)
                    
        rv = {}
        ConNet(id)
        return rv.keys()



    def ConnectedNets(self):
        """ pre: True
           post: list of lists connected networks in self
        """

        rv = []
        sm = self.Copy()

        while len(sm.cnames)>0:
            net = self.ConnectedNet(sm.cnames[0])
            for n in net:
                sm.DelCol(n)
            rv.append(net)

        return rv
        


    def AdjMtx(self):
        """ The integer adjacency matrix of self """

        rv = DynMatrix.matrix(Conv = int, rnames = self.rnames, cnames = self.cnames)
        for met in self.rnames:
            reacs = self.InvolvedWith(met).keys()
            for r in reacs:
                for neighbour in self.InvolvedWith(r).keys():
                    rv[r,neighbour] = 1
        return rv
















