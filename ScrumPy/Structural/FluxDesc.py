


#
##
### FluxDesc: A dictionary-like class to hold and analyse flux vectors
##
#

import types,  exceptions

class FluxDesc:
    
    IsFluxDesc = True # indicate that we are an instance of FLuxDesc or sub-class thereof
    
    def __init__(self,  names=[], fluxes=[], model=None, UseAll=False):
        """pre: len(fluxes) == len(names) OR fluxes == [],
                   model != None: IsSubset(names, model.sm.cnames)
                   UseAll: model != None"""
       
        self.model = model
        self.UseAll = UseAll
        if model != None:
            self.__initWithModel__ (names,  fluxes)
        else:
            self.__initNoModel__(names, fluxes)
            
    
    def __initWithModel__(self,  names, fluxes):
        
        if self.UseAll:
            self.__initNoModel__(self.model.sm.cnames, [])
            for name, val in zip(names, fluxes):
                self[name] = val
                
        else:
            self.__initNoModel__(names, fluxes)
            
                
            
        
    def __initNoModel__(self, names, fluxes):
        
        if fluxes == []:
            self.fluxes = [0.0] * len(names)
        else:
            self.fluxes = fluxes[:]
        
                    
        self.IdxMap = {}  # map names to indices
        idx = 0
        for name in names:
            self.IdxMap[name] = idx
            idx+=1
            
        self.names = names[:] # so that we can maintain the order
           
    
    def __getidx__(self, i):
        try:
            if type(i) == types.StringType:
                i = self.IdxMap[i]
            return i
        except:
            raise exceptions.KeyError(i)
        
        
    def __getitem__(self, i):
        return self.fluxes[self.__getidx__(i)]
        
        
    def __setitem__(self, i, val):
        self.fluxes[self.__getidx__(i)] = val
        
        
    def __len__(self):
        return len(self.names)
        
        
    def has_key(self, k):
        try:
            self.__getidx__(k)
            return True
        except:
            return False
    
    
    def keys(self):
        return self.names[:] # order of names corresponds to order of self.fluxes
    
    
    def values(self):
        return self.fluxes[:]
        
    def _CheckHaveModel(self):
        if self.model==None:
            raise exceptions.AttributeError("No model associated with this Flux description")
   
   
    def AsTuple(self):
        """pre: True
          post: -> flux values as a tuple in order of names
        """
        return tuple(self.fluxes)
        
        
    def AsWholeTuple(self):
        """" pre: True
            post: IF Associated with a model return a tuple of fluxes such that dMet/dT = self.model.sm.Mul(self.AsWholeTuple)
                    ELSE raise ValueError
        """
        
        self._CheckHaveModel()
            
        if self.UseAll:
            return self.AsTuple()
        else:
            return FluxDesc(self.names, self.fluxes, self.model,  UseAll=True).AsTuple()
            
        
            
        
        
            
        
        
            
        


        
        
