/*
BitEM header file. Structure in mainly indended for development, scope of header is likely to narrow 
as project develops.
*/

#ifndef BITEM_H
#define BITEM_H

#include "SetList.h"
#include "ARVecList.h"
#include "Py2C.h" 
#include "IntVecList.h"
#include<Python.h>



/*#include "Py2C.h" */

/******************** typedefs *****************************/

typedef struct HybTab *HybTab_t ;



 /************* housekeeping ***************/

HybTab_t NewHybTab(ulint reacs) ;
	/* pre : reacs -  number of reactions in condensed sparse sm
	  post : memory for HybTab_t has been allocated and usable HybTab_t returned
	*/
	


HybTab_t HybTablInitFrPy(PyObject *ptab) ;
	/* pre : ptab is a correctly initialised Python tableau
	  post : HybTab_t representation of ptab has been returned
	*/
	



void DelHybTab(HybTab_t hyb) ;
	/* pre : hyb is usable
	  post : memory allocated to hyb has been freed
	*/


ulint HybTabnReacs(HybTab_t hyb) ;
	/* pre : hyb is initialised
	  post : hyb->nReac has been returned
	*/



/**************** algorithm *****************/



HybTab_t EMCalc(HybTab_t hyb) ;
	/* pre : hyb usable. hyb->NumTabl sparse, condensed kernel matrix of ScrumPy sm, such that each 
		ARVec in NumTabl corresponds to a colvector in sm.
		hyb->SetTabl is empty.
		
	 post : Copy of hyb->NumTabl is processed.
	 	All elementry modes in original sm are expressed as sets in copy of hyb->SetTabl.
		Copy of hyb has been returned.
	*/



HybTab_t ProcessRow(HybTab_t hyb) ;
	/* pre : hyb usable. hyb->currRow <= HybTabnReacs(hyb), hyb->SetTabl->len >= hyb->NumTabl->len
	
	  post : hyb->NumTabl[hyb->currRow, :] >= 0,  hyb->SetTabl[hyb->currRow, :] = (1|0) 
	  	(all pos elements in NumTabl up to currRow are non-negative, equivalent positions in 
		SetTabl are True (1, if positive) or False (0, if zero), all vectors with negative elements at index 
		currRow have been removed and combined with positve vectors by EnumerateCand()) ;
	  	hyb->NumTabl->len' >= hyb->NumTabl->len, hyb->SetTabl->len' >= hyb->SetTabl->len
		(new elementry modes have potentially been added to bottom of NumTabl and SetTabl
		by EnumerateCand())
	*/

void EnumerateCand(HybTab_t hyb, ARVecList_t NumPos, ARVecList_t NumNeg, SetList_t SetPos, SetList_t SetNeg) ;
	/* pre : hyb as required by ProcessRow(), NumPos/Neg subset of hyb->NumTabl such that element 
		at hyb->currRow is pos/neg for all vectors, SetPos/Neg subset of hyb->SetTabl such that element 
		at hyb->currRow is 1/0 for all vectors. NumPos->len==SetPos->len and NumNeg->len==SetNeg->len.

	  post : all adjacent combinations of SetNeg and SetPos have been added to hyb->SetTabl, corresponding
	  	combinations of NumNeg and NumPos added to hyb->NumTabl.
	*/
	
void MakeCands(HybTab_t hyb,ARVecList_t NP,ARVecList_t NN,SetList_t SP,SetList_t SN,SetList_t CS,ARVecList_t CN) ;
	/*  pre	: hyb, NP, NN, SP, SN as required of hyb, NumPos, NumNeg, SetPos, SetNeg, respectively by 
		  EnumerateCand().
		  CS and CN are empty lists.
		    
	   post : CS (list of sets) and CN (list of numerical vectors) have been populated with all
	          combinations of positive (NP and SP) and negative (NN and SN) vectors such that
		  no vector is a subset of another vector.
	
	*/

void CheckCandModeList(Set_t cand_mode, SetList_t cand_list, ARVec_t comb_nvec, ARVecList_t comb_arvl) ;
	/*  pre	: cand_mode and comb_nvec are of same effective lenght (SetMax(cand_mode) >= ARVecLen(comb_nvec)).
		  cand_list and comb_arvl are (possibly empty) lists.
		    
	   post : cand_mode and comb_nvec have been added to cand_list and comb_arvl, respectively, if 
	   	  cand_mode is not a superset of any preexisting sets in cand_list. Any modes that are supersets 
		  of cand_mode have been removed from cand_list (corresponding modes have been removed from comb_arvl).
	*/
	
Bool CheckCMode(Set_t cand_mode, SetList_t cand_list, ARVec_t comb_numvec, ARVecList_t comb_arvlist, Bool *cand_del) ;
	/*  pre	: Arguments as required by CheckCandModeList(). *cand_del is FALSE.
		    
	   post : If cand_mode is a superset of current vector in cand_list, current vectors of
	          cand_list and comb_arvlist have been changed to the bottom vectors, cand_mode (and comb_numvec)
		  have been deleted and *cand_del has been changed to TRUE, thus preventing CheckCandModeList() 
		  from using cand_mode and comb_numvec. If current vector in cand_list is superset of 
		  cand_mode it has been removed from cand_list (and current vector in comb_arvlist has been deleted).

	*/

Bool AdjacentTest(HybTab_t hyb, Set_t set_new) ;
	/* pre : hyb as required by EnumerateCand(). set_new is a vector from set list processed by MakeCands().

	  post : If set_new is not a superset of any preexisting modes in hyb->SetTabl, function is TRUE. 
	  If true, bitwise OR has been added to
	  hyb->SetTabl.
	*/	

ARVec_t CombineNumVecs(ulint idx, ARVec_t PosVec, ARVec_t NegVec) ;	
	/*  pre : 0 <= idx <= hyb->currRow (for hyb as required ). 
		  PosVec[hyb->currRow] >= 0, NegVec[hyb->currRow] < 0.
	
	   post : Gaussian combination (eq. 9 in G&K (2005)) of NumTabPos/Neg has been returned.
	*/





/********** auxillaries ************/
Bool AnyNegs(HybTab_t hyb) ;
	/*  pre : hyb usable, ARVLLen(hyb->NumTabl) == SetLLen(hyb->SetTabl)
	
	   post : If any element at index hyb->currRow in hyb->NumTabl > 0, corresponding
	   	  index in hyb->SetTabl has memeber. If any element at index hyb->currRow 
		  in hyb->NumTabl < 0, Bool TRUE has been returned, else FALSE.
	*/
	
void SortEls(HybTab_t hyb, SetList_t set_pos, SetList_t set_neg, ARVecList_t num_pos, ARVecList_t num_neg) ;
	/*  pre : hyb usable, ARVLLen(hyb->NumTabl) == SetLLen(hyb->SetTabl).
		  set_pos, set_neg, num_pos, num_neg are empty, usable structures. 
	
	   post : All vectors in hyb->NumTabl with element > 0 (< 0) at hyb->currRow have been copied to int_pos
	   	  (extracted from hyb-> num_neg), corresponding vector in hyb->SetTabl has been copied to set_pos 
		  (set_neg) with memeber at hyb->currRow deleted (keept empty).
	*/
	
Bool CondModVecs(HybTab_t hyb, SetList_t set_pos, SetList_t set_neg, ARVecList_t num_pos, ARVecList_t num_neg) ;
	/*  pre : Arguments as required by SortEls(...)
	
	   post : If element hyb->currRow in current hyb->NumTabl > 0 (< 0) vector is copied to
	   	  num_pos (extracted to num_neg). The same actions are carried out for set_pos and 
		  set_neg, respectively. Returs True if extracted vector was top node.
	   
	*/
	
	
Bool CheckAdj(HybTab_t hyb, Set_t set_new) ;

HybTab_t HybTabDup(HybTab_t hyb) ;
	/* pre : hyb hybrid tableau with content
	  post : conten of hyb copied and returned, *LCur(hyb/rv->*Tabl)==*LBot(hyb/rv->*Tabl)
	*/


Bool IsDone(HybTab_t hyb) ;
	/* pre : hyb usable.
	  post : if (hyb->currRow -1) == hyb->nReac (all reactions have been processed) algorithm 
	  is done and function evaluates to true, else false.
	*/
	
Bool IsLastRow(HybTab_t hyb) ;
	/* pre : hyb usable.
	  post : if hyb->currRow == (HybTabnReacs(hyb) = 1) curent row is the last row 
	  	 function evaluates to true
	*/
		
int GetSetZeros(Set_t mode, ulint max_idx) ;
	/* pre : SetMax(mode) <= nreacs
	  post : The number of empty indeces in mode[0 ... max_idx] has been returned.
	*/


Bool OKNumberZeros(Set_t mode, ulint nreacs, ulint min_zero) ;
	/* pre : SetMax(mode) <= nreacs, min_zero - hyb->MinZeros.
	  post : If mode has more than min_zeros zero elements it may be elementary, 
	         else it can be discarded.
	*/

void RemoveBFFutiles(HybTab_t hyb) ;
	/* pre : hyb usable, IsDone(hyb)
	  post : All modes in hyb->SetTabl which only involves forward-backwards
	  	 reaction pairs have been removed.	
	*/

Bool CondDelMode(Set_t cand_mode, SetList_t targ_mode_set) ;
	/* pre : SetMax(cand_mode) == SetMax(SetLCur(targ_mode_set))
	  post : If cand_mode is equal to current set in targ_mode_set,
	  	 current set in targ_mode_set has been deleted.
	*/	


void CTabl2PyTabl(PyObject *ptabl, HybTab_t hyb) ;


extern int *PyIntList2CIntArry(PyObject *pilist) ;
	/* pre: pilist is a Python list of integers
	  post: returns a newly calloc()'d array of pilist integer contents */

extern IntVecList_t PyListList2IntVecList(PyObject *pilistlist) ;
	/* pre: pilistlist is a Python list of lists of integers
	  post: returns the IntVecList_t equivalent of pilistlist */



/************** top-level: call low-level functions and export results to Python ******/


void GetBitEM(PyObject *ptab) ;
	/* pre: ptabl is a correctly initialised Python tableau
	  post: ptabl' contains the elementary modes
	 */
	 


extern PyObject *BitEM(PyObject *self, PyObject *args);
	/* Function called from Python  */


#endif
