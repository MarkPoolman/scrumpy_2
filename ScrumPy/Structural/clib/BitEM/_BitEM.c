/*
C file for implementation of BitEM algorithm. See BitEM.py for original Python prototype.

*/

#include <stdio.h>
#include <stdlib.h>



#include "_BitEM.h"


/** other typedefs inherited from vecList, as in original **/

struct HybTab {
	IntVecList_t IntTabl,  BackFutiles; /* List of indices of split reversible reactions */
	
	ARVecList_t NumTabl ;/* List of arb rat vectors, which are colvectors, not rowvectors */
	
	SetList_t SetTabl ; /* List of Sets, boolean colvectors */
	
	ulint currRow, nReac, MinZeros ; /* currRow is current row being processed, nReac numberof reacs in model
					MinZeros is the minimal number of zeros in an EM in the last row*/
} ;


 /************* housekeeping ***************/



HybTab_t NewHybTab(ulint reacs) {
	/* pre : reacs -  number of reactions in condensed sparse sm
	  post : memory for HybTab_t has been allocated and usable HybTab_t returned
	*/
	
	HybTab_t rv ;
	
	rv = (HybTab_t) malloc(sizeof(struct HybTab)) ;
	rv->NumTabl = ARVLNew() ;
	rv->SetTabl = SetLNew() ;
	rv->BackFutiles = IntVLNew() ;
	rv->currRow = 0 ;
	rv->nReac = reacs ;
	rv->MinZeros = 0 ; /* must be set by other means (e.g. HybTablInitFrPy or manual) */
	
	return (rv) ;
}

/* 
 
In Python: make nReac available as self.nReac
*/


HybTab_t HybTablInitFrPy(PyObject *ptab) {
	/* pre : ptab is a correctly initialised Python tableau
	  post : HybTab_t representation of ptab has been returned
	*/

	HybTab_t rv ;
	Set_t empty_set ;
	ulint reacs, n ;
	ARVecList_t copy_arvlist ;
	IntVecList_t copy_intlist ;
	

	rv = NewHybTab(PyInt_AsUnsignedLongMask(PyObject_GetAttrString(ptab, "nReac"))) ;
	
	copy_arvlist = PyListList2ArVecList(PyObject_GetAttrString(ptab, "rows")) ; /* note this should 
										be m.sm.NullSpace().Transpose() 
										from scrumpy */
								
	copy_intlist = PyListList2IntVecList(PyObject_GetAttrString(ptab, "BackIdx")) ; /* list of list of
											 idx of back and forward
											rxn, if any mode only 
											involves any of these sets,
										it is futile.
											*/
	
	
	ARVLCur2Top(copy_arvlist) ;
	for (n = 0 ; n < ARVLLen(copy_arvlist) ; n++){
		ARVLIns(rv->NumTabl, ARVecDup(ARVLCur(copy_arvlist))) ;
		ARVLCurNext(copy_arvlist) ;	
	}
	
	
	IntVLCur2Top(copy_intlist) ;	
	for (n = 0 ; n < IntVLLen(copy_intlist) ; n++){
		IntVLIns(rv->BackFutiles, IntVecDup(IntVLCur(copy_intlist))) ;
		IntVLCurNext(copy_intlist) ;
	}								
									
	
	ARVLCur2Top(rv->NumTabl) ;
	for(n = 0 ; n < ARVLLen(rv->NumTabl) ; n++){
		empty_set = SetNew(HybTabnReacs(rv)) ;
		SetLIns(rv->SetTabl, empty_set) ;
		ARVLCurNext(rv->NumTabl) ;
	}
	
	
	rv->MinZeros = SetLLen(rv->SetTabl) - 1 ; /* MinZeros = q-m-1, where (q-m) is dim of I in K' = [I / K],
						       since I is square (q-m) is also the number of vectors in 
							SetTabl	*/
	
	ARVLDel(copy_arvlist) ;
	IntVLDel(copy_intlist) ;
	
	return (rv) ;
}

// HybTab_t HybTablInitFrPy(PyObject *ptab) {
// 	/* pre : ptab is a correctly initialised Python tableau
// 	  post : HybTab_t representation of ptab has been returned
// 	*/
// 
// 	HybTab_t rv ;
// 	Set_t empty_set ;
// 	ulint reacs, n ;
// 	
// 
// 	rv = NewHybTab(PyInt_AsUnsignedLongMask(PyObject_GetAttrString(ptab, "nReac"))) ;
// 	
// 	rv->NumTabl = PyListList2ArVecList(PyObject_GetAttrString(ptab, "rows")) ; /* note this should 
// 										be m.sm.NullSpace().Transpose() 
// 										from scrumpy */
// 	
// 										
// 	rv->BackFutiles = PyListList2IntVecList(PyObject_GetAttrString(ptab, "BackIdx")) ; /* list of list of
// 											 idx of back and forward
// 											rxn, if any mode only 
// 											involves any of these sets,
// 											it is futile.
// 											*/
// 	
// 	ARVLCur2Top(rv->NumTabl) ;
// 	for(n = 0 ; n < ARVLLen(rv->NumTabl) ; n++){
// 		empty_set = SetNew(HybTabnReacs(rv)) ;
// 		SetLIns(rv->SetTabl, empty_set) ;
// 		ARVLCurNext(rv->NumTabl) ;
// 	}
// 	
// 	rv->MinZeros = SetLLen(rv->SetTabl) - 1 ; /* MinZeros = q-m-1, where (q-m) is dim of I in K' = [I / K],
// 						       since I is square (q-m) is also the number of vectors in 
// 							SetTabl	*/
// 	
// 	return (rv) ;
// }
// 




void DelHybTab(HybTab_t hyb) {
	/* pre : hyb is usable
	  post : memory allocated to hyb has been freed
	*/
	ARVLDel(hyb->NumTabl) ;
	SetLDel(hyb->SetTabl) ;
	IntVLDel(hyb->BackFutiles) ;
	free(hyb) ;
}


ulint HybTabnReacs(HybTab_t hyb) {
	/* pre : hyb is initialised
	  post : hyb->nReac has been returned
	*/
	
	return (hyb->nReac) ;
}


/**************** algorithm *****************/



HybTab_t EMCalc(HybTab_t hyb) {
	/* pre : hyb usable. hyb->NumTabl sparse, condensed kernel matrix of ScrumPy sm, such that each 
		ARVec in NumTabl corresponds to a colvector in sm.
		hyb->SetTabl is empty.
		
	 post : Copy of hyb->NumTabl is processed.
	 	All elementry modes in original sm are expressed as sets in copy of hyb->SetTabl.
		Copy of hyb has been returned.
	*/
	
	HybTab_t out_tab ;
	
	out_tab = HybTabDup(hyb) ;
	
	
	
	while (!IsDone(out_tab)){ 
		out_tab = ProcessRow(out_tab) ;
		fprintf(stderr,"curr set size: %lu\n",SetLLen(out_tab->SetTabl)) ;
		out_tab->currRow++ ;
	}
	
	return (out_tab) ;
}


HybTab_t ProcessRow(HybTab_t hyb) {
	/* pre : hyb usable. hyb->currRow <= HybTabnReacs(hyb), hyb->SetTabl->len >= hyb->NumTabl->len
	
	  post : hyb->NumTabl[hyb->currRow, :] >= 0,  hyb->SetTabl[hyb->currRow, :] = (1|0) 
	  	(all pos elements in NumTabl up to currRow are non-negative, equivalent positions in 
		SetTabl are True (1, if positive) or False (0, if zero), all vectors with negative elements at index 
		currRow have been removed and combined with positve vectors by EnumerateCand()) ;
	  	hyb->NumTabl->len' >= hyb->NumTabl->len, hyb->SetTabl->len' >= hyb->SetTabl->len
		(new elementry modes have potentially been added to bottom of NumTabl and SetTabl
		by EnumerateCand())
	*/
	ARVecList_t num_pos, num_neg ;
	SetList_t set_pos, set_neg ;
	int n ;
	num_pos = ARVLNew() ;
	num_neg = ARVLNew() ;
	set_pos = SetLNew() ;
	set_neg = SetLNew() ;
	
	// fprintf(stderr,"curr row: %d\n",hyb->currRow) ;
// 	fprintf(stderr,"curr set size: %d\n",SetLLen(hyb->SetTabl)) ;
// 	fprintf(stderr,"curr arv size: %d\n",ARVLLen(hyb->NumTabl)) ;
// 	
	if (AnyNegs(hyb)) {
		SortEls(hyb, set_pos, set_neg, num_pos, num_neg) ;
		EnumerateCand(hyb, num_pos, num_neg, set_pos, set_neg) ;
	}
	
	
	/* fprintf(stderr,"len set pos: %d\n", SetLLen(set_pos)) ;
	fprintf(stderr,"len set neg: %d\n", SetLLen(set_neg)) ;
	fprintf(stderr,"len num neg: %d\n", ARVLLen(num_pos)) ;
	fprintf(stderr,"len num neg: %d\n\n", ARVLLen(num_neg)) ;
	
	PrintARVL(hyb->NumTabl) ; */
	
	ARVLDel(num_pos) ;
	ARVLDel(num_neg) ;
	SetLDel(set_pos) ;
	SetLDel(set_neg) ;
	
	return (hyb) ;
}




void EnumerateCand(HybTab_t hyb, ARVecList_t NumPos, ARVecList_t NumNeg, SetList_t SetPos, SetList_t SetNeg) {
	/* pre : hyb as required by ProcessRow(), NumPos/Neg subset of hyb->NumTabl such that element 
		at hyb->currRow is pos/neg for all vectors, SetPos/Neg subset of hyb->SetTabl such that element 
		at hyb->currRow is 1/0 for all vectors. NumPos->len==SetPos->len and NumNeg->len==SetNeg->len.

	  post : all adjacent combinations of SetNeg and SetPos have been added to hyb->SetTabl, corresponding
	  	combinations of NumNeg and NumPos added to hyb->NumTabl.
	*/
	
	
	SetList_t comb_setlist = SetLNew() ;
	ARVecList_t comb_arvlist = ARVLNew() ;
	Bool are_adj ;
	
	MakeCands(hyb,NumPos,NumNeg,SetPos,SetNeg,comb_setlist,comb_arvlist) ;
	
	SetLCur2Top(comb_setlist) ;
	ARVLCur2Top(comb_arvlist) ;
	
	int i ;
	for (i = 0 ; i <SetLLen(comb_setlist) ; i++) {
		are_adj = AdjacentTest(hyb, SetLCur(comb_setlist)) ;
		if (are_adj) {
			ARVLApp(hyb->NumTabl, ARVecDup(ARVLCur(comb_arvlist))) ;
		}
		SetLCurNext(comb_setlist) ;
		ARVLCurNext(comb_arvlist) ;
	}
	SetLDel(comb_setlist) ;
	ARVLDel(comb_arvlist) ; 
}
	
	
	


void MakeCands(HybTab_t hyb,ARVecList_t NP,ARVecList_t NN,SetList_t SP,SetList_t SN,SetList_t CS,ARVecList_t CN) {
	/*  pre	: hyb, NP, NN, SP, SN as required of hyb, NumPos, NumNeg, SetPos, SetNeg, respectively by 
		  EnumerateCand().
		  CS and CN are empty lists.
		    
	   post : CS (list of sets) and CN (list of numerical vectors) have been populated with all
	          combinations of positive (NP and SP) and negative (NN and SN) vectors such that
		  no vector is a subset of another vector.
	
	*/
	
	ARVec_t comb_numvec ;
	Set_t cand ;
	Bool are_adj ;
	int i, j ;
	
	ARVLCur2Top(NP) ;
	ARVLCur2Top(NN) ;
	SetLCur2Top(SP) ;
	SetLCur2Top(SN) ;

	
	for(i = 0 ; i < ARVLLen(NN) ; i++){
		for(j = 0 ; j < ARVLLen(NP) ; j++){
			cand = SetNew(HybTabnReacs(hyb)) ;
			SetUnion(cand, SetLCur(SP), SetLCur(SN)) ;
			comb_numvec = CombineNumVecs(hyb->currRow, ARVLCur(NP), ARVLCur(NN)) ;	
			if (SetLLen(CS) > 0) {
				CheckCandModeList(cand, CS, comb_numvec, CN) ;
			}
			else {
				SetLApp(CS, cand) ;
				ARVLApp(CN, comb_numvec) ;	
			}
			ARVLCurNext(NP) ;
			SetLCurNext(SP) ;
		}
		ARVLCur2Top(NP) ;
		SetLCur2Top(SP) ;
		
		ARVLCurNext(NN) ;
		SetLCurNext(SN) ;
	}
	
	// fprintf(stderr,"len cands: %d\n", SetLLen(CS)) ;
}



void CheckCandModeList(Set_t cand_mode, SetList_t cand_list, ARVec_t comb_nvec, ARVecList_t comb_arvl) {
	/*  pre	: cand_mode and comb_nvec are of same effective lenght (SetMax(cand_mode) >= ARVecLen(comb_nvec)).
		  cand_list and comb_arvl are (possibly empty) lists.
		    
	   post : cand_mode and comb_nvec have been added to cand_list and comb_arvl, respectively, if 
	   	  cand_mode is not a superset of any preexisting sets in cand_list. Any modes that are supersets 
		  of cand_mode have been removed from cand_list (corresponding modes have been removed from comb_arvl).
	*/
	
	Bool vec_pop, cand_del = FALSE;
	SetLCur2Top(cand_list) ;
	ARVLCur2Top(comb_arvl) ;
	vec_pop = CheckCMode(cand_mode,cand_list,comb_nvec,comb_arvl, &cand_del) ;
	while (!SetLCurAtBot(cand_list)) {
		if (!vec_pop){
			SetLCurNext(cand_list) ;
			ARVLCurNext(comb_arvl) ;
		}
		vec_pop = CheckCMode(cand_mode,cand_list,comb_nvec,comb_arvl, &cand_del) ;
	}
	if (!cand_del) {
		SetLApp(cand_list, cand_mode) ;
		ARVLApp(comb_arvl,comb_nvec) ;
	}
}

Bool CheckCMode(Set_t cand_mode, SetList_t cand_list, ARVec_t comb_numvec, ARVecList_t comb_arvlist, Bool *cand_del) {
	/*  pre	: Arguments as required by CheckCandModeList(). *cand_del is FALSE.
		    
	   post : If cand_mode is a superset of current vector in cand_list, current vectors of
	          cand_list and comb_arvlist have been changed to the bottom vectors, cand_mode (and comb_numvec)
		  have been deleted and *cand_del has been changed to TRUE, thus preventing CheckCandModeList() 
		  from using cand_mode and comb_numvec. If current vector in cand_list is superset of 
		  cand_mode it has been removed from cand_list (and current vector in comb_arvlist has been deleted).
		  If deleted vector in list was top vector, TRUE has been returned.
	*/

	Bool vec_pop = FALSE ;
	if (SetIsSub(SetLCur(cand_list), cand_mode)) {
		SetDel(cand_mode) ;
		ARVecDel(comb_numvec) ;
		*cand_del = TRUE ;
		SetLCur2Bot(cand_list) ;
		ARVLCur2Bot(comb_arvlist) ;
	}
	else if (SetIsSub(cand_mode, SetLCur(cand_list))) {
		vec_pop	= SetLCurAtTop(cand_list) ;
		SetDel(SetLExtract(cand_list)) ;
		ARVecDel(ARVLExtract(comb_arvlist)) ;
	}
	return vec_pop ;
}


Bool AdjacentTest(HybTab_t hyb, Set_t set_new) { 
	/* pre : hyb as required by EnumerateCand(). set_new is a vector from set list processed by MakeCands().

	  post : If set_new is not a superset of any preexisting modes in hyb->SetTabl, function is TRUE. 
	  If true, bitwise OR has been added to
	  hyb->SetTabl.
	*/
	
	Bool are_adj ;
	SetLCur2Top(hyb->SetTabl) ;
	ARVLCur2Top(hyb->NumTabl) ;
	if (IsLastRow(hyb)) {
		if (OKNumberZeros(set_new, hyb->currRow, hyb->MinZeros)) {
			are_adj = CheckAdj(hyb, set_new) ;	
		}
		else {
			are_adj = FALSE ;
		}
		
	} 
	else {
		are_adj = CheckAdj(hyb, set_new) ;
	}
	while (are_adj && !SetLCurAtBot(hyb->SetTabl)) {
		SetLCurNext(hyb->SetTabl) ;
		ARVLCurNext(hyb->NumTabl) ;
		are_adj = CheckAdj(hyb, set_new) ; 
	}
	if (are_adj){
		SetLApp(hyb->SetTabl, SetDup(set_new)) ;
	}
	
	return (are_adj) ;	
}



ARVec_t CombineNumVecs(ulint idx, ARVec_t PosVec, ARVec_t NegVec) {
	/*  pre : 0 <= idx <= hyb->currRow (for hyb as required ). 
		  PosVec[hyb->currRow] >= 0, NegVec[hyb->currRow] < 0.
	
	   post : Gaussian combination (eq. 9 in G&K (2005)) of NumTabPos/Neg has been returned.
	*/
	
	ARVec_t neg_dup, pos_dup ;
	
	//char *str,*str2 ;
	
	neg_dup = ARVecDup(NegVec) ;
	pos_dup = ARVecDup(PosVec) ;
	
	ArbRat_t p, n ;
	
	n = ARVecGetElDup(neg_dup, idx) ;
	p = ARVecGetElDup(pos_dup, idx) ;
	
	//printels(neg_dup) ;
	//printels(pos_dup) ;
	
	ARVecMulSc(neg_dup, p, 0) ;
	ARVecMulSc(pos_dup, n, 0) ;
	
	//ArbRat2Str(n,&str) ;
	//ArbRat2Str(p,&str2) ;
	
	//fprintf(stderr,"neg: %s\n",str) ;
	//fprintf(stderr,"pos: %s\n", str2) ;
	
	ARVecSub(neg_dup, pos_dup, 0) ;
	
	//printels(neg_dup) ;
	//printels(pos_dup) ;
	
	ARVecDel(pos_dup) ;
	ArbRatDel(p) ;
	ArbRatDel(n) ;
	
	return (neg_dup) ;
}
	

/********** auxillaries ************/


Bool AnyNegs(HybTab_t hyb) {
	/*  pre : hyb usable, ARVLLen(hyb->NumTabl) == SetLLen(hyb->SetTabl)
	
	   post : If any element at index hyb->currRow in hyb->NumTabl > 0, corresponding
	   	  index in hyb->SetTabl has memeber. If any element at index hyb->currRow 
		  in hyb->NumTabl < 0, Bool TRUE has been returned, else FALSE.
	*/
	int n ;
	Bool any_negs ;
	any_negs = FALSE ;
	//char *str ;
	ArbRat_t temp ;
	
	ARVLCur2Top(hyb->NumTabl) ;
	SetLCur2Top(hyb->SetTabl) ;
	
	for(n = 0 ; n < ARVLLen(hyb->NumTabl); n++) {
		//dup = ArbRatDup(ARVecGetEl(ARVLCur(hyb->NumTabl),hyb->currRow)) ;
		//ArbRat2Str(dup, &str);
		//fprintf(stderr, "curr el: %s \n", str) ;

		temp = ARVecGetElDup(ARVLCur(hyb->NumTabl),hyb->currRow) ;
		
		if(ArbRatIsPos(temp)) {
			SetAddMem(SetLCur(hyb->SetTabl), hyb->currRow) ;
			//fprintf(stderr, "curr el is pos \n") ;
		}
		else if(ArbRatIsNeg(temp)) {
			any_negs = TRUE ;
		}
		ARVLCurNext(hyb->NumTabl) ;
		SetLCurNext(hyb->SetTabl) ;
	}
	ArbRatDel(temp) ;
	return (any_negs) ;
}



void SortEls(HybTab_t hyb, SetList_t set_pos, SetList_t set_neg, ARVecList_t num_pos, ARVecList_t num_neg) {
	/*  pre : hyb usable, ARVLLen(hyb->NumTabl) == SetLLen(hyb->SetTabl).
		  set_pos, set_neg, num_pos, num_neg are empty, usable structures. 
	
	   post : All vectors in hyb->NumTabl with element > 0 (< 0) at hyb->currRow have been copied to int_pos
	   	  (extracted from hyb-> num_neg), corresponding vector in hyb->SetTabl has been copied to set_pos 
		  (set_neg) with member at hyb->currRow deleted (keept empty).
	*/
	Bool vec_pop ;
	ARVLCur2Top(hyb->NumTabl) ;
	SetLCur2Top(hyb->SetTabl) ;
	
	vec_pop = CondModVecs(hyb, set_pos, set_neg, num_pos, num_neg) ;
	
	while (!ARVLCurAtBot(hyb->NumTabl)) {	
		if (!ARVLCurAtBot(hyb->NumTabl) && !vec_pop) {
			ARVLCurNext(hyb->NumTabl) ;
			SetLCurNext(hyb->SetTabl) ;	
		}				
		vec_pop = CondModVecs(hyb, set_pos, set_neg, num_pos, num_neg) ;
	}
	
}


Bool CondModVecs(HybTab_t hyb, SetList_t set_pos, SetList_t set_neg, ARVecList_t num_pos, ARVecList_t num_neg) {
	/*  pre : Arguments as required by SortEls(...)
	
	   post : If element hyb->currRow in current hyb->NumTabl > 0 (< 0) vector is copied to
	   	  num_pos (extracted to num_neg). The same actions are carried out for set_pos and 
		  set_neg, respectively. Returs True if extracted vector was top node.
	   
	*/
	
	ArbRat_t curr_el ;
	Bool vec_pop ;
	vec_pop = FALSE ;
	curr_el = ARVecGetElDup(ARVLCur(hyb->NumTabl),hyb->currRow) ;
	if (ArbRatIsPos(curr_el)) { 
		ARVLIns(num_pos, ARVecDup(ARVLCur(hyb->NumTabl))) ;
		SetLIns(set_pos, SetDup(SetLCur(hyb->SetTabl))) ; 
		SetDelMem(SetLTop(set_pos), hyb->currRow) ;
									/* thus, cur element in set_pos is False,
									but element in SetTabl is True. For
									cur element ++ cur element in set_pos 
									will be True. If set_neg lenght > 0, 
									any adjacent mode will be created by 
									comb with set_neg, where cur element 
									will be False since negative mode is
									eliminated (see CombineNumVecs). */
									
	}
	
	else if (ArbRatIsNeg(curr_el)) {
		vec_pop = ARVLCurAtTop(hyb->NumTabl) ;
		ARVLIns(num_neg, ARVLExtract(hyb->NumTabl)) ;
		SetLIns(set_neg, SetLExtract(hyb->SetTabl)) ;
		
	}
	ArbRatDel(curr_el) ;
	
	return (vec_pop) ;
}




Bool CheckAdj(HybTab_t hyb, Set_t set_new) {
/* NB: not nec to check that cur set is not the pos or neg set it came from
since, neg set is no longer in hyb and set_new will always miss the element 
in hyb->currRow, so it cannnot be a superset of the positive set it came from 
(this did not apply to old SetPos). */

	Set_t set_cand ;
	Bool are_adj ;
	set_cand = SetNew(HybTabnReacs(hyb)) ;
	SetUnion(set_cand, set_new, SetLCur(hyb->SetTabl)) ;
	are_adj = !SetIsEqual(set_cand, set_new) ;
	SetDel(set_cand) ;

	return (are_adj) ;
}



HybTab_t HybTabDup(HybTab_t hyb) {
	/* pre : hyb hybrid tableau with content
	  post : content of hyb copied and returned, *LCur(hyb/rv->*Tabl)==*LBot(hyb/rv->*Tabl)
	*/
	HybTab_t rv ;
	int n ;
	
	rv = NewHybTab(HybTabnReacs(hyb)) ;
	
	ARVLCur2Top(hyb->NumTabl) ;
	SetLCur2Top(hyb->SetTabl) ;
	IntVLCur2Top(hyb->BackFutiles) ;
	
	
	for (n = 0 ; n < ARVLLen(hyb->NumTabl) ; n++){
		ARVLIns(rv->NumTabl, ARVecDup(ARVLCur(hyb->NumTabl))) ;
		ARVLCurNext(hyb->NumTabl) ;
	}
	
	for (n = 0 ; n < SetLLen(hyb->SetTabl) ; n++){
		SetLIns(rv->SetTabl, SetDup(SetLCur(hyb->SetTabl))) ;
		SetLCurNext(hyb->SetTabl) ;
	}
	
	for (n = 0 ; n < IntVLLen(hyb->BackFutiles) ; n++){
		IntVLIns(rv->BackFutiles, IntVecDup(IntVLCur(hyb->BackFutiles))) ;
		IntVLCurNext(hyb->BackFutiles) ;
	}
	
	rv->currRow = hyb->currRow;
	rv->MinZeros = hyb->MinZeros ;
	
	return (rv) ;
}

Bool IsDone(HybTab_t hyb) {
	/* pre : hyb usable.
	  post : if hyb->currRow == HybTabnReacs(hyb) (all reactions have been processed) algorithm 
	  is done and function evaluates to true, else false.
	*/
	
	
	return ((hyb->currRow) == HybTabnReacs(hyb)) ;

}


Bool IsLastRow(HybTab_t hyb) {
	/* pre : hyb usable.
	  post : if hyb->currRow == (HybTabnReacs(hyb) = 1) curent row is the last row 
	  	 function evaluates to true
	*/
	
	
	return ((hyb->currRow) == (HybTabnReacs(hyb) - 1)) ;

}

int GetSetZeros(Set_t mode, ulint max_idx) {
	/* pre : SetMax(mode) >= max_idx
	  post : The number of empty indeces in mode[0 ... max_idx] has been returned.
	*/

	int n_zeros = 0 ;
	ulint n ;
	
	for (n = 0 ; n <=  max_idx; n++) {
		if (!SetHasMem(mode, n)){
			n_zeros++ ;
		}
	}
	return n_zeros ;
}



Bool OKNumberZeros(Set_t mode, ulint nreacs, ulint min_zero) {
	/* pre : SetMax(mode) <= nreacs, min_zero - hyb->MinZeros.
	  post : If mode has more than min_zeros zero elements it may be elementary, 
	         else it can be discarded.
	*/
	
	return (GetSetZeros(mode, nreacs) >= min_zero) ;
}


 
void RemoveBFFutiles(HybTab_t hyb) {
	/* pre : hyb usable, IsDone(hyb), hyb->BackFutiles populated
	  post : All modes in hyb->SetTabl which only involves forward-backwards
	  	 reaction pairs have been removed.	
	*/
	
	int n, i ;
	Set_t bf_pair, futile ;
	Bool vec_pop ;
	
	IntVLCur2Top(hyb->BackFutiles) ;
	for (n = 0 ; n < IntVLLen(hyb->BackFutiles) ; n++) {
		bf_pair = SetNew(HybTabnReacs(hyb)) ;
		for (i = 0 ; i <  IntVecLen(IntVLCur(hyb->BackFutiles)) ; i++) {
			SetAddMem(bf_pair, IntVecGetEl(IntVLCur(hyb->BackFutiles), i)) ;
		}
		
		SetLCur2Top(hyb->SetTabl) ;
		vec_pop = CondDelMode(bf_pair, hyb->SetTabl) ;
		while (!SetLCurAtBot(hyb->SetTabl)) {
			if (!SetLCurAtBot(hyb->SetTabl) && !vec_pop) {
				SetLCurNext(hyb->SetTabl) ;
			}
			vec_pop = CondDelMode(bf_pair, hyb->SetTabl) ;
		}
		SetDel(bf_pair) ; 
		IntVLCurNext(hyb->BackFutiles) ;
	}
}

	
Bool CondDelMode(Set_t cand_mode, SetList_t targ_mode_set) {
	/* pre : SetMax(cand_mode) == SetMax(SetLCur(targ_mode_set))
	  post : If cand_mode is equal to current set in targ_mode_set,
	  	 current set in targ_mode_set has been deleted.
	*/	
	Set_t futile ;
	Bool vec_pop ;
	vec_pop = FALSE ;
	if (SetIsEqual(cand_mode, SetLCur(targ_mode_set))) {
		vec_pop = SetLCurAtTop(targ_mode_set) ;
		futile = SetLExtract(targ_mode_set) ;
		SetDel(futile) ;
	}
	return (vec_pop) ;
}

/*********** aux: C and Py interface **********************/

void CTabl2PyTabl(PyObject *ptabl, HybTab_t hyb){ 
  	Set_t curset ;
  	SetList_t tabl ;
  	PyObject *PyRes, *PyRow, *PyInt ;
  
	ulint col, nrows, row, ncols ;

	tabl = hyb->SetTabl ;
	nrows = SetLLen(tabl) ;
	ncols = HybTabnReacs(hyb) ;
	PyRes = PyObject_GetAttrString(ptabl, "Results") ;
	
	SetLCur2Top(tabl) ;
	for(row = 0 ; row < nrows ; row++){
		curset = SetLCur(tabl) ;
		PyRow = PyList_New(ncols) ;
		Py_INCREF(PyRow) ;
		for(col = 0 ; col < ncols ; col++){
			if (SetHasMem(curset, col)) {
				PyInt = PyInt_FromLong(1) ;
			} 
			else {
				PyInt = PyInt_FromLong(0) ;
			}
			PyList_SetItem(PyRow, col, PyInt) ;
			/* don't think Py_INCREF() is needed here... */
		}
		PyList_Append(PyRes, PyRow) ;
		SetLCurNext(tabl) ;
	}
} 



/* 
int *PyIntList2CIntArry(PyObject *pilist){

  	int *rv, len, n ;
  	
	len = PyList_Size(pilist) ;
	if(len > 0){
		rv = calloc(sizeof(int), len) ;
		for( n = 0 ; n < len ; n ++)
			rv[n] = PyInt_AsLong(PyList_GetItem(pilist, n)) ;
	}
	else rv = 0 ;
		
	return rv ;
}		 

IntVecList_t PyListList2IntVecList(PyObject *pilistlist){

	PyObject *pilist ;
  	IntVecList_t rv ;
  	IntVec_t vec ;
  	int *tmp_vec ;
  	int len, n ;
  	ulint idx, nest_len ;
  
	rv = IntVLNew() ;
	len = PyList_Size(pilistlist) ;
	for(n = 0 ; n < len ; n++){
		pilist = PyList_GetItem(pilistlist,n) ;
		nest_len = PyList_Size(pilist) ;
		vec = IntVecNew(nest_len) ;
		tmp_vec = PyIntList2CIntArry(pilist) ;
		for (idx = 0 ; idx < nest_len ; idx++){
			IntVecSetEl(vec, idx, tmp_vec[idx]) ;
		}
		IntVLApp(rv, vec) ; 
	}
	return rv ;
}
 */


/************** top-level: call low-level functions and export results to Python ******/


void GetBitEM(PyObject *ptab) {
	/* pre: ptabl is a correctly initialised Python tableau
	  post: ptabl' contains the elementary modes */
	 
	HybTab_t init_tab, res_tab ;
	
	init_tab = HybTablInitFrPy(ptab) ;
	
	res_tab = EMCalc(init_tab) ;
	
	RemoveBFFutiles(res_tab) ;
	fprintf(stderr,"curr set size: %lu\n",SetLLen(res_tab->SetTabl)) ;
	CTabl2PyTabl(ptab, res_tab) ;
	
	DelHybTab(init_tab) ;
	DelHybTab(res_tab) ;
}

extern PyObject *BitEM(PyObject *self, PyObject *args) {
	/* Function called from Python */


	PyObject *ptab ;
	PyArg_ParseTuple(args, "O", &ptab) ;
	
	GetBitEM(ptab) ;
	
	Py_INCREF(Py_None) ; 
	return Py_None ;
	
}
/*************** test *********************/
/* test basic functionalities here, test python interface in python */



#ifdef TEST


 
main(){

 #ifdef DONT_COMPILE_THIS	

/* create and destroy */

	ulint nreacs ;
	nreacs = 200 ;
	HybTab_t hyb ;
	
	hyb = NewHybTab(nreacs) ;
	fprintf(stderr, "hyb = %x\t, len = %d\n", hyb, HybTabnReacs(hyb)) ;
	
	PrintARVL(hyb->NumTabl) ;
	PrintSetL(hyb->SetTabl) ;
	
	DelHybTab(hyb) ; 
	
	
	


/* dupicate hybrid */
	
	ulint nreacs, n ;
	nreacs = 5 ;
	HybTab_t hyb, hyb_cop ;
	ARVec_t v ;
	
	hyb = NewHybTab(nreacs) ;
	
	v = ARVecNew(nreacs) ;
	
	for (n = 0 ; n < nreacs ; n++){
		ARVecSetEl(v,n, ArbRatNew(n,nreacs)) ;
	}
	
	for(n = 0 ; n < nreacs ; n++){
		ARVLIns(hyb->NumTabl, ARVecDup(v)) ;
	}

	
	hyb_cop = HybTabDup(hyb) ;
	
	PrintARVL(hyb->NumTabl) ;
	
	PrintARVL(hyb_cop->NumTabl) ;
	
	DelHybTab(hyb) ; 
	DelHybTab(hyb_cop) ; 

	
/* populate hyb->IntTabl and make empty SetTabl of same size*/
	

	ulint nreacs, n ;
	nreacs = 5 ;
	HybTab_t hyb ;
	Set_t empty_set ;
	ARVec_t v ;
	hyb = NewHybTab(nreacs) ;
	v = ARVecNew(nreacs) ;
	
	for (n = 0 ; n < nreacs ; n++){
		ARVecSetEl(v,n, ArbRatNew(1,1)) ;
	}
	
	for(n = 0 ; n < nreacs ; n++){
		ARVLIns(hyb->NumTabl, ARVecDup(v)) ;
	}
	ARVLCur2Top(hyb->NumTabl) ;
	for(n = 0 ; n < ARVLLen(hyb->NumTabl) ; n++){
		empty_set = SetNew(HybTabnReacs(hyb)) ;
		SetLIns(hyb->SetTabl, empty_set) ;
		ARVLCurNext(hyb->NumTabl) ;
	}
	PrintARVL(hyb->NumTabl) ;
	PrintSetL(hyb->SetTabl) ;
	
	DelHybTab(hyb) ; 
	
	
	
/* test IsDone */	
	
	ulint nreacs, n ;
	nreacs = 5 ;
	HybTab_t hyb ;
	Set_t empty_set ;
	ARVec_t v ;
	hyb = NewHybTab(nreacs) ;
	v = ARVecNew(nreacs) ;
	for(n = 0 ; n < nreacs ; n++){
		ARVLIns(hyb->NumTabl, ARVecDup(v)) ;
	}
	
	fprintf(stderr, "currRow %d \n",  hyb->currRow) ;
	
	while (!IsDone(hyb)) {
		hyb->currRow++ ;
		if (IsDone(hyb)) {
			fprintf(stderr, "IsDone is true, currRow %d \n",  hyb->currRow) ;	
		}
		else {
			fprintf(stderr, "IsDone is false, currRow %d \n",  hyb->currRow) ;	
		}
	}
	/*
	for(n = 0 ; n < HybTabnReacs(hyb) ; n++){
		hyb->currRow++ ;
		if (IsDone(hyb)) {
			fprintf(stderr, "IsDone is true, currRow %d \n",  hyb->currRow) ;	
		}
		else {
			fprintf(stderr, "IsDone is false, currRow %d \n",  hyb->currRow) ;	
		}
		
	} */
	
	DelHybTab(hyb) ; 
	
	
	
	
	
/* test CombineNumVecs */

	
	ARVec_t i_pos, i_neg ;
	ARVecList_t list ;
	ulint idx, n ;
	
	i_pos = ARVecNew(6) ;
	i_neg = ARVecNew(6) ;
	list = ARVLNew() ;
	
	idx = 0 ;
	int v_pos[] = { 1, 1, 1, 1, 0, 0} ;
	int v_neg[] = { -1, -1, 0, 0, -1, -1} ; /*eliminate negative elements below */
	
	for(n = 0 ; n < 6 ; n++ ) {
		ARVecSetEl(i_pos, n, ArbRatNew(v_pos[n],1)) ;
		ARVecSetEl(i_neg, n, ArbRatNew(v_neg[n],1)) ;
	}
	
	printels(i_pos) ;
	printels(i_neg) ;
	
	i_neg = CombineNumVecs(1, i_pos, i_neg) ;
	
	printels(i_pos) ;
	printels(i_neg) ;
	
	i_neg = CombineNumVecs(4, i_pos, i_neg) ;
	
	printels(i_pos) ;
	printels(i_neg) ;
	
	ARVLDel(list) ;
	
	ARVecDel(i_pos);
	ARVecDel(i_neg);
	
	/* output (only modified vecs):
	
	1  1  1  1  0  0  
	0  0  1  1  -1  -1 //el 0 eliminated
	
	1  1  1  1  0  0
	0  0  0  0  0  0 //el 4 eliminated
	*/
	

	

	#endif

	exit(0) ;
}


#endif

/*
void MakeCands(HybTab_t hyb,ARVecList_t NP,ARVecList_t NN,SetList_t SP,SetList_t SN,SetList_t CS,ARVecList_t CN) {
	ARVec_t comb_numvec ;
	Set_t cand ;
	Bool are_adj ;
	int i, j ;
	
	ARVLCur2Top(NP) ;
	ARVLCur2Top(NN) ;
	SetLCur2Top(SP) ;
	SetLCur2Top(SN) ;
	
	for(i = 0 ; i < ARVLLen(NN) ; i++){
		for(j = 0 ; j < ARVLLen(NP) ; j++){
			cand = SetNew(HybTabnReacs(hyb)) ;
			SetUnion(cand, SetLCur(SP), SetLCur(SN)) ;
			comb_numvec = CombineNumVecs(hyb->currRow, ARVLCur(NP), ARVLCur(NN)) ;	
			if (SetLLen(comb_setlist) > 0) {
				CheckCandModeList(cand, CS, comb_numvec, CN) ;
			}
			else {
				SetLApp(CS, cand) ;
				ARVLApp(CN, comb_numvec) ;	
			}
			ARVLCurNext(NP) ;
			SetLCurNext(SP) ;
		}
		ARVLCur2Top(NP) ;
		SetLCur2Top(SP) ;
		
		ARVLCurNext(NN) ;
		SetLCurNext(SN) ;
	}
}



void CheckCandModeList(Set_t cand_mode, SetList_t cand_list, ARVec_t comb_nvec, ARVecList_t comb_arvl) {
	
	Bool vec_pop, cand_del = FALSE ;
	SetLCur2Top(cand_list) ;
	ARVLCur2Top(comb_arvl) ;
	vec_pop = CheckMode(cand_mode,cand_list,comb_nvec,comb_arvl, cand_del) ;
	while (!SetLCurAtBot(cand_list)) {
		if (!vec_pop){
			SetLCurNext(cand_list) ;
			ARVLCurNext(comb_arvl) ;
		}
		vec_pop = CheckMode(cand_mode,cand_list,comb_nvec,comb_arvl, cand_del) ;
	}
	if (!cand_del) {
		SetLApp(cand_list, cand_mode) ;
		ARVLApp(comb_arvl,comb_nvec) ;
	}
}
	

Bool CheckMode(Set_t cand_mode, SetList_t cand_list, ARVec_t comb_numvec, ARVecList_t comb_arvlist, Bool cand_del) {

	Bool vec_pop = FALSE ;
	
	if (SetIsSub(SetLCur(cand_list), cand_mode)) {
		SetDel(cand_mode) ;
		ARVecDel(comb_numvec) ;
		cand_del = TRUE ;
		SetLCur2Bot(cand_list) ;
		ARVLCur2Bot(comb_arvlist) ;
	}
	else if (SetIsSub(cand_mode, SetLCur(cand_list))) {
		vec_pop	= SetLCurAtTop(cand_list) ;
		SetDel(SetLExtract(cand_list)) ;
		ARVecDel(ARVLExtract(comb_arvlist)) ;
	}
	return vec_pop ;
}
 */


