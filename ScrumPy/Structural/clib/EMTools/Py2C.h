

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/***
M.G.Poolman
09/04/01 onward
Py2C, convert python objects to c equivalents
***/

#ifndef PY2C_H
#define PY2C_H

#include <Python.h>
#include "gmpy.h"
#include "ARVecList.h"
#include "IntVecList.h"

/* #include "FltVecList.h" */



extern ArbRat_t PyInt2ArbRat(PyObject *pint) ;
	/* pre: pint is a Python integer object
	  post: returns the ArbRat representation of pint */

extern ArbRat_t PyRat2ArbRat(PympqObject *prat) ;
	/* pre: rat is a Python Rat
	  post: returns the ArbRat equivalent of prat */

extern int MaxInPyIntList(PyObject *pilist) ;
	/* pre:  pilist is a Python list of integers
	  post: returns the maximum value in pilist */

extern int *PyIntList2CIntArry(PyObject *pilist) ;
	/* pre: pilist is a Python list of integers
	  post: returns a newly calloc()'d array of pilist integer contents */

extern Set_t PyIntList2Set(PyObject *pilist) ;
	/* pre: pilist is a Python list of integers
	  post: returns the set of values in pilist */

extern ARVec_t PyIntList2ARVec(PyObject *pilist) ;
	/* pre: pilist is a Python list of integers
	  post: returns the ARVec equivalent of pilist */
	   
		
extern ARVecList_t PyListList2ArVecList(PyObject *pilistlist) ;
	/* pre: pilistlist is a Python list of lists of integers
	  post: returns the ARVec_t equivalent of pilistlist */
	  
	  
extern IntVecList_t PyListList2IntVecList(PyObject *pilistlist) ;
	/* pre: pilistlist is a Python list of lists of integers
	  post: returns the IntVecList_t equivalent of pilistlist */
	  
/* extern FltVecList_t PyListList2FltVecList(PyObject *pilistlist) ;*/
	/* pre: pilistlist is a Python list of lists of integers
	  post: returns the FltVecList_t equivalent of pilistlist */ 
	  
#endif
