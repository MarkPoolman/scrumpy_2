

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/**
M.G.Poolman 05/04/01 onwards
ARVecList - lists of ARVec_t
**/

#ifndef ARVECLIST_H
#define ARVECLIST_H


#include"ARVec.h"

typedef struct ARVecList *ARVecList_t ;



/*** create and destroy  ***/


extern ARVecList_t ARVLNew(void) ;
	/* pre: True
	  post: returns a usable ARVecList_t 
	 */
	 
void ARVLDel(ARVecList_t l) ;
	/* pre: l usable
	  post: l' !usable, resources freed 
	 */
	 

	/***** info about lists *****/

ulint ARVLLen(ARVecList_t l) ;
	/* pre: l usable
	  post: returns length of l 
	*/
	 
	 
	 /***** retrieve items in situ ******/
	 
ARVec_t ARVLTop(ARVecList_t l) ;
	/* pre: ARVLLen(l) > 0
	  post: l' == l, returns topmost item
	*/
	
	
ARVec_t ARVLCur(ARVecList_t l) ;
	/* pre: ARVLLen(l) > 0
	  post: l' == l, returns current item
	*/
	
	
ARVec_t ARVLBot(ARVecList_t l) ;
	/* pre: ARVLLen(l) > 0
	  post: l' == l, returns bottom item
	*/
	 

	/**** Add items ****/

void ARVLIns(ARVecList_t l, ARVec_t v) ;
	/* pre: usable(l,v), !(ARVLApp(l, v) || ARVLIns(l, v))
	  post: ARVLTop(l') == v
	 */
	 
void ARVLApp(ARVecList_t l, ARVec_t v) ;
	/* pre: usable(l,v), !(ARVLApp(l, v) || ARVLIns(l, v))
	  post: ARVLBot(l') == v
	 */
	 
	 
	/**** Remove items ***/
	 
ARVec_t ARVLPop(ARVecList_t l) ;
	/* pre: ARVLLen(l) > 0
	  post: ARVLLen(l') == ARVLLen(l) -1,
	  		ARVLTop(l') != ARVLTop(l),
			ARVLTop(l)  == ARVLPop(l) ;
	*/


ARVec_t ARVLChop(ARVecList_t l) ;
	/* pre: ARVLLen(l) > 0
	  post: ARVLLen(l') == ARVLLen(l) -1,
	  		ARVLBot(l') != ARVLBot(l),
			ARVLChop(l) == ARVLBot(l) ;
	*/


ARVec_t ARVLExtract(ARVecList_t l) ;
	/* pre: ARVLLen(l) > 0
	  post: v = ARVLExtract(ARVecList_t l) => v not present in l
	  		ARVLCurAtTop(l) => ARVLCurAtTop(l')
			!ARVLCurAtTop(l) => ARVLCurPrev(l) */

	
	
	
 	/**** Navigate  ***/
	
void ARVLCur2Top(ARVecList_t l) ;
	/* pre: usable(l)
	  post: ARVLCur(l') == ARVLTop(l')
	*/
	
void ARVLCur2Bot(ARVecList_t l) ;
	/* pre: usable(l)
	  post: ARVLCur(l') == ARVLBot(l')
	*/ 
 
void ARVLCurNext(ARVecList_t l) ;
	/* pre: ARVLCur(l) != ARVLBot(l) 
	  post: current moved to next in sequence
	*/ 
 
 
void ARVLCurPrev(ARVecList_t l) ;
	/* pre: ARVLCur(l) != ARVLTop(l) 
	  post: current moved to previous in sequence
	*/  
 
Bool ARVLCurAtTop(ARVecList_t l) ;
	/* pre: True
	  post: ARVLCurAtTop(l) => ARVLCur(l) == ARVLTop(l) */ 
 
 
Bool ARVLCurAtBot(ARVecList_t l) ;
	/* pre: True
	  post: ARVLCurAtTop(l) => ARVLCur(l) == ARVLTop(l) */ 
 
 
void PrintARVL(ARVecList_t l) ;
 
#endif
