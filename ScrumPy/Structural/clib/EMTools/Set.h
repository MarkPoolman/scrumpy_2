

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/***
M.G.Poolman 27/03/01 onward
Set  -- An ADT handling sets of integers
***/


#ifndef SET_H
#define SET_H



/***************** Boolean constants if not already defined *****/
#ifndef FALSE
#define FALSE (ulint) 0
#define TRUE !FALSE
#endif

/**************** a Boolean pseudo-type if we don't already have it ****/
#ifndef Bool
#define Bool ulint
#endif



typedef unsigned long int ulint ;
/** just a convenient abbrev. **/


typedef struct Set *Set_t ;
/** not usable until assigned by a function below ***/




	/************** create and destroy  *****************/
	

Set_t SetNew(ulint len) ;
	/* pre: True
	  post: SetMax(SetNew(len)) >= len
	  		for all valid m: !SetHasMem(s,m) 
	*/


void SetDel(Set_t s) ;
	/* pre: s usable
	  post: s not usable, resources freed 
	*/



	/************* get some useful info ****************/
ulint nthCh(Set_t s, ulint n) ;

char *SetStr(Set_t s) ;
	/* pre: s usable
	  post: SetStr(s) == a newly alloc()'d string representation of s
	*/
	
ulint SetMax(Set_t s) ;
	/* pre: s usable
	  post: SetMax(s) == maximum value a member of s may take
	*/

Bool SetIsEmpty(Set_t s) ;
	/* pre: s usable
	  post: SetIsEmpty(s) => s is empty   :-) */



	/************** Set operations  *******************/ 
	
	/*** with members  ***/



Bool SetHasMem(Set_t s, ulint m) ;
	/* pre: m <= SetMax(s)
	  post: SetHasMem(s,m) => m present in s
	*/


void SetAddMem(Set_t s, ulint m) ;
	/* pre: m <= SetMax(s)
	  post: SetHasMem(s', m)
	*/


void SetDelMem(Set_t s, ulint m) ;
	/* pre: m <= SetMax(s)
	  post: !SetHasMem(s',m)
	*/


void SetAddMemBool(Set_t s, ulint m, Bool b) ;
	/* pre: m <= SetMax(s)
	  post: SetHasMem(s', m, b) == b
	 */

void SetMakeEmpty(Set_t s) ;
	/* pre: s usable
	  post: for m[0..SetMax(s): !SetHasMem(s',m)
	*/ 

	
	
	/*** with other sets ***/
	

Set_t SetDup(Set_t s) ;
	/* pre: s usable
	  post: returns a duplicate of s */

Bool SetIsSub(Set_t s1, Set_t s2) ;
	/* pre: SetMax(s1) == SetMax(s2)
	  post: SetIsSub(s1,s2) => s1 is a (possibly equal) Subset of s2
	*/


Bool SetIsEqual(Set_t s1, Set_t s2) ;
	/* pre: SetMax(s1) == SetMax(s2)
	  post: SetIsEqual(s1, s2) => 
	  	for all valid m: SetHasMem(s1, m) == SetHasMem(s2, m)
	*/
	
void SetInter(Set_t out, Set_t s1, Set_t s2) ;
	/* pre: SetMax(out) >= (SetMax(s1) == SetMax(s2))
	  post: out == the intersection of s1 with s2
	*/

void SetUnion(Set_t out, Set_t s1, Set_t s2) ;
	/* pre: SetMax(out) >= (SetMax(s1) == SetMax(s2))
	  post: out == the union of s1 and s2
	*/

void SetCompl(Set_t out, Set_t s) ;
	/* pre: SetMax(out) == SetMax(s)
	  post: for m[0..SetMax(s)]: 
				SetHasMem(out,m) <=> !SetHasMem(s ,m)
				SetHasMem(s ,m) <=> !SetHasMem(out,m) 
	*/
	
	
	
#endif
