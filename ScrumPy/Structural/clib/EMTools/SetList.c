/**
H. Hartman
IntVec - vectors of Sets
**/



#include<stdio.h>
#include<stdlib.h>
#include "SetList.h"


/*************** LL node stuff, copied from IntVecList and adapted ************/


struct Node  {
	struct Node *Next ;
	Set_t Vec ;
}  ;

typedef struct Node *Node_p ;

static Node_p NewNode(void){

	return (Node_p) calloc(sizeof(struct Node), 1 ) ;
} 


static void DelNode(Node_p n){

	if(n->Vec)
		SetDel(n->Vec) ;
		
	free(n) ;
} 


static Node_p ParNode(Node_p Adam, Node_p targ){ /* find parent in list */

	if(Adam->Next == targ)
		return Adam ;
		
	return ParNode(Adam->Next, targ) ;
}



static void PrintNodes(Node_p n){
	
	char *setstr ;

	if(n){
		/* fprintf(stderr, "%x\t%x\t| %x\n", n, n->Next, n->Vec) ;*/
		setstr = SetStr(n->Vec) ;
		fprintf(stderr, "%s\n",setstr) ;
		free(setstr) ;
		PrintNodes(n->Next) ;
	}
}


static void DelNodes(Node_p n){

	/* fprintf(stderr, "deln %x\n", n) ;  */
	if(n){
		DelNodes(n->Next) ;
		DelNode(n) ;
	}
}

/******** lists **********/

struct SetList{

	Node_p Top, Cur, Bot ;
	int len ;
} ;


/*** Create and Destroy  ***/


SetList_t SetLNew() {
	/* pre: True
	  post: returns a usable SetList_t
	 */

	return (SetList_t) calloc(sizeof(struct SetList), 1) ;
} ;


void SetLDel(SetList_t l){
	/* pre: l usable
	  post: l' !usable, resources freed 
	 */
	 
	DelNodes(l->Top) ;
	free(l) ;
}


/***** info about lists *****/


ulint SetLLen(SetList_t l) {
	/* pre: l usable
	  post: returns length of l 
	*/
	
	return l->len ;
}


/***** retrieve items *******/

Set_t SetLTop(SetList_t l) {
	/* pre: SetLLen(l) > 0
	  post: l' == l, returns topmost item
	*/
	
	return (l->Top->Vec) ;
}
	

Set_t SetLCur(SetList_t l){
	/* pre: SetLLen(l) > 0
	  post: l' == l, returns current item
	*/
	
	return (l->Cur->Vec) ;
}


Set_t SetLBot(SetList_t l) {
	/* pre: SetLLen(l) > 0
	  post: l' == l, returns bottom item
	*/
	
	return (l->Bot->Vec) ;
}


SetList_t SetLDup(SetList_t l) { 
	/* pre: SetLLen(l) > 0
	  post: a duplicate of l has been returned, SetLCur2Bot(l)
	*/
	SetList_t cpy ;
	
	cpy = SetLNew() ;
	SetLCur2Top(l) ;
	SetLIns(cpy, SetDup(SetLCur(l))) ;
	while (SetLCur(l) != SetLBot(l)){
		SetLCurNext(l) ;
		SetLIns(cpy, SetDup(SetLCur(l))) ;
	}
	
	return (cpy) ;	
}

/**** Add items ****/

void SetLIns(SetList_t l, Set_t  s) {
	/* pre: usable(l,s), !(SetLApp(l, s) || SetLIns(l, s))
	  post: SetLTop(l') == s
	 */
	 
	Node_p new ;
	 
	new = NewNode() ;
	new->Vec = s ;
	new->Next = l->Top ;
	l->Top = new ;
	l->len++ ;
	if (l->len == 1){
		l->Cur = l->Bot = l->Top ;
	}
}
	 

void SetLApp(SetList_t l, Set_t s) {
	/* pre: usable(l,s) !(SetLApp(l, s) || SetLIns(l, s)) 
	  post: SetLBot(l') == s
	 */
	
	Node_p new ;
	if(l->len == 0)
		return(SetLIns(l,s)) ;
		
	new = NewNode() ;
	new->Vec = s ;
	
	l->Bot->Next = new ;
	l->Bot = new ;
	l->len++ ;
}



	 
	/**** Remove items ***/
	 
Set_t SetLPop(SetList_t l){
	/* pre: SetLLen(l) > 0
	  post: SetLLen(l') == SetLLen(l) -1,
	  		SetLTop(l') != SetLTop(l),
			SetLTop(l)  == SetLPop(l) ;
	*/

	Set_t rv ;
	Node_p old ;
	
	old = l->Top ;
	l->Top = l->Top->Next ;
	
	if(l->Cur == old)
		l->Cur = l->Cur->Next ;
	
	if(l->Bot == old)
		l->Bot = 0 ;
		
	l->len-- ;
	rv = old->Vec ;
	old->Vec = 0 ;
	free(old) ;
	return rv ;
}


Set_t SetLChop(SetList_t l) {
	/* pre: SetLLen(l) > 0
	  post: SetLLen(l') == SetLLen(l) -1,
	  		SetLBot(l') != SetLBot(l),
			SetLChop(l) == SetLBot(l) ;
	*/
	
		
	Set_t rv ;
  	Node_p old ;
  
  	if(l->Bot == l->Top)
		return(SetLPop(l)) ;
  
  	old = l->Bot ;
	l->Bot = ParNode(l->Top, l->Bot) ;
	l->Bot->Next = 0 ;
	
	
	if(l->Cur == old)
		l->Cur = l->Bot ;
		
	l->len-- ;
	rv = old->Vec ;
	old->Vec = 0 ;
	free(old) ;
	return rv ;
}
	

Set_t SetLExtract(SetList_t l) {
	/* pre: SetLLen(l) > 0
	  post: v = SetLExtract(SetList_t l) => v not present in l
	  		SetLCurAtTop(l) => SetLCurAtTop(l')
			!SetLCurAtTop(l) => SetLCurPrev(l) */

	Node_p parent ;
	Set_t rv ; 
	
	if(l->Cur == l->Top){
		return(SetLPop(l)) ;
	}
	if(l->Cur == l->Bot){
		return SetLChop(l) ;
	} 
		
	parent = ParNode(l->Top, l->Cur) ;  /* chop out the current node */
	parent->Next = l->Cur->Next ;  

	rv = l->Cur->Vec ;					/* extract the information from it */	
	free(l->Cur) ;						/* destroy the old node */
	l->Cur = parent ;					/* point l->Cur somewhere useful */
	l->len-- ;	
	return (rv) ;
}



	
 /**** Navigate  ***/
	
void SetLCur2Top(SetList_t l) {
	/* pre: usable(l)
	  post: SetLCur(l') == SetLTop(l')
	*/
	
	l->Cur = l->Top ;
}

void SetLCur2Bot(SetList_t l){
	/* pre: usable(l)
	  post: SetLCur(l') == SetLBot(l')
	*/ 
	
	l->Cur = l->Bot ;
}


void SetLCurNext(SetList_t l) {
	/* pre: SetLCur(l) != SetLBot(l) 
	  post: current moved to next in sequence
	*/ 
 	l->Cur = l->Cur->Next ;
}


void SetLCurPrev(SetList_t l) {
	/* pre: SetLCur(l) != SetLTop(l) 
	  post: current moved to previous in sequence
	*/ 
 
 	l->Cur = ParNode(l->Top, l->Cur) ;
}


Bool SetLCurAtTop(SetList_t l){
	/* pre: True
	  post: SetLCurAtTop(l) => SetLCur(l) == SetLTop(l) */ 
 
 	return(l->Cur == l->Top) ;
}


Bool SetLCurAtBot(SetList_t l){
	/* pre: True
	  post: SetLCurAtTop(l) => SetLCur(l) == SetLTop(l) */ 
 
 	return(l->Cur == l->Bot) ;
}


/********* printing *********/

void PrintSetL(SetList_t l){

	fprintf(stderr, "%p\t%d\t%p\t%p\t%p\n\n", l, l->len, l->Top, l->Cur, l->Bot) ;
	PrintNodes(l->Top) ;
}


/********** testing *********/


#ifdef TEST

 
 
 main(){
 
 #ifdef DONT_COMPILE_THIS
 
 /********* create and destroy ****/

 
 	SetList_t l ;
	Set_t s ;
	ulint len = 5 ;

	s = SetNew(len) ;
	l = SetLNew() ;
	
	fprintf(stderr, "l = %x\t, len = %d\n", l, SetLLen(l)) ;
	
	PrintSetL(l) ;
	
	SetLDel(l) ;
	fprintf(stderr, "del\n") ;
	l = SetLNew() ;
 	SetLIns(l, s) ;
	fprintf(stderr, "insert l = %x\t, len = %d\n", l, SetLLen(l)) ;
	fprintf(stderr, "vpointer %x\t%x\t%x\t%x\n", 
		s,
		SetLTop(l),
		SetLCur(l),
		SetLBot(l)) ;
		
	SetLDel(l) ;
	fprintf(stderr, "done\n") ;
	



/**** longer list this time ****/


 	SetList_t l ;
	Set_t s ;
 	ulint len,n ;
  
  	len = 5 ;
	s = SetNew(len) ;
	l = SetLNew() ;
	
	for(n = 0 ; n < len ; n++){
		SetLIns(l, SetDup(s)) ;
		SetLApp(l, SetDup(s)) ;
	}
	PrintSetL(l) ;
	SetLDel(l);




	
/******** remove items -1 SetLPop ************/


 	SetList_t l ;
 	Set_t s ;
 	ulint len,n ;
  
  	len = 5 ;
	s = SetNew(len) ;
	l = SetLNew() ;
	
	for(n = 0 ; n < len ; n++){
		SetLIns(l, SetDup(s)) ;
		SetLApp(l, SetDup(s)) ;
	}
	PrintSetL(l) ;


	while(SetLLen(l) != 0){
		s = SetLPop(l) ;
		fprintf(stderr, "\npopped %x\n", s) ;
		PrintSetL(l) ;
	}


	SetLDel(l) ;	
	SetLDel(cop) ;


/************ duplicate content ***********/
#endif
	SetList_t l, cop ;
 	Set_t s ;
 	ulint len,n ;
  
  	len = 5 ;
	s = SetNew(len) ;
	l = SetLNew() ;
	
	for(n = 0 ; n < len ; n++){
		SetLIns(l, SetDup(s)) ;
		SetLApp(l, SetDup(s)) ;
	}
	
	fprintf(stderr, "\n\n Org:") ;
	PrintSetL(l) ;
	
	cop = SetLDup(l) ;
	
	fprintf(stderr, "\n\n Copy:") ;
	
	PrintSetL(cop) ;
	
	SetLDel(l) ;
	SetLDel(cop) ;

#ifdef DONT_COMPILE_THIS

/******** remove items -2 SetLChop ************/


 	SetList_t l ;
 	Set_t s ;
 	ulint len,n ;
  
  	len = 20 ;
	s = SetNew(len) ;
	l = SetLNew() ;
	
	for(n = 0 ; n < len ; n++){
		SetLIns(l, SetDup(s)) ;
		SetLApp(l, SetDup(s)) ;
	}
	PrintSetL(l) ;


	while(SetLLen(l) != 0){
		s = SetLChop(l) ;
		fprintf(stderr, "\nchopped %x\n", s) ;
		PrintSetL(l) ;
	}


	SetLDel(l);		

	
	

 	SetList_t l ;
 	Set_t s ;
	ulint len,n ;
  
  	len = 2 ;
	s = SetNew(len) ;
	l = SetLNew() ;
	
	for(n = 0 ; n < len ; n++){
		SetLIns(l, SetDup(s)) ;
		SetLApp(l, SetDup(s)) ;
	}
	
	SetLCur2Top(l) ;
	while(SetLBot(l) != SetLCur(l)){
		fprintf(stderr, "cur = %x\n",SetLCur(l)) ; 
		SetLCurNext(l) ;
	}
	fprintf(stderr, "cur = %x\n\n",SetLCur(l)) ; ;
	
	while(SetLTop(l) != SetLCur(l)){
		fprintf(stderr, "cur = %x\n",SetLCur(l)) ;
		SetLCurPrev(l) ;
	}
	fprintf(stderr, "cur = %x\n\n",SetLCur(l)) ;
	
	SetLCur2Bot(l) ;
	while(SetLTop(l) != SetLCur(l)){
		fprintf(stderr, "cur = %x\n",SetLCur(l)) ;
		SetLCurPrev(l) ;
	}
	fprintf(stderr, "cur = %x\n\n",SetLCur(l)) ;
	
	SetLDel(l);	

	
	

  	SetList_t l ;
  	Set_t s,s1 ;
  	ulint len,n ;
  
  	len = 20 ;
	s = SetNew(len) ;
	s1 = SetDup(s) ;
	l = SetLNew() ;

	
	for(n = 0 ; n < len ; n++){
		SetCompl(s,s1) ;
		SetLIns(l, SetDup(s)) ;
	}
	
	SetLCur2Bot(l) ;
	do{
		s = SetLExtract(l) ;
		fprintf(stderr, "\nvvvvvvvvv\n") ;
		fprintf(stderr, "%s\n", SetStr(s)) ;
		fprintf(stderr, "\n") ;
		PrintSetL(l) ;
		fprintf(stderr, "\n^^^^^^^^\n") ;
		if(!SetLCurAtTop(l))
			SetLCurPrev(l) ;
	}while(!SetLCurAtTop(l)) ;
	
	#endif
	exit(0) ;

}


#endif

