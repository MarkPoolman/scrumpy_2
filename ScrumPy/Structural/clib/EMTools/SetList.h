/**
H. Hartman
IntVec - vectors of Sets
**/

#include "Set.h" 


typedef struct SetList *SetList_t ;


extern SetList_t SetLNew() ;
	/* pre: True
	  post: returns a usable SetList_t
	 */

	


void SetLDel(SetList_t l);
	/* pre: l usable
	  post: l' !usable, resources freed 
	 */
	 
	
/***** info about lists *****/


ulint SetLLen(SetList_t l) ;
	/* pre: l usable
	  post: returns length of l 
	*/
	


/***** retrieve items *******/

Set_t SetLTop(SetList_t l) ;
	/* pre: SetLLen(l) > 0
	  post: l' == l, returns topmost item
	*/
	
	
	

Set_t SetLCur(SetList_t l);
	/* pre: SetLLen(l) > 0
	  post: l' == l, returns current item
	*/
	


Set_t SetLLBot(SetList_t l) ;
	/* pre: SetLLen(l) > 0
	  post: l' == l, returns bottom item
	*/
	
SetList_t SetLDup(SetList_t l) ;
	/* pre: SetLLen(l) > 0
	  post: a duplicate of l has been returned, SetLCur2Bot(l)
	*/	

/**** Add items ****/
	
	
void SetLIns(SetList_t l, Set_t  s) ;
	/* pre: usable(l,s), !(SetLApp(l, s) || SetLIns(l, s))
	  post: SetLTop(l') == s
	 */
	
	 

void SetLApp(SetList_t l, Set_t s) ;
	/* pre: usable(l,s) !(SetLApp(l, s) || SetLIns(l, s)) 
	  post: SetLBot(l') == s
	 */


	 
	/**** Remove items ***/
	 
Set_t SetLPop(SetList_t l);
	/* pre: SetLLen(l) > 0
	  post: SetLLen(l') == SetLLen(l) -1,
	  		SetLTop(l') != SetLTop(l),
			SetLTop(l)  == SetLPop(l) ;
	*/



Set_t SetLChop(SetList_t l) ;
	/* pre: SetLLen(l) > 0
	  post: SetLLen(l') == SetLLen(l) -1,
	  		SetLBot(l') != SetLBot(l),
			SetLChop(l) == SetLBot(l) ;
	*/
	
		
	

Set_t SetLExtract(SetList_t l) ;
	/* pre: SetLLen(l) > 0
	  post: v = SetLExtract(SetList_t l) => v not present in l
	  		SetLCurAtTop(l) => SetLCurAtTop(l')
			!SetLCurAtTop(l) => SetLCurPrev(l) */

	



	
 /**** Navigate  ***/
	
void SetLCur2Top(SetList_t l) ;
	/* pre: usable(l)
	  post: SetLCur(l') == SetLTop(l')
	*/
	

void SetLCur2Bot(SetList_t l) ;
	/* pre: usable(l)
	  post: SetLCur(l') == SetLBot(l')
	*/ 



void SetLCurNext(SetList_t l) ;
	/* pre: SetLCur(l) != SetLBot(l) 
	  post: current moved to next in sequence
	*/ 
 	


void SetLCurPrev(SetList_t l) ;
	/* pre: SetLCur(l) != SetLTop(l) 
	  post: current moved to previous in sequence
	*/ 
 
 	


Bool SetLCurAtTop(SetList_t l) ;
	/* pre: True
	  post: SetLCurAtTop(l) => SetLCur(l) == SetLTop(l) */ 
 


Bool SetLCurAtBot(SetList_t l) ;
	/* pre: True
	  post: SetLCurAtTop(l) => SetLCur(l) == SetLTop(l) */ 
 


/********* printing *********/

void PrintSetL(SetList_t l);

	
