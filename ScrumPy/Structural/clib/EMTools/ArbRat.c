

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/*
Mark Poolman 21/03/01 onwards
ArbRat - Arbitrary precision rational number ADT, 
based on the gnu multiple precision (gmp) rational numbers
(maybe, depending on platform)
*/



#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#include "ArbRat.h"





ArbRat_t ArbRatCreate(void) {
	/* pre: True
	  post: Returns a Usable ArbRat_t of value zero
	*/

  ArbRat_t rv  ;
	rv = (MP_RAT *) malloc(sizeof(MP_RAT)) ;
	
	mpq_init(rv) ;
	return (rv) ;
}





ArbRat_t ArbRatNew(long int num, long int den) {
	/* pre: den != 0
	  post: returns Usable ArbRat representation of num/den
	*/
	
  ArbRat_t rv ;

  
	if (den < 0){
		den *= -1 ;
		num *= -1 ;
	}	/* can't have -ve denominators */
	
	rv = ArbRatCreate() ;
	
	mpq_set_si(rv, num, den) ;
	mpq_canonicalize(rv) ;
 
 
	return rv ;
}



  
ArbRat_t ArbRatDup(ArbRat_t r) {
	/* pre:	r Usable
	  post:  returns a usable dupiclate of r
	*/
	
  ArbRat_t rv ;
  
	rv = ArbRatCreate() ;
	mpq_set(rv,r) ;

	return rv ;
}



	
void ArbRatDel(ArbRat_t r) {
	/* pre: True
	  post: r' Not usuable, resources used by r freed
	*/ 
	
	mpq_clear(r) ;
	free(r) ;
}


extern void ArbRatSet(ArbRat_t r1, ArbRat_t r2){
	/* pre: r1,r2, Usable
	  post: r2' == r2, ArbRatIsEq(r1, r2) 
	*/
			
	mpq_set(r1, r2) ;
} 




void ArbRatNeg(ArbRat_t r) {
	/* pre: r usable
	  post: r' = -r    (quicker than mul by -1 )
	 */
	

  	mpq_neg(r,r) ;
} ;
	
	
void ArbRatAdd(ArbRat_t r1, ArbRat_t r2) {
	/* pre: r1,r2, Usable
	  post: r1' == r1 + r2
	*/

	mpq_add(r1,r1,r2) ;
}	
	


	
ArbRat_t ArbRatAddDup(ArbRat_t r1, ArbRat_t r2) {
	/* pre: r1,r2, Usable
	  post: ArbRatAddDup(r1, r2) == r1 + r2
	*/
	
	ArbRat_t rv ;
	rv = ArbRatCreate() ;
	mpq_add(rv, r1, r2) ;
	return rv ;
}



void ArbRatSub(ArbRat_t r1, ArbRat_t r2) {
	/* pre:	r1,r2, Usable
	  post: r1' == r1 - r2
	*/
	
	mpq_sub(r1,r1,r2) ;
}



ArbRat_t ArbRatSubDup(ArbRat_t r1, ArbRat_t r2) {
	/* pre: r1,r2, Usable
	  post: ArbRatAddDup(r1, r2) == r1 - r2
	*/
	

	ArbRat_t rv ;
	rv = ArbRatCreate() ;
	mpq_sub(rv, r1, r2) ;
	return rv ;
}



	
void ArbRatMul(ArbRat_t r1, ArbRat_t r2) {
	/* pre: r1,r2, Usable
	  post: r1' == r1 * r2
	*/
	
	
	mpq_mul(r1,r1,r2) ;
}
	




ArbRat_t ArbRatMulDup(ArbRat_t r1, ArbRat_t r2) {
	/* pre: r1,r2, Usable
	  post: ArbRatAddDup(r1, r2) == r1 * r2
	*/
	
		
	ArbRat_t rv ;
	rv = ArbRatCreate() ;
	mpq_mul(rv, r1, r2) ;
	return rv ;
}
	
	
	
void ArbRatDiv(ArbRat_t r1, ArbRat_t r2) {
	/* pre:	r1,r2, Usable
	  post: r1' == r1 / r2
	*/
	
	mpq_div(r1,r1,r2) ;
}
	
	
	
ArbRat_t ArbRatDivDup(ArbRat_t r1, ArbRat_t r2) {
	/* pre: r1,r2, Usable
	  post: ArbRatAddDup(r1, r2) == r1 / r2
	*/
	
	ArbRat_t rv ;
	rv = ArbRatCreate() ;
	mpq_div(rv, r1, r2) ;
	return rv ;
}
	

void ArbRat2Ints(ArbRat_t r, long int *num, long int *den){

	*num = mpz_get_si(mpq_numref(r)) ;
	*den = mpz_get_si(mpq_denref(r)) ;
} ;
	
	
bool ArbRat2Str(ArbRat_t r, char **str) {
	/* pre: r Usable, str references a valid pointer to char
	  post: *str' is a newly alloc()'d string representation of r
	*/
	
  size_t lennum, lenden, lendex, memreq ;
  bool rv = FALSE ;
  
	lennum = mpz_sizeinbase(mpq_numref(r),ArbRatReprBase) ;/* length of str reprn of num */
	
	lenden = mpz_sizeinbase(mpq_denref(r),ArbRatReprBase) ;/* ditto den */
	lendex =  5 ; 											/* strlen("+-/" and 2 nulls) */
	memreq =  lennum + lenden + lendex ;					/* total mem for this reprn */

	if(*str = (char *) calloc(memreq, 1)){					/* alloc mem for string */
		char *p = *str ;									/* p to index into string */
		mpz_get_str(p, ArbRatReprBase, mpq_numref(r)) ;		/* get str rep of numerator */
		while ( *p != 0) 									/* find the end of it */
			p++ ;
			
		*p = '/' ; p++ ;									/* decorate */
			
		mpz_get_str(p, ArbRatReprBase, mpq_denref(r)) ;	/* get str rep of denom */
		while ( *p != 0) 									/* find the end */
			p++ ;
	  	
	  	rv = TRUE ;
	}
	
	return rv ; 
}	
	
	
	
	
	
	
	

bool ArbRatEqZero(ArbRat_t r) {
	/* pre: r usable
	  post: ArbRateqZero(r) => r == 0
	*/

	return mpq_sgn(r) == 0 ;
}


	
bool ArbRatIsNeg(ArbRat_t r) {
	/* pre: r  usable
	  post: ArbRatIsNeg(r) => r < 0
	 */
	 
	 return mpq_sgn(r) == -1 ;
}
	 
	 
	 
bool ArbRatIsPos(ArbRat_t r) {
	/* pre: r  usable
	  post: ArbRatIsNeg(r) => r > 0
	 */
	 
	 return mpq_sgn(r) == 1 ;
}
	 
	 
	 
bool ArbRatEq(ArbRat_t r1, ArbRat_t r2) {
	/* pre: r  usable
	  post: ArbRatIsNeg(r) => r1 == r2
	 */

	return mpq_equal(r1,r2) ;
}


bool ArbRatGt(ArbRat_t r1, ArbRat_t r2) {
	/* pre: r  usable
	  post: ArbRatIsNeg(r) => r1 > r2
	 */

	return mpq_cmp(r1,r2) > 0 ;
}




bool ArbRatLt(ArbRat_t r1, ArbRat_t r2) {
	/* pre: r  usable
	  post: ArbRatIsNeg(r) => r1 < r2
	 */

	return mpq_cmp(r1,r2) < 0 ;
}



Pympq_new_RETURN ArbRat2PyRat(ArbRat_t r){
  Pympq_new_RETURN rv ;
  
  	fprintf(stderr, "< ArbRat2PyRat ") ;
  
  	rv = Pympq_new() ;
	fprintf(stderr, " Pympq_new() ") ;
	/*mpq_set(rv->q,r) ;*/
	fprintf(stderr, " mpq_set ") ;
	fprintf(stderr, " ArbRat2PyRat>") ;
	return rv ;
} 
	
	
	
	
	
#ifdef TEST
long int fact(long int x){

	if (x < 0) return -fact(-x) ;
	
	if (x == 0) return 1 ;
	
	return x*fact(x-1) ;
}   




main(){

ArbRat_t r ;
 char *str ;
 
 signed long int x,y ;
 Pympq_new_RETURN pyrat ;

	r = ArbRatNew(1,2) ;
	/*pyrat = ArbRat2PyRat(r) ;*/
	pyrat = Pympq_new() ;
} 










#ifdef DO_NOT_COMPILE_THIS_BIT 
	for (x = -5 ; x < 5 ; x++){   /**** test conversion to/from gmpy rats   ****/
 		for(y = -5 ; y < 5 ; y++){
			if(y != 0){
				r = ArbRatNew(x,y) ;
				ArbRat2Str(r, &str);
				fprintf(stderr, "%d %d %s  ", x,y,str) ;
			}
		}
		fprintf(stderr, "\n") ;
	}


 ArbRat_t r ;
 char *str ;
 
 signed long int x,y ;

 
	for (x = -5 ; x < 5 ; x++){   /**** test basic creation and str reprn   ****/
 		for(y = -5 ; y < 5 ; y++){
			if(y != 0){
				r = ArbRatNew(x,y) ;
				ArbRat2Str(r, &str);
				fprintf(stderr, "%d %d %s  ", x,y,str) ;
			}
		}
		fprintf(stderr, "\n") ;
	}
	


 ArbRat_t r, r2 ;				/********* test duplication **********/
 char *str, *str2 ;
 
 signed long int x,y ;
	for (x = -5 ; x < 5 ; x++){
 		for(y = -5 ; y < 5 ; y++){
			if(y != 0){
				r = ArbRatNew(x,y) ;
				r2 = ArbRatDup(r) ;
				ArbRat2Str(r, &str);
				ArbRat2Str(r2, &str2);
				fprintf(stderr, "%s %s  ",str, str2) ;
			}
		}
		fprintf(stderr, "\n") ;
	}
						
				
 ArbRat_t r, r2 ;		/******* test in place Add and Sub  ***********/		
 char *str, *str2 ;
 signed long int x,y ;
 	r2 = ArbRatCreate() ;
	for (x = 0 ; x < 100 ; x++){
 		for(y = 1 ; y < 100 ; y++){
			if(y != 0){
				r = ArbRatNew(x,y) ;
				ArbRatAdd(r2,r) ;
				ArbRat2Str(r, &str);
				ArbRat2Str(r2, &str2);
				fprintf(stderr, "%s %s\n",str, str2) ;
			}
		}
		
	}
	
	for (x = 0 ; x < 100 ; x++){
 		for(y = 1 ; y < 100 ; y++){
			if(y != 0){
				r = ArbRatNew(x,y) ;
				ArbRatSub(r2,r) ;
				ArbRat2Str(r, &str);
				ArbRat2Str(r2, &str2);
				fprintf(stderr, "%s %s\n",str, str2) ;
			}
		}
		
	}			
		
		

			
 ArbRat_t r, r1, r2, r3 ;			/********* test duplicate Add and Sub  ****************/
 char *str, *str1, *str2, *str3 ;
 signed long int x,y ;
 	r2 = ArbRatCreate() ;
	for (x = -5 ; x < 5 ; x++){
 		for(y = -5 ; y < 5 ; y++){
			if(y != 0 && x !=0){
				r = ArbRatNew(x,y) ;
				r1 = ArbRatNew(y,x) ;
				r2 = ArbRatAddDup(r,r1) ;
				r3 = ArbRatSubDup(r,r) ;
				ArbRat2Str(r, &str);
				ArbRat2Str(r1, &str1);
				ArbRat2Str(r2, &str2);
				ArbRat2Str(r3, &str3);
				fprintf(stderr, "%s\t%s\tAdd\t=\t%s\t::: Sub =\t%s\n",str, str1, str2, str3) ;
			}
		}
		
	}		




			
 ArbRat_t r, r2 ;		/******* test in place Mul and Div  ***********/		
 char *str, *str2 ;
 signed long int x,y ;
 	r2 = ArbRatNew(131071,534287) ; /* a couple of largish primes */ 
	for (x = -10 ; x < 10 ; x++){
 		for(y = -10 ; y < 10 ; y++){
			if(y != 0 && x != 0 ){
				r = ArbRatNew(x,y) ;
				ArbRatMul(r2,r) ;
				ArbRat2Str(r, &str);
				ArbRat2Str(r2, &str2);
				fprintf(stderr, "%s %s\n",str, str2) ;
			}
		}
		
	}
	for (x = -10 ; x < 10 ; x++){
 		for(y = -10 ; y < 10 ; y++){
			if(y != 0 && x != 0 ){
				r = ArbRatNew(x,y) ;
				ArbRatDiv(r2,r) ;
				ArbRat2Str(r, &str);
				ArbRat2Str(r2, &str2);
				fprintf(stderr, "%s %s\n",str, str2) ;
			}
		}
		
	}			
	


			
 ArbRat_t r, r1, r2, r3 ;			/********* test duplicate Mul and Div  ****************/
 char *str, *str1, *str2, *str3 ;
 signed long int x,y ;
 	r2 = ArbRatCreate() ;
	for (x = -5 ; x < 5 ; x++){
 		for(y = -5 ; y < 5 ; y++){
			if(y != 0 && x !=0){
				r = ArbRatNew(fact(x),fact(y)) ;
				r1 = ArbRatNew(fact(y),fact(x)) ;
				r2 = ArbRatMulDup(r,r1) ;
				r3 = ArbRatDivDup(r,r1) ;
				ArbRat2Str(r, &str);
				ArbRat2Str(r1, &str1);
				ArbRat2Str(r2, &str2);
				ArbRat2Str(r3, &str3);
				fprintf(stderr, "%s\t%s\tMul\t=\t%s\t::: Div =\t%s\n",str, str1, str2, str3) ;
			}
		}
		
	}		
	exit(0) ;
}



#endif 
#endif 
	
	
	

