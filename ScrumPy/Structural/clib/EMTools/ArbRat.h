

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/*
Mark Poolman 21/03/01
ArbRat - Arbitrary precision rational number ADT, 
based on the gnu multiple precision (gmp) rational numbers
(maybe, depending on platform)
*/


#ifndef ARBRAT_H
#define ARBRAT_H



	/* boolean constants, in case we don't already have them */
#ifndef FALSE
#define FALSE 0
#define TRUE !FALSE
#endif



#define bool int
	/* just to distinguish semantics */



#include <gmp.h>

typedef MP_RAT *ArbRat_t ;
#include <Python.h>
#include "gmpy.h"

			/******* Creation and Destruction ***********/


extern ArbRat_t ArbRatCreate(void) ;
	/* pre: True
	  post: Returns a Usable ArbRat_t of value zero
	*/

extern ArbRat_t ArbRatNew(long int num, long int den) ;
	/* pre: den != 0
	  post: returns Usable ArbRat representation of num/den
	*/

extern ArbRat_t ArbRatDup(ArbRat_t r) ;
	/* pre:	r Usable
	  post:  returns a usable dupiclate of r
	*/

extern void ArbRatDel(ArbRat_t r) ;
	/* pre: True
	  post: r' Not usuable, resources used by r freed
	*/ 




			/********* Arithmetic Functions ************/


extern void ArbRatSet(ArbRat_t r1, ArbRat_t r2) ;
	/* pre: r1,r2, Usable
	  post: r2' == r2, ArbRatIsEq(r1, r2) 
	*/


extern void ArbRatNeg(ArbRat_t r) ;
	/* pre: r usable
	  post: r' = -r    (quicker than mul by -1 )
	 */
		
extern void ArbRatAdd(ArbRat_t r1, ArbRat_t r2) ;
	/* pre: r1,r2, Usable
	  post: ' == r1 + r2
	*/
	
extern ArbRat_t ArbRatAddDup(ArbRat_t r1, ArbRat_t r2) ;
	/* pre: r1,r2, Usable
	  post: ArbRatAddDup(r1, r2) == r1 + r2
	*/
	
extern void ArbRatSub(ArbRat_t r1, ArbRat_t r2) ;
	/* pre:	r1,r2, Usable
	  post: r1' == r1 - r2
	*/
	
extern ArbRat_t ArbRatSubDup(ArbRat_t r1, ArbRat_t r2) ;
	/* pre: r1,r2, Usable
	  post: ArbRatAddDup(r1, r2) == r1 - r2
	*/
	
extern void ArbRatMul(ArbRat_t r1, ArbRat_t r2) ;
	/* pre: r1,r2, Usable
	  post: r1' == r1 * r2
	*/
	
extern ArbRat_t ArbRatMulDup(ArbRat_t r1, ArbRat_t r2) ;
	/* pre: r1,r2, Usable
	  post: ArbRatAddDup(r1, r2) == r1 * r2
	*/
	
extern void ArbRatDiv(ArbRat_t r1, ArbRat_t r2) ;
	/* pre:	r1,r2, Usable
	  post: r1' == r1 / r2
	*/
	
extern ArbRat_t ArbRatDivDup(ArbRat_t r1, ArbRat_t r2) ;
	/* pre: r1,r2, Usable
	  post: ArbRatAddDup(r1, r2) == r1 / r2
	*/



			/********** Comparison Functions   **********/
			
			
extern bool ArbRatEqZero(ArbRat_t r) ;
	/* pre: r usable
	  post: ArbRateqZero(r) => r == 0
	*/
	
extern bool ArbRatIsNeg(ArbRat_t r) ;
	/* pre: r  usable
	  post: ArbRatIsNeg(r) => r < 0
	 */
	 
extern bool ArbRatIsPos(ArbRat_t r) ;
	/* pre: r  usable
	  post: ArbRatIsPos(r) => r > 0
	 */
	 
extern bool ArbRatEq(ArbRat_t r1, ArbRat_t r2) ;
	/* pre: r  usable
	  post: ArbRatIsNeg(r) => r1 == r2
	 */

extern bool ArbRatGt(ArbRat_t r1, ArbRat_t r2) ;
	/* pre: r  usable
	  post: ArbRatIsNeg(r) => r1 > r2
	 */

extern bool ArbRatLt(ArbRat_t r1, ArbRat_t r2) ;
	/* pre: r  usable
	  post: ArbRatIsNeg(r) => r1 < r2
	 */


			/********** String representation of rats **********/

static const int ArbRatReprBase = 10   ; /* number base for string reprns of rats */

extern bool ArbRat2Str(ArbRat_t r, char **str) ;
	/* pre: r Usable, str references a valid pointer to char
	  post: *str' is a newly alloc()'d string representation of r
	*/

                  /********** Conversion of rats **********/

extern void ArbRat2Ints(ArbRat_t r, long int *num, long int *den) ;

extern Pympq_new_RETURN ArbRat2PyRat(ArbRat_t r) ;



#endif
