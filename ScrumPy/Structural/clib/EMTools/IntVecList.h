/**
H. Hartman
IntVec - vectors of integers
**/


#ifndef INTVECLIST_H
#define INTVECLIST_H

#include "IntVec.h" 

typedef struct IntVecList *IntVecList_t ;


/*** Create and Destroy  ***/

extern IntVecList_t IntVLNew() ;
	/* pre: True
	  post: returns a usable IntVecList_t 
	 */
	 
void IntVLDel(IntVecList_t l) ;
	/* pre: l usable
	  post: l' !usable, resources freed 
	 */	 


 
 /***** info about lists *****/
	  
ulint IntVLLen(IntVecList_t l) ;
	/* pre: l usable
	  post: returns length of l 
	*/
	
	
/***** retrieve items *******/
	
IntVec_t IntVLCur(IntVecList_t l);
	/* pre: IntVLLen(l) > 0
	  post: l' == l, returns current item
	*/
	
	
IntVec_t IntVLCur(IntVecList_t l);
	/* pre: IntVLLen(l) > 0
	  post: l' == l, returns current item
	*/
	
	

IntVec_t IntVLBot(IntVecList_t l) ;
	/* pre: IntVLLen(l) > 0
	  post: l' == l, returns bottom item
	*/
	
/**** Add items ****/


void IntVLIns(IntVecList_t l, IntVec_t v) ;
	/* pre: usable(l,v), !(IntVLApp(l, v) || IntVLIns(l, v))
	  post: IntVLTop(l') == v
	 */
	 
void IntVLApp(IntVecList_t l, IntVec_t v) ;
	/* pre: usable(l,v) !(IntVLApp(l, v) || IntVLIns(l, v)) 
	  post: IntVLBot(l') == v
	 */


	 
/**** Remove items ***/
	 
IntVec_t IntVLPop(IntVecList_t l);
	/* pre: IntVLLen(l) > 0
	  post: IntVLLen(l') == IntVLLen(l) -1,
	  		IntVLTop(l') != IntVLTop(l),
			IntVLTop(l)  == IntVLPop(l) ;
	*/

IntVec_t IntVLChop(IntVecList_t l) ;
	/* pre: IntVLLen(l) > 0
	  post: IntVLLen(l') == IntVLLen(l) -1,
	  		IntVLBot(l') != IntVLBot(l),
			IntVLChop(l) == IntVLBot(l) ;
	*/

IntVec_t IntVLExtract(IntVecList_t l) ;
	/* pre: IntVLLen(l) > 0
	  post: v = IntVLExtract(IntVecList_t l) => v not present in l
	  		IntVLCurAtTop(l) => IntVLCurAtTop(l')
			!IntVLCurAtTop(l) => IntVLCurPrev(l) */


/**** Navigate  ***/

void IntVLCur2Top(IntVecList_t l) ;
	/* pre: usable(l)
	  post: IntVLCur(l') == IntVLTop(l')
	*/

void IntVLCur2Bot(IntVecList_t l);
	/* pre: usable(l)
	  post: IntVLCur(l') == IntVLBot(l')
	*/ 

void IntVLCurNext(IntVecList_t l) ;
	/* pre: IntVLCur(l) != IntVLBot(l) 
	  post: current moved to next in sequence
	*/ 

void IntVLCurPrev(IntVecList_t l) ;
	/* pre: IntVLCur(l) != IntVLTop(l) 
	  post: current moved to previous in sequence
	*/ 

Bool IntVLCurAtTop(IntVecList_t l);
	/* pre: True
	  post: IntVLCurAtTop(l) => IntVLCur(l) == IntVLTop(l) */ 
	  
	  
Bool IntVLCurAtBot(IntVecList_t l);
	/* pre: True
	  post: IntVLCurAtTop(l) => IntVLCur(l) == IntVLTop(l) */
	   
/****** printing **********/	  
	   
void PrintIntVL(IntVecList_t l);


#endif
