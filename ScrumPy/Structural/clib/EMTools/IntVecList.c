/**
H. Hartman
IntVec - vectors of integers
**/

#include <stdio.h>
#include <stdlib.h>

#include "IntVecList.h"


/*************** LL node stuff, copied from ARVecList and adapted (bypasses the need for including all AR stuff) ************/


struct Node  {
	struct Node *Next ;
	IntVec_t Vec ;
}  ;

typedef struct Node *Node_p ;

static Node_p NewNode(void){

	return (Node_p) calloc(sizeof(struct Node), 1 ) ;
} 


static void DelNode(Node_p n){

	if(n->Vec)
		IntVecDel(n->Vec) ;
		
	free(n) ;
} ;


static Node_p ParNode(Node_p Adam, Node_p targ){ /* find parent in list */

	if(Adam->Next == targ)
		return Adam ;
		
	return ParNode(Adam->Next, targ) ;
}



static void PrintNodes(Node_p n){

	if(n){
		/* fprintf(stderr, "%x\t%x\t| %x\n", n, n->Next, n->Vec) ;*/
		printEls(n->Vec) ;
		PrintNodes(n->Next) ;
	}
}


static void DelNodes(Node_p n){

	fprintf(stderr, "deln %p\n", n) ; 
	if(n){
		DelNodes(n->Next) ;
		DelNode(n) ;
	}
}

/******** lists **********/

struct IntVecList{

	Node_p Top, Cur, Bot ;
	int len ;
} ;


/*** Create and Destroy  ***/


IntVecList_t IntVLNew() {
	/* pre: True
	  post: returns a usable IntVecList_t 
	 */

	return (IntVecList_t) calloc(sizeof(struct IntVecList), 1) ;
} ;


void IntVLDel(IntVecList_t l){
	/* pre: l usable
	  post: l' !usable, resources freed 
	 */
	 
	DelNodes(l->Top) ;
	free(l) ;
}


/***** info about lists *****/


ulint IntVLLen(IntVecList_t l) {
	/* pre: l usable
	  post: returns length of l 
	*/
	
	return l->len ;
}


/***** retrieve items *******/

IntVec_t IntVLTop(IntVecList_t l) {
	/* pre: IntVLLen(l) > 0
	  post: l' == l, returns topmost item
	*/
	
	return (l->Top->Vec) ;
}
	

IntVec_t IntVLCur(IntVecList_t l){
	/* pre: IntVLLen(l) > 0
	  post: l' == l, returns current item
	*/
	
	return (l->Cur->Vec) ;
}


IntVec_t IntVLBot(IntVecList_t l) {
	/* pre: IntVLLen(l) > 0
	  post: l' == l, returns bottom item
	*/
	
	return (l->Bot->Vec) ;
}

IntVecList_t IntVLDup(IntVecList_t l) { 
	/* pre: IntVLLen(l) > 0
	  post: a duplicate of l has been returned, IntVLCur2Bot(l)
	*/
	IntVecList_t cpy ;
	
	cpy = IntVLNew() ;
	IntVLCur2Top(l) ;
	IntVLIns(cpy, IntVecDup(IntVLCur(l))) ;
	while (IntVLCur(l) != IntVLBot(l)){
		IntVLCurNext(l) ;
		IntVLIns(cpy, IntVecDup(IntVLCur(l))) ;
	}
	
	
	return (cpy) ;	
}




/**** Add items ****/

void IntVLIns(IntVecList_t l, IntVec_t v) {
	/* pre: usable(l,v), !(IntVLApp(l, v) || IntVLIns(l, v))
	  post: IntVLTop(l') == v
	 */
	 
	Node_p new ;
	 
	new = NewNode() ;
	new->Vec = v ;
	new->Next = l->Top ;
	l->Top = new ;
	l->len++ ;
	if (l->len == 1){
		l->Cur = l->Bot = l->Top ;
	}
}
	 

void IntVLApp(IntVecList_t l, IntVec_t v) {
	/* pre: usable(l,v) !(IntVLApp(l, v) || IntVLIns(l, v)) 
	  post: IntVLBot(l') == v
	 */
	
	Node_p new ;
	if(l->len == 0)
		return(IntVLIns(l,v)) ;
		
	new = NewNode() ;
	new->Vec = v ;
	
	l->Bot->Next = new ;
	l->Bot = new ;
	l->len++ ;
}



	 
	/**** Remove items ***/
	 
IntVec_t IntVLPop(IntVecList_t l){
	/* pre: IntVLLen(l) > 0
	  post: IntVLLen(l') == IntVLLen(l) -1,
	  		IntVLTop(l') != IntVLTop(l),
			IntVLTop(l)  == IntVLPop(l) ;
	*/

	IntVec_t rv ;
	Node_p old ;
	
	old = l->Top ;
	l->Top = l->Top->Next ;
	
	if(l->Cur == old)
		l->Cur = l->Cur->Next ;
	
	if(l->Bot == old)
		l->Bot = 0 ;
		
	l->len-- ;
	rv = old->Vec ;
	old->Vec = 0 ;
	free(old) ;
	return rv ;
}


IntVec_t IntVLChop(IntVecList_t l) {
	/* pre: IntVLLen(l) > 0
	  post: IntVLLen(l') == IntVLLen(l) -1,
	  		IntVLBot(l') != IntVLBot(l),
			IntVLChop(l) == IntVLBot(l) ;
	*/
	
		
	IntVec_t rv ;
  	Node_p old ;
  
  	if(l->Bot == l->Top)
		return(IntVLPop(l)) ;
  
  	old = l->Bot ;
	l->Bot = ParNode(l->Top, l->Bot) ;
	l->Bot->Next = 0 ;
	
	
	if(l->Cur == old)
		l->Cur = l->Bot ;
		
	l->len-- ;
	rv = old->Vec ;
	old->Vec = 0 ;
	free(old) ;
	return rv ;
}
	

IntVec_t IntVLExtract(IntVecList_t l) {
	/* pre: IntVLLen(l) > 0
	  post: v = IntVLExtract(IntVecList_t l) => v not present in l
	  		IntVLCurAtTop(l) => IntVLCurAtTop(l')
			!IntVLCurAtTop(l) => IntVLCurPrev(l) */

	Node_p parent ;
	IntVec_t rv ; 
	
	if(l->Cur == l->Top){
		return(IntVLPop(l)) ;
	}
	if(l->Cur == l->Bot){
		return IntVLChop(l) ;
	} 
		
	parent = ParNode(l->Top, l->Cur) ;  /* chop out the current node */
	parent->Next = l->Cur->Next ;  

	rv = l->Cur->Vec ;					/* extract the information from it */	
	free(l->Cur) ;						/* destroy the old node */
	l->Cur = parent ;					/* point l->Cur somewhere useful */
	l->len-- ;	
	return (rv) ;
}



	
 /**** Navigate  ***/
	
void IntVLCur2Top(IntVecList_t l) {
	/* pre: usable(l)
	  post: IntVLCur(l') == IntVLTop(l')
	*/
	
	l->Cur = l->Top ;
}

void IntVLCur2Bot(IntVecList_t l){
	/* pre: usable(l)
	  post: IntVLCur(l') == IntVLBot(l')
	*/ 
	
	l->Cur = l->Bot ;
}


void IntVLCurNext(IntVecList_t l) {
	/* pre: IntVLCur(l) != IntVLBot(l) 
	  post: current moved to next in sequence
	*/ 
 	l->Cur = l->Cur->Next ;
}


void IntVLCurPrev(IntVecList_t l) {
	/* pre: IntVLCur(l) != IntVLTop(l) 
	  post: current moved to previous in sequence
	*/ 
 
 	l->Cur = ParNode(l->Top, l->Cur) ;
}


Bool IntVLCurAtTop(IntVecList_t l){
	/* pre: True
	  post: IntVLCurAtTop(l) => IntVLCur(l) == IntVLTop(l) */ 
 
 	return(l->Cur == l->Top) ;
}


Bool IntVLCurAtBot(IntVecList_t l){
	/* pre: True
	  post: IntVLCurAtTop(l) => IntVLCur(l) == IntVLTop(l) */ 
 
 	return(l->Cur == l->Bot) ;
}


/********* printing *********/

void PrintIntVL(IntVecList_t l){

	fprintf(stderr, "%p\t%d\t%p\t%p\t%p\n\n", l, l->len, l->Top, l->Cur, l->Bot) ;
	PrintNodes(l->Top) ;
}


/********** testing *********/


#ifdef TEST

 
 
 main(){
 
 #ifdef DONT_COMPILE_THIS
 
 /********* create and destroy ****/

 
  	IntVecList_t l ;
	IntVec_t v ;

	v = IntVecNew(5) ;
	l = IntVLNew() ;
	
	fprintf(stderr, "l = %x\t, len = %d\n", l, IntVLLen(l)) ;
	
	PrintIntVL(l) ;
	
	IntVLDel(l) ;
	fprintf(stderr, "del\n") ;
	l = IntVLNew() ; 
 	IntVLIns(l, v) ;
	fprintf(stderr, "insert l = %x\t, len = %d\n", l, IntVLLen(l)) ;
	fprintf(stderr, "vpointer %x\t%x\t%x\t%x\n", 
		v,
		IntVLTop(l),
		IntVLCur(l),
		IntVLBot(l)) ;
		
	IntVLDel(l) ;
	fprintf(stderr, "done\n") ;
	



/**** longer list this time ****/


 	IntVecList_t l ;
	IntVec_t v ;
 	int len,n ;
  
  	len = 5 ;
	v = IntVecNew(len) ;
	l = IntVLNew() ;
	
	for(n = 0 ; n < len ; n++){
		IntVLIns(l, IntVecDup(v)) ;
		IntVLApp(l, IntVecDup(v)) ;
	}
	PrintIntVL(l) ;
	IntVLDel(l);





/******** remove items -1 IntVLPop ************/


 	IntVecList_t l ;
 	IntVec_t v ;
 	int len,n ;
  
  	len = 5 ;
	v = IntVecNew(len) ;
	l = IntVLNew() ;
	
	for(n = 0 ; n < len ; n++){
		IntVLIns(l, IntVecDup(v)) ;
		IntVLApp(l, IntVecDup(v)) ;
	}
	PrintIntVL(l) ;


	while(IntVLLen(l) != 0){
		v = IntVLPop(l) ;
		fprintf(stderr, "\npopped %x\n", v) ;
		PrintIntVL(l) ;
	}


	IntVLDel(l);	
	
/************ duplicate content ***********/
#endif
	IntVecList_t l, cop ;
 	IntVec_t v ;
 	ulint len,n ;
  
  	len = 5 ;
	v = IntVecNew(len) ;
	l = IntVLNew() ;
	
	for(n = 0 ; n < len ; n++){
		IntVLIns(l, IntVecDup(v)) ;
		IntVLApp(l, IntVecDup(v)) ;
	}
	
	fprintf(stderr, "\n\n Org:") ;
	PrintIntVL(l) ;
	
	cop = IntVLDup(l) ;
	
	fprintf(stderr, "\n\n Copy:") ;
	
	PrintIntVL(cop) ;
	
	IntVLDel(l) ;
	IntVLDel(cop) ;

#ifdef DONT_COMPILE_THIS

/******** remove items -2 IntVLChop ************/


 	IntVecList_t l ;
 	IntVec_t v ;
 	int len,n ;
  
  	len = 20 ;
	v = IntVecNew(len) ;
	l = IntVLNew() ;
	
	for(n = 0 ; n < len ; n++){
		IntVLIns(l, IntVecDup(v)) ;
		IntVLApp(l, IntVecDup(v)) ;
	}
	PrintIntVL(l) ;


	while(IntVLLen(l) != 0){
		v = IntVLChop(l) ;
		fprintf(stderr, "\nchopped %x\n", v) ;
		PrintIntVL(l) ;
	}


	IntVLDel(l);		



 	IntVecList_t l ;
 	IntVec_t v ;
	int len,n ;
  
  	len = 2 ;
	v = IntVecNew(len) ;
	l = IntVLNew() ;
	
	for(n = 0 ; n < len ; n++){
		IntVLIns(l, IntVecDup(v)) ;
		IntVLApp(l, IntVecDup(v)) ;
	}
	
	IntVLCur2Top(l) ;
	while(IntVLBot(l) != IntVLCur(l)){
		fprintf(stderr, "cur = %x\n",IntVLCur(l)) ; 
		IntVLCurNext(l) ;
	}
	fprintf(stderr, "cur = %x\n\n",IntVLCur(l)) ; ;
	
	while(IntVLTop(l) != IntVLCur(l)){
		fprintf(stderr, "cur = %x\n",IntVLCur(l)) ;
		IntVLCurPrev(l) ;
	}
	fprintf(stderr, "cur = %x\n\n",IntVLCur(l)) ;
	
	IntVLCur2Bot(l) ;
	while(IntVLTop(l) != IntVLCur(l)){
		fprintf(stderr, "cur = %x\n",IntVLCur(l)) ;
		IntVLCurPrev(l) ;
	}
	fprintf(stderr, "cur = %x\n\n",IntVLCur(l)) ;
	
	IntVLDel(l);	


  	IntVecList_t l ;
  	IntVec_t v ;
  	int len,n ;
  
  	len = 20 ;
	v = IntVecNew(len) ;
	l = IntVLNew() ;
	
	for(n = 0 ; n < len ; n++){
		IntVecAddSc(v, 1,0) ;
		IntVLIns(l, IntVecDup(v)) ;
	}
	
	IntVLCur2Bot(l) ;
	do{
		v = IntVLExtract(l) ;
		fprintf(stderr, "\nvvvvvvvvv\n") ;
		printEls(v) ;
		fprintf(stderr, "\n") ;
		PrintIntVL(l) ;
		fprintf(stderr, "\n^^^^^^^^\n") ;
		if(!IntVLCurAtTop(l))
			IntVLCurPrev(l) ;
	}while(!IntVLCurAtTop(l)) ;
	#endif
	exit(0) ;

}


#endif












