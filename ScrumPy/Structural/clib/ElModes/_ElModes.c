

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/***
M.G.Poolman  11/04/01 onwards
ElModes - for generate Elementary Modes from a Python Tableau
***/

#include <stdio.h>

#include <stdlib.h>
#include "Py2C.h"
#include"ARVecList.h"
/** other typedefs inherited from vecList **/

#include"_ElModes.h"

/*
An extra comment
to demonstrate cvs
*/

struct Tabl {

	Set_t 	Irrevs,	/* the set of irreversible reactions		*/ 
			Zeros_i,/* S(m_i^j) in Schuster, Fell + Dandekar	*/   
			Zeros_k,/* S(m_k^j)			---"---				 	*/
			Zeros_l,/* S(m_l^(j+1)		---"---					*/
			Inter_ik, /* S(m_i^j) n S(m_k^j)					*/
			NegStos ; /* reactions w/ negative k in the next potential mode */
			
	ulint RowLen,	/* length of tableau rows */
		  nMets,	/* no. of metabolites  */
		  ColElim ; /* columns eliminated so far */
	ARVecList_t Tabl ; /* the Tableau */
} ;





typedef struct Tabl *Tabl_t ;



void TablDel(Tabl_t tab){

    SetDel(tab->Irrevs) ;
	SetDel(tab->Zeros_i) ;
	SetDel(tab->Zeros_k) ;
	SetDel(tab->Zeros_l) ;
	SetDel(tab->Inter_ik) ;
	SetDel(tab->NegStos) ;
	
	ARVLDel(tab->Tabl) ;
	free(tab) ;
}

Tabl_t TablIni(PyObject *ptabl) {

  Tabl_t rv ;
  int *Irrevs, nirr, n ;
  
    fprintf(stderr, "<TablIni ") ;

  	rv = (Tabl_t) malloc(sizeof(struct Tabl))  ;
	
	rv->RowLen = PyInt_AsLong(PyObject_GetAttrString(ptabl, "RowLen")) ;
	 fprintf(stderr, "RowLen %d ", rv->RowLen ) ;
	
	rv->nMets = PyInt_AsLong(PyObject_GetAttrString(ptabl, "nMets")) ;
	fprintf(stderr, " nmets %d ", rv->nMets ) ;
	rv->ColElim = 0 ;
	
	rv->Irrevs = SetNew(rv->RowLen) ;
	Irrevs = PyIntList2CIntArry(PyObject_GetAttrString(ptabl, "IrrevIdxs")) ;
	fprintf(stderr, " got irrevs " ) ;

	nirr = PyInt_AsLong(PyObject_GetAttrString(ptabl, "IrrevLen")) ;
	fprintf(stderr, " nirr %d, *Irrevs %x",nirr, Irrevs ) ;
	if(Irrevs){
		for(n = 0 ; n < nirr ; n++){
			fprintf(stderr, "adding irrev %d (%d)\n", n, Irrevs[n]) ;
			SetAddMem(rv->Irrevs, Irrevs[n]) ;
		}
	}


	rv->Zeros_i = SetNew(rv->RowLen) ;
	rv->Zeros_k = SetNew(rv->RowLen) ;
	rv->Zeros_l = SetNew(rv->RowLen) ;
	rv->Inter_ik = SetNew(rv->RowLen) ;
	rv->NegStos = SetNew(rv->RowLen) ;
	rv->Tabl = PyListList2ArVecList(PyObject_GetAttrString(ptabl, "rows")) ;

	free(Irrevs) ;
	fprintf(stderr, " TablIni>\n") ;

	
	return(rv) ;
}

Tabl_t TablNew(Tabl_t tab){
	/* Pre: true
	  Post: returns a duplicate of tab, but with an empty veclist */
	  
  Tabl_t rv ;
	  
  	rv = (Tabl_t) malloc(sizeof(struct Tabl))  ;
	
	rv->RowLen = tab->RowLen ;
	rv->nMets = tab->nMets ;
	
	rv->Irrevs 	=	SetDup(tab->Irrevs) ;
	rv->Zeros_i =	SetDup(tab->Zeros_i) ;
	rv->Zeros_k =	SetDup(tab->Zeros_k) ;
	rv->Zeros_l =	SetDup(tab->Zeros_l) ;
	rv->Inter_ik =	SetDup(tab->Inter_ik) ;
	rv->NegStos = SetNew(rv->RowLen) ;
	
	rv->Tabl = ARVLNew() ; ;

	return(rv) ;
}



Bool CorrectRevers(Set_t Irrevs, Set_t tmp, ARVec_t vec){
	/* check, and attempt to correct vec so that it conforms to reversibility contraints in 
	Irrevs using tmp as workspace */


	ARVecNegs(tmp, vec) ;
	SetInter(tmp, tmp, Irrevs) ;
	if(!SetIsEmpty(tmp)){
		ARVecNegate(vec) ;
		ARVecNegs(tmp, vec) ;
		SetInter(tmp, tmp, Irrevs) ;
	}
	return (SetIsEmpty(tmp)) ;
}




void InsertOrReplace(ARVecList_t tabl, ARVec_t row, Set_t ZsInRow, Set_t ZsInNext, int CurCol){
	/* pre: ZsInRow is set of indxs of zero vals in row 
	insert row into next, removing redundant rows from next a la condition 7, 
	contents of  ZsInNext not defined  */
	
#ifdef TEST_COND7
	
  int count, n ;
	
	ARVLCur2Top(tabl) ;
	count = 0 ;
	for(n = 0 ; n < ARVLLen(tabl) ; n++){
		ARVecZeros(ZsInNext, ARVLCur(tabl), CurCol) ;
		if(SetIsSub(ZsInRow, ZsInNext)){
			count++ ;
			fprintf(stderr, "cond 7.%d %d\n", count, ARVLLen(tabl) ) ;
			ARVecDel(ARVLExtract(tabl)) ;
			fprintf(stderr, "7del1 %d\n", ARVLLen(tabl)) ;
		}
		if(!ARVLCurAtBot(tabl))
			ARVLCurNext(tabl) ;
	}
#endif
	ARVLIns(tabl, row) ;
}





void ConditionalAddRow(Tabl_t Cur, Tabl_t Next){

  int n, len, curcol ;
  Bool OKSoFar ;
  ArbRat_t k ;
  ARVec_t toprow, candrow, newrow ; 				/* top row and candidate row for combination */

	OKSoFar = TRUE ; 								/* well, nothing seems to have gone wrong yet */

	toprow  = ARVLTop(Cur->Tabl) ;
	candrow = ARVLCur(Cur->Tabl) ;
	curcol = Cur->ColElim ;
	
	if(!ArbRatEqZero(ARVecGetEl(candrow, curcol) )){
		
		SetInter(Cur->Inter_ik, Cur->Zeros_i, Cur->Zeros_k) ; 		
		len = ARVLLen(Next->Tabl) ;						/* test condition 4 */
		ARVLCur2Top(Next->Tabl) ;
		for( n = 0 ; n < len && OKSoFar ; n++){
			ARVecZeros(Next->Zeros_l, ARVLCur(Next->Tabl), Cur->nMets -1) ;
			OKSoFar = !SetIsSub( Cur->Inter_ik, Next->Zeros_l) ;
			ARVLCurNext(Next->Tabl) ;
		}
		
		if(OKSoFar){									/* condition 4 OK */
			k = ArbRatDivDup(ARVecGetEl(toprow, curcol), 
							 ARVecGetEl(candrow, curcol)) ;
			newrow = ARVecDup(candrow) ;				/* make the newrow to be inserted */ 
			ARVecMulSc(newrow, k, curcol) ;
			ARVecSub(newrow, toprow, curcol) ;
			if(CorrectRevers(Cur->Irrevs, Cur->NegStos, newrow))  /* check no -ve irrevs */ 
				InsertOrReplace(Next->Tabl, 			/* put new row into next, possibly */
								newrow, 				/* removing redundant rows in next */
								Cur->Inter_ik, 
								Next->Zeros_l,
								curcol) ;
			else
				ARVecDel(newrow) ;						/* violates irrev constraints, can't use */
														
			ArbRatDel(k) ;
		}
	}
}


Bool AddDirect(Tabl_t Cur, Tabl_t Next){

  int n, len ;
  ARVec_t curvec ;
  Bool CanAddIt ;
  
  	CanAddIt = FALSE ;
	curvec = ARVLTop(Cur->Tabl) ;
	if(ArbRatEqZero(ARVecGetEl(curvec, Cur->ColElim))){
		CanAddIt = TRUE ; 			/* at least so far */
		ARVecZeros(Cur->Zeros_i, curvec, Cur->nMets-1) ; /* get pos of zeros in row */
		len = ARVLLen(Next->Tabl) ;
		ARVLCur2Top(Next->Tabl) ;
		for(n = 0 ; n < len && CanAddIt ; n++){
			ARVecZeros(Next->Zeros_i, ARVLCur(Next->Tabl), Cur->nMets-1) ;
			CanAddIt = !SetIsSub(Cur->Zeros_i, Next->Zeros_i) ;
			ARVLCurNext(Next->Tabl) ;
		}
	}
	if(CanAddIt)
		ARVLIns(Next->Tabl, ARVLPop(Cur->Tabl)) ;
		
	return CanAddIt ;
}
		


void CalcNextTabl(Tabl_t Cur, Tabl_t Next){
	
  int n, len ;
  ARVec_t curvec ;
  ARVecList_t curtabl ;
  
	curtabl = Cur->Tabl ;
	while(ARVLLen(curtabl) > 0){				/* elimintate all rows */
		curvec = ARVLTop(curtabl) ; 			/* read first row from current tableau */
		ARVecZeros(Cur->Zeros_i, curvec, Cur->nMets-1) ; /* get pos of zeros in first row */
		if(!AddDirect(Cur, Next)){				/* can't add the current topmost to next tableau, so */
			len = ARVLLen(curtabl) ;
			ARVLCur2Top(curtabl) ;
			for(n = 1 ; n < len ; n++){			/* loop over remaining items */  
				ARVLCurNext(curtabl) ;			/* next candidate to combine w/ top vec */
				ARVecZeros(Cur->Zeros_k,		/* get the set of zero posns of the candidate */
					ARVLCur(curtabl), Cur->nMets-1) ;
				ConditionalAddRow(Cur, Next) ;	/* attempt to generate new row in Next */
			}
			ARVecDel(ARVLPop(curtabl)) ;		/* no further use for this top row */
		}
	}
	Next->ColElim = Cur->ColElim + 1 ;
}
	


void CTabl2PyTabl(PyObject *ptabl, Tabl_t CurTabl){

  ARVec_t curvec ;
  ARVecList_t tabl ;
  PyObject *PyRes, *PyRow, *PyRatStr ;
  char *RatStr;
  
  long int row, nrows, col, ncols ;

	tabl = CurTabl->Tabl ;
	nrows = ARVLLen(tabl) ;
	ncols = CurTabl->RowLen ;
	PyRes = PyObject_GetAttrString(ptabl, "Results") ;
	
	ARVLCur2Top(tabl) ;
	for(row = 0 ; row < nrows ; row++){
		curvec = ARVLCur(tabl) ;
		PyRow = PyList_New(ncols) ;
		Py_INCREF(PyRow) ;
		for(col = 0 ; col < ncols ; col++){
			ArbRat2Str(ARVecGetEl(curvec, col),&RatStr) ;
			PyRatStr = PyString_FromString(RatStr) ;
			Py_INCREF(PyRatStr) ;
			free(RatStr) ;
			PyList_SetItem(PyRow, col, PyRatStr) ;
		}
		PyList_Append(PyRes, PyRow) ;
		ARVLCurNext(tabl) ;
	}
} 



void CheckElementary(Tabl_t tabl){
	/* remove any non-elementary modes that might have crept in */
	
	ARVecList_t list = tabl->Tabl ;
	int n,len = ARVLLen(list), IsElem , SomeLeft, NoNext ;
	ARVec_t cur ;
	Set_t nzcur, nztest ;
	nzcur = tabl->Zeros_i ;
	nztest = tabl->Zeros_k ;
	

	for (n = 1 ; n < len ; n++){   /* from 1 so as to skip if only one on list */
		ARVLCur2Top(list) ; 
		cur = ARVLPop(list) ;
		ARVecNonZeros(nzcur, cur, 0) ;
		if (ARVLLen(list) != 0){
			IsElem = 1 ; SomeLeft = 1 ;
			while (IsElem && SomeLeft){
				NoNext = 0 ;
				ARVecNonZeros(nztest, ARVLCur(list),0) ;
				IsElem = !SetIsSub(nztest, nzcur) ;
				if(IsElem && SetIsSub(nzcur,nztest)) {
					NoNext = ARVLCurAtTop(list) ; /* don't go to "next" if we remove top, current will be the next */
					ARVecDel(ARVLExtract(list)) ;
					len -- ;
				}
				SomeLeft = !ARVLCurAtBot(list) ;
				if (SomeLeft && !NoNext)
					ARVLCurNext(list) ;
			}
			if (IsElem){
				ARVLApp(list,cur) ;
			}
			else{
				ARVecDel(cur) ;
				len -- ;
			}
		}
	}
}
			
			
		
	
	

void CalcElModes(PyObject *ptabl ) {
	/* pre: ptabl is a correctly initialised Python tableau
	  post: ptabl' contains the elementary modes
	 */
	
	 
  Tabl_t CurTabl, NexTabl, Swap ;
  int n ;
    fprintf(stderr, "\n <CalcElModes") ;
	CurTabl = TablIni(ptabl) ;
	fprintf(stderr, " curt ") ;
	NexTabl = TablNew(CurTabl) ;
	fprintf(stderr, " next ") ;
	
	for(n = 0 ; n < CurTabl->nMets ; n++){
		fprintf(stderr, "t %d of %d s = %d\n", n, CurTabl->nMets, ARVLLen(CurTabl->Tabl)) ; 
		
		CalcNextTabl(CurTabl, NexTabl) ;
		Swap = CurTabl ;
		
		CurTabl = NexTabl ;
		NexTabl = Swap ;
	}
	CheckElementary(CurTabl) ;
	CTabl2PyTabl(ptabl, CurTabl) ;
	TablDel(CurTabl) ;
	TablDel(NexTabl) ;
	fprintf(stderr, " CalcElModes>") ;
}




extern PyObject *ElModes(PyObject *self, PyObject *args){

	PyObject *in ;
	fprintf(stderr,"\n< ElModes 01.07.05 - 1 ") ;
	PyArg_ParseTuple(args, "O", &in) ;
	fprintf(stderr," Parsed tuple ") ;

	CalcElModes(in) ;
	
	fprintf(stderr,"\n04.02.04 - 3 ElModes>\n") ;
	Py_INCREF(Py_None) ;
	return Py_None ;
} ;












