import sys


from ScrumPy.Util import Sci
from ScrumPy.Data import DataSets


def Consistify(E, Obs):
    "delete and reorder columns in Modes and Obs so as to leave them consistent"

    for reac in E.rnames[:]:
        if  not reac  in Obs.cnames:
            E.DelRow(reac)

    for ob  in Obs.cnames[:]:
        if not ob in E.rnames:
            Obs.DelCol(ob)

    Obs.ColReorder(E.rnames)  # column ordering of E and Obs now identical



def DecomposeVec(E, v,Irrevs):

    #print "DecomposeVec",E, v,Irrevs

    sciv = Sci.matrix(v).transpose()
    #E = E.Copy(float)
    sciE = Sci.matrix(E.rows)
    g = Sci.pinv(sciE)

    e = g*sciv # <<<------  the decomposition : e = v.E#
    evals = e.transpose().tolist()[0]

    Again = False
    EMnames = E.cnames[:]
    for i in range(len(evals)):
        if evals[i] <0 and EMnames[i] in Irrevs:
            Again = True
            E.DelCol(EMnames[i])
    if Again:
        print "Again"
        return  DecomposeVec(E,v, Irrevs)
    else:
        vrec = Sci.vector(sciE * e)
        diff = vrec.transpose()-v
        RelErr = Sci.Mod(diff[0])/Sci.Mod(v)
        return E.cnames, evals, RelErr, vrec



def DecomposeSet(Modes, Obs):

    #print "DecomposeSet 1",Modes, Obs

    E = Modes.mo.Copy(float)
    Obs = Obs.Copy()

    #print "DecomposeSet 2",E, Obs

    Consistify(E,Obs)

    #print "DecomposeSet 3",E, Obs

    rv = []
    Irrevs = Modes.Irrevs()

    for v in Obs:
        rv.append(DecomposeVec(E,v,Irrevs))
        print ".",

    return rv


class Decompose(DataSets.DataSet):

    def __init__(self,Modes, Obs):

        
        self.Recs = []                                # a list of recovered fluxes
        self.Modes = Modes
        ItemNames=["RelErr"] + Modes.mo.cnames
        DataSets.DataSet.__init__(self, ItemNames=ItemNames)  # create self as  an empty data set for the assignments
        
        Raw_e =  DecomposeSet(Modes,Obs)            # get a list of result tuples

        for e in Raw_e:
            names,vals,relerr = e[0],e[1],e[2]
            self.NewRow()
            self[-1,0] = relerr
            for n in range(len(names)):
                self[-1,names[n]] = vals[n]
      
        #self.Recs.append(e[3])                     # hang on to the recovered fluxes - ? Why we need ??
            
        self.rnames = Obs.rnames[:]
        for c in Obs.cnames:                           # populate self with unused obs (e.g. time)
            if c not in Modes.mo.cnames:               # not an observation used in the calculation
                self.NewCol(Obs.GetCol(c),c)           # so copy it in




    def FromDS(self,ds,Copy=1):

        if Copy:
            ds = ds.Copy()
        self.rows = ds.rows
        self.rnames = ds.rnames
        self.cnames = ds.cnames
        self.Conv = ds.Conv


    def Filter(self, *args, **kwargs):

        rv = Decompose(self.Modes)
        rv.FromDS(
            DataSets.DataSet.Filter(self, *args, **kwargs), Copy=0
        )
        return rv






















