import EnzSubset, StoMat
from ScrumPy.Util import Format, Sci, Stack, Set
from ScrumPy.ModelDescription.lexer import t_Irrev, t_BackIrrev, t_Rever

##TODO:
##    make method of Model?


requote_list = lambda list: map(lambda string: requote_str(string), list)
requote_str = lambda string: '"'+str(string)+'"'

    

def _update_quotes(m):
    ''' Private. Update current m.md.QuoteMap with new (unquoted) reactions '''
    
    for reac in m.sm.cnames:
        if not m.md.QuoteMap.has_key(reac):
            m.md.QuoteMap[reac] = reac
    
    
def make_mtx_mul_consistant(mtx1, mtx2):
    ''' PRIVATE
            pre      :       mtx1.cnames superset of mtx2.rnames, (at least) mtx1 is StoMat
            post    :       mtx1.cnames  = mtx2.rnames, by removal of difference between  mtx1.cnames and mtx2.rnames
    '''
    remove = Set.Difference(mtx1.cnames, mtx2.rnames)
    for r in remove:
        mtx1.DelReac(r)

def RenameEss(ess,sfx):
    '''
            pre     :   ess - instance of EssDic
                        sfx - suffix to append to ess keys

            post    :   all es keys ends with '_'+sfx
    '''

    for k in ess.keys():
        if len(ess[k])>1:
            ess.RenameSS(k,k+"_"+str(sfx))


def HasSubsets(ess):

    '''
            pre     :   ess is instance of EssDic
            post    :   True if ess has subset(s)
    '''

    for ss in ess:
        if len(ess[ss]) >1:
            return True
    return False


def ReadModel(fname, recname = None):
    '''
            pre     :   fname - name of condesed model
                        recname - name of record file, if not provided will be obtaiend as fname.split('.spy')[0]
            pst     :   CondensedModel instance with model and record as in args

    '''

    m_cond = ScrumPy.Model(fname)
    if not recname:
        recname = fname.split('.spy')[0] #we're importing the record file, so drop .py from name
    rec = __import__(recname)
    record = rec.record[:]
    stack = Stack.Stack()
    for item in record:
        stack.Push(item)
    cond = CondensedModel(m_cond,stack=stack)
    return cond
    
    


class CondensedModel:
    '''
    Class for condensing models, saving condensed model and record.
    '''
    def __init__(self,m, stack = None):
        '''
            pre     :   m - model
                        stack - if None, new, empty Stack instance is created, else self.Stack is populated with stack
            post    :   CondensedModel instance created with model and Stack as specified in args, nits (number of iterations)
                        initialised to zero

        '''
        
        self.model = m
        self.Stack = Stack.Stack()
        if stack:
            stack = stack.Copy()
            while len(stack) !=0:
                self.Stack.Push(stack.Pop())
       
        self.nits = 0

    def Condense(self):
        '''
                pre     :   True
                post    :   model in self.model has been condensed to subsets

        '''

        isos = self.model.DelIsoforms()
        e = self.model.EnzSubsets()
        print self.nits
        e.RemoveSS("DeadReacs")
        RenameEss(e,self.nits)
        self.nits += 1
        self.Stack.Push([isos,e,e.ReacRevProps])
        
        emtx = e.ToMtx(ExclDeadIncs=True)
        make_mtx_mul_consistant(self.model.smx, emtx)
        smx = self.model.smx.Mul(emtx)
        smx.RevProps=emtx.RevProps
        smx.ZapZeroes()
        smx.Irrevs = e.Irrev_ss()

        self.model.smx=smx
        self.model.sm = smx.Copy()
        extern = self.model.Externals()
        _update_quotes(self.model)
        
        for ext in Set.Intersect(extern,self.model.sm.rnames):
            self.model.sm.DelRow(ext)
    
        CanCondense = len(isos) >0 or HasSubsets(e)
        CanCondense = CanCondense and len(self.model.sm) >0
    
        if CanCondense:  
            self.Condense()

    def SaveCondModel(self, ReQuote=True, fname=None, recname=None):
        '''
                pre     :   ReQuote - temp., true if saved model should be quoted like original model
                            fname - name of model file, if None original filename + _Condensed.spy
                post    :   condensed model has been saved with fname
        '''
        
        if not fname:
            modelname = [key for key in self.model.md.FileNames if self.model.md.FileNames[key][0]=='None'][0]
            root = modelname.split('.spy')[0]
            fname = root+"_Condensed.spy"
        if not recname:
            recname = fname.replace('.spy','.py')
             
        self.model.smx.ToScrumPy(self.model.md.QuoteMap,fname,Externs=self.model.Externals())
        record_file = open(recname,"w")
        record_file.write('record = ')
        record_file.write(Format.FormatNested(self.Stack._Stack,Quoted=False))
        record_file.close()


    def ExpandSol(self, sol):
        '''
                pre     :   sol - matrix of solutions (possibly singleton) where, sol.rnames == self.model.sm.cnames and model is condensed
                post    :   expanded solution returned where sol.cnames == exp_sol.cnames, exp_sol.rnames is subset of original model
                            (dead reactions, inconsitant subsets, and isostoichiometric reactions are ignored)
        '''

        if not self.Stack.IsEmpty():
            stack_item = self.Stack.Pop()
            isos, ess_dict, rev_dict = stack_item[0], stack_item[1], stack_item[2]
            ssmtx = self.make_essmtx(ess_dict)
            ssmtx.ColReorder(sol.rnames)
            exp_sol = ssmtx.Mul(sol)
            for iso in isos:
                if any(exp_sol.GetRow(iso)):
                    exp_sol = self.make_iso_sol(exp_sol, iso, isos, rev_dict[iso]==t_Rever)
            exp_sol = self.ExpandSol(exp_sol)
        else:
            exp_sol = sol
        
        return exp_sol


    def make_iso_sol(self, solmtx, k, isos, k_rev):
        '''
            private: given a solution matrix (solmtx), a isostoichiometry dict (isos),
            and a key k in isos, return solmtx such that all alternative solutions associated with k in solmatx
            involving all items in isos[k] have been added to solmtx. If k is reversible (k_rev == True) k
            and any item in isos[k] can form a futile cycle
        '''

        r2c_dict = solmtx.InvolvedWith(k)
        for iso in isos[k]:
            for c in r2c_dict:
                new_c = c+'_'+iso
                new_stodict = solmtx.InvolvedWith(c)
                new_stodict[iso] = new_stodict[k]
                del new_stodict[k]
                solmtx.NewReaction(new_c,new_stodict)
            if k_rev: #k is reversible, i.e. iso must also be, k and iso can form futile cycle
                futile_sol={k:solmtx.Conv(1),iso:solmtx.Conv(-1)}
                futile_name = k+'_'+iso+'_futile'
                solmtx.NewReaction(futile_name,futile_sol)
        return solmtx
                

    def make_essmtx(self,ess_dict): #model field in EssDict object in stack_item will be condensed, thus EssDict.ToMtx will not work
        '''
                            private
                pre     :   ess_dict - dict of ess to rxn
                post    :   matrix of subset dictionary in stack_item returned
        '''

        cnames = ess_dict.keys()
        rnames = []
        for ss in ess_dict:
            rnames.extend(ess_dict[ss].keys())
        ssmtx = StoMat.StoMat(rnames = rnames, cnames = cnames)
        for ss in ess_dict:
            for reac in ess_dict[ss]:
                ssmtx[reac,ss] = ess_dict[ss][reac]
        return ssmtx
    
    def GetIsoList(self):
        '''
                pre     :   not self.Stack.IsEmpty()
                post   :   lsit of isostoichiometry disctionaries returned   
        '''
        
        stack_copy = self.Stack.Copy()
        rv = []
        while not stack_copy.IsEmpty():
            rv.append(stack_copy.Pop()[0])
        rv.reverse()
        return rv
        
        



            



    
