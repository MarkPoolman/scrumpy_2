

"""
Module for CPLEX-based Minimal Cut Set analysis. Algorithm is based on "Enumeration of Smallest Intervention Strategies in
Genome-Scale Metabolic Networks", Axel von Kamp and Steffen Klamt (2014).

"""
try:
    from ScrumPy.LP import ScrumPyCMIP
except ImportError:
    raise ImportError("Couldn't import ScrumPyCMIP, probably because cplex isn't installed.")
    

####################

from ScrumPy.Structural import StoMat
from ScrumPy.Util import Set
from ScrumPy.Data import DataSets
from ScrumPy.LP import ScrumPyLP
from ScrumPy.ModelDescription import lexer


#Some definitions of static variables, all inequaleties are inclusive (>=, <=) 
GREATER_THAN = ScrumPyCMIP .GREATER_THAN
LESS_THAN =ScrumPyCMIP .LESS_THAN
EQUAL_TO = ScrumPyCMIP .EQUAL_TO
RANGED =ScrumPyCMIP .RANGED

#internal defenitions for problem types
lin =ScrumPyCMIP .lin
#quad = ScrumPyCMIP .quad
indi = ScrumPyCMIP .indi
bina = ScrumPyCMIP.bina#not currenly in use

#other internal
VerySmall =ScrumPyCMIP .VerySmall
IsZero = ScrumPyCMIP.IsZero
_fix_back =ScrumPyCMIP ._fix_back



#local defs
vp  = 'vp'
vn = 'vn'
targ_var = 'w'

#internal tags

indivar_tag =  ScrumPyCMIP.indivar_tag
indiconstr_tag = ScrumPyCMIP .indiconstr_tag
constr_tag =  ScrumPyCMIP.constr_tag
 
#local tags

vp_tag = 'vp_'
vn_tag = 'vn_'
back_tag = '_back'



def _make_dual_revmet_mtx(sm,  targs=None,  targ_mtx =None):
    '''private, pre-processign of stoichiometry matrix for MCS object.'''
    sm_dual = sm.Copy()
    sm_dual.Transpose()
    sm_dual.RevProps={}
    for c in sm_dual.cnames:
        sm_dual.RevProps[c]=lexer.t_Rever
    sm_dual=ScrumPyLP.SplitRev(sm_dual)
    cn = sm.cnames[:]
    vp_names=[vp_tag+r for r in cn]
    vn_names=[vn_tag+r for r in cn]
    ident_pos = StoMat.StoMat(r=len(cn), c=len(cn), rnames=vp_names, cnames=vp_names)
    ident_neg = StoMat.StoMat(r=len(cn), c=len(cn), rnames=vn_names, cnames=vn_names)
    ident_pos.MakeIdent(1)
    ident_neg.MakeIdent(-1)

    if targ_mtx==None and targs!=None:
        targ_mtx =  StoMat.StoMat(r=len(cn), c=1, rnames=cn, cnames=[targ_var])
        if Set.IsSubset(targs, cn):
            for targ in Set.Intersect(targs, cn):
                targ_mtx[targ, targ_var] = -1
    elif targ_mtx==None and targs==None:
        sys.stderr.write('Supply targ or targ_mtx!\n')
        return sm_dual
    sm_dual.AugCol(ident_pos)
    sm_dual.AugCol(ident_neg)
    sm_dual.AugCol(targ_mtx)
    return sm_dual
    
    
def _split_selected(sm, fixed, bound):
    '''private, splitting of reactions if fixed or bounds are included in MCS object'''
    rv = sm.Copy()
    rv.Reversed = reversed = []
    rv.Backs = backs = []
    for r in fixed:
       if  fixed[r]<0.0:
           if sm.RevProps[r] ==lexer.t_Rever:
               backs .append(r)
           elif sm.RevProps[r] == StoMat.t_BackIrrev:
               reversed.append(r)
    for r in bound:
       if bound[r][0]<0.0 or bound[r][0]==None:
            if sm.RevProps[r] ==lexer.t_Rever:
               backs .append(r)
            elif sm.RevProps[r] == lexer.t_BackIrrev:
               reversed.append(r)
    for r in backs[:]:
        sto = sm.InvolvedWith(r)
        rback = r+back_tag
        rv.NewReaction(rback,sto)
        rv.MulCol(rback,k=-1)
    for r in reversed[:]:
        rv.MulCol(r,  k=-1)
    return rv
    

class MCS(ScrumPyCMIP.cmip):

    def __init__(self, m, targs=None, fixed={}, bound={}, q_coeff =-1):
        
        self.model=m
        self.sm = _split_selected(m.sm, fixed, bound)
        self.Backs = self.sm.Backs[:]
        self.Reversed = self.sm.Reversed[:]
        cn =  self.sm.cnames[:]
        self.targ_mtx =  StoMat.StoMat(r=len(cn), rnames=cn) #targets, whether reactions, flux points or bounds are 
                                                                                                  #included as a matrix, augumented to dual mtx: 
                                                                                                  #N_dual*r_dual*I-T x [u v w]^T = 0, where T mtx is target mtx. 
                                                                                                  #See von Kamp and Klam (2014) for details.
        self.targ_vec = StoMat.StoMat(r=1)
        if targs!=None:
            self.targs=targs[:]
            self.targ_vec.NewCol(col=[-1], name=targ_var+'_0')
            self.targ_mtx.NewCol(col=[0]*len(cn), name=targ_var+'_0')
            for targ in targs:
                self.targ_mtx[targ,targ_var+'_0']=-1
        if fixed:
            self._set_fixed(fixed)
        if bound:
            self._set_bound(bound)
        sm_dual_rev = _make_dual_revmet_mtx(self.sm, targs, self.targ_mtx)
        self.constr_mtx=sm_dual_rev.Copy()
        self.sm_dual_rev = sm_dual_rev.Copy()
        ScrumPyCMIP.cmip.__init__(self, m, sm=sm_dual_rev)  #call to constructor of superclass, cmip, note that the sm that goes into constr. is sm_dual
        self.indicator_constraints_names = []
        if not (len(self.targ_mtx.cnames)==len(self.targ_vec.cnames)==0):
            w_constr=self.targ_mtx.cnames[:]
            self.targ_vec.ChangeType(float)
            flag, range, rhs=self._clp__getbnds(None, q_coeff)
            self.linear_constraints.add(names=['incon_constr'], lin_expr=[[w_constr,self.targ_vec.GetRow(0)]],senses=[flag], rhs=[rhs], range_values=[range])
            self.linear_constraints_names.append('incon_constr')
        self.updateRowDics()
        self.updateColDics()
    
    def _set_fixed(self, fixed):
        '''private'''
        cn=self.sm.cnames[:]
        for k in fixed:
            is_rev = k in self.sm.Backs or k in self.sm.Reversed
            flux = fixed[k]
            if flux<0.0:
                self._set_targ_col(k+back_tag, lo=-flux, hi=-flux)
                self._set_targ_col(k, lo=0.0, hi=0.0)
            elif flux>=0.0:
                 self._set_targ_col(k, lo=flux, hi=flux)
                 if is_rev:
                     self._set_targ_col(k+back_tag, lo=0.0, hi=0.0)

    
    def _set_targ_col(self, key, coeff_lo=-1, coeff_hi=1, lo=None, hi=None):
        '''private'''
        cn=self.sm.cnames[:]
        if lo!=None:
           col_lo = [0]*len(cn)
           col_lo[cn.index(key)]=coeff_lo
           len_mtx=len(self.targ_mtx.cnames)
           self.targ_mtx.NewCol(col=col_lo, name=targ_var+'_'+str(len_mtx))
           self.targ_vec.NewCol(col=[-lo], name=targ_var+'_'+str(len_mtx))
        if hi!=None:
            col_hi = [0]*len(cn)
            col_hi[cn.index(key)]=coeff_hi
            len_mtx=len(self.targ_mtx.cnames)
            self.targ_mtx.NewCol(col=col_hi, name=targ_var+'_'+str(len_mtx))
            self.targ_vec.NewCol(col=[hi], name=targ_var+'_'+str(len_mtx))
            
            
    def _set_bound(self, bound):
        '''private'''
        for k in bound:
            lo, hi=bound[k]
            is_rev = k in self.Backs or k in self.Reversed
            if not None in (lo, hi):
                if lo<0.0:
                    self._set_targ_col(k+back_tag, hi=-lo)
                elif lo>=0.0:
                    if is_rev:
                        self._set_targ_col(k+back_tag, lo=0.0, hi=0.0)
                    self._set_targ_col(k, lo=lo)
                if hi<0.0:
                    self._set_targ_col(k+back_tag, lo=-hi)
                    self._set_targ_col(k,lo=0.0, hi=0.0)
                elif hi>=0.0:
                    if is_rev and lo<0.0:
                        self._set_targ_col(k+back_tag, lo=0.0)
                    self._set_targ_col(k, hi=hi)
            elif lo==None: #lo and hi cannot be None at the same time
                if hi<0.0:
                    self._set_targ_col(k+back_tag, lo=-hi)
                    self._set_targ_col(k,lo=0.0, hi=0.0)
                elif hi>=0.0:
                    if is_rev:
                        self._set_targ_col(k+back_tag, lo=0.0)
                    self._set_targ_col(k,lo=0.0, hi=hi)
            elif hi==None:
                if lo<0.0:
                     self._set_targ_col(k+back_tag,lo=0.0,  hi=-lo)
                     self._set_targ_col(k,lo=0.0)
                elif lo>=0.0:
                    if is_rev:
                        self._set_targ_col(k+back_tag,lo=0.0,  hi=0.0)
                    self._set_targ_col(k,lo=lo) 
                     
                     
    def _get_constr_mtx(self):
        return self.constr_mtx
        
    def _get_targ_mtx(self):
        return self.targ_mtx
    
    def _get_rhs_mtx(self):
        return self.targ_vec
    
    def GetNames(self, type):
        '''     pre      :   type -  lin||indi||'all'||(vp||vn)
                post    :   returns list of variables in accodence with type  
        '''
        if type==lin:
            return self.GetColNames()
        elif type==indi:
            return self.GetIndiVarNames()
        elif type == 'all':
            return self.GetAllVarNames()
        elif type == vp or type == vn:
            return self.GetAllMCSVars()
           
           
    def GetPrimSol(self, IncZeroes=False,FixBack=True,AsMtx=False,threshold=VerySmall, kind=lin):
        
        sol={}
        variables  = self.GetNames(kind)
        sol = dict(zip(variables,self.solution.get_values(variables)))
        cols = sol.keys()  
        for r in cols:
            if IsZero(sol[r],threshold) and not IncZeroes:
                del sol[r]
        for rev in Set.Intersect(self.sm.Reversed,  sol.keys()):
            sol[rev] *= -1
        if FixBack:
            sol=_fix_back(sol)
        if  AsMtx:
            rv = StoMat.StoMat(cnames=["lp_sol"],rnames=sol.keys(),Conv=float)
            for r in sol:
                rv[r][0] = sol[r]
        else:
            rv = sol
        return rv

    
    def GetAllMCSVars(self):
        '''     pre        :       True
                post      :       Return all vp and vn variables
        '''
        return [i for i in self.variables_names[:] if i.startswith(vp_tag) or  i.startswith(vn_tag)]
        
    def SetInt2ContConstr(self,vars=[], coeff=1,  tol=1):
        '''
            pre     :       vars - continuous variables to constraint to indicator variables, default all cont. varisables.
                              coeff - coefficient of vars in constraint, default 1.
                              tol - cutoff cont. flux below which a reaction is concidered to be off, defualt 1.
            post    :      For each vars (r_i) indicator constraint added: z_i=0 -> r_i =0.0 and z_i =1 -> r_i >= tol
        '''
        vars = vars or self.GetNames(vp)
        for var in vars:
            self.AddIndiConstraint(lin_expr=[[var], [coeff]], sense=GREATER_THAN, rhs=tol, indvar=indivar_tag+var,complemented=0) 
            self.AddIndiConstraint(lin_expr=[[var], [coeff]], sense=EQUAL_TO, rhs=0, indvar=indivar_tag+var,complemented=1)
    
    def AddRevConstr(self, revs=[]):
        ''' pre: self.SetInt2ContConstr() has been executed
        post: z_i_forward + z_i_rev <= 1, for indicator variables for reaction i.
        '''
        if len(revs)==0:
            for col in self.sm_dual_rev.cnames: 
                if col.startswith(vp_tag) or col.startswith(vn_tag):
                    revs.append(col)
        rev_pairs={}
        for p in revs:
            for n in revs:
                if p.split(vp_tag)[-1]==n.split(vn_tag)[-1]:
                    rev_pairs[p]=n
        for p in rev_pairs:
            name= constr_tag+str(self.linear_constraints.get_num())
            self.linear_constraints_names.append(name)
            flag, range, rhs=self._clp__getbnds(0.0, 1.0)
            self.linear_constraints.add(lin_expr=[[[indivar_tag+p, indivar_tag+rev_pairs[p]], [1,1]]],senses=[flag], rhs=[rhs],range_values=[range], names=[name])
    
    def ExludeVpVnSol(self,  sol):
        '''
                pre     :       sol - dictionary-representation of solution.
                post   :         is exluded from feasible flux space
        '''
        opp_dict = {vp_tag: vn_tag, vn_tag: vp_tag}
        all_vars = []
        for act in sol:  
            act_split =  act.split(indivar_tag)
            reac = act_split[-1][3:]
            type = act_split[-1][0:3]
            opposite = indivar_tag+opp_dict[type]+reac
            all_vars.extend([act, opposite])
        cut = len(sol) - 1
        self.AddSumConstraint(lowBound = None, upBound = cut, cnames = all_vars)
            

    def GetElMoBySize(self, max_mode=None):
        '''
            pre     :       True. Only linear constraints added.
                              max_size - largest possible mode, default all reactions in the model.
            post    :     Elementary modes up to max_size returned as a dictionary or dataset.
        '''
        self.SetLenObjective(self.GetNames(vp))
        vars = self.GetNames(indi)
        rv={}
        if max_mode==None:
            max_mode = len(vars)
        self.AddRevConstr() 
        self.AddNonZeroConstr() 
        self.Solve(False)
        if not self.IsStatusOptimal():
            sys.stderr.write("Problem infeasible! \n")
            return rv
        ref_sol = self.GetPrimSol(kind=vp)
        size = len(ref_sol)
        self.ClearObjective()
        self.Solve(False)
        while size <= max_mode and self.IsStatusOptimal():
            self.AddLenConstr(size, constr_name='size_constr')
            self.PopulateSolutionPool()
            pool = self.GetSolPool(sol_type=indi, FixBack=False)
            for sol in pool:
                rv['sol_'+str(len(rv))]=pool[sol]
                self.ExludeVpVnSol(pool[sol])
            self.DelRow('size_constr')
            size+=1
            self.Solve(False)
        return rv
    
    def PostProcess(self, modes):
        '''
            pre         :       modes is dict of rv from self.GetElMoBySize()
            post        :       returns dictionary where keys are keys in 'modes' and values are dict of unique reactions to empty strings
        '''
        rv={}
        check_unique={}
        for  mode in modes:
            rv[mode]={}
            for targ in modes[mode]:
                reac_taged = targ.split(indivar_tag)[-1]
                if vp_tag in targ:
                    reac = targ.split(indivar_tag)[-1].split(vp_tag)[-1]
                elif vn_tag in targ:
                    reac = targ.split(indivar_tag)[-1].split(vn_tag)[-1]
                rv[mode][reac]=modes[mode][targ]
            key_str=rv[mode].keys()[:]
            key_str.sort()
            key_str=str(key_str)
            if check_unique.has_key(key_str):
                del rv[mode]
            else:
                check_unique[key_str]=''
    
        return rv
        
def GetMCS(m, targs=None, fixed={}, bounds={},  max_size=None):
    '''
            pre     :       m - model
                              targs - list of target reactions to abolish. Either targs, fixed or bounds must be supplied. Use of fixed or bounds is discuraged.
                              fixed - fixed flux values to abolish.
                              bounds - flux bounds to abolish.
                              max_size - maximum size of any one cut set
                              
            post    :       returns nested dictionary such that each key represents a cut set (in accordance with pre-conditions),
                                subdictionaries (keys) contain knock-out targets.
    '''
    
    mcs = MCS(m, targs, fixed, bounds)
    em_mcs = mcs.GetElMoBySize(max_size)
    em_post = mcs.PostProcess(em_mcs)
    
    return em_post
        
