"""


ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

import sys,os

Path = None

##
## If idle is located in some non-standard location
## (i.e. not under the directories pointed to by sys.path)
## hand edit the "Path" variable above to point to the
## directory in which IdleConf.py is to be found
##
##
##
##




pathl = __file__.split(os.sep)
path = ""
for p in pathl[1:-3]:
   path += os.sep+p
sys.path.append(path) # make sure that ScrumPy is on the path


from  ScrumPy.Util import File


errmsg = "!!! Check you python distribution\n\
!!! and/or hand edit ScrumPy/GUI/IdlePath.py\n!!!"

if Path == None:
    
    path = sys.path[:]
    for p in sys.path:
        if p.startswith("/home"):
            path.remove(p)
            
    res = File.FindIn(sys.path, "IdleConf.py")
    if len(res) == 0:
        print "!!!\n!!!\n can't find IdleConf.py" +errmsg
        raise ImportError
    elif len(res) >1:
        print "!!!\n!!! More than one IdleConf.py found !\n\
        !!! which one should I use ?" + errmsg
        raise ImportError
    else:
        Path = res[0]
    
