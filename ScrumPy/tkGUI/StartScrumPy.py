#!/usr/bin/python


"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

import os,sys

print "StartScrumPy - scipy devel branch 10/11"

# now set up the idle stuff - IdleConf shoud be on IdlePath
path = __file__.rsplit(os.sep, 3)[0]
print path,  __file__
sys.path.append(path)
import ScrumPy
from ScrumPy.ThirdParty.idle import IdleConf


idle_dir = os.path.dirname(IdleConf.__file__)
IdleConf.load(idle_dir)

# defer importing Pyshell until IdleConf is loaded
from ScrumPy.ThirdParty.idle import PyShell # but don't start until we have sorted out ScrumPy
#PyShell = gui.PyShell


idleinit ="""
import ScrumPy
from ScrumPy.tkGUI import gui
from ScrumPy.ThirdParty.idle import ObjectBrowser
ScrumPy.Init(gui)
gui.init4idle(PyShell)
"""

sys.argv.append("-c")
sys.argv.append(idleinit)
sys.argv.append("-t")

sys.argv.append("ScrumPy - scipy devel branch")

PyShell.main()








