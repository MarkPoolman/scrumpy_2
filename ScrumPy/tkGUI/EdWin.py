"""


ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
from ScrumPy.ThirdParty.idle import PyShell, EditorWindow

import Tkinter


import FileDialogs

#$ event <<compile>>
#$ win <Alt-c>
#$ unix <Alt-c>



class EdWin(EditorWindow.EditorWindow):
    
        def __init__(self, FileName=None, UI=None, Root=None, *args, **argsl):

            EditorWindow.EditorWindow.__init__(self,
                                                 PyShell.flist,
                                                 FileName,
                                                 None,
                                                 PyShell.root)
            self.ui = UI   # the editor window only knows about its own file contents. Menu events get passed back to the ui of whic it is a part
            self.guiroot = Root
            self.MakeMenu()
            self.text.bind("<<compile>>",self.MenuCompile)
            
        
        def close(self):
            
            if self.text != None:# text == None => window already closed
                EditorWindow.EditorWindow.close(self)


        def SaveFile(self):
            if self.text != None:   # text == None => window already closed
                self.menubar.children["file"].invoke(8)
                

        def MenuCompile(self):
            self.ui.Reload()


        def MenuSaveSBML(self):
            fname = FileDialogs.SaveFileName()
            self.ui.SaveSBML(str(fname)) # str because libSBML barfs on unicode


        def MenuHideChildren(self):
            self.ui.HideChildren()


        def MenuShowChildren(self):
            self.ui.ShowChildren()




        def MakeMenu(self):

            Menu = Tkinter.Menu(PyShell.root)
            Menu.add("command",{"label":"Compile", "command":self.MenuCompile})
            Menu.add("command",{"label":"Save SBML", "command": self.MenuSaveSBML})
            Menu.add("command", {"label":"Hide Children", "command": self.MenuHideChildren})
            Menu.add("command", {"label":"Show Children", "command": self.MenuShowChildren})
           
            
            self.menubar.add("cascade",{"label":"ScrumPy","menu":Menu})


