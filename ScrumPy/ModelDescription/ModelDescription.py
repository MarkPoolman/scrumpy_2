
import os, sys, types,  math
from ScrumPy.Util import Stack,  Set,  Types


import Parser,  lexer

ToolsPath = "Tools" # added to sys path, relative to the first model file

DeQuote = lambda s: s[1:-1] if s[0] == s[-1] == '"' else s

Str2Float = lambda s: eval(s+".0") if "/" in s else float(s)


FunNames = "pow",  "exp",  "log",  "log10",  "sqrt"
FunDic = dict(zip(FunNames,  map(lambda s: getattr(math,  s), FunNames)))
# dictionary of functions usuable by initialisations
DebugParser=False


class Sto:
    def __init__(self, sto):
        """ pre: sto=[ [ (CoeffS1,Subst1),...], [ (Prod1, CoefP1),...], Direc]
                    Direc = "<>" | "->" | "<-"
                     (supplied by Parser) """


        def TupList2Dic(tl, pfx=""):
            rv = {}
            for t in tl:
                rv[t[1]] = pfx+t[0]
            return rv


        self.lhs = sto[0]
        self.rhs = sto[1]
        self.Direc = sto[2]

        self.lhsd = TupList2Dic(sto[0], pfx="-")
        self.rhsd =TupList2Dic(sto[1])

    def __str__(self):
        return self.AsStr()

    def AsStr(self):
        lhs = " + ".join(   map(lambda tup:" ".join(tup),  self.lhs) )
        rhs = " + ".join(   map(lambda tup:" ".join(tup),  self.rhs) )
        return  " ".join((lhs,  self.Direc,  rhs))

    def AsDic(self):
        rv  = dict(self.lhsd)
        rv.update(self.rhsd)
        return rv


    def Conflicting(self):
        """ pre: True
            post: list of metabolites common to lhs and rhs"""

        return Set.Intersect(self.lhsd.keys(),  self.rhsd.keys())

    def SetAutoPolymer(self, ap):

        conflicts = self.Conflicting()
        if len(conflicts) >0:
            if ap == "Produced":
                hsd = self.lhsd
            else:
                hsd = self.rhsd

            for met in conflicts:
                del hsd[met]
                    
        
   
    
    
    def MassActionFun(self, Params=("1" ,"1"), TypeFun=float):
      
        def StoTerm2MassAct(stoterm):
            
            power = Str2Float(stoterm[0])
            
    
            if power == 1:
                return stoterm[1]
            else:
                return "pow(" + stoterm[1] +", " +  str(power) +")"
                #return " ".join(( stoterm[1], "**", str(power) ))
        
    
        rv = ""
        
        
        if len(Params) == 2:
            V,  Keq = Params
        else:
            print "Bad Params for mass action"
            #TODO: graceful handling of error condition
            
            
        lhs = self.lhs
        rhs = self.rhs
        
        Substs = ""
        for met in lhs[:-1]:
            Substs += StoTerm2MassAct(met) + " * "
        met = lhs[-1:][0]
        Substs += StoTerm2MassAct(met) 
        
        Prods = ""    
        for met in rhs[:-1]:
            Prods += StoTerm2MassAct(met) + " * "
        met = rhs[-1:][0]
        Prods += StoTerm2MassAct(met) 
        
        rv = " ".join((V,"*(", Substs, "-",  Prods,  "/", Keq, ")"))
      
    
        return rv



    
    def EquilFun(self, Keq):
      
        def StoTerm2MassAct(stoterm):
            
            power = Str2Float(stoterm[0])
            
            if power == 1:
                return stoterm[1]
            else:
                return "pow(" + stoterm[1] +", " +  str(power) +")"
                #return " ".join(( stoterm[1], "**", str(power) ))
        
    
        rv = ""
        lhs = self.lhs
        rhs = self.rhs
        
        Substs = ""
        for met in lhs[:-1]:
            Substs += StoTerm2MassAct(met) + " * "
        met = lhs[-1:][0]
        Substs += StoTerm2MassAct(met) 
        
        Prods = ""    
        for met in rhs[:-1]:
            Prods += StoTerm2MassAct(met) + " * "
        met = rhs[-1:][0]
        Prods += StoTerm2MassAct(met) 
        
        if Keq.count(" ") >0 and Keq[0] != "(":
            Keq = "(" + Keq + ")" # if Keq is an expression enusure it is parenthesised
            
        
        rv = " ".join((Substs, "-",  Prods,  "/", Keq))
      
        return rv





class Reaction:

    def __init__(self,  Id, FName,   LineNo, sto, kin):

        self. Id = Id
        self.LineNo = LineNo
        self.FName = FName
        
        self.Sto = Sto(sto) # sto and kin are productions from the parser
        if kin == (lexer.t_DefaultKin):
            kin = (lexer.t_DefaultKin, "1") # default Keq if not specified
        self.Kin = kin
        
        # we  don't process kin at this point as we don't yet know if we need to.
        
        self.IsEquil = False
        self.IsDefaultKin=False



    def SetKin(self, TypeFun=float):
        
        if self.Kin[0] == lexer.t_DefaultKin:
            if self.Kin[1] == "1":
               Params= "1" ,"1"
            else:
                Params = self.Kin[2:-1].split(",")
            self.Kin = self.Sto.MassActionFun(Params, TypeFun=TypeFun)
            self.IsDefaultKin=True

        elif self.Kin[0] == lexer.t_EquilKin:
            self.IsEquil = True
            Keq = self.Kin[1:]
            self.Kin = self.Sto.EquilFun(Keq)
            
      
   

    def Reactants(self):
        return self.Sto.AsDic().keys()
        
    





class Error:

    def __init__(self, FName,  LineNo, Type,  Msg=""):

        self.FName  = FName
        self.LineNo = str(LineNo)
        self.Type = Type
        self.Msg = Msg

    def __str__(self):

        return "".join(("Error in ", self.FName, " at or before line:",  self.LineNo, " - ",   self.Type, "\n",  self.Msg))

#

class Warning(Error):
    
    def __str__(self):
        return "".join(("Warning: ", self.FName, " at or before line:",  self.LineNo, " - ",   self.Type, "\n",  self.Msg))
        
        

class Directive:

    def __init__(self,   Default, ValidArgs=[]):

        self.ValidArgs = ValidArgs
        self.Args = []
        self.Default = Default

    def __str__(self):
            return ",".join(self.GetArgs())

    def IsDefault(self):
        return self.Args==[]

    def SetArgs(self, args):
        self.Args=args

    def HasValidArgs(self):
        return self.IsDefault() or Set.IsSubset(self.Args, self.ValidArgs)

    def ResetArgs(self):
        self.Args = []

    def GetArgs(self):
        if self.IsDefault():
            return [self.Default]
        else:
            return self.Args[:]
            
            
class ElTypeDirective(Directive):
    
    TypeMap =   {
        "Float" : Types.Float, 
        "Rat"     : Types.ArbRat, 
        "Int"     : Types.Int
    }
    
    def __init__(self):

        self.ValidArgs =  self.TypeMap.keys()
        self.Args = []
        self.Default = "Rat"

    
    def GetElType(self):
        return self.TypeMap[self.GetArgs()[0]] # map the string description of ElType to the actual type



class ExtenDirective(Directive):

    def __init__(self):
        Directive.__init__(self, Default="")

    def HasValidArgs(self):
        return True

    def SetArgs(self, args):

        for arg in args:
            if not arg in self.Args:
                self.Args.append(arg)






class ModelDescription:
    

    def __init__(self,  FileName):
        
        self.Init(FileName)
    
    def Reload(self):
        
        self.Init(self.GetRootFilePath())
        
    
    def Init(self, FileName):
        
        if not os.sep in FileName:
            self.Path = "."
        else:
            self.Path,  FileName = FileName.rsplit(os.sep, 1)

        Tools = os.sep.join((self.Path, ToolsPath))
        if not Tools in sys.path:
            sys.path.append(Tools)
                            

        
            
        self.CurFile = self.CurLin = "None"
        self.FileNames  = {FileName:(self.CurFile, self.CurLin)} # value is the parent file and line included
        self.FileStack = Stack.Stack()

        self.Reactions    = {}
        self.ReacIDs      = {}
        self.Parameters   = {}
        self.Metabolites  = {}
        self.xMetabolites = []
        self.Values       = {} # we don't know if a  values is a parameter or metabolite until we've parsed the whole input file so we keep them here first
        self.Identifiers  = {} # keep a record of all identifiers to check for unitialised later
        self.Functions = {}  # so that we can discriminate function ids form numerical ids
        self.DynAssigs = {}  # dynamically assigned values
        self.ExplDiffs  = {}   # explicit diffrential equations
        self.Errors   = []
        self.Warnings = []
        self.QuoteMap = {}

        self.InitDirecs()
        Parser.InitMD(self)
        self.NewFile(FileName)

        self.ProcDirecs()
        self.InitMets()
        if len(self.Metabolites) == 0 and len(self.ExplDiffs) ==0:
            pass
            #self.NoInternalsError()
        else:
            self.InitQuoteMap()
            self.InitKin()



    def InitDirecs(self):
        self.Directives = {
            "External"       : ExtenDirective(),
            "Structural"   : Directive(Default="False",ValidArgs= ["True", "False"] ),
            "ElType"           : ElTypeDirective(), # default and valid args defined in the class
            "AutoExtern"   : Directive(Default="False", ValidArgs=["True", "False"]),
            "AutoPolymer" : Directive(Default="Ignore", ValidArgs=["Ignore","Consumed","Produced"]),
            "Include"         : None, #TODO: should Include be a directive or at all? what to do ?
            "DeQuote"         : Directive(Default="False",ValidArgs=["True", "False"])
        }



##
# Post-parse initialisation methods


    def ProcDirecs(self):
        """ process directives that modify internal data but are not otherwise processed during __init__"""

        if not self.Directives["AutoPolymer"].IsDefault():
               option = self.Directives["AutoPolymer"].GetArgs()[0]
               for reac in self.Reactions:
                   self.Reactions[reac].Sto.SetAutoPolymer(option)
#FIXME: remove the AutoPolymer directive, always treat common subst and prods as error
        el_t = self.Directives["ElType"].GetArgs()[0]
        if el_t in self.Identifiers:
            del self.Identifiers[el_t] # if we're a kinetic model, el_t will be seen as an unitialised parameter



    def InitQuoteMap(self):
        """ pre: InitMets"""
        
        AllIds = self.Reactions.keys() + self.Metabolites.keys() + Set.Union(self.Parameters.keys() ,  self.xMetabolites)
        # All identifier names, Set.Union because in kinetic models external metabolites are also parameters
        
        
        DQIds = map(DeQuote,  AllIds) # dequoted IDs 
        
        DupDQs = set(filter(lambda s: DQIds.count(s)>1,  DQIds)) # IDs in quoted and unquoted form
        
        if len(DupDQs) >0:
            e = Error(self.CurFile, "Multiple", str( list(DupDQs)) +" defined in quoted and unquoted form.")
            self.Errors.append(e)
            #TODO: make this error message a bit more interpretable
        else:
            QM = dict(zip(AllIds,  DQIds))
            QM.update(dict(zip(DQIds, AllIds)))
            self.QuoteMap = QM
#       
#        for i in AllIds:
#            dqi = DeQuote(i)
#            if self.QuoteMap.has_key(dqi):
#                w = Warning(self.CurFile, "Multiple",  i +" defined in quoted and unquoted form. This may cause problems!")
#                self.Warnings.append(w)
##TODO: handle this better and make it an error
#            self.QuoteMap[i] = dqi
#            self.QuoteMap[dqi] = i



    def InitMets(self):

        #AllMets = set()
        #for r in self.Reactions.values():
        #   AllMets.update(r.Reactants())
        AllMets = set(self.Metabolites.keys())
        self.Metabolites = {}

        xMets =set(filter(lambda s: s.upper().startswith("X_") or  s.upper().startswith('"X_'),  AllMets))
        if not self.Directives["External"].IsDefault():
            xMets.update(self.Directives["External"].GetArgs())

        IntMets = AllMets -  xMets

        self.xMetabolites = list(set(xMets))
        self.Parameters = dict(self.Values)
        
        

        for met in IntMets:
            if self.Values.has_key(met):
                val = self.Values[met]
                del self.Parameters[met]   # once internal mets are removed we will have only parameters left
            else:
                val = 0

            self.Metabolites[met]  = val

        



   
  
    def InitKin(self): 
        
        if self.Directives["Structural"].IsDefault():
            #TypeFun = lambda x: float(Types.ArbRat(x)) if self.Directives["ElType"].IsDefault() else float
            for Reac in self.Reactions:
                self.Reactions[Reac].SetKin()
                
            Uninit = Set.Complement(self.Identifiers, 
                self.Values.keys() +
                self.Functions.keys() + 
                self.DynAssigs.keys() + 
                self.ExplDiffs.keys() +
                self.Reactions.keys()
            )
            if len(Uninit) != 0:
                self.UninitValError(Uninit)
            
            
    def UninitValError(self,  Uninit):
        
        File = self.GetRootFile()
        Line = " Unknown"
        msg = "Uninitialised value(s): " + " ".join(Uninit)
        
        Err = Error(File,  Line,  msg)
        self.Errors.append(Err)

    def CheckUnused(self):
        # remove any reactions that only involve external mets, remove them and set a warning
        print "CheckUnused needs implementing"

#
##
### Methods invoked by the parser
##
#

#  Error handling - generally invoked by the parser, but can be called by external code if required
    def DupReactionIDError(self,  ReacId):

        OldFile, OldLine = self.ReacIDs[ReacId]
        Msg = " ".join((ReacId,"previously defined in",  OldFile,  "at line", str( OldLine)))

        Err = Error(self.CurFile, self.CurLin,   "Duplicate Reaction", Msg)
        self.Errors.append(Err)
        
        
    def ReacIsMetError(self,  ReacID):
        
        Msg = ReacID + " previously defined as a metabolite"
        Err = Error(self.CurFile, self.CurLin,   "Redifined metabolite", Msg)
        self.Errors.append(Err)
        
    def MetIsReacError(self,  met):
        
        Msg = met + " previously defined as a reaction"
        Err = Error(self.CurFile, self.CurLin,   "Redifined reaction", Msg)
        self.Errors.append(Err)
        
    def CommonSubstAndProdError(self, ReacId, met):
        
        Msg = met + " defined as substrate and prod in " + ReacId
        Err = Error(self.CurFile, self.CurLin,   "Bad stoichiometry", Msg)
        self.Errors.append(Err)

    def DupFileError(self,  FileName):

        OldFile,  OldLine = self.FileNames[FileName]
        Msg = " ".join((FileName,"previously included from",  OldFile,  "at line",  str(OldLine)))
        Err = Error(self.CurFile,  self.CurLin,  "Duplicate import", Msg)
        self.Errors.append(Err)


    def BadFileError(self, FileName):
        Err = Error(self.CurFile,  self.CurLin,  "Failed to open", FileName)
        self.Errors.append(Err)
        
    def BadDynAssigError(self, Id):
        Msg = "Can't explicitly initialise a dynamically assigned parameter: "
        Err = Error(self.CurFile,  self.CurLin,  Msg, Id)
        self.Errors.append(Err)


    def BadRootFileError(self, FileName):
        Err = Error("None",  "None",  "Failed to open root file", FileName)
        self.Errors.append(Err)


    def BadTokenError(self, Token):
        Err = Error(self.CurFile,  self.CurLin,  "Unrecognised character sequence", Token)
        self.Errors.append(Err)


    def SyntaxError(self):
        Err = Error(self.CurFile,  self.CurLin,  "Syntax error")
        self.Errors.append(Err)


    def UnknownDirectiveError(self, Direc):
        Err = Error(self.CurFile,  self.CurLin,  "Unlnown directive", Direc)
        self.Errors.append(Err)


    def BadDirecArgsError(self, Direc):
        Err = Error(self.CurFile,  self.CurLin,  "Bad arguments to directive", Direc)
        self.Errors.append(Err)
        
        
    def InternalError(self, Msg):
         Err = Error(self.CurFile,  self.CurLin,  "Internal error - please report", Msg)
         self.Errors.append(Err)
         
    def BadInitError(self,  Msg):
        
        Err = Error(self.CurFile,  self.CurLin,  "Couldn't initialise", Msg)
        self.Errors.append(Err)
        print Err
        
    def NoInternalsError(self):
        Err = Error(self.CurFile,  None,  "No internal metabolites defined !")
        self.Errors.append(Err)


        
#
# Production handling - *only* to be invoked by the Parser
#
    def IncrLine(self):
        self.CurLin += 1
        
    def NoteIdent(self,  id):
        self.Identifiers[id] = True
        
        
    def NoteFun(self, fun):
        self.Functions[fun] = True


    def NewReacID(self, ReacId):
        #Because  the Parser only recognises the whole reaction, the lexer has to tell us when we find a new reaction id

        if self.ReacIDs.has_key(ReacId):
            self.DupReactionIDError(ReacId)
        
        if self.Metabolites.has_key(ReacId):
            self.ReacIsMetError(ReacId)
        
        
        self.ReacIDs[ReacId] =  self.CurFile, self.CurLin 
        # add it even if it's bad - parser will still invoke AddReaction() as long as syntax is OK
    


    def AddReaction(self, ReacId, sto,  kin):
       
        File, Line = self.ReacIDs[ReacId]
        reac = Reaction(ReacId,  File,  Line,  sto,  kin)
        
        for met in reac.Sto.Conflicting():
            self.CommonSubstAndProdError(ReacId,  met)
        
        for met in reac.Reactants():
            self.Metabolites[met] = 0
            if self.ReacIDs.has_key(met):
                self.MetIsReacError(met)
        
        
        self.Reactions[ReacId] = reac
   
    
    def InitVal(self, Id,  val):
        
        if self.DynAssigs.has_key(Id):
            self.BadDynAssigError(Id)
        
        elif self.CurLin != None:
            
            py = Id +"="+val
            try:          
                exec py in FunDic,  self.Values
            except:
                self.BadInitError(py)
                
                
    def NewDynAssig(self, Id, val):
      
        if self.Values.has_key(Id):
            self.BadDynAssigError(Id)
        else:
            self.DynAssigs[Id] = val, self.CurLin,  self.CurFile
            self.Values[Id] = 0
        
        
#TODO: Error conditions for dynamic assigs and ExplDiffs
    def NewExplDiff(self,  Id,  val):
       
        self.ExplDiffs[Id] = val 
  






  
    def SetDirec(self, Direc,  args): # Set a directive, (not a drection !)

        if Direc == "Include":           # Include is a special case as we process it immediately
            if type(args) != types.ListType:
                args = [args]
            for File in args:
                self.NewFile(File, Child=True)

        else:
            if not self.Directives.has_key(Direc):
                self.UnknownDirectiveError(Direc)
            else:
                d = self.Directives[Direc]
                d.SetArgs(args)
                if not d.HasValidArgs():
                    self.BadDirecArgsError(Direc)
                    d.ResetArgs()


    def NewFile(self,  FileName,  Child=False):
        
        GoodFile=True # start optimistic
#TODO need a separate method to clean and check goodness of FileName

        if FileName[0] == '"' and len(FileName) >2:
            FileName = FileName[1:-1]
            

        if Child and self.FileNames.has_key(FileName):
            self.DupFileError(FileName)

        else:
            PathFileName = os.sep.join((self.Path, FileName))

            try:
                if os.path.exists(PathFileName):
                    InStr= open(PathFileName).read().strip()
                else:
                    try:
                        open(PathFileName, "w") # try to create empty file if it doesn't exist
                        InStr=""
                    except:
                        GoodFile = False
            except:
                GoodFile = False
                
            if not GoodFile:
               
                if Child:
                     self.BadFileError(FileName)
                else:
                    self.BadRootFileError(FileName)
                    del self.FileNames[FileName] 

            else:
                self.FileNames[FileName] = self.CurFile, self.CurLin # key is the child, value is the parent
                self.FileStack.Push((self.CurFile, self.CurLin))
                self.CurLin  = 1
                self.CurFile = FileName
                
                if InStr != "":   # don't try to parse empty files
                    lexer, parser = Parser.NewLexerParser(DebugParser)
                    Parser.SetLex(lexer)
                    parser.parse(InStr)
                    Parser.SetLex(lexer) # NewFile can be called through indirect recursion in parser.parse()
                                                     # make sure the lexer is returned id it's orinal state on return
                                                     
                self.CurFile,  self.CurLin = self.FileStack.Pop()







#
##
### Methods invoked outside the parser
##
#

#
## Interrogation
#
    def GetRootFile(self):
        
        for f in self.FileNames:
            if self.FileNames[f] == ("None", "None"):
                return f

    def GetRootFilePath(self):
        
        rootfile = self.GetRootFile()
        if rootfile != None:
            return os.sep.join((self.Path, self.GetRootFile()))

        # rootfile == None => rootfile not loaded
        
    def IsKinetic(self):
        
        return self.Directives["Structural"].IsDefault()
                
            
    def GetQuoteMappedNames(self, names):
        return map(lambda s: self.QuoteMap[s],  names)

    def GetIntMetNames(self,  DeQuote=True):

        rv =  self.Metabolites.keys()
        if DeQuote:
            rv =self.GetQuoteMappedNames(rv)

        return rv


    def GetExtMetNames(self, DeQuote=True):

        rv =  self.xMetabolites[:]
        #TODO xMetabolites not (all?) in self.QuoteMap !!
        if DeQuote:
            rv =self.GetQuoteMappedNames(rv)

        return rv


    def GetReacNames(self, DeQuote=True):

        rv =  self.Reactions.keys()
        if DeQuote:
            rv =self.GetQuoteMappedNames(rv)

        return rv
        
        
    def GetEquilReacs(self, DeQuote=True):
        
        rv =  filter(lambda reac: self.Reactions[reac].IsEquil,  self.Reactions)
        if DeQuote:
            rv =self.GetQuoteMappedNames(rv)

        return rv
        
        
          
    def GetNonEquilReacs(self, DeQuote=True):
        
        rv =  filter(lambda reac: not self.Reactions[reac].IsEquil,  self.Reactions)
        if DeQuote:
            rv =self.GetQuoteMappedNames(rv)

        return rv
        
        
    def GetParamNames(self,  DeQuote=True):
        
        rv = self.Parameters.keys()
        if DeQuote:
            rv =self.GetQuoteMappedNames(rv)

        return rv


    def GetOrigReacName(self, reac): # because if we're getting reacs from sm, they may be dequoted

        rv = None
        if self.Reactions.has_key(reac):
            rv =  reac
        elif self.QuoteMap.has_key(reac):
            reac = self.QuoteMap[reac]
            if self.Reactions.has_key(reac):
                rv =  reac

        return rv


    def GetStoDic(self, reac):

        mreac = self.GetOrigReacName(reac)
        rv = self.Reactions[mreac].Sto.AsDic()

        
        mets = rv.keys()   
        for met in mets:
            qmet = self.QuoteMap[met]
            if qmet != met:
                rv[self.QuoteMap[met]] = rv[met]
                del rv[met]

        return rv
            
        
    def GetReacDirec(self, reac):
        reac = self.GetOrigReacName(reac)
        return self.Reactions[reac].Sto.Direc
        
        
    def GetReacKinetic(self, reac):
        reac = self.GetOrigReacName(reac)
        return self.Reactions[reac].Kin
        
    def GetDynAssigNames(self):
        return self.DynAssigs.keys()
        
    def GetDynAssigEqn(self, Id):
        return self.DynAssigs[Id][0]
        
    def GetExplDiffEqn(self, name): # TODO: quote/dequote check
        return self.ExplDiffs[name]
        
    def GetExplDiffNames(self):
        return self.ExplDiffs.keys()
        
    def ExplicitOnly(self):
        return len(self.Reactions) == 0


    def WhereIs(self, reac):

        rv = None, None
        reac = self.GetOrigReacName(reac)
        if reac != None:
            rv = self.Reactions[reac].FName,  self.Reactions[reac].LineNo

        return rv
        
    def GetDirectiveVal(self, Directive):
        vals = self.Directives[Directive].GetArgs()
        if len(vals)==1:
            return vals[0]
        else:
            return vals
            
    def GetElType(self):
        return self.Directives["ElType"].GetElType()
#
# Modification - by external code
#

    def NewParam(self, pname, val=0.0):
        
        if self.Parameters.has_key(pname):
            print "!! Warning: NewParam",  pname,  "already exists - ignorinig !!"
        else:
            self.Values[pname] = self.Parameters[pname]= val
            self.QuoteMap[pname] = DeQuote(pname)
            
    
    
    def SetParam(self,  pname,  val):
        if not self.Parameters.has_key(pname):
            print "!! Warning: SetParam",  pname,  "doesn't exist - ignorinig !!"
        else:
            self.Parameters[pname] = val
        
            
    def DelReaction(self, reac):
        pass
    
    def NewReaction(self, reac, stodic,  direc):  #NOT the same as NewReacID or AddReaction
        pass
        
    def NewSto(self, reac,  stodic):
        pass
        
    def NewDirec(self, reac,  direc):
        pass
