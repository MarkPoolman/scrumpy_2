    

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

from ScrumPy.ThirdParty.Ply import lex

# some elementary regexes
Letter = r"[a-zA-Z]"
Int = r"\d+"                                    # Integer
Dec = Int + "\." + Int                          # Decimal
Exp = r"([E|e][\+|\-]?)" +Int                   # Exponent
Real = Dec  + "("+ Exp +")?" + "|" + Int + Exp  # Real - dec w/ optional exp or int with exp
Rat = Int+"/"+Int
NL = r"\n"


DirecList = [
    "External",
    "Structural",
    "ElType",
    #"AutoExtern",
    "AutoDuplicateR",
   # "AutoPolymer",
    "Include",
    "DeQuote"]


Lexer = None
ModelDescription = None


def Init(lexr):
    global Lexer
    Lexer = lexr


def InitMD(md):
    global ModelDescription
    ModelDescription = md


def NewLexer(debug=False):
    return  lex.lex(debug=debug)


def NewLineFunc(t):
    r"\n"
    ModelDescription.IncrLine()
    t.lineno = ModelDescription.CurLin


def t_COMMENT(t):
    r"\#.*"
 
    NewLineFunc(t)


def ReacIdFunc(t):
    r'([a-zA-Z]\w*:)|(".*?":)'
    t.type = "ReacId"
    t.value = t.value[:-1]  # remove the colon
    ModelDescription.NewReacID(t.value)
    return t


def IdentFunc(t):
    r'([a-zA-Z][\.\w]*) | (".*?")'


    if t.value in DirecList:
        t.type = "Directive"
    else:
        t.type = "Ident" 
        ModelDescription.NoteIdent(t.value)

    if "\n" in t.value: #TODO check if this is needed
        NewLineFunc()

    return t


def t_error(t):
    ModelDescription.BadTokenError(t.value)
    t.lexer.skip(1)


tokens = (
  "DefaultKin",
  "EquilKin", 
  "Ident",
  "Irrev",
  "BackIrrev",
  "Rever",
  "ReacId",
 # "BlockReac", 
  "Real",
  "Int",
  "Rat",
  "LPar",
  "RPar",
  "Add",
  "Sub",
  "Mul",
  "Div",
  "Pow",
  "Eq",
  "DynEq", 
  "DiffEq", 
  "Comma",
  "Directive"
)


t_NL = NewLineFunc
t_ReacId = ReacIdFunc
t_Ident = IdentFunc
#t_BlockReac = r"\|"
t_DefaultKin = "~"
t_EquilKin = "!"
t_Irrev = r"->"
t_BackIrrev = r"<-"
t_Rever = r"<>"
t_Real = Real
t_Int = Int
t_Rat = Rat
t_LPar = r"\("
t_RPar = r"\)"
t_Add = r"\+"
t_Sub = r"-"
t_Mul = r"\*"
t_Div = r"/"
t_Pow = "\*\*"
t_Eq = "="
t_DynEq = ":="
t_DiffEq = ".="
t_Comma = ","
t_ignore = " \t"

