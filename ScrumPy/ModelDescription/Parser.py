

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""




from ScrumPy.ThirdParty.Ply import yacc
#NB Must have ScrumPy.ThirdParty.Ply, not just ThirdParty.Ply or yacc can't find the lexer  ?? why ??

from ScrumPy.BasicInfo import Revision
TabModule = "ScrumPyParser_"+Revision # name of the module automatically generated by ply

import lexer
tokens = lexer.tokens



ModelDescription = None
debug=0




##
##  ---------  Error handling functions  ------------
##

def p_error(t):  # t - token, handler for general syntax errors, invoked by yacc

    ModelDescription.SyntaxError()
    TellMe("p_error 1", str(t))
  
    # scan input until start of next reaction
    tok = yacc.token()
    while tok and tok.type != "ReacId":
        tok = yacc.token()
        
    TellMe("p_error 2", str(tok))

    return tok



def TellMe(name,tok):
    if debug:
        print name,tok



###################### Parser rules start here

def p_Model(t):
    """Model : Statement
            | Statement Model"""

    t[0] = t[1]


def p_Statement(t):
    """Statement : Reaction
                 | Assig
                 | DynAssig
                 | ExplDiff
                 | Direc """
                  
        
    t[0] = t[1]


def p_Assig(t):
    """Assig : Ident Eq Expr"""
    t[0] = t[1]+t[2]+t[3]
    ModelDescription.InitVal(t[1], t[3])


def p_DynAssig(t): # dynamic assignment (monitor vars etc)
    """DynAssig : Ident DynEq Expr"""
    t[0] = t[1]+t[2]+t[3]
    ModelDescription.NewDynAssig(t[1], t[3])
    

def p_ExplDiff(t):  # explicit differential equations
    """ExplDiff : Ident DiffEq Expr"""
    t[0] = t[1]+t[2]+t[3]
    ModelDescription.NewExplDiff(t[1], t[3])
    
def p_Direc(t):
    """Direc : Directive LPar ArgList RPar
                 | Directive LPar RPar """

    if len(t.slice)==5:  # i.e.we have an arglist
        args = t[3].split()
        
    else:
        args = ["True"]

    ModelDescription.SetDirec(t[1], args)

    t[0] = t[1]  # no news is good news


def p_Reaction(t):
    """Reaction : ReacId  Stoich Kinetic"""
   

    ModelDescription.AddReaction(t[1],  t[2],  t[3])
    t[0] = t[1]
   


#def p_BlockedReaction(t):
 #   """BlockedReaction : ReacId  BlockReac Stoich Kinetic """
 #   t[0] = t[1]

def p_Stoich(t):
    """Stoich   : HalfSto Rever HalfSto
                | HalfSto Irrev HalfSto
                | HalfSto BackIrrev HalfSto"""

    t[0] = (t[1],t[3],t[2])
    TellMe("Stoich",t[0])


def p_HalfSto(t):
    """ HalfSto : OneMet
                | OneMet Add HalfSto"""
    t[0] = [t[1]]
    try:
        t[0] += t[3]
    except:
        pass
    TellMe ("HalfSto",t[0])


def p_OneMet(t):
    """OneMet   : Ident
                | Int Ident
                | Real Ident
                | Rat Ident"""


    if len(t) == 3:
        sto, met = t[1], t[2]
    else:
        sto, met = "1", t[1]

    t[0] = (sto,met)




def p_Kinetic(t):
    """ Kinetic : Expr
                | DefaultKin
                | DefaultKin LPar Num Comma Num RPar
                | EquilKin Expr"""
#TODO replace DefaultKin weith MassActKin and allow optional Keq

    #t[0]  = t[1]  # later on we will use Exprs for elasts too
   
    
    if t[1] in (lexer.t_DefaultKin,  lexer.t_EquilKin):
      
        t[0] = t[1] +  " ".join(t[2:])
    else:      
        t[0]= t[1]

precedence = (
    ('left', 'Add', 'Sub'),
    ('left', 'Mul', 'Div'),
    ('left', 'Pow')
    #('right', 'UMin')
    )


def p_Expr(t):
    """Expr : Expr Add Expr
            | Expr Sub Expr
            | Expr Mul Expr
            | Expr Div Expr
            | Power
            | UMin
            | Num
            | Fun"""

    if len(t.slice)==4:
        t[0] = " ".join((t[1],  t[2],  t[3])) # seperate with spaces to ease later kinetic compilations see PyEval.BuildRateEqns
    else:
        t[0] = t[1]



def p_Power(t):
    """Power : Expr Pow Expr"""

    t[0] = "pow( "+ t[1] + " , " + t[3] + " )"
    # NB the white space matters here!


def p_UMin(t):
    """UMin : Sub Expr"""

    t[0] = t[1] + t[2]


def p_Num(t):
    """Num  : Real
            | Int
            | Rat
            | Ident"""
    t[0] = t[1]
    
    




def p_Fun(t):
    """Fun  : Ident LPar ArgList RPar
            | LPar ArgList RPar"""
    
    ModelDescription.NoteFun(t[1])
    
            
    t[0] = " ".join(t[1:])
    
    #try:
     #   t[0] = t[1] + t[2] + t[3] + t[4]
      #  MDesc.FunList.append((t[1],lexer.CurLin))
    #except:
     #   t[0] = " ".join((t[1] ,  t[2] ,  t[3]))


def p_ArgList(t):
    """ArgList : Expr
               | Expr Comma ArgList"""
    t[0] = t[1]
    try:
        t[0] += " " + t[3]
    except:
        pass


def InitMD(md):

    global ModelDescription
    ModelDescription = md
    lexer.InitMD(md)


def SetLex(lexr):

    lexer.Init(lexr)



def NewLexerParser(debug=False):

    return lexer.NewLexer(debug=debug), yacc.yacc(debug=debug,  tabmodule=TabModule)


