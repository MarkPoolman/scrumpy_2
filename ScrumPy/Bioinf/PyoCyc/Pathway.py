import Base, Tags

DefaultFile="pathways.dat"

class Record(Base.Record):
    ParentFields = [Tags.ReacList]
    ChildFields = [Tags.SuPath]
    RecordClass="Pathway"

    def GetReactions(self):

        Reactions = []

        for p in self.GetParents():

            if p.RecordClass == "Reaction":
                Reactions.append(p)
            elif p.RecordClass == "Pathway":
                Reactions.extend(p.GetReactions())
            else:
                print "Unknown record class in pathway.GetReacUIDs", p.RecordClass

        return Reactions


class DB(Base.DB):
    def __init__(self,path=Base.DefaultPath,**kwargs):
        Base.DB.__init__(
            self,
            path=path,
            file=DefaultFile,
            RecClass=Record,
            **kwargs
        )


#
