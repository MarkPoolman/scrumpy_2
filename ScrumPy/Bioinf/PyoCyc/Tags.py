Cats =            "CATALYZES"
ChemForm = "CHEMICAL-FORMULA"
Coeff =           "^COEFFICIENT"
Comment=     "COMMENT"
ComName =   "COMMON-NAME"
CompOf =      "COMPONENT-OF"
Comps =        "COMPONENTS"
DBLinks =      "DBLINKS"
Delim =          " - "
EC =               "EC-NUMBER"
Enz =              "ENZYME"
EnzReac =       "ENZYMATIC-REACTION"
Gene =            "GENE"
InPath =          "IN-PATHWAY"                                # the pathway(s) in which a reaction is found
Left =              "LEFT"                                             # Left (substrate) of a reaction
LeftEnd =         "LEFT-END-POSITION"                   # leftmost base in a gene
MolWt =           "MOLECULAR-WEIGHT"
Prod=              "PRODUCT"
Reac =             "REACTION"
ReacEq =         "REACTION-EQUATION"
ReacList =       "REACTION-LIST"                              # reactions in a pathway
ReacDir =        "REACTION-DIRECTION"

RecEnd =        "//"                           # End of Record

RegBy=          "REGULATED-BY"
Regul =         "REGULATOR"
Regulates =    "REGULATES"
RegEnt =       "REGULATED-ENTITY"

RegMode =     "MODE"
Right =           "RIGHT"                                             # Right (product) of reaction
RightEnd =     "RIGHT-END-POSITION"                     # rightmost base in a gene
SuPath =         "SUPER-PATHWAY"                            # defined but not used in ecocyc ? future expansion ?
TransDir=       "TRANSCRIPTION-DIRECTION"
Types  =     "TYPES"
UID =              "UNIQUE-ID"


Import =        "#IMPORT"                                        # not part of ecocyc - use to identify files imported from 3rd party input


                       ## tagged comments, generated when converting non-biocyc - biocyc
TCReac=         "COMMENT <Reaction>"

NR = "Not reported"   # not strictly a Tag, default value for missing essential (parent or child) fields
Null = "Not found"       # not strictly a Tag, value for for primary search for non-existent  key.
# TODO merge NR and Null into one ???



def IsComment(string):                                           # global comment, not comment field in record
    return string.find("#") ==0  or string.find(RecEnd) == 0

#
