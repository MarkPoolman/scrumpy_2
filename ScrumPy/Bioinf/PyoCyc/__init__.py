from  Organism import Organism
from  AnnotatedOrganism import AnnotatedOrganism

import Base
import os

def Avail():

#
# TODO: symlinks not recognised
#
    rv = []

    for localpath in os.listdir(Base.DefaultPath):
        report   = localpath
        abspath  = Base.DefaultPath + localpath
        realpath = os.path.realpath(abspath)
        #print localpath,  os.path.islink(abspath)

        if os.path.isdir(realpath) and "genes.dat" in os.listdir(realpath):
            if os.path.islink(abspath):
                report += "".join((" (-> ", realpath.rsplit(os.sep)[-1], ")" ))

            rv.append(report)

        rv.sort()


    return rv



def PrintAvail():

    for a in Avail():
        print a
