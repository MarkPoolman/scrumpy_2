#
##
### ChemTrans - interface to the chemical translation service http://cts.fiehnlab.ucdavis.edu/
### See http://cts.fiehnlab.ucdavis.edu/moreServices/index for details
##
#
import exceptions
import urllib2
import json
JsonDecode = json.JSONDecoder().decode

BaseURL = "http://cts.fiehnlab.ucdavis.edu/service/"

def CleanHTTP(http):
    return http.replace(" ", "%20")

def ReadURL(Request):

    url = BaseURL+CleanHTTP(Request)
    try:
        return JsonDecode(urllib2.urlopen(url).read())
    except:
        raise exceptions.LookupError, "failed to read "+url
        




FromValues = ReadURL("conversion/fromValues")
ToValues = ReadURL("conversion/toValues")


def ConvertName(From,To,Name):

    Req = "/".join(("convert",From,To,Name))
    return ReadURL(Req)


