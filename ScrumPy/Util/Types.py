

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""


# Arbitray rational, rename the mpq type for backwards compatability
# need to improve str and __repr__ at some point

import exceptions, types

try:
    from gmpy import mpq as ArbRat
except:
    from gmpy2 import mpq as ArbRat # eg centos and maybe others



# trivial subclass of string to allow negation and inline addition and subtraction - maybe more later
class Str(str):
    def __neg__(self):
        return "-" + self

    def __isub__(self, other):
        self = str(int(self) - int(other))
        return self

    def __iadd__(self,other):
        self = str(int(self) + int(other))
        return self


def Sign(num):

    return ["","+","-"][cmp(num,0)]



def StrToNum(str_num, num_t):
    """ try to return the numeric, num_t, representation of str_num,
        raise ValueError if not possible """

    for fun in float, ArbRat:
        try:
            return num_t(fun(str_num))
        except:
            pass
    raise exceptions.ValueError, "can't convert " + str_num + " to a number of type" + str(num_t)





def Float(num):

    try:
        return float(num)
    except:
        if type(num) == types.StringType:
            numden = num.split("/")
            if len(numden) ==2:
                try:
                    return float(numden[0])/float(numden[1])
                except:
                    pass

    raise exceptions.TypeError, "Can't cast " + str(num) + " to float"


def Int(num):

    try:
        return int(float(num))
    except:
        if type(num) == types.StringType:
            numden = num.split("/")
            if len(numden) ==2 and numden[1]=="1":
                try:
                    return int(numden[0])
                except:
                    pass

    raise exceptions.TypeError, "Can't cast " + str(num) + " to int"


#TODO:  remove or move this to ModelDescription
TypeToDirective = {
            type(Int(1)): "int",
            type(Float(1)): "float",
            type(Str(1)):"string",
            type(ArbRat(1)): "ArbRat"
        }

