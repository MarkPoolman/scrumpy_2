
class Stack:
    
    def __init__(self):
        self._Stack = []

    def __len__(self):
        return len(self._Stack)

    def Copy(self):
        rv = self.__class__()
        stack = self._Stack[:]
        for item in stack:
            rv.Push(item)
        return rv

    def IsEmpty(self):
        return len(self)==0
    
    def __len__(self):
        return len(self._Stack)
        
    def IsEmpy(self):
        return len(self)==0
        
    def Push(self,  item):
        self._Stack.append(item)
        
    def Top(self):
        """ pre: not self.IsEmpty() """
        return self._Stack[-1]
        
    def Pop(self):
        """ pre: not self.IsEmpty()
           post: len(self') = len(self)-1 """
        return  self._Stack.pop()
        
