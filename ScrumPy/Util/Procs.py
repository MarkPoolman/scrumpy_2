import os

from subprocess import Popen, PIPE

def GetChildren():

    pid = os.getpid()
    pids = []

    pstree = Popen(["pstree","-p", str(pid)],stdout=PIPE)
    pout = pstree.communicate()[0]
    pstree.wait()

    pout = pout.split("\n")
    pout = filter(lambda s: not "{python}" in s, pout)
    pout = "".join(pout).replace("(","\t").replace(")","\t").split("\t")

    for p in pout:
        try:
            pids.append(int(p))
        except:
            pass

    pids.sort(reverse=True)
    pids.remove(pstree.pid)
    pids.remove(pid)
    return pids

    
def KillChildren():

    for pid in GetChildren():
        try:
            os.kill(pid, 9)
            os.waitpid(pid,0)
        except:
            pass

        
    

