

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 2006 -

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

"""
Common entry point for "scientific" functions, especially on vectors and matrices.
Currently using scipy, but this may change in future.
"""


import math,  fractions

import scipy,  numpy

import Seq



matrix = scipy.matrix
vector = scipy.array
linspace = scipy.linspace
zeros = numpy.zeros



from scipy.linalg  import svd,  pinv,  eigvals
# NOTE: ignore warnings about unused imports, they are for re-export to other modules.



TOL=1e-12 # smallest difference between two numbers that can be considered non-zero


def HistoLists(data,  **kwargs):
    
    counts,  bins = numpy.histogram(data, **kwargs)
    return bins, counts

def Gcd(nums):
    """ pre: nums is a sequence of numbers, lenums) >1
        post: greatest common denominator of nums
    """

    g = fractions.gcd(nums[0], nums[1])
    if len(nums)>2:
        return Gcd([g]+ nums[2:])
    return g


def Integise(nums,  KeepSign=True):
    """ pre: Gcd(nums)
         post: copy of nums converted to integer whilst maintaining ratios
         
         Note: beware of rounding error if nums contain floats - especially recurring decimals
    """
    
    if max(nums) == min(nums): # special cases of all the same and/or all zero

        if nums[0] == 0:
            rv = nums[:]
        else:
            if KeepSign:
                one = abs(nums[0])/nums[0]
            else:
                one = nums[0]/nums[0]
            
            rv = [one] * len(nums)
            
    else:
        g = Gcd(nums)
        if KeepSign:
            g = abs(g)
            
        rv = map(lambda x:int(x/g),  nums)
        
    return rv




def PolyFit(Xdat, Ydat, deg=1):
    "polynomial fit of X,Y of degree deg"

    return scipy.polyfit(Xdat,Ydat,deg)



def null(A,tol=TOL,AsList=False):
    "orthonormal nullspace, elements are floats"

    u,w,vt=svd(A,full_matrices=1)
    v,i = Seq.PosOfFirst(w.tolist(),lambda x: x <= tol) #TODO  avoid using Seq and w.tolist
    if i == -1:
        i = len(w)
    rv = vt[i:,].transpose()

    if AsList:
        return rv.tolist()
    return rv

def mul(a,b,AsList=False):

    return a*b


def Mod(vec):
    return math.sqrt(scipy.vdot(vec,vec))

def DotProd(a, b):
    return scipy.vdot(a, b)
    
def RelLen(a, b):
    return Mod(a)/Mod(b)
    
    
def EucDist(a,b):
    " the Euclidean distance between a,b "

    moda = Mod(a)
    modb = Mod(b)
    cosab =CosTheta(a, b)

    return math.sqrt(abs(moda*moda + modb*modb - 2*moda*modb*cosab))    

def SumSq(seq):

    return sum(map(lambda x:x*x,seq))
    
def RootSumSq(seq):
    return math.sqrt(SumSq(seq))


def SumSqDiff(vals, const):

        return(SumSq(map(lambda x: x-const, vals)))

def Deriv(x,y):
    """ pre: len(x) == len(y)==3, all x and y numeric
       post: Deriv(x,y) == slope at x[1],y[1], by three point linear derivation """
    return 0.5 * (
        ((y[1] - y[0]) / (x[1] - x[0])) +
        ((y[2] - y[1]) / (x[2] - x[1]))
    )




def CosTheta(veca,vecb):
    veca = map(float, veca) 
    vecb = map(float, vecb) 
    
    rv = scipy.vdot(veca,vecb)/(Mod(veca)*Mod(vecb))
    if rv >1.0:
        rv = 1.0
    elif rv < -1.0:
        rv = -1.0

    return rv


AbsCosTheta = lambda a,b: abs(CosTheta(a,b))


def AreParallel(a,b, tol=TOL):
    return 1-AbsCosTheta(a,b) <= tol

def AreOrthogonal(a,b,tol = TOL):
    return AbsCosTheta(a,b) <= tol


Theta    = lambda a,b : math.acos(CosTheta(a,b))
AbsTheta = lambda a,b : abs(Theta(a,b))
Q1Theta  = lambda a,b : math.acos(AbsCosTheta(a,b))

Log2 = lambda x: math.log(x, 2)






