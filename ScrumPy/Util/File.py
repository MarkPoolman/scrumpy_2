

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
import os

def IsYounger(FName1, FName2):
    """pre: Exists(FName1) and Exists(FName2)
    post : returns boolean indicating FName1 has more recent modification time than FName2 """

    return os.stat(FName1)[8] > os.stat(FName2)[8]

def IsAbsPath(FName):
    """ pre: type(FName) == <type 'string'>
       post: IsAbsPath(FName) => FNames is an absolute (aot a relative) path name """

    return FName[0] == "/"

def FindIn(Paths, targ):
    """ pre: Paths is a list of paths
       post: FindIn(Paths, targ) list of unique paths, rooted in Paths, containing targ"""

    rv = []
    seen = []
    def GotTarg(Targ_List, d, files):
        "Targ_List is (targ,list) tuple"
        if Targ_List[0] in files:
            Targ_List[1].append(d)

    Paths.sort(lambda x,y: len(x) - len(y))
    for p in Paths:
        OK = 1 
        for s in seen:
            if p.startswith(s):
                OK = 0
        
        if OK:
            seen.append(p)
            os.path.walk(p, GotTarg, (targ,rv))
            
    return rv

    
       
