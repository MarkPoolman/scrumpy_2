


class Sorter:


    class Sortel:
        def __init__(self, udat):
            self.udat = udat

        def __cmp__(self, other):
            
            diff =  self.score - other.score
            if diff <0: return -1
            if diff > 0: return 1
            return 0

        def eval(self, fun, **fundat):
            self.score = fun(self.udat, **fundat)


    
    def __init__(self,Items=[]):
        
        self.list = []
        for i in Items:
            self.append(i)

    def append(self, dat):
        self.list.append(Sorter.Sortel(dat))

    def extend(self,items=[]):
        for i in items:
            self.append(i)

    def Clear(self):
        self.list =[]

    def Sort(self, fun, **fundat):
        for el in self.list:
            el.eval(fun, **fundat)

        self.list.sort()
        rv = []
        for el in self.list:
            rv.append(el.udat)
        return rv
            

        
