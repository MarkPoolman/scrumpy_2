
"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""


# Kali - devours all, returns None


debug = 0


class Kali:
    def __init__(self, ident=None,*args,**kwargs):
        self.ident = ident
        

    def __str__(self):
        return "Kali from " + str(self.ident) + "\n"

    def __repr__(self):
        return str(self)

    def __getitem__(self, i):
        return None

    def __setitem__(self, i):
        pass

    if debug:
        def __getattr__(self, a):
            print "Kali  ", self.ident, a
            return self.DoNothing
    else:
        def __getattr__(self, a):
            return self.DoNothing

    def DoNothing(self, *args, **kwargs):
        pass
