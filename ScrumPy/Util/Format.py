
import os, types,  exceptions


def StrDate():
    d = os.popen("date").read().split()
    return "_".join([d[1],d[2],"".join(d[3].split(":")),d[-1]])


def RoundedFloat(x, sf=3):
    """return x as a string rounded to sf sig. figs"""
    if sf>16:
        raise exceptions.OverflowError("Excess prescision requested")
        # if you want to "round" a floating point number to more than 16 sf, you'll have to figure it out for yourself!
    return"{:.sfg}".replace("sf", str(sf)).format(x)
    
def AsRoundedFloat(x, sf=3):
    return RoundedFloat(float(x),  sf)

def IsQuotedStr(i):
    if type(i) != str:
        return False
    if len(i)<2:
        return False
    if i[0] == "'" or i[0] == '"':
        return i[0] == i[-1]
    return False


def QuoteStr(i,*args,**kwargs):
    if IsQuotedStr(i):
        return i 
    else:
        return str(i).join(('"','"'))

def StripQuote(i):
    if IsQuotedStr(i):
        return i[1:-1]
    else:
        return i
    


def FormatListOrTuple(l, pad, depth=0,Delims=True,Quoted=True):

    curtype=type(l)

    if curtype == types.TupleType:
        opend,closed,isep = {True:("(\n", ")",",\n"), False:("\n","","\n")}[Delims]
    else:
        opend,closed,isep = {True:("[\n", "]",",\n"), False:("\n","","\n")}[Delims]

    rv = opend
    depth+=1
    for i in l:
        rv += pad*depth + FormatNested(i,pad,depth,Delims=Delims,Quoted=Quoted)+isep
            
    return rv  + pad*(depth-1)+closed
  

def FormatDic(d, pad,depth=0,Delims=True,Quoted=True):

    opend,closed,isep = {True: ("{\n","}",",\n"), False:("\n","","\n")}[Delims]
   
    rv = opend
    depth+=1
    for k in d.keys():
        rv += pad*depth + QuoteStr(k) + " : " + FormatNested(d[k], pad, depth,Delims=Delims,Quoted=Quoted) + isep
        
    return rv +pad*(depth-1)+closed




def FormatNested(struct, pad="    ", depth=0, AsType=None,Delims=True,Quoted=True):

    FormatLookup = { types.DictType : FormatDic,
                     types.ListType : FormatListOrTuple,
                     types.TupleType: FormatListOrTuple,
                     types.StringType: QuoteStr
                    }

    struct_t = type(struct)
    
    if AsType==None:
        if not FormatLookup.has_key(struct_t):
            if hasattr(struct,"keys"):
                AsType=types.DictType
            elif hasattr(struct, "index"):
                AsType=types.TupleType
        else:
            AsType=struct_t
                
   
    if FormatLookup.has_key(AsType):
        return FormatLookup[AsType](struct, pad,depth,Delims,Quoted=Quoted)
    elif Quoted:
        return  QuoteStr(struct)
    else:
        return str(struct)
            
    
