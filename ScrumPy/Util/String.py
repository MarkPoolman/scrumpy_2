

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 2009

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

import time
def IsMatch(regex, s):
    matches = regex.findall(s)
    return len(matches) == 1 and matches[0] == s


def IsQuoted(s):
    return len(s)>2 and (s[0] == s[-1] == '"')


def DeQuote(s):
    if IsQuoted(s):
        return s[1:-1]
    return s


def EnQuote(s):
    if not IsQuoted(s):
        return s.join(('"','"'))
    return s


def TimeStamp():
    return str(time.time()).replace(".", "")


