import types

braces = ("{","}")
sqbracs = ("[","]")

backsl = "\\"
eol = " \\\\"
tabsep = " & "

textwidth="\\textwidth"
textheight="\\textheight"
bang = "!"
reschars = r"\#$%&~_^{}"

def DeTex(s):
    
    for ch in reschars:
        s = s.replace(ch, "\\"+ch)
    return s



def FloatToTex(f, sf=3):


    if type(f) != types.FloatType:
        return ""

    if f == 0.0:
        return "0"

    format = "".join(("%1.",str(sf-1),"e"))

    fs = format % f
    mant,exp = fs.split("e")

    af = abs(f)
    if af<10 and af>1:
        exp=""
    else:
        if abs(int(exp))<10:
            exp = exp.replace("0","")
        exp = exp.replace("+","")
        exp = exp.join((r"\times 10^{","}"))
        
    return "".join(("$",mant,exp,"$"))

def Option(text):

    if text == "":
        return ""
    return text.join(sqbracs)
    
def OptList(opts):

    if len(opts)==0:
        return ""
    return "".join(map(Option, opts))


def Argument(text):

    if text == "":
        return ""
    return text.join(braces)


def ArgList(args):

    if len(args) == 0:
        return ""
    return "".join(map(Argument,args))


def StartCom(com, opts=[], args=[]):
    return backsl + com + OptList(opts) + ArgList(args)
    


def Command(com, opts=[], args=[], text=""):

    return backsl + com + OptList(opts) + ArgList(args) + Argument(text)


def Begin(env, opts=[], args=[]):

    return Command("begin", opts, [env]+args)


def End(env):

    return Command("end", text=env)


def Environ(env, opts=[], args=[], text=""):

    return Begin(env, opts, args) + text + End(env)


def Center(text):

    return Environ("center", text=text)


def IncGfx(file, opts=[]):

    return Command("includegraphics", opts=opts, text=file)


def TabRow(items):

    return tabsep.join(items) + eol



def MakeNiceString(s):

    s = s.replace("_","\\_")

    return s # maybe more later


class LaTeX:

    def __init__(self):

        self.indent = ""

        self.Begin = ""
        self.Content = []
        self.End = ""


    def Copy(self):

        rv = LaTeX()
        rv.Begin = self.Begin
        rv.End = self.End
        for c in self.Content:
            if hasattr(c, "Copy"):
                rv.Content.append(c.Copy())
            else:
                rv.Content.append(c)
        return rv


    def MakeCommand(self, com, opts=[], args=[]):

        self.Begin =  Command(com, opts, args) + "{"
        self.End = "}"


    def MakeEnvironment(self, env, opts=[], args=[]):

        self.Begin = Begin(env, opts, args)
        self.End = End(env)



    def AddContent(self, Content):
    
        self.Content.append(Content)


    def __str__(self):

        strlist =  [self.indent + self.Begin]
        nextindent = self.indent + "\t"
        for c in self.Content:
            if self.__class__ == c.__class__ or self.__class__ in c.__class__.__bases__ :
                c.indent = nextindent
                cstr = str(c)
            else:
                cstr= nextindent + str(c)
           
            strlist.append(cstr)
        strlist.append(self.indent+self.End)

        return  "\n".join(strlist)


    def AddCommand(self,com, opts=[], args=[]):

        rv = LaTeX()
        rv.MakeCommand(com, opts, args)
        self.AddContent(rv)
        return rv


    def AddEnvironment(self, env, opts=[], args=[]):

        rv = LaTeX()
        rv.MakeEnvironment(env, opts, args)
        self.AddContent(rv)
        return rv


    def AddTabular(self, ncols=2, format="c"):

        rv = Tabular(ncols, format)
        self.AddContent(rv)
        return rv
        

    def AddMultiTabular(self, items, nrows =3, ncols = 2, format="c", holder=None):

     
        step = nrows*ncols

        while len(items)>step:
            
            cur, items = items[:step], items[step:]
            tab = Tabular(ncols, format)
            tab.AddRows(cur)
            if holder != None:
                content = holder.Copy()
                content.Content.append(tab)
            else:
                content = tab
            self.Content.append(content)

        if len(items) != 0:
            
            tab = Tabular(ncols, format)
            tab.AddRows(items)
            if holder != None:
                content = holder.Copy()
                content.Content.append(tab)
            else:
                content = tab
            self.Content.append(content)


    def AddOneLine(self, line):
        self.Content.append(line)

    def InsertOneLine(self, line):
        self.Content.insert(line)


    def Resize(self, width=textwidth, height=bang):

        self.Begin = Command("resizebox", args=[width,height]) +"{"+ self.Begin
        self.End += "}"
                             
            


    
class Tabular(LaTeX):

    def __init__(self, ncols, format="c"):

        LaTeX.__init__(self)

        self.ncols = ncols

        self.MakeEnvironment("tabular", args=[format*ncols])


  

    def AddRow(self, row):

        missing = self.ncols - len(row)
        if missing <0:
            raise  ValueError, "row too long"

        row = row + [""] * missing

        self.AddContent(TabRow(row))


    def AddRows(self, items):

        while len(items) > self.ncols:

            self.AddRow(items[:self.ncols])
            items = items[self.ncols:]

        self.AddRow(items)

        
        
        
    
        

        

    



    
    
