#
##
### Generic gnuplot interface
##
#

from subprocess import Popen, PIPE,  STDOUT

import time
import threading
import Queue


class AsyncReader(threading.Thread):
    '''

    from http://stefaanlippens.net/python-asynchronous-subprocess-pipe-reading
    
    Helper class to implement asynchronous reading of a file
    in a separate thread. Pushes read lines on a queue to
    be consumed in another thread.
    '''
 
    def __init__(self, fd):

        threading.Thread.__init__(self)
        self._fd = fd
        self.Q =Queue.Queue()
        self._stop = False
 
    def run(self):
        '''The body of the tread: read lines and put them on the queue.'''
        for line in iter(self._fd.readline, ''):
            if self._stop:
                return
            self.Q.put(line)
            
    def stop(self):
        self._stop =True
 
    def eof(self):
        '''Check whether there is no more content to expect.'''
        return not self.is_alive() and self.Q.empty()
        
    def Get(self):
        '''return the last line read from fd - None if queue empty'''
        
        if self.Q.empty():
           rv = None
           
        else:
            rv = self.Q.get().strip()
           
        return rv
    

class Gnuplot(Popen):
    
    WaitTime = 0.1 # time to wait before re-prompting gnuplot for a response
    MaxWait = 0.5  # max time to wait before assuming gnuplot has finnished

    def __init__(self):

        Popen.__init__(self, "gnuplot", stdin=PIPE, stdout=PIPE, stderr=STDOUT)
      

        self.FromGP =  AsyncReader(self.stdout)
        self.FromGP.start()
        
        self.WinNos = []
        
    def NewWinNo(self):
        
        rv = 0
        while rv  in self.WinNos:
            rv += 1
        self.WinNos.append(rv)
        return rv
        
    def FreeWinNo(self, n):
        
        self.WinNos.remove(n)
        self('set term "" ' + str(n) + " close")
        
        
        
    def __del__(self):
        
        while self.FromGP.Get() !=None:
            pass

        try:
            self("quit")  # try - we might be dead allready
            self.wait()
        except:
            pass
    
        self.FromGP.stop()
    
        
        
    def __call__(self,cmd):
        
        rv = []
        
        print >>self.stdin,cmd + ' ; print "DONE"\n'
        
        Waited = 0
        
        line = self.FromGP.Get()
        while line != "DONE" and Waited < self.MaxWait:
            
            if line == None:
                time.sleep(self.WaitTime)
                Waited += self.WaitTime
            elif line.endswith(":"):
                print >>self.stdin, "\n"
            else:
                rv.append(line)
                Waited = 0
            line =  self.FromGP.Get()
            #print line
        
        return rv
        
    def Terminals(self):
        """ pre: True
           post: Terminals()-> list of available terminals"""
           
        return  map(lambda s: s.split()[0], self("set term")[1:])
        
GlobalGP = Gnuplot()
        
        
