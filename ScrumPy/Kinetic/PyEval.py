

import types

import sympy
from ScrumPy.Util import Sci,  Set, Format

class DepReport:
    # simple class to record data about dependecies in a system
    # for later vieing by the user
    
    def __init__(self, mets, depmets, eqns, sol,  cmtx):

        self.mets = mets
        self.depmets =  depmets
        self.indepmets  = Set.Complement(mets, depmets)
        self.eqns  = eqns
        self.sol = sol
        self.__cmtx  = cmtx # NB: cmtx is live so don't expose to user

    def __str__(self):

        rvl = []
        rvl.append("Metabolites:\n"+ Format.FormatNested(self.mets, Quoted=False))
        rvl.append("Dependents:\n" + Format.FormatNested(self.depmets, Quoted=False))
        rvl.append("Independents:\n" + Format.FormatNested(self.indepmets, Quoted=False))
        rvl.append("Dependency equation(s):\n" + Format.FormatNested(self.eqns, Quoted=False))
        rvl.append("Solution(s):\n" + Format.FormatNested(self.sol, Quoted=False))
        rvl.append("Conservation matrix:\n" + str(self.__cmtx))

        return "\n\n".join(rvl)
    

        
        
class Evaluator:

    def __init__(self,  model):

        md = model.md
        self.model = model
        
        self.ParamDic = dict(zip(md.Parameters,  range(len(md.Parameters))))
        self.Params  = Sci.vector(map(float, md.Parameters.values()))
        
        self.RateDic =  dict(zip(model.sm.cnames, range(len(model.sm.cnames))))
        self.Rates = Sci.vector([0.0] * len(self.RateDic))
        
        self.MetDic = dict(zip(model.sm.rnames,  range(len(model.sm.rnames))))
        self.Mets = Sci.vector([0.0]* len(self.MetDic))
        for met in self.MetDic:
           self.Mets[self.MetDic[met]] = md.Metabolites[met]

        self.dMet = Sci.vector([0.0] * len(self.MetDic))
        
        if self.GetDepEqns():
            self.ReorderMtxs()
            self.BuildSM()
            self.BuildNameRefDic()
            self.BuildDepEqns()
            self.BuildRateEqns()
            self.Jac = Sci.matrix([[0.]*self.nIndeps]*self.nIndeps)
        

    def GetDepEqns(self):

        OK = True
        md = self.model.md
        self.DepSols = {}
          
        deps = self.DetermineDeps()
        self.nDeps = len(deps)
        self.nIndeps = len(self.Mets) - self.nDeps
        eqns = []
        sols = []


        if self.nDeps > 0:
 
            eqns = map(lambda e: md.GetReacKinetic(e), md.GetEquilReacs())
            eqns.extend(self.model.cmtx.AsEqns())

            if self.nIndeps < 0:
                print "!! Error - no solutions - more dependencies than metabolites"
                OK = False
                
            else:
                if self.nIndeps == 0:
                    print "!! Warning - no independent metabolites"
           
                sols = sympy.solve(eqns,  deps,  dict=True)
                lensols = len(sols)
                if lensols == 0:
                    print "!! Error - no solutions for dependencies"
                    OK = False
                
                else:
                    if lensols >1:
                        print "!! Warning - multiple solutions for dependencies, using the first"
                    
                    if type(sols) == types.ListType:
                        sols = sols[0]
                        # sympy 0.6 sols is only a list if there are muyliple solutions (??)
                        # sympy 0.7 sols is always a list
                        
                    for sol in sols.keys():
                        self.DepSols[str(sol)] = str(sols[sol])
                    
                
                if len(self.DepSols) != self.nDeps:
                    print "!! Error -",  len(sols),  " solutions for",  len(deps),  "dependencies"
                    OK = False
                    
        self.DepReport = DepReport( self.MetDic.keys(),deps, eqns, sols, self.model.cmtx)
        return OK

        
    def DetermineDeps(self):
        
        rv = self.model.GetEquilDeps()
        cmtx = self.model.cmtx
        for c in cmtx.cnames:                
            mets = cmtx.InvolvedWith(c).keys()
            if Set.IsSubset(mets,rv):
                print "!! Warning:", mets, "overdetermined"
            else:
                rv.append(Set.Complement(mets, rv)[0])

        return rv
    
    def BuildNameRefDic(self):
        
        nrd = self.NameRefDic={}
        mets = self.model.sm.rnames
        reacs = self.model.sm.cnames
        
        for name in self.ParamDic:
            nrd[name] = "self.Params[" + str(self.ParamDic[name]) + "]"
            
        for name in  mets:
            nrd[name] =  "self.Mets[" + str(mets.index(name))  + "]"
            
        for name in reacs:
            nrd[name] = "self.Rates[" + str(reacs.index(name)) + "]"
  
        
        
    def ReorderMtxs(self):
        
        deps = self.DepSols.keys()
              
        order = deps + Set.Complement(self.model.sm.rnames,  deps)
        self.model.sm.RowReorder(order)
        self.model.cmtx.RowReorder(order)
        
        order = deps + Set.Complement(self.model.smx.rnames,  deps)
        self.model.smx.RowReorder(order)
        
        order = self.model.md.GetEquilReacs() + self.model.md.GetNonEquilReacs()
        self.model.sm.ColReorder(order)
        self.model.smx.ColReorder(order)
        
        
    def BuildSM(self):
        
        sm = self.model.sm.Copy(Conv = float)
        self.sm = sm.ToSciMtx()
        
       
    def BuildDepEqns(self):
        
        depeqns = []
        
        for d in self.DepSols:        
            rhs = self.SymToSpy(self.DepSols[d])
            lhs = self.NameRefDic[d] + " = "
            depeqns.append(lhs + rhs)

        self.DepEqns = compile("\n".join(depeqns),"deps","exec")
        #print "\n".join(depeqns)

         
    def SymToSpy(self, expr):
        
        ops = "+-/*()"
        rvl = []
        nrd = self.NameRefDic
        
        strex = str(expr).replace("**",  " ^ ")
        for op in ops:
            strex = strex.replace(op, " " + op + " ")
        
        for tok in strex.split():
            if nrd.has_key(tok):
                tok = nrd[tok]
            rvl.append(tok)
            
        return " ".join(rvl).replace("^",  "**")
 

    def BuildRateEqns(self):      
        
        md = self.model.md
        
        eqns = self.BuildKinEqns(md.GetNonEquilReacs())
        self.RateEqns = compile("\n".join(eqns), "ScrumPyKinetic",  "exec")
        

    def BuildKinEqns(self,  reacs):

        md = self.model.md
        nrd = self.NameRefDic

        eqns = []
        for name in reacs:
            eqnl =  [name ,  " = "] + md.GetReacKinetic(name).split()
            for idx in range(len(eqnl)):
                item = eqnl[idx]
                if nrd.has_key(item):
                    eqnl[idx] = nrd[item]
            
            eqn = " ".join(eqnl) + "\n"
            eqns.append(eqn)
            
        return eqns
            
    def Eval(self):
        exec self.DepEqns
        exec self.RateEqns
        dmet =  self.sm.dot(self.Rates).getA1()
        self.dMet[self.nDeps:] = dmet[self.nDeps:]
        
     
        
    def UpdateJac(self, pert=1e-5):
        
        yl = Sci.vector([0.]*self.nIndeps)
        yh = Sci.vector([0.]*self.nIndeps)
        
        pert2 = 2 * pert
        for met in range(self.nIndeps):

            val = self.Mets[met]
            self.Mets[met] -= pert * val
            self.Eval()
            yl[:] = self.dMet[self.nDeps:]
            self.Mets[met] += pert2 * val
            self.Eval()
            yh[:] = self.dMet[self.nDeps:]
            self.Mets[met] = val
            self.Jac[met] = map(lambda yh, yl:(yh-yl)/pert2,  yh, yl)
     
        
    def GetMets(self):
         return self.Mets[:]

    def GetIndepMets(self):
        return self.Mets[self.nDeps:]

    def GetDerivs(self):
        return self.dMet[:]
        
    def GetIndepDerivs(self):
        return self.dMet[self.nDeps:]

    def GetRates(self):
        return self.Rates[:]
 
    def GetParams(self):
        return self.Params[:]

    def SetIndepMets(self,  mets):
        self.Mets[self.nDeps:] = mets

    def CalcVels(self):
        exec self.RateEqns

    def GetVal(self, name):
        if not self.NameRefDic.has_key(name):
            print "!! Warning - ", name, "not found"
            return Sci.scipy.nan
        else:
            return eval (self.NameRefDic[name])

    def SetVal(self, name, val):
        if not self.NameRefDic.has_key(name):
            print "!! Warning - ignoring attempt to set value to nonexistent", name
        else:
            exec self.NameRefDic[name] + "=" +str(val)














