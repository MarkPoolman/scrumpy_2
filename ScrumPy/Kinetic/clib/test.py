import LowLevelKinMod,  math

m = LowLevelKinMod.LowLevelKinMod()

m.Load("./test.so")



Rates = [0.0] * m.nRates
Mets = [0.0] * m.nMets

m.UpdateVals()
m.GetVals("D",  Mets)

print m.Integrate(0.5)
print m.GetSSErr()

print m.Integrate(0.5)
print m.GetSSErr()

print m.Integrate(0.5)
print m.GetSSErr()

print m.Integrate(0.5)
print m.GetSSErr()

m.UpdateVals()
m.GetVals("D",  Mets)

print math.sqrt(sum(map(lambda x:x*x,  Mets[:m.nIndeps])))
