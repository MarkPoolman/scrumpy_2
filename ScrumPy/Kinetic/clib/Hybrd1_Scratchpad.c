/*
 extern void hybrd1_ ( void (*fcn)( int *n, double *x,
    double *fvec, int *iflag),

        int *n, double *x, double *fvec, 
        double *tol, int *info, double *wa, 
        int *lwa); 
*/

#include<stdlib.h>
#include <minpack.h>
 


#include"Hybrd1_Scratchpad.h"

const double DefaultSolverTol = 1e-10 ;


 
    
struct Hybrd1_Scratchpad{
    hybrd1_func_t func ;
    int n ;  /* number of variables */
    double *x ; /* initial / final estimate. length n */
    double *fvec ; /* output dy, length n */
    double tol ;
    int info ;  /* output from hybrd1_ */
    double *wa ; /* work array */
    int lwa ; /*  length of wa >= (n*(3*n+13))/2 */
} ; 




void Del_SSP(Hybrd1_Scratchpad_t sp){    
    if (sp->wa != 0) free(sp->wa) ;
    if (sp->fvec != 0) free(sp->fvec) ;
    free(sp) ;
}


Hybrd1_Scratchpad_t New_SSP(int nFree, double *Mets,  hybrd1_func_t func) {
    
    Hybrd1_Scratchpad_t rv ;
    
    rv = (Hybrd1_Scratchpad_t) malloc(sizeof(*rv)) ; 
    
    if(rv != 0){
        rv->func = func ;
        rv->n = nFree ;
        rv->x = Mets ;
        rv->tol = DefaultSolverTol ;
        rv->lwa = (nFree*(3*nFree+13))/2 ;
        
        rv->fvec = (double *) calloc((size_t) nFree, sizeof(double)) ;
        rv->wa = (double *) calloc((size_t) rv->lwa, sizeof(double)) ;
            
        if( rv->fvec == 0 || rv->wa == 0 ){
            Del_SSP(rv) ; 
            rv = 0 ;
        }
    }
    return rv ;
}

int Solve_SSP(Hybrd1_Scratchpad_t sp){
    
    hybrd1_ ( FunCast sp->func, &sp->n, sp->x, sp->fvec, &sp->tol, &sp->info, sp->wa, &sp->lwa) ;
    return sp->info ;
}


