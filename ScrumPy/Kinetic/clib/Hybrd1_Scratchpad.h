 
 
#ifndef SOLVERSCRATCHPAD_H
#define SOLVERSCRATCHPAD_H 1

 /*
 Interface to the minpackc  hybrd1 solver
 */



#if defined __cminpack_decl_fcn_nn__ || defined __minpack_decl_fcn_nn__
#define FunCast (minpack_func_nn)
#else
#define FunCast
#endif

typedef void (*hybrd1_func_t)( int *n, double *x, double *fvec, int *iflag) ;


typedef struct   Hybrd1_Scratchpad  *Hybrd1_Scratchpad_t  ; 

extern Hybrd1_Scratchpad_t New_SSP(int nFree, double *Mets, hybrd1_func_t func) ;
extern  void Del_SSP(Hybrd1_Scratchpad_t sp) ; 

extern int Solve_SSP(Hybrd1_Scratchpad_t sp) ;

#endif
