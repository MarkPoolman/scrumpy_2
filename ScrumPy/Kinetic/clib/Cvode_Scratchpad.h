

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
cp
    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
#ifndef INTEGSCRATCHPAD_H
#define INTEGSCRATCHPAD_H 1


/* cvode Header files  */

#include <sundials/sundials_types.h>  /* definition of type realtype                 */
#include <cvode/cvode.h>              /* prototypes for CVode* functions and         */
                                      /* constants CV_BDF, CV_NEWTON, CV_SV,         */
                                      /* CV_NORMAL, CV_SUCCESS, and CV_ROOT_RETURN   */
#include <cvode/cvode_dense.h>        /* prototype for CVDense  and macro DENSE_ELEM */
#include <nvector/nvector_serial.h>   /* definitions of type N_Vector, macro         */
                                      /* NV_Ith_S, and prototypes for N_VNew_Serial  */
                                      /* and N_VDestroy                              */

#include <cvode/cvode_spgmr.h>
#ifdef __cplusplus
#define extern extern "C"
#endif


typedef void(*ModelFunc_t)(double,  double *)   ;


struct Integ_ScratchPad {

    int nFree ;
	  realtype atol, rtol, t, tout;
    double  *y, *dy;
    N_Vector cv_y,             /* y data in */
             cv_yout ;         /* y data out (can we use cv_y for both ?) */
    void *cvode_mem;
    ModelFunc_t func ;
} ;


typedef struct Integ_ScratchPad *Integ_ScratchPad_t ;

extern Integ_ScratchPad_t New_ISP(int nFree, double *Mets, ModelFunc_t func, double rtol, double atol) ;

extern void Reset_ISP(Integ_ScratchPad_t sp, double t, double rtol, double atol) ;

extern void Del_ISP(Integ_ScratchPad_t sp) ;

extern int Integrate_ISP(Integ_ScratchPad_t sp, double t) ;


#ifdef __cplusplus
#undef extern
#endif

#endif
