const int nParams = 66, nMets = 18, nIndeps =16, nRates = 21 ;


double Params[66]={0.05, 1.0, 0.0, 0.05, 0.075, 5000000000.0, 200.0, 0.84, 0.29, 14.994519999999996, 12.0, 0.05, 0.014, 40.0, 0.63, 0.03, 250.0, 0.25, 0.7, 1.0, 16000000.0, 2.5, 7.1, 0.02, 0.077, 1.0, 13.0, 0.1, 1.0, 0.9, 0.075, 2.0, 0.04, 10000.0, 0.7, 0.74, 0.02, 0.25, 2.512e-05, 4.0, 0.4, 12.0, 0.1, 10.0, 0.02, 0.07, 0.013, 0.21, 0.058, 0.08, 0.08, 0.00031, 0.4, 0.85, 0.49955, 0.67, 1.0, 40.0, 40.0, 0.3, 0.5, 0.084, 2.3, 3500.0, 340.0, 22.0} ;

double Mets[18]={0.49806, 0.00235, 1.56486, 0.29345, 1.36481, 0.41021, 1.5662, 0.18206, 0.00599, 3.1396, 0.02776, 0.01334, 0.00363, 0.33644, 0.14825, 0.00541, 3.35479, 0.00149} ;

double dMets[18]={0.49806, 0.00235, 1.56486, 0.29345, 1.36481, 0.41021, 1.5662, 0.18206, 0.00599, 3.1396, 0.02776, 0.01334, 0.00363, 0.33644, 0.14825, 0.00541, 3.35479, 0.00149} ;

double Rates[21]={0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0} ; 
void DepEqns(){
	Mets[16] = - Mets[0] - 2.0 * Mets[14] + Params[9] - Mets[3] - Mets[5] - Mets[4] - 2.0 * Mets[10] - Mets[7] - Mets[9] - Mets[11] - Mets[6] - Mets[8] - Mets[1] - 2.0 * Mets[13] - Mets[15] - 2.0 * Mets[2] - Mets[12] ;
	Mets[17] = - Mets[0] + Params[54];
} ;
void RateEqns(void){
		 Rates[0]  =  Params[33] * Mets[1] * Mets[0] / ( ( Mets[1] + Params[3] * ( 1 + Mets[16] / Params[31] + Mets[13] / Params[18] + Mets[6] / Params[39] ) ) * ( Mets[0] * ( 1 + Mets[17] / Params[21] ) + Params[0] * ( 1 + Mets[17] / Params[40] ) ) ) ;
	 Rates[1]  =  Params[5] * ( Mets[9] - Mets[7] / Params[48] ) ;
	 Rates[2]  =  Params[37] * Params[16] * Mets[16] / ( Params[17] * ( 1 + ( 1 + Params[35] / Params[60] ) * ( Mets[6] / Params[14] + Mets[16] / Params[17] + Mets[3] / Params[24] + Mets[11] / Params[30] ) ) ) ;
	 Rates[3]  =  Params[5] * ( Mets[12] - Mets[1] / Params[55] ) ;
	 Rates[4]  =  Params[64] * Mets[13] / ( Mets[13] + Params[36] * ( 1 + Mets[16] / Params[7] + Mets[10] / Params[32] + Mets[2] / Params[4] + Mets[6] / Params[29] + Params[47] / Params[45] ) ) ;
	 Rates[5]  =  Params[57] * Mets[7] * Mets[0] / ( ( Mets[7] + Params[49] ) * ( 1 + Mets[17] / Params[43] ) * ( Mets[0] + Params[50] ) + Params[50] * Mets[6] / ( Params[42] * Mets[16] ) + ( Params[23] * Mets[4] ) + ( Params[44] * Mets[10] ) ) ;
	 Rates[6]  =  Params[5] * ( Mets[14] * Params[47] * Params[38] - Params[8] * Mets[11] * Mets[6] / Params[20] ) ;
	 Rates[7]  =  Params[5] * ( Mets[11] * Mets[15] - Mets[12] * Mets[8] / Params[53] ) ;
	 Rates[8]  =  Params[5] * ( Mets[16] * Mets[0] - ( Mets[14] * Mets[17] / Params[51] ) ) ;
	 Rates[9]  =  Params[5] * ( Mets[3] * Mets[11] - Mets[10] / Params[22] ) ;
	 Rates[10]  =  Params[5] * ( Mets[4] - Mets[9] / Params[62] ) ;
	 Rates[11]  =  Params[16] * Mets[3] / ( Params[24] * ( 1 + ( 1 + Params[35] / Params[60] ) * ( Mets[6] / Params[14] + Mets[16] / Params[17] + Mets[3] / Params[24] + Mets[11] / Params[30] ) ) ) ;
	 Rates[12]  =  Params[6] * Mets[10] / ( Mets[10] + Params[15] * ( 1 + Mets[4] / Params[34] + Mets[6] / Params[41] ) ) ;
	 Rates[13]  =  Params[5] * ( Mets[4] * Mets[11] - Mets[5] * Mets[12] / Params[61] ) ;
	 Rates[14]  =  Params[5] * ( Mets[11] - Mets[3] / Params[65] ) ;
	 Rates[15]  =  Params[5] * ( Mets[8] - Mets[1] / Params[52] ) ;
	 Rates[16]  =  Params[63] * Mets[17] * Mets[6] / ( ( Mets[17] + Params[12] ) * ( Mets[6] + Params[59] ) ) ;
	 Rates[17]  =  Params[5] * ( Mets[5] * Mets[3] - Mets[2] / Params[26] ) ;
	 Rates[18]  =  Params[13] * Mets[2] / ( Mets[2] + Params[46] * ( 1 + Mets[6] / Params[10] ) ) ;
	 Rates[19]  =  Params[16] * Mets[11] / ( Params[30] * ( 1 + ( 1 + Params[35] / Params[60] ) * ( Mets[6] / Params[14] + Mets[16] / Params[17] + Mets[3] / Params[24] + Mets[11] / Params[30] ) ) ) ;
	 Rates[20]  =  Params[58] * Mets[6] / ( Mets[6] + Params[27] * ( 1 + Mets[7] / Params[11] ) );
}  ;
void DiffEqns(double *Diffs){
	Diffs[0] = -Rates[0] + Rates[16]-Rates[5]-Rates[8] ;
	Diffs[1] = -Rates[0] + Rates[3] + Rates[15] ;
	Diffs[2] = -Rates[18] + Rates[17] ;
	Diffs[3] = -Rates[11] + Rates[14]-Rates[9]-Rates[17] ;
	Diffs[4] =  + Rates[12]-Rates[13]-Rates[10] ;
	Diffs[5] = -Rates[17] + Rates[13] ;
	Diffs[6] =  + 2.0 * Rates[5] + Rates[6] + Rates[11] + Rates[12] + Rates[2]-Rates[16] + Rates[18] + Rates[19]-Rates[20] ;
	Diffs[7] = -Rates[5] + Rates[1] + Rates[20] ;
	Diffs[8] =  + Rates[7]-Rates[15] ;
	Diffs[9] = -Rates[1] + Rates[10] ;
	Diffs[10] = -Rates[12] + Rates[9] ;
	Diffs[11] = -Rates[19] + Rates[6]-Rates[7]-Rates[13]-Rates[14]-Rates[9] ;
	Diffs[12] = -Rates[3] + Rates[7] + Rates[13] ;
	Diffs[13] =  + Rates[0]-Rates[4] ;
	Diffs[14] = -Rates[6] + Rates[8] ;
	Diffs[15] =  + Rates[18]-Rates[7];
} ;

void Eval(double t, double *dy){

    if (dy ==0)
        dy = dMets ;

    DepEqns() ;
    RateEqns() ;
    DiffEqns(dy) ;
}

