

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/********

M.G. Poolman 8/11/99

LowLevelKinMod.h -- C++ class interface to Scampi model API
"low level" wrt python
********/

#include<Python.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include<arrayobject.h>


#include"Cvode_Scratchpad.h"
#include"Hybrd1_Scratchpad.h"

typedef void (*vvFunc_t) (void)     ;
typedef void (*vdpFunc_t)(double *) ;



class LowLevelKinMod {

    public:

        LowLevelKinMod() ;
        ~LowLevelKinMod() ;

        void PrintMsg(char *msg) ;
        void Load(char *libname, double rtol, double atol) ;


        double GetNthParam(int n) ;
        double GetNthMet(int n) ;
        double GetNthRate(int n) ;
        double GetNthDiff(int n) ;
        void   GetVals(char *What, PyObject *OutList) ;
        
        double GetSSErr(void) ;

        void SetNthMet(int n, double val) ;
        void SetNthParam(int n, double val) ;
        void SetVals(char *What, PyObject *InList) ;


        void UpdateVals(void) ;
        int  Integrate(double t) ;
        int SimTo(PyObject *arr)  ;
        int  Solve(void) ;
        
        void ResetInteg(double t, double rtol, double atol) ;


        int nMets, nIndeps, nRates, nParams, t_Idx ;

        vvFunc_t  DepEqns, RateEqns ;

  private:

        double *Params, *Mets, *dMets, *Rates ;

        ModelFunc_t Eval ;
    

        vdpFunc_t DiffEqns ;

        void *dlib ;

        Integ_ScratchPad_t ISP ;
        Hybrd1_Scratchpad_t SSP ;
        
        int GetBufInfo(char *What, double **Buf, size_t *Len); 
        void UpdateBuffer(double *buf) ;

} ;

