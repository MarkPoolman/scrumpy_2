/* std headers */

#include<stdlib.h>
#include<stdio.h>

#include"Cvode_Scratchpad.h"

#define ErrLog(err) fprintf(stderr,"ScrumPy CVODE Log: %s\n", err) ;


const double DefaultIntegTol = 1e-6 ;





int CVFunc(realtype t, N_Vector y, N_Vector dy, void *f_data){
	/* function called by cvode to calculate new dy */

	Integ_ScratchPad_t sp ;
	int n ;
	sp = (Integ_ScratchPad_t) f_data ;

	for (n=0 ; n < sp->nFree ; n++){                         /* copy from cvode to our own vectors */
		sp->y[n] = (double) NV_Ith_S(y,n) ;
	}
/*
    TODO: why doesn't sp->y = NV_DATA_S(y) ; work?
*/

   	sp->func(t, NV_DATA_S(dy))  ;

    return 0 ;
/*
    TODO: check for -ve concs and return >0
*/
}



Integ_ScratchPad_t New_ISP(int nFree, double *Mets, ModelFunc_t func, double rtol,  double atol) {

	Integ_ScratchPad_t rv ;
	/*double Tol = DefaultIntegTol ;*/
  int n;

    rv = (Integ_ScratchPad_t) malloc(sizeof(*rv)) ;
    if (rv == NULL){
        ErrLog("\n!!\n!!Out of memory in NewInteg_ScratchPad !!\n!!");
        return NULL ;
    }

    rv->nFree = nFree ;
    rv->y = Mets ;
    rv->dy = (double *) calloc((size_t) nFree,sizeof(double)) ;
    rv->func = func ;
	                                                   /* allocate vectors for cvode */
    rv->cv_y = N_VNew_Serial(nFree) ;                 /* metabolite values */
    rv->cv_yout =  N_VNew_Serial(nFree) ;             /* for values calculated by CVode() */

    rv->rtol = (realtype)(rtol) ;
    rv->atol = (realtype)(atol) ;
    for (n=0 ; n<nFree ; n++){                        /* initialise cvode vectors */
        NV_Ith_S((rv->cv_y),n) = (realtype)(Mets[n]) ;
    }

    rv->cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);    /* create our initial cvode "object" */
    CVodeInit(                                       /* give it it's memory  */
			rv->cvode_mem,                             /* the cvode object */
			CVFunc,                                    /* function that will calculate cv_dy from cv_y */
			(rv->t) ,                      /* time 0 */
			rv->cv_y
		);
    CVodeSStolerances(rv->cvode_mem, rv->rtol, rv->atol);
    CVodeSetUserData(rv->cvode_mem, rv);                  /* tell cvode where to find the scratchpad */
    CVDense(rv->cvode_mem, nFree) ;
    CVodeSetMaxNumSteps(rv->cvode_mem,2000) ; /* tell cvode to try a bit harder (default is 500 */

    return rv ;
}



void Reset_ISP(Integ_ScratchPad_t sp, double t, double rtol, double atol){
    
    int n ;
    

    for (n=0 ; n<sp->nFree ; n++){                        /* initialise cvode vectors */
        NV_Ith_S((sp->cv_y),n) = (realtype)(sp->y[n]) ;
    }
    sp->t  = t ;
    sp->rtol = rtol ;
    sp->atol = atol ;
   CVodeReInit(                                       /* give it it's memory  */
        sp->cvode_mem,                             /* the cvode object */
        (sp->t) ,                      /* time 0 */
        sp->cv_y
    );
    CVodeSStolerances(sp->cvode_mem, sp->rtol, sp->atol);
    CVodeSetUserData(sp->cvode_mem, sp);                  /* tell cvode where to find the scratchpad */
    CVDense(sp->cvode_mem, sp->nFree) ;
    CVodeSetMaxNumSteps(sp->cvode_mem,2000) ; /* tell cvode to try a bit harder (default is 500 */
	
}

void Del_ISP(Integ_ScratchPad_t sp){

        free(sp->dy) ;
        N_VDestroy_Serial(sp->cv_y) ;
        N_VDestroy_Serial(sp->cv_yout) ;
        CVodeFree(&sp->cvode_mem) ;
        free(sp) ;
}


int Integrate_ISP(Integ_ScratchPad_t sp, double TimeEnd) {

    return CVode(
        sp->cvode_mem,
        (realtype) TimeEnd +sp->t,
        sp->cv_y,
        &(sp->t),
        CV_NORMAL /* itask*/
	 );

}

