

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""


#TODO deal with no parameters if all reactions are ~



import   random, exceptions,  sys

from ScrumPy.Data import DataSets
from ScrumPy.Util import  Sci,  HopcroftKarp

from ScrumPy.Structural.Model import Model as StructuralModel
from ScrumPy.Structural import FluxDesc


import CEval

def LeastConMets(sm):
    """ least connected metabolite(s) in a sto mtx """
#TODO make this a method of sm

    rvd ={}

    for r in sm.rnames:
        rvd[r] = len(sm.InvolvedWith(r))
    least = min(rvd.values())

    for k in rvd.keys():
        if rvd[k] > least:
            del rvd[k]
    return rvd.keys()


# a string representing time, avoids use of literals in code
TimeString = "Time"



def SimRange(Start, Stop,  Steps):
    """ scipy linspace without the start value, including the end value
         Because we don't generally want to simulate to the current time"""
         
    rv,s = Sci.linspace(Start,  Stop, Steps,  endpoint=False,retstep=True)
    rv += s
    return rv


class Monitor(DataSets.DataSet):

    def __init__(self, model, Params=[], UpdateIfNotOK="Ignore"):

        ValidUDNOK =  "Yes","Zero",  "Ignore"
        if UpdateIfNotOK not in ValidUDNOK:
            raise exceptions.ValueError, "UpdateIfNotOK must be one of " +  " ".join(ValidUDNOK)

        if Params==[]:
            Params = model.GetParamNames()
            
    

        self.model=model
        if hasattr(model, "sm"):
            self.VarNames = ["Time"] + model.sm.cnames + model.sm.rnames + model.md.GetExplDiffNames()
        else:
            self.VarNames = ["Time"] + model.md.GetExplDiffNames()
        self.UpdateIfNotOK=UpdateIfNotOK

        ItemNames = self.VarNames+Params

        DataSets.DataSet.__init__(self, ItemNames=ItemNames, SrcDic=model)


    def Update(self):

        if self.model.IsOK() or self.UpdateIfNotOK=="Yes":
            DataSets.DataSet.Update(self)
        elif self.UpdateIfNotOK=="Zero":
            vars = [0]*len(self.VarNames)
            pars = self.model.GetVals(self.Params)
            self.NewRow(vars+pars)
        elif self.UpdateIfNotOK!="Ignore":
            raise exceptions.ValueError, "Unknown value "+self.UpdateIfNotOK+" for UpdateIfNotOK"
        #self.Plot()




class Model (StructuralModel):
    """ The Kinetic model class    """
    def __init__(self, md):

        StructuralModel.__init__(self, md)
        
        Valid = self.md.IsKinetic() and (len(md.Reactions) >0 or md.ExplicitOnly())

        if Valid and len(md.Errors) ==0 :
            self.InitKin()
            self.OK = False # lsat sim or sol call was successful
##
##    def __getattr__(self, a):
##
##        if a == "Evaluator":
##            raise exceptions.AttributeError("Not a kinetic model!")
##

    def Reload(self, FromUI=False):
        
        if not FromUI and hasattr(self,  "ui"):
            self.ui.Reload()  
        # if we've not been invoked via the ui, recall ourself via the ui to ensure that files are saved etc.
        
        else:
            StructuralModel.Reload(self)
            if len(self.md.Errors) ==0:
                if not self.md.IsKinetic():
                    if hasattr(self,  "Evaluator"):
                        del self.Evaluator
                    # we have changed from kinetic to structural, deleting the evaluator means that kinetic functions
                    # will raise an exception as would a model that was structural from the start
                else:
                    self.InitKin()

    def InitKin(self):

        md = self.md
        self.GetParamNames = md.GetParamNames
        self.SSTol = 1e-5
        
        if not md.ExplicitOnly(): # no stoichiometry if explicit only
    
            self.cmtx = cm = self.ConsMoieties().Copy(Conv=float)
            cm.cnames = map(lambda s: "CSUM_" + str(s),  range(len(cm.cnames)))
    
            for name in cm.cnames:  # calculate the conserved totals
                c = 0
                for r in cm.rnames:
                    c += float(md.Metabolites[r]) * cm[r, name]
                md.NewParam(name, c)

        md.NewParam("Time")
        self.Evaluator = CEval.Evaluator(self)
        
        if self.Evaluator.LLKM == None:
            print >> sys.stderr,  "Error: couldn't compile/load model (this is a bug!!)"
        else:
    
            for attr in [
            'GetMets',
            'GetIndepMets',
            'GetDerivs',
            'GetRates',
            'GetParams',
            'Eval',
            'CalcVels', 
            'GetSSErr'
            ]:
                setattr(self, attr, getattr(self.Evaluator, attr))
    
            self.GetInitState()
    
            self["Time"]=0.0
    
            self.DynMons = []
            self.StatMons = []



    def __repr__(self):
        return "ScrumPy kinetic model"


    def __del__(self):
        self.Destroy()

    def Destroy(self):
        if hasattr(self, "LowLevel"):
            self.Evaluator.Destroy()

    def __getitem__(self, name):
        return self.Evaluator.GetVal(name)

    def __setitem__(self, name, val):
        self.Evaluator.SetVal(name, val)

    def IsOK(self):
        return True #self.OK

    def ShowDepInfo(self):
        print self.Evaluator.DepReport



    def GetInitState(self):
        self._InitState = self.GetIndepMets()


    def SetInitState(self):
        self.SetIndepMets(self._InitState)
        self.Eval()


    def GetEquilDeps(self):
        """ sets of mets involved in equilibrium relationships """

        reacs = {}
        rv = []
        
        EQReacs = self.md.GetEquilReacs()

        for reac in EQReacs:
            reacs[reac] = self.sm.InvolvedWith(reac).keys()
        
        h = HopcroftKarp.HopcroftKarp(reacs) # TODO modify hk so as not to change its input!
        # See refs in the HopcroftKarp module - is this the right/best way to achieve this ??
        mm = h.maximum_matching()
        
        for reac in EQReacs:
            rv.append(mm[reac])
            
        return rv
        
        



    def GetConsMets(self):
        """ sets of mets inovlved in conservation relationships """

        rv = []
        for c in self.cmtx.cnames:
            rv.append(self.cmtx.InvolvedWith(c).keys())
        return rv



    def GetEigValsJ(self):
        print "GetEigVals not implemented"
        
        
    def GetJac(self):
        print "GetEigVals not implemented"
        
    def GetFluxDesc(self,  AmDevel=False):
        if not AmDevel:
            print "! Not implemented !"
        else:
            return  FluxDesc.FluxDesc(self.sm.cnames, self.GetRates(), self)
            
            
    def GetFluxDic(self):
        
        return dict(zip(self.sm.cnames,  self.GetRates()))


    def Simulate(self, StepSize=0.01, NSteps=1):
        
        self.Evaluator.ResetInteg(self["Time"]) # because model might have ben changed since last call

        for n in range(NSteps):
            self.Evaluator.Integrate(StepSize)
            self.UpdateDynMons()

            
    def SimTo(self,  Time,  nSteps):
        
        Now = self["Time"]
        
        Times = SimRange(Now,  Now+Time,  nSteps)
        nVals = self.Evaluator.GetNVals()
        tidx = self.Evaluator.GetTimeIdx()
        res = Sci.zeros((nSteps, nVals))
        res[:, tidx] = Times
        
        self.Evaluator.SimTo(res)
        
        rv = DataSets.DataSet(ItemNames=self.GetAllNames(Diffs=True))
        rv.rows= res.tolist()
        
        return rv



    def FindSS(self, SimTime=0.1, SimSteps=50):
        
        t = self["Time"]

        while self.GetSSErr()> 0.1 and SimSteps >0:
            self.Evaluator.Integrate(SimTime)
            SimSteps -=1
        
        self["Time"] = t # reset ISP as well ?
            
        self.Evaluator.Solve()
        self.UpdateStatMons()
      

    def GetVals(self, names):
        """ pre: names is a list of strings
           post: returns a list of the values of names """

        rv = []
        for n in names:
            rv.append(self[n])

        return rv

    def SetVals(self, names, vals):
        """ pre: names is a list of strings, vals is a list of float,
                 len(names) == len (vals)
           post: GetVals(names) == vals"""

        for i in range(len(names)):
            self[names[i]] = vals[i]


    def Update(self, dic):
        for k in dic:
            self[k] = dic[k]




    def GetAllNames(self,  Diffs=False):
        """ return a list (of strings) of all identifiers in the model
             Diffs: include names of the met differential as "d_MetName" 
        """
        pNames = self.Evaluator.GetOrderedNames("P")
        rNames = self.Evaluator.GetOrderedNames("R")
        mNames = self.Evaluator.GetOrderedNames("M")
        if Diffs:
            dNames = map(lambda s: "d_"+s, mNames)
        else:
            dNames = []
            
        return(
            pNames +
            rNames +
            mNames +
            dNames
        )

    def keys(self):
        return self.GetAllNames()
    """
    because models anyway have dictionary-like characteristics we supply
    a keys function for completeness. This is useful for the monitor
    classes which can then be updated from any dicionary-like instance
    """

    def PingConc(self, pert=1e-6):
        """ perturb all concentrations by multiplying by a random number [1-pert..1+pert]"""

        for met in self.md.GetIntMetNames():
            self[met] *=  random.uniform(1-pert,1+pert)



    def ScaledSensits(self, pname, Vars, Scaled=True,  SS=True, Pert=1e-3):
#TODO PingConc(pert)
        if SS:
            def Eval():
                self.PingConc()
                self.FindSS()
        else:
            Eval = self.CalcVels

        pval = self[pname]
        delta_p = pval*Pert
        TwodP = 2 * delta_p

        if Scaled:
            Eval()
            midvars = self.GetVals(Vars)   # mid of 3 point differential

        self[pname] += delta_p
        Eval()
        hivars = self.GetVals(Vars)         # high point of 3 point diff

        self[pname] -= TwodP
        Eval()
        lovars = self.GetVals(Vars)         # low point of 3 point diff

        self[pname] = pval

        dydx = map(lambda yh, yl:(yh-yl)/(TwodP),  hivars, lovars)
        if Scaled:
            dydx = map(lambda d, y:d*pval/y, dydx, midvars)

        return dydx




    def AddDynMonitor(self, mon=None, *args, **kwargs):
        if mon != None:
            self.DynMons.append(mon)
        else:
            mon = Monitor(self,  *args, **kwargs)
            mon.SetPlotX("Time")
            self.DynMons.append(mon)
            return mon


    def AddStatMonitor(self,mon=None,  *args,  **kwargs):
        if mon != None:
            self.StatMons.append(mon)
        else:
            mon = Monitor(self, *args,  **kwargs)
            self.StatMons.append(mon)
            return mon


    def UpdateDynMons(self):
        for mon in self.DynMons:
            mon.Update()


    def UpdateStatMons(self):
        for mon in self.StatMons:
            mon.Update()

    def RemMon(self, mon):
        """ remove mon from static and dynamic monitor lists """
        if mon in self.StatMons:
            del self.StatMons[self.StatMons.index(mon)]

        if mon in self.DynMons:
            del self.DynMons[self.DynMons.index(mon)]

    def GetDepSols(self):
        '''
                pre     :   True
                post    :   Dependency solutions returned as list of strings
        '''
        sols = self.Evaluator.DepReport.sol
        rv = []
        for sol in sols:
            rv.append(str(sol)+' = '+str(sols[sol]))
        return rv











