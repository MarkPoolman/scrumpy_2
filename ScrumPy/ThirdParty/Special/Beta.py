
#
# Beta function, adapted fron Gary Strangmans stats module
#

import exceptions, math

from Gamma import Lgamma

def betacf(a,b,x):
    """ Continued fraction form of the incomplete Beta function, betai.
        (See NRiC 6.3)
    """
    
    ITMAX = 200
    EPS = 3.0e-7

    bm = az = am = 1.0
    qab = a+b
    qap = a+1.0
    qam = a-1.0
    bz = 1.0-qab*x/qap
    for i in range(ITMAX+1):
        em = float(i+1)
        tem = em + em
        d = em*(b-em)*x/((qam+tem)*(a+tem))
        ap = az + d*am
        bp = bz+d*bm
        d = -(a+em)*(qab+em)*x/((qap+tem)*(a+tem))
        app = ap+d*az
        bpp = bp+d*bz
        aold = az
        am = ap/bpp
        bm = bp/bpp
        az = app/bpp
        bz = 1.0
        if (abs(az-aold)<(EPS*abs(az))):
            return az
        
    raise exceptions.ArithmeticError



def betai(a,b,x):
    """ Incomplete beta function:
        (See NRiC 6.3)
        pre: type(a,b,x) = float, 0<=x<=1
        
    """
   
    if (x==0.0 or x==1.0):
        rv = 0.0
    else:
        bt = math.exp(
            Lgamma(a+b)  -
            Lgamma(a)    -
            Lgamma(b)    +
            a*math.log(x)+
            b*math.log(1.0-x)
        )
            
        if (x<(a+1.0)/(a+b+2.0)):
            rv =  bt*betacf(a,b,x)/a
        else:
            rv =  1.0-bt*betacf(b,a,1.0-x)/b

    return rv

