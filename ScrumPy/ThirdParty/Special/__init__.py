
#
# "Special" functions, mainly used for statistical calulations
#

from Beta import betai

from Err import Erf, Erfc

from Gamma import Gamma, Lgamma
    

def Gamma_p(a,x, MaxIter=100):
    """ Incomplete Gamma function NRiC eq. 6.2.5  """

    # (haven't bothered with the extended fraction version
    # even with quite large values of x (~40) this converges
    # quite happily, and the main loop is very simple)
    
    EPS = 1e-8  
    gln= Gamma.Lgamma(a)
    ap = a
    d = total = 1.0/a

    n = 0
    done = False
    while n < MaxIter and not done:
        
        n  += 1
        ap += 1
        d  *= x/ap 
        total += d
        done = abs(d) < abs(total)*EPS

    if done:
        return total*math.exp(-x+a*math.log(x)-(gln));
    
    print "!! gser failed to converge !!"
        
