import sys

print >>sys.stderr, """ Debian 7 is no longer supported, but will probably work. 
Upgrade to Debian 8 if you can!
"""


PyVer   =  sys.version[:3]     # python version
PyPfx   =  sys.exec_prefix     # path to binary
PyCom = "python"+PyVer   # binary

GccVer = ""                          # gcc version, leave empty for the distribution default

PyDevPkg = "python" + PyVer +"-dev"                # python development package .
PyInc = PyPfx+"/include/python"+PyVer+"/"    # python  include path


Packages=[
    "gcc"+GccVer,
    "g++"+GccVer,
    "libstdc++"+GccVer+"-dev"
    "make",
    "cmake",         # for scope currently in dev.
    PyDevPkg,
    "libgmp3-dev",   # gnu multiple precision library - development package
    "libblas-dev",   # prequisite for manual scipy instalation.
    "gfortran",      # ditto
    "swig",          # this can vary  between distros, maybe need (e.g.) swig-python as well
    "gnuplot-x11",
    "python-sympy",
	"python-scipy",
	"libsundials-serial-dev",
	"libcminpack-dev",
    
    "python-ply",    # python lex/yacc
    "python-gmpy",

    "python-tk",     # ditto

    "python-qt4",    # needed by ete2 tree explorer
]

InstallCommand="apt-get --yes install "

LinkFlags = "-shared -Wl,--copy-dt-needed-entries,--no-as-needed"
# for gcc <= 4.4 LinkFlags = "-shared"
