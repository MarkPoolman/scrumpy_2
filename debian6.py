# Configuration file for ScrumPy installation.
# please send modified versions to mgpoolman@brookes.ac.uk
# svn test

import os, sys

print >>sys.stderr """
Debian 6 (squeeze) is no longer supported. 

If you really want to install on squeeze, then python2.7, related packages in the Packages list below
and sundials/cvodes + minpack will all need to be installed from source.

Much better to upgrade !
"""


sys.exit()

# Basic info about the python installation
# needed here, but don't change them.
PyVer = sys.version[:3]
PyPfx = sys.exec_prefix
PyCom = "python"+PyVer

GccVer="" #"-4.7"   # CHECK is 4.7 essential or can we live with the default ? - see LinkFlags below.
# name of python development package and include path.
# edit if needed for specific distro (and report back)
PyInc = PyPfx+"/include/python"+PyVer+"/"
PyDevPkg = "python" + PyVer +"-dev"


# select specific ScrumPy exension modules
# leave as true except for development/debugging purposes
Kinetics=True
ElModes=True
LP = True

# where to put the executable and ScrumPy package files
# at present you will still need write permission in
# /usr/lib/pythonPyVer/site-packages
#ExecPath = "/usr/local/bin"
#InstallPath = "/usr/lib/python" + PyVer + "/site-packages/ScrumPy"

ExecPath = "/home/mark/bin"
InstallPath = "home/mark/ScrumPy"

# prerequisite packages. check local info and edit as needed.

Packages=[
    "gcc"+GccVer,
    "g++"+GccVer,
    "make",
    "cmake",         # for scope currently in dev.
    PyDevPkg,
    "gnuplot-x11",
    "libgmp3-dev",   # gnu multiple precision library - development package
    "swig",          # this can vary  between distros, maybe need (e.g.) swig-python as well
    "python-gmpy",
    "python-sympy",
    
    "python-ply",    # python lex/yacc
    "python-numpy",
    "blt",           # GUI extensions
    "python-pmw",    # should chain off of blt, but maybe not ?
    "python-tk",     # ditto
    "python-wxtools", # for the new wx interface
    "python-qt4",    # needed by ete2 tree explorer
    "python-svn"     # for AnnotatedOrganism extension to PyoCyc
]

# how to install packages, edit as needed for non apt based distros
# and report back.
InstallCommand="apt-get --yes install "


#
##
###  Do not edit below here - devel/debug only  #######################################################
##
#


# leave these as they are
ExtnPath = InstallPath+"/cextns"
ExtnLib  = ExtnPath+"/lib"
ExtnInc  = ExtnPath+"/include"
PyInst = PyCom+" setup.py install "
SDLib = InstallPath+"/Kinetic/clib/scampi/cvode/sundials/lib"
# TODO fix sundials configure to put libs in ExtnLib


# and these
IncludePaths = [PyInc,  ExtnInc, "/usr/local/include/sundials", "/usr/local/include/cvodes"]
LibPaths = [ExtnLib, SDLib]
Libs = ["gmp", "stdc++"]

# how to configure/make/install the third party clibs
# leave them as they are
pfx  = "--prefix="+InstallPath+"/cextns"
epfx = "--exec-prefix="+InstallPath+"/cextns"
Configure = " ".join(("./configure", pfx, epfx))
ConfMakeInst = "\n".join((Configure,"make","make install"))


# arguments passed to "make" for the ScrumPy c extension modules.
# only change if essential
CC  = "gcc"+GccVer   # c compiler
CPP = "g++"+GccVer   # c++ compiler
LNK = "gcc"+GccVer   # linker
# if you change them make sure that flags below are valid


# flags for compilation and linking of ScrumPy c extension modules.
# leave these unless CC/CPP/LNK have been changed
CInclFlags = ""
for i in IncludePaths:
    CInclFlags += " -I"+i+" "

LinkFlags = "-shared " #-Wl,--copy-dt-needed-entries,--no-as-needed"
# Wl,--copy-dt-needed-entries,--no-as-needed needed because ld default behaviour changed for gcc >4.4

for L in LibPaths:
    LinkFlags += " -L"+L + " "
for L in Libs:
    LinkFlags += "-l"+L +" "

CCompileFlags="-c  -O2 -fPIC -DHAVE_CONFIG_H" + CInclFlags
CPPCompileFlags="-c -O2 -fPIC -DHAVE_CONFIG_H" + CInclFlags


# leave these EVEN IF values above have changed
CCompile   = "'" + CC  + " "+ CCompileFlags +"'"
CPPCompile = "'" +CPP + " " +CPPCompileFlags+"'"
Link = "'" +LNK + " " + LinkFlags+"'"

# leave these
ThirdPartyPy = ["gmpy"]           # packages w/ seperate setup.py scripts
ThirdPartyC  = ["glpk"] #["gmp","glpk"]           # C libs with configure/make/install

# packages handled by the local package manager
def InstallPackages():

    fails=[]
    for p in Packages:
       if os.system(InstallCommand+p) !=0:
           fails.append(p)

    return fails
