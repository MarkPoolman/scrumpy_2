import sys

print "ScrumPy installing on centos 7"

PyVer   =  sys.version[:3]     # python version
PyPfx   =  sys.exec_prefix     # path to binary
PyCom = "python"+PyVer   # binary

GccVer = ""                          # gcc version, leave empty for the distribution default

PyDevPkg = "python-devel"                # python development package .
PyInc = PyPfx+"/include/python"+PyVer+"/"    # python  include path

IncludePaths = ["/usr/include/cminpack-1"]
LibPaths = ["/usr/lib64"]
SubstLibs = {
    "minpack":"cminpack"
}

Packages=[
    "gcc"+GccVer,
    "gcc-c++"+GccVer,
    "make",
    "cmake",         # for scope currently in dev.
    PyDevPkg,
    "gmp-devel",   # gnu multiple precision library - development package
    "swig",          # this can vary  between distros, maybe need (e.g.) swig-python as well
    "gnuplot",
	"sundials-devel",
	"cminpack-devel",

    "sympy",
    
    "python-ply",    # python lex/yacc
    "numpy",
    "scipy", 
    "python-gmpy2",
    "blt-devel",           # GUI extensions
    "python-pmw",    # should chain off of blt, but maybe not ?
    "pygtk2-devel",     # ditto
    "wxPython-devel", # for the new wx interface
    "PyQt4-devel",    # needed by ete2 tree explorer
    "pysvn",     # for AnnotatedOrganism extension to PyoCyc
]

InstallCommand="yum -y install "

LinkFlags = "-shared -Wl,--copy-dt-needed-entries,--no-as-needed"
# for gcc <= 4.4 LinkFlags = "-shared"
