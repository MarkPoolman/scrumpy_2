#!/usr/bin/python

#TODO Build from version no of this working copy,
# Optional from head or other version

#svn test - eric5

import os
import time
import pysvn

SpySrc = os.getcwd()+"/"
Tmp = "./"

#

def BuildRel(src=SpySrc,dst=SpySrc):

    rev,date = GetSvnInfo(src)
    SpyTmp = Tmp+"ScrumPy"+rev
    print rev
    Export(src, SpyTmp)
    ver = UpdateBasicInfo(SpyTmp,rev,date)
    BuildTGZ(SpyTmp, dst,ver,rev)
    #os.system("rm -rf "+SpyTmp)



def GetSvnInfo(repo):

    svn = pysvn.Client()
    info = svn.info(repo)

    rev = str(info["commit_revision"].number)

    comtime = info["commit_time"]
    ymd = list(time.gmtime(comtime)[:3])
    ymd.reverse()
    date = "/".join(map(lambda i: str(i),ymd))

    return rev, date



def Export(repo, dst):

    #svn = pysvn.Client()
    #svn.export(repo, dst)
    os.system("svn update")
    os.system(" ".join(["svn export",repo,dst]))
    os.system("rm -rf "+dst+"/dev") # don't distribute the dev area



def UpdateBasicInfo(path,rev,date):


    BasicInfoF = path +"/ScrumPy/BasicInfo.py"
    BasicInfo = open(BasicInfoF).read()
    BasicInfo = BasicInfo.replace("#DATE#", date).replace("#REV#",rev)

    open(BasicInfoF,"w").write(BasicInfo)

    for line in BasicInfo.split("\n"):
        if line.startswith("Version"):
            return line.split("=")[1].strip()



def BuildTGZ(src, dst, ver, rev):

    tgz = "-".join(("/ScrumPy",ver,rev))+ ".tgz"
    cmd = " ".join(("tar zcf", dst+tgz, src))
    os.system(cmd)


if __name__ == "__main__":
    BuildRel()
